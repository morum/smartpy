module Main where

import qualified Data.ByteString.Lazy.Char8 as B
import SmartPy
import System.Environment
import Text.Printf

main :: IO ()
main = do
  progName <- getProgName
  getArgs >>= \case
    [inFile] -> do
      input <- readFile inFile
      B.putStrLn $ parsePythonModule inFile input
    _ -> error $ printf "usage: %s <input file>" progName
