module SmartPy where

import qualified Data.Aeson as A
import Data.ByteString.Lazy (ByteString)
import Data.Maybe
import qualified Language.Python.Version3.Parser as Py
import SmartPy.Export
import SmartPy.Parser
import SmartPy.SExpr
import SmartPy.Syntax

contractInfo :: [Method] -> A.Value
contractInfo methods = A.toJSON $
  (`mapMaybe` methods) $ \case
    Method (KOnchainView name) _ _ _ -> Just $ A.toJSON name
    Method (KOffchainView name) _ _ _ -> Just $ A.toJSON name
    _ -> Nothing

prepareChunk :: (Int, ParseResult) -> A.Value
prepareChunk = \case
  (l, PR_Module m) -> do
    let els = (`map` modElements m) $ \case
          ModuleEltL _ (FunDef name _ _ _) -> (name, ("def", A.toJSON ()))
          ModuleEltL _ (ExprDef name _) -> (name, ("def", A.toJSON ()))
          ModuleEltL _ (TypeDef name _) -> (name, ("typeDef", A.toJSON ()))
          ModuleEltL _ (ContractDef name _ methods) -> (name, ("contractClass", contractInfo methods))
    A.toJSON (l, ("module", prettySExpr $ getS m, els))
  (l, PR_Entrypoint ep) -> do
    A.toJSON (l, ("entrypoint", prettySExpr $ getS ep))

parsePythonModule :: String -> String -> ByteString
parsePythonModule fileName input =
  case Py.parseModule input fileName of
    Left err -> error $ show err
    Right (m, _tokens) ->
      A.encode $
        A.toJSON $
          map prepareChunk $
            chunksFromPythonModule m
