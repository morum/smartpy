module SmartPy.SExpr where

data SExpr = Atom String | List [SExpr] deriving (Show)

prettySExpr :: SExpr -> String
prettySExpr (Atom "") = show ""
prettySExpr (Atom x) = x
prettySExpr (List xs) = "(" <> unwords (map prettySExpr xs) <> ")"
