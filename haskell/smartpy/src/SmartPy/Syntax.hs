module SmartPy.Syntax where

-- Convention for naming syntax constructors: Use the names from
-- https://docs.python.org/3/reference/datamodel.html. If not
-- available refer to
-- https://docs.python.org/3/reference/grammar.html.

import Language.Python.Common.SrcLocation (SrcSpan)

data TypeL = TypeL SrcSpan Type
  deriving (Show)

data Layout
  = LayoutLeaf String
  | LayoutNode Layout Layout
  deriving (Show)

data Type
  = TUnit
  | TNat
  | TInt
  | TIntOrNat
  | TString
  | TBytes
  | TAddress
  | TSignature
  | TKey
  | TKeyHash
  | TVariant [(String, TypeL)] (Maybe Layout)
  | TRecord [(String, TypeL)] (Maybe Layout)
  | TOption TypeL
  | TContract TypeL
  | TMap TypeL TypeL
  | TSet TypeL
  | TBigMap TypeL TypeL
  | TList TypeL
  | TTimestamp
  | TBool
  | TLambda Effects TypeL TypeL
  | TTuple [TypeL]
  | TMutez
  | TChainId
  | TNever
  | TAbbrev (Maybe String) String
  | TTicket TypeL
  | TBls12_381_g1
  | TBls12_381_g2
  | TBls12_381_fr
  | TOperation
  deriving (Show)

data Prim0
  = Amount
  | Balance
  | Now
  | SelfAddress
  | Sender
  | Source
  | TotalVotingPower
  | EntrypointMap
  deriving (Show)

data Prim1
  = Blake2b
  | Concat
  | HashKey
  | ImplicitAccount
  | Keccak
  | Len
  | Sum
  | Not
  | Invert
  | Pack
  | Reversed
  | SetDelegate
  | Sha256
  | Sha3
  | Sha512
  | ToAddress
  | ToInt
  | ToNat
  | ToBytes
  | VotingPower
  | Items
  | Values
  | Keys
  | Elements
  | Neg
  | Abs
  | IsNat
  | ReadTicket
  | JoinTickets
  | PairingCheck
  | InvertBytes
  deriving (Show)

data Prim2
  = Add
  | AddHomo
  | AddSeconds
  | And
  | Apply
  | AndBytes
  | AndInfix
  | OrBytes
  | OrInfix
  | Compare
  | Div
  | Ediv
  | EqBin
  | GeBin
  | GtBin
  | LeBin
  | LShiftBytes
  | LShiftInfix
  | RShiftBytes
  | RShiftInfix
  | LtBin
  | Map
  | Max
  | Min
  | Mod
  | Mul
  | MulHomo
  | Neq
  | Or
  | Sub
  | SubMutez
  | Ticket
  | XorBytes
  | XorInfix
  | SplitTicket
  deriving (Show)

data Prim3
  = CheckSignature
  | Range
  | SplitTokens
  | Transfer
  | GetAndUpdate
  | If
  | Slice
  | TestTicket
  deriving (Show)

data Literal
  = LUnit
  | LIntOrNat Integer
  | LInt Integer
  | LNat Integer
  | LString String
  | LBool Bool
  | LMutez Integer
  | LTimestamp Integer
  | LAddress String
  | LChainId String
  | LBytes String
  | LKey String
  | LKeyHash String
  | LBls12_381_g1 String
  | LBls12_381_g2 String
  | LBls12_381_fr String
  deriving (Show)

data ExprL = ExprL SrcSpan Expr
  deriving (Show)

data QualIdent = QualIdent (Maybe String) String
  deriving (Show)

data Expr
  = Literal Literal
  | Var String
  | Prim0 Prim0
  | Prim1 Prim1 ExprL
  | Prim2 Prim2 ExprL ExprL
  | Prim3 Prim3 ExprL ExprL ExprL
  | Attr ExprL String
  | Call ExprL (Maybe ExprL)
  | Params
  | Storage
  | Private
  | IsVariant String ExprL
  | Variant String (Maybe ExprL)
  | Record [(String, ExprL)]
  | Emit (Maybe String) Bool ExprL
  | Cons ExprL ExprL
  | Operations
  | ChainId
  | Level
  | OpenVariant String (Maybe ExprL) ExprL
  | GetContract TypeL ExprL (Maybe String)
  | List [ExprL]
  | ListComprehension ExprL String ExprL
  | MkMap Bool [(ExprL, ExprL)]
  | Self (Maybe String)
  | Convert ExprL
  | Tuple [ExprL]
  | Fst ExprL
  | Snd ExprL
  | GetOpt ExprL ExprL
  | Contains ExprL ExprL
  | GetItem ExprL ExprL
  | GetItemDefault ExprL ExprL ExprL
  | GetItemMessage ExprL ExprL ExprL
  | SetTypeExpr ExprL TypeL
  | UpdateMap ExprL ExprL ExprL
  | Set [ExprL]
  | Lambda Effects String [StatementL]
  | Unpack ExprL TypeL
  | PrivateMethod String
  | InlineMichelson String [TypeL] [TypeL] [ExprL]
  | EntrypointId String
  | View String ExprL ExprL TypeL
  | CreateContract Bool QualIdent ExprL ExprL ExprL
  deriving (Show)

data VariantPatternL = VariantPatternL SrcSpan VariantPattern
  deriving (Show)

data VariantPattern
  = VPVariant String (Maybe String)
  deriving (Show)

data StatementL = StatementL SrcSpan Statement
  deriving (Show)

data Statement
  = StmtExpr ExprL
  | Assign ExprL ExprL
  | Verify ExprL (Maybe ExprL)
  | IfStmt ExprL StatementL (Maybe StatementL)
  | Failwith ExprL
  | Seq [StatementL]
  | While ExprL StatementL
  | For String ExprL StatementL
  | Trace ExprL
  | SetType ExprL TypeL
  | Never ExprL
  | DelItem ExprL ExprL
  | UpdateSet ExprL ExprL Bool
  | Return ExprL
  | Match ExprL [(VariantPatternL, StatementL)]
  | CallInit QualIdent [ExprL]
  deriving (Show)

data WithStorage = NoStorage | ReadOnly | ReadWrite
  deriving (Show)

data Effects = Effects
  { withStorage :: WithStorage,
    withOperations :: Bool
  }
  deriving (Show)

data MethodKind
  = KInit
  | KEntrypoint String
  | KPrivate {mkName :: String, mkEffects :: Effects}
  | KOnchainView String
  | KOffchainView String
  deriving (Show)

data Method = Method
  { methodKind :: MethodKind,
    methodParameters :: [String],
    methodBody :: StatementL,
    methodLineNo :: SrcSpan
  }
  deriving (Show)

data Init = Init
  { initParameters :: [String],
    initBody :: StatementL,
    initlineNo :: Maybe SrcSpan
  }
  deriving (Show)

data ModuleElt
  = TypeDef String TypeL
  | ExprDef String ExprL
  | FunDef String [String] Effects StatementL
  | ContractDef
      { meName :: String,
        meParents :: [QualIdent],
        meMethods :: [Method]
      }
  deriving (Show)

data ModuleEltL = ModuleEltL SrcSpan ModuleElt
  deriving (Show)

data Module = Module
  { modName :: String,
    modElements :: [ModuleEltL],
    modLineNo :: SrcSpan
  }
  deriving (Show)

data SingleEntrypoint = SingleEntrypoint
  { singleName :: String,
    singleHasParam :: Bool,
    singleLineNo :: Maybe SrcSpan,
    singleBody :: StatementL
  }
  deriving (Show)

data OldContract = OldContract'
  { oldEntrypoints :: [SingleEntrypoint]
  }
  deriving (Show)
