module SmartPy.Export (HasS (..)) where

import Data.Maybe
import qualified Language.Python.Common.SrcLocation as P
import SmartPy.SExpr
import SmartPy.Syntax hiding (List)
import qualified SmartPy.Syntax as Syntax

data LineNo = LineNo String Int
  deriving (Show)

mkExpr :: String -> [SExpr] -> SExpr
mkExpr f xs = List $ Atom f : xs

lineNoOfSpan :: P.SrcSpan -> LineNo
lineNoOfSpan = \case
  P.SpanMultiLine {..} -> LineNo span_filename span_start_row
  P.SpanCoLinear {..} -> LineNo span_filename span_row
  P.SpanPoint {..} -> LineNo span_filename span_row
  P.SpanEmpty -> error "lineOfSpan: SpanEmpty"

instance HasS a => HasS [a] where
  getS xs = List $ map getS xs

instance HasS LineNo where
  getS (LineNo fn n) = List [Atom "Some", List [Atom $ show fn, Atom $ show n]]

instance HasS P.SrcSpan where
  getS = getS . lineNoOfSpan

mk :: P.SrcSpan -> String -> [SExpr] -> SExpr
mk l f xs = List $ getS l : Atom f : xs

mkNoLoc :: String -> [SExpr] -> SExpr
mkNoLoc f xs = List $ Atom f : xs

class HasS a where
  getS :: a -> SExpr

instance HasS Bool where getS x = Atom $ show x

instance HasS Layout where
  getS = \case
    LayoutLeaf l -> List [Atom l]
    LayoutNode x y -> List [getS x, getS y]

instance HasS TypeL where
  getS (TypeL l e) = case e of
    TTimestamp -> Atom "timestamp"
    TMutez -> Atom "mutez"
    TAbbrev Nothing name -> List [Atom "typeRef", Atom name, getS l]
    TAbbrev (Just mod) name -> List [Atom "typeRef", Atom mod, Atom name, getS l]
    TBool -> Atom "bool"
    TAddress -> Atom "address"
    TSignature -> Atom "signature"
    TUnit -> Atom "unit"
    TNat -> Atom "nat"
    TInt -> Atom "int"
    TIntOrNat -> Atom "intOrNat"
    TString -> Atom "string"
    TBytes -> Atom "bytes"
    TKey -> Atom "key"
    TKeyHash -> Atom "key_hash"
    TChainId -> Atom "chain_id"
    TNever -> Atom "never"
    TVariant entries layout ->
      let layout' = case layout of
            Nothing -> Atom "Variable"
            Just layout -> List [Atom "Value", getS layout]
       in List [Atom "variant", List $ (`map` entries) $ \(cons, t) -> List [Atom cons, getS t], layout', getS l]
    TRecord entries layout ->
      let layout' = case layout of
            Nothing -> Atom "Variable"
            Just layout -> List [Atom "Value", getS layout]
       in List [Atom "record", List $ (`map` entries) $ \(cons, t) -> List [Atom cons, getS t], layout', getS l]
    TOption x -> List [Atom "option", getS x]
    TContract x -> List [Atom "contract", getS x]
    TSet t -> List [Atom "set", getS t]
    TMap tk tv -> List [Atom "map", getS tk, getS tv]
    TBigMap tk tv -> List [Atom "big_map", getS tk, getS tv]
    TList t -> List [Atom "list", getS t]
    TLambda (Effects NoStorage False) t1 t2 -> List [Atom "lambda", getS t1, getS t2]
    TLambda effects t1 t2 -> do List [Atom "lambda", getS effects, getS t1, getS t2]
    TTuple ts -> List $ Atom "tuple" : map getS ts
    TTicket t -> List [Atom "ticket", getS t]
    TBls12_381_g1 -> Atom "bls12_381_g1"
    TBls12_381_g2 -> Atom "bls12_381_g2"
    TBls12_381_fr -> Atom "bls12_381_fr"
    TOperation -> Atom "operation"

prettyPrim0 :: Prim0 -> String
prettyPrim0 = \case
  Source -> "source"
  Sender -> "sender"
  SelfAddress -> "self_address"
  Amount -> "amount"
  Balance -> "balance"
  Now -> "now"
  TotalVotingPower -> "total_voting_power"
  EntrypointMap -> "entrypoint_map"

prettyPrim1 :: Prim1 -> String
prettyPrim1 = \case
  Blake2b -> "blake2b"
  Concat -> "concat"
  HashKey -> "hash_key"
  ImplicitAccount -> "implicit_account"
  Keccak -> "keccak"
  Len -> "size"
  Sum -> "sum"
  Not -> "not"
  Invert -> "invert"
  Pack -> "pack"
  Reversed -> "rev"
  SetDelegate -> "set_delegate"
  Sha256 -> "sha256"
  Sha3 -> "sha3"
  Sha512 -> "sha512"
  ToAddress -> "to_address"
  ToInt -> "to_int"
  ToNat -> "to_nat"
  ToBytes -> "to_bytes"
  VotingPower -> "voting_power"
  Items -> "items"
  Values -> "values"
  Keys -> "keys"
  Elements -> "elements"
  Neg -> "neg"
  Abs -> "abs"
  IsNat -> "is_nat"
  ReadTicket -> "read_ticket"
  JoinTickets -> "join_tickets"
  PairingCheck -> "pairing_check"
  InvertBytes -> "invert_bytes"

prettyPrim2 :: Prim2 -> String
prettyPrim2 = \case
  Add -> "ADD"
  AddHomo -> "add"
  AddSeconds -> "add_seconds"
  And -> "and"
  Apply -> "apply_lambda"
  AndBytes -> "and_bytes"
  AndInfix -> "and_infix"
  OrBytes -> "or_bytes"
  OrInfix -> "or_infix"
  Compare -> "COMPARE"
  Div -> "div"
  Ediv -> "ediv"
  EqBin -> "eq"
  GeBin -> "ge"
  GtBin -> "gt"
  LeBin -> "le"
  LShiftInfix -> "lshift_infix"
  LShiftBytes -> "lshift_bytes"
  RShiftInfix -> "rshift_infix"
  RShiftBytes -> "rshift_bytes"
  LtBin -> "lt"
  Map -> "map_function"
  Max -> "max"
  Min -> "min"
  Mod -> "mod"
  Mul -> "MUL"
  MulHomo -> "mul_homo"
  Neq -> "neq"
  Or -> "or"
  SplitTicket -> "split_ticket"
  Sub -> "sub"
  SubMutez -> "sub_mutez"
  Ticket -> "ticket"
  XorBytes -> "xor_bytes"
  XorInfix -> "xor_infix"

prettyPrim3 :: Prim3 -> String
prettyPrim3 = \case
  Range -> "range"
  SplitTokens -> "split_tokens"
  Transfer -> "transfer"
  CheckSignature -> "check_signature"
  GetAndUpdate -> "get_and_update"
  If -> "eif"
  Slice -> "slice"
  TestTicket -> "test_ticket"

instance HasS Literal where
  getS = \case
    LUnit -> mkExpr "unit" []
    LIntOrNat i -> mkExpr "intOrNat" [Atom $ show i]
    LInt i -> mkExpr "int" [Atom $ show i]
    LNat i -> mkExpr "nat" [Atom $ show i]
    LString x -> mkExpr "string" [Atom $ show x]
    LBool b -> mkExpr "bool" [Atom $ show b]
    LMutez b -> mkExpr "mutez" [Atom $ show b]
    LTimestamp b -> mkExpr "timestamp" [Atom $ show b]
    LAddress b -> mkExpr "address" [Atom $ show b]
    LChainId b -> mkExpr "chain_id_cst" [Atom b]
    LBytes b -> mkExpr "bytes" [Atom b]
    LKey b -> mkExpr "key" [Atom $ show b]
    LKeyHash b -> mkExpr "key_hash" [Atom $ show b]
    LBls12_381_g1 b -> mkExpr "bls12_381_g1" [Atom $ show b]
    LBls12_381_g2 b -> mkExpr "bls12_381_g2" [Atom $ show b]
    LBls12_381_fr b -> mkExpr "bls12_381_fr" [Atom $ show b]

instance HasS QualIdent where
  getS = \case
    QualIdent Nothing i -> Atom i
    QualIdent (Just mod) i -> List [Atom mod, Atom i]

instance HasS ExprL where
  getS (ExprL l e) = case e of
    Var name -> mk l "var" [Atom $ show name]
    Prim0 p -> mk l (prettyPrim0 p) []
    Prim1 p e1 -> mk l (prettyPrim1 p) [getS e1]
    Prim2 p e1 e2 -> mk l (prettyPrim2 p) [getS e1, getS e2]
    Prim3 p e1 e2 e3 -> mk l (prettyPrim3 p) [getS e1, getS e2, getS e3]
    Call f Nothing -> mk l "call" [getS f]
    Call f (Just x) -> mk l "call" [getS f, getS x]
    Attr e name -> mk l "attr" [getS e, Atom $ show name]
    Params -> mk l "parameter" []
    Literal lit -> mk l "literal" [getS lit]
    Storage -> mk l "storage" []
    Private -> mk l "private" []
    Variant name Nothing ->
      mk l "variant" [Atom name, getS $ ExprL l $ Literal LUnit]
    Variant name (Just x) ->
      mk l "variant" [Atom name, getS x]
    IsVariant name x -> mk l "is_variant" [getS x, Atom name]
    Record entries -> mk l "record" $ map (\(attr, x) -> List [Atom attr, getS x]) entries
    Emit tag withType x ->
      mk l "emit" [Atom $ fromMaybe "" tag, getS withType, getS x]
    Operations -> mk l "operations" []
    Cons x xs -> mk l "cons" [getS x, getS xs]
    ChainId -> mk l "chain_id" []
    Level -> mk l "level" []
    OpenVariant name missingMessage x -> mk l "open_variant" [getS x, Atom name, maybe (Atom "None") getS missingMessage]
    GetContract t addr ep -> let entrypoint = Atom $ fromMaybe "" ep in mk l "contract" [entrypoint, getS t, getS addr]
    Syntax.List xs -> mk l "list" $ map getS xs
    Syntax.ListComprehension e x xs -> mk l "list_comprehension" [getS e, Atom x, getS xs]
    MkMap big entries -> let name = if big then "big_map" else "map" in mk l name $ (`map` entries) $ \(k, v) -> List [getS k, getS v]
    Self Nothing -> mk l "self" []
    Self (Just ep) -> mk l "self" [Atom ep]
    Convert x -> mk l "convert" [getS x]
    Tuple xs -> mk l "tuple" $ map getS xs
    Fst x -> mk l "first" [getS x]
    Snd x -> mk l "second" [getS x]
    Contains c key -> mk l "contains" [getS c, getS key]
    GetOpt c key -> mk l "get_opt" [getS c, getS key]
    GetItem c key -> mk l "get_item" [getS c, getS key]
    GetItemDefault c key dflt -> mk l "get_item_default" [getS c, getS key, getS dflt]
    GetItemMessage c key msg -> mk l "get_item_message" [getS c, getS key, getS msg]
    SetTypeExpr x t -> mk l "type_annotation" [getS x, getS t]
    UpdateMap k v m -> mk l "update_map" [getS m, getS k, getS v]
    Set xs -> mk l "set" $ map getS xs
    Lambda (Effects NoStorage False) x cs -> mk l "lambda" [Atom x, List $ map getS cs]
    Lambda effects x cs -> mk l "lambda" [getS effects, Atom x, List $ map getS cs]
    Unpack x t -> mk l "unpack" [getS x, getS t]
    PrivateMethod x -> mk l "private_method" [Atom x]
    InlineMichelson code tsIn tsOut args ->
      mk l "call_michelson" $
        (mkNoLoc "op" $ Atom (show code) : map getS tsIn ++ Atom "out" : map getS tsOut)
          : map getS args
    EntrypointId name -> mk l "entrypoint_id" [Atom name]
    View name param addr ty -> mk l "view" [Atom name, getS param, getS addr, getS ty]
    CreateContract push qi baker amount storage -> mk l "create_contract" [getS push, getS qi, getS baker, getS amount, getS storage]

instance HasS StatementL where
  getS (StatementL l s) = case s of
    StmtExpr e -> mk l "expr" [getS e]
    Assign lhs rhs -> mk l "assign" [getS lhs, getS rhs]
    Verify e Nothing -> mk l "verify" [getS e]
    Verify e (Just msg) -> mk l "verify" [getS e, getS msg]
    IfStmt e c1 Nothing -> mk l "if_block" [getS e, getS c1]
    IfStmt e c1 (Just c2) -> mk l "if_then_else" [getS e, getS c1, getS c2]
    Failwith e -> mk l "failwith" [getS e]
    Seq cs -> List $ map getS cs
    While cond body -> mk l "while_block" [getS cond, getS body]
    For name gen body -> mk l "for_group" [Atom name, getS gen, getS body]
    Trace e -> mk l "trace" [getS e]
    SetType x t -> mk l "set_type" [getS x, getS t]
    Never x -> mk l "never" [getS x]
    DelItem m k -> mk l "del_item" [getS m, getS k]
    UpdateSet s k b -> mk l "update_set" [getS s, getS k, getS b]
    Return x -> mk l "result" [getS x]
    Match scrutinee cases ->
      let f = \case
            (VariantPatternL _ (VPVariant constructor Nothing), body) ->
              mk l "match" [Atom "dummy", Atom constructor, Atom "dummy", List [getS body]]
            (VariantPatternL _ (VPVariant constructor (Just var)), body) ->
              mk l "match" [Atom "dummy", Atom constructor, Atom var, List [getS body]]
       in mk l "match_cases" [getS scrutinee, Atom "dummy", List $ map f cases]
    CallInit cls args -> let kargs = [] in mk l "call_init" [getS cls, getS args, List kargs]

instance HasS a => HasS (Maybe a) where
  getS = \case
    Nothing -> Atom "None"
    Just x -> getS x

instance HasS SingleEntrypoint where
  getS SingleEntrypoint {..} =
    List
      [ Atom singleName,
        getS True, -- originate
        Atom "None", -- lazify
        Atom "None", -- lazifyNoCode
        Atom "None", -- checkNoIncomingTransfer
        getS singleHasParam,
        Atom "None", -- parameterType
        getS singleLineNo,
        getS singleBody
      ]

instance HasS WithStorage where
  getS = \case
    NoStorage -> Atom "None"
    ReadOnly -> List [Atom "Some", Atom "read-only"]
    ReadWrite -> List [Atom "Some", Atom "read-write"]

instance HasS Effects where
  getS (Effects st ops) = List [getS st, getS ops]

instance HasS MethodKind where
  getS = \case
    KInit -> Atom "init"
    KEntrypoint name -> List [Atom "entrypoint", Atom name]
    KPrivate {..} -> List [Atom "private", Atom mkName, getS mkEffects]
    KOffchainView name -> List [Atom "offchain_view", Atom name]
    KOnchainView name -> List [Atom "onchain_view", Atom name]

instance HasS Method where
  getS Method {..} =
    mk
      methodLineNo
      "method"
      [getS methodKind, List (map Atom methodParameters), getS methodBody]

instance HasS ModuleEltL where
  getS = \case
    ModuleEltL l (TypeDef name rhs) -> mk l "type_def" [Atom name, getS rhs]
    ModuleEltL l (ExprDef name rhs) -> mk l "expr_def" [Atom name, getS rhs]
    ModuleEltL l (FunDef name params effects body) -> mk l "fun_def" [Atom name, List $ map Atom params, getS effects, getS body]
    ModuleEltL l (ContractDef name parents methods) -> mk l "contract_def" [Atom name, List $ map getS parents, List $ map getS methods]

instance HasS Module where
  getS Module {..} = mk modLineNo "module" $ Atom modName : map getS modElements
