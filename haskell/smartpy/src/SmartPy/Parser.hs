{-# OPTIONS -fmax-pmcheck-models=100000 #-}

module SmartPy.Parser (chunksFromPythonModule, ParseResult (..)) where

import Data.Function (on)
import Data.List
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Maybe
import Language.Python.Common (Pretty)
import Language.Python.Common.AST hiding
  ( And,
    Assign,
    Expr,
    For,
    List,
    Module,
    Or,
    Set,
    Statement,
    Tuple,
    Var,
    While,
    Xor,
  )
import qualified Language.Python.Common.AST as P
import Language.Python.Common.Pretty (prettyText)
import Language.Python.Common.PrettyAST ()
import Language.Python.Common.SrcLocation
import SmartPy.Syntax hiding (Apply, Call, Lambda, Return)
import qualified SmartPy.Syntax as S
import System.Environment
import System.Exit
import System.IO (hPutStrLn, stderr)
import System.IO.Unsafe (unsafePerformIO) -- shudder!
import Text.Printf (printf)

impossible :: String -> a
impossible msg = error $ "impossible: " <> msg

data ParseResult
  = PR_Module Module
  | PR_Entrypoint SingleEntrypoint

prettySpan :: SrcSpan -> String
prettySpan = \case
  SpanMultiLine {span_filename, span_start_row, span_start_column} -> printf "%s:%d:%d" span_filename span_start_row span_start_column
  SpanCoLinear {span_filename, span_row, span_start_column, span_end_column} -> printf "%s:%d:%d-%d" span_filename span_row span_start_column span_end_column
  SpanPoint {span_filename, span_row, span_column} -> printf "%s:%d:%d" span_filename span_row span_column
  SpanEmpty -> "::"

lineNoOfSpan' :: SrcSpan -> Int
lineNoOfSpan' = \case
  SpanMultiLine {span_start_row} -> span_start_row
  SpanCoLinear {span_row} -> span_row
  SpanPoint {span_row} -> span_row
  SpanEmpty -> error "lineOfSpan: SpanEmpty"

mkTypeL :: SrcSpan -> Type -> TypeL
mkTypeL l e = TypeL l e

mkExprL :: SrcSpan -> Expr -> ExprL
mkExprL l e = ExprL l e

mkStatementL :: SrcSpan -> Statement -> StatementL
mkStatementL l e = StatementL l e

mkVariantPatternL :: SrcSpan -> VariantPattern -> VariantPatternL
mkVariantPatternL l e = VariantPatternL l e

parseError :: SrcSpan -> String -> a
parseError loc msg = unsafePerformIO $ do
  let msg' = prettySpan loc <> ": error: " <> msg
  env <- getEnvironment
  let insideBrowser = null env
  if True || insideBrowser
    then do
      errorWithoutStackTrace $ msg' <> "\n"
    else do
      hPutStrLn stderr msg'
      exitWith $ ExitFailure 1

-- TODO Print warnings only if there is no error.
warn :: SrcSpan -> String -> a -> a
warn loc msg = seq $ unsafePerformIO warn
  where
    warn = hPutStrLn stderr $ prettySpan loc <> ": warning: " <> msg

debug :: Bool
debug = False

unexpected :: (Show a, Span a, Pretty a) => a -> String -> b
unexpected x msg = parseError (getSpan x) out
  where
    out
      | debug = msg <> "\n  " <> show x
      | otherwise = msg <> ": " <> prettyText x

chunksFromPythonModule :: ModuleSpan -> [(Int, ParseResult)]
chunksFromPythonModule (P.Module statements) = concatMap extractFromStatement statements

parseModule :: StatementSpan -> Module
parseModule = \case
  Fun (Ident modName _) [] Nothing body _ ->
    let modElements = concatMap parseModuleElt body
        modLineNo = getSpan body
     in S.Module {..}
  x -> unexpected x $ "Not a module"

parseModuleElt :: StatementSpan -> [ModuleEltL]
parseModuleElt = \case
  x@P.Class {} -> [parseContractDef x]
  P.Assign [P.Var (Ident lhs _) _] rhs l -> [ModuleEltL l $ ExprDef lhs $ parseExpr (Context [] False) rhs]
  Fun (Ident f _) params Nothing body l -> do
    let params' = map parseParam params
    let c = Context [] True
    [ModuleEltL l $ FunDef f params' noEffects $ mkStatementL (getSpan body) $ Seq $ map (parseStatement c) body]
  Decorated [Decorator [Ident "sp" _, Ident "effects" _] effects _] (Fun (Ident f _) params Nothing body l) _ -> do
    let params' = map parseParam params
    let c = Context [] True
    let effects' = parseEffects effects
    [ModuleEltL l $ FunDef f params' effects' $ mkStatementL (getSpan body) $ Seq $ map (parseStatement c) body]
  AnnotatedAssign _ (P.Var (Ident "type" _) _) (Just _) l -> parseError l "'type' is a reserved word."
  AnnotatedAssign (P.Var (Ident "type" _) _) (P.Var (Ident name _) _) (Just rhs) l -> [ModuleEltL l $ TypeDef name $ parseType rhs]
  x -> unexpected x "Not a module statement"

extractFromStatement :: StatementSpan -> [(Int, ParseResult)]
extractFromStatement = \case
  Decorated [Decorator [Ident "sp" _, Ident "entry_point" _] [ArgKeyword (Ident "new_syntax" _) (Bool True _) _] _] decoree l ->
    let ep = mkSingleEntrypoint l decoree
     in [(lineNoOfSpan' $ getSpan decoree, PR_Entrypoint ep)]
  Decorated [Decorator [Ident "sp" _, Ident "entrypoint" _] [ArgKeyword (Ident "new_syntax" _) (Bool True _) _] _] decoree l ->
    let ep = mkSingleEntrypoint l decoree
     in [(lineNoOfSpan' $ getSpan decoree, PR_Entrypoint ep)]
  Decorated [Decorator [Ident "sp" _, Ident "module" _] [] _] body _l -> [(lineNoOfSpan' $ getSpan body, PR_Module $ parseModule body)]
  -- traverse:
  Decorated _ decoree _ -> extractFromStatement decoree
  Class _ _ body _ -> concatMap extractFromStatement body
  _ -> []

mkSingleEntrypoint :: SrcSpan -> P.Statement SrcSpan -> SingleEntrypoint
mkSingleEntrypoint l = \case
  Fun (Ident name _) params _ body l2 ->
    case params of
      Param (Ident "self" _) Nothing Nothing _ : rest -> do
        SingleEntrypoint
          { singleName = name,
            singleHasParam = not $ null rest,
            singleLineNo = Just l2,
            singleBody =
              let rest' = map parseParam rest
               in mkStatementL l2 $ Seq $ map (parseStatement $ Context rest' False) body
          }
      Param (Ident _ l) _ _ _ : _ -> parseError l "first argument to entrypoint must be 'self'"
      _ -> parseError l2 $ "first argument to entrypoint must be 'self'"
  _ -> parseError l "sp.entrypoint must be followed by a definition"

parseParam :: ParameterSpan -> String
parseParam = \case
  Param {param_name = Ident name _, param_py_annotation = Nothing, param_default = Nothing, param_annot = _} -> name
  x -> unexpected x "Not a param"

parseContractDef :: StatementSpan -> ModuleEltL
parseContractDef = \case
  Class
    (Ident name _)
    args
    class_body
    l -> do
      let parents = case parsePositionalArgs args of
            [] -> unexpected l "must inherit from sp.Contract or superclass"
            [Dot ((P.Var (Ident "sp" _) _)) (Ident "Contract" _) _] -> []
            xs -> (`map` xs) $ \case
              P.Var (Ident x _) _ -> QualIdent Nothing x
              Dot ((P.Var (Ident m _) _)) (Ident x _) _ -> QualIdent (Just m) x
              _ -> unexpected l "not a class identifier"
      ModuleEltL l $ ContractDef name parents $ mapMaybe parseMethod class_body
  x -> unexpected x "Not a contract definition"

parseOffchainView :: SrcSpan -> [Argument SrcSpan] -> P.Statement SrcSpan -> Method
parseOffchainView l decoArgs def = do
  let f = \case
        x | () == () -> unexpected x "Invalid argument to sp.offchain_view"
        _ -> 0 :: Int
  let x = sum $ map f decoArgs
  seq x $ case def of
    Fun (Ident name _) params _ body l2 ->
      case params of
        Param (Ident "self" _) Nothing Nothing _ : rest -> do
          let rest' = map parseParam rest
          let methodKind = KOffchainView name
              methodParameters = rest'
              methodBody = mkStatementL l2 $ Seq $ map (parseStatement $ Context rest' False) body
              methodLineNo = l2
          Method {..}
        Param (Ident _ l) _ _ _ : _ -> parseError l "first parameter of offchain view must be 'self'"
        _ -> parseError l2 $ "first parameter of offchain view must be 'self'"
    _ -> parseError l "sp.offchain_view must be followed by a definition"

parseOnchainView :: SrcSpan -> [Argument SrcSpan] -> P.Statement SrcSpan -> Method
parseOnchainView l decoArgs def = do
  let f = \case
        x | () == () -> unexpected x "Invalid argument to sp.onchain_view"
        _ -> 0 :: Int
  let x = sum $ map f decoArgs
  seq x $ case def of
    Fun (Ident name _) params _ body l2 ->
      case params of
        Param (Ident "self" _) Nothing Nothing _ : rest -> do
          let rest' = map parseParam rest
          let methodKind = KOnchainView name
              methodParameters = rest'
              methodBody = mkStatementL l2 $ Seq $ map (parseStatement $ Context rest' False) body
              methodLineNo = l2
          Method {..}
        Param (Ident _ l) _ _ _ : _ -> parseError l "first parameter of onchain view must be 'self'"
        _ -> parseError l2 $ "first parameter of onchain view must be 'self'"
    _ -> parseError l "sp.onchain_view must be followed by a definition"

noEffects :: Effects
noEffects = Effects NoStorage False

parseEffects :: [Argument SrcSpan] -> Effects
parseEffects = (`foldl` noEffects) $ \(Effects st op) -> \case
  ArgKeyword (Ident "with_operations" _) v _ ->
    case v of
      Bool b _ -> Effects st b
      x -> unexpected x "'with_operations' must be either True or False."
  ArgKeyword (Ident "with_storage" _) v _ ->
    case v of
      Strings ["\"read-only\""] _ -> Effects ReadOnly op
      Strings ["\"read-write\""] _ -> Effects ReadWrite op
      Strings ["\'read-only\'"] _ -> Effects ReadOnly op
      Strings ["\'read-write\'"] _ -> Effects ReadWrite op
      x -> unexpected x "'with_storage' must be either \'read-only\' or \'read-write\'."
  x -> unexpected x "Invalid argument to sp.private"

parsePrivateMethod :: SrcSpan -> [Argument SrcSpan] -> P.Statement SrcSpan -> Method
parsePrivateMethod l decoArgs def = do
  -- TODO guard against the same argument given twice?
  let mkEffects = parseEffects decoArgs
  case def of
    Fun (Ident mkName _) params _ body l2 ->
      case params of
        Param (Ident "self" _) Nothing Nothing _ : rest -> do
          let rest' = map parseParam rest
          let methodKind = KPrivate {..}
              methodParameters = rest'
              methodBody = mkStatementL l2 $ Seq $ map (parseStatement $ Context [] False) body
              methodLineNo = l2
          Method {..}
        Param (Ident _ l) _ _ _ : _ -> parseError l "first parameter of private method must be 'self'"
        _ -> parseError l2 $ "first parameter of private method must be 'self'"
    _ -> parseError l "sp.private must be followed by a definition"

parseEntrypoint :: SrcSpan -> [Argument SrcSpan] -> P.Statement SrcSpan -> Method
parseEntrypoint l decoArgs def = do
  let f = \case
        ArgKeyword (Ident "new_syntax" _) (Bool _ _) l -> parseError l "'new_syntax' flag inside sp.module."
        x | () == () -> unexpected x "Invalid argument to sp.entrypoint"
        _ -> 0 :: Int
  let x = sum $ map f decoArgs
  seq x $ case def of
    Fun (Ident name _) params _ body l2 ->
      case params of
        Param (Ident "self" _) Nothing Nothing _ : rest -> do
          let rest' = map parseParam rest
          let methodKind = KEntrypoint name
              methodParameters = rest'
              methodBody = mkStatementL l2 $ Seq $ map (parseStatement $ Context rest' False) body
              methodLineNo = l2
          Method {..}
        Param (Ident _ l) _ _ _ : _ -> parseError l "first parameter of entrypoint must be 'self'"
        _ -> parseError l2 $ "first parameter of entrypoint must be 'self'"
    _ -> parseError l "sp.entrypoint must be followed by a definition"

parseMethod :: StatementSpan -> Maybe Method
parseMethod = \case
  Fun (Ident "__init__" _) params Nothing body l2 ->
    case params of
      Param (Ident "self" _) Nothing Nothing _ : rest -> do
        let rest' = map parseParam rest
        let methodKind = KInit
            methodParameters = rest'
            methodBody = mkStatementL l2 $ Seq $ map (parseStatement $ Context [] False) body
            methodLineNo = l2
        Just Method {..}
      Param (Ident _ l) _ _ _ : _ -> parseError l "first argument to __init__ must be 'self'"
      _ -> parseError l2 $ "first argument to __init__ must be 'self'"
  Decorated [Decorator [Ident "sp" _, Ident "entrypoint" _] decoArgs l1] def _ ->
    Just $ parseEntrypoint l1 decoArgs def
  Decorated [Decorator [Ident "sp" _, Ident "private" _] decoArgs l1] def _ ->
    Just $ parsePrivateMethod l1 decoArgs def
  Decorated [Decorator [Ident "sp" _, Ident "onchain_view" _] decoArgs l1] def _ ->
    Just $ parseOnchainView l1 decoArgs def
  Decorated [Decorator [Ident "sp" _, Ident "offchain_view" _] decoArgs l1] def _ ->
    Just $ parseOffchainView l1 decoArgs def
  Decorated [Decorator [Ident "sp" _, Ident "entry_point" l] decoArgs l1] def _ ->
    warn l "sp.entry_point is deprecated. Please use sp.entrypoint instead." $
      Just $
        parseEntrypoint l1 decoArgs def
  P.StmtExpr (Strings _ _) _ -> Nothing
  Pass _ -> Nothing
  x -> unexpected x "Not a method"

parseRow :: [Argument SrcSpan] -> [(String, TypeL)]
parseRow row = sortBy (compare `on` fst) $
  (`map` row) $ \case
    ArgKeyword (Ident name _) x _ -> (name, parseType x)
    x -> unexpected x "Unexpected positional argument"

parseLayout :: P.Expr SrcSpan -> Layout
parseLayout = \case
  Paren x _ -> parseLayout x
  Strings [x] _ -> LayoutLeaf x
  P.Tuple [x, y] _ -> LayoutNode (parseLayout x) (parseLayout y)
  x -> unexpected x "not a layout"

parseSpTypeIdent :: SrcSpan -> IdentSpan -> TypeL
parseSpTypeIdent loc =
  let mk = mkTypeL loc
   in \case
        Ident "unit" _ -> mk TUnit
        Ident "nat" _ -> mk TNat
        Ident "key" _ -> mk TKey
        Ident "key_hash" _ -> mk TKeyHash
        Ident "int" _ -> mk TInt
        Ident "int_or_nat" _ -> mk TIntOrNat
        Ident "string" _ -> mk TString
        Ident "bytes" _ -> mk TBytes
        Ident "signature" _ -> mk TSignature
        Ident "address" _ -> mk TAddress
        Ident "timestamp" _ -> mk TTimestamp
        Ident "bool" _ -> mk TBool
        Ident "mutez" _ -> mk TMutez
        Ident "never" _ -> mk TNever
        Ident "chain_id" _ -> mk TChainId
        Ident "bls12_381_fr" _ -> mk TBls12_381_fr
        Ident "bls12_381_g1" _ -> mk TBls12_381_g1
        Ident "bls12_381_g2" _ -> mk TBls12_381_g2
        Ident "operation" _ -> mk TOperation
        Ident x _ -> unexpected loc $ "sp." <> x <> " type is undefined"

parseType :: ExprSpan -> TypeL
parseType e =
  let loc = getSpan e
      mk = mkTypeL loc
   in case e of
        Dot (P.Var (Ident "sp" _) _) x _ -> parseSpTypeIdent loc x
        P.Var (Ident "int" _) _ -> mk TInt
        P.Var (Ident "bytes" _) _ -> mk TBytes
        P.Var (Ident "bool" _) _ -> mk TBool
        P.Var (Ident "never" _) _ -> mk TNever
        Subscript (Dot (P.Var (Ident "sp" _) _) (Ident "contract" _) _) t _ -> mk $ TContract (parseType t)
        Subscript (Dot (P.Var (Ident "sp" _) _) (Ident "option" _) _) t _ -> mk $ TOption (parseType t)
        Subscript (P.Var (Ident "list" _) _) t _ -> mk $ TList (parseType t)
        Subscript (Dot (P.Var (Ident "sp" _) _) (Ident "list" _) _) t _ -> mk $ TList (parseType t)
        Subscript (P.Var (Ident "set" _) _) t _ -> mk $ S.TSet $ parseType t
        Subscript (Dot (P.Var (Ident "sp" _) _) (Ident "set" _) _) t _ -> mk $ S.TSet $ parseType t
        Subscript (Dot (P.Var (Ident "sp" _) _) (Ident "or_" _) _) (P.Tuple [left, right] _) _ -> mk $ TVariant [("Left", parseType left), ("Right", parseType right)] Nothing
        Subscript (P.Var (Ident "map" _) _) (P.Tuple [k, v] _) _ -> mk $ TMap (parseType k) (parseType v)
        Subscript (Dot (P.Var (Ident "sp" _) _) (Ident "map" _) _) (P.Tuple [k, v] _) _ -> mk $ TMap (parseType k) (parseType v)
        Subscript (Dot (P.Var (Ident "sp" _) _) (Ident "big_map" _) _) (P.Tuple [k, v] _) _ -> mk $ TBigMap (parseType k) (parseType v)
        Subscript (Dot (P.Var (Ident "sp" _) _) (Ident "lambda_" _) _) (P.Tuple [a, b] _) _ -> mk $ TLambda noEffects (parseType a) (parseType b)
        Call (Dot (P.Var (Ident "sp" _) _) (Ident "lambda_" _) _) (ArgExpr a _ : ArgExpr b _ : effects) _ -> mk $ TLambda (parseEffects effects) (parseType a) (parseType b)
        Subscript (Dot (P.Var (Ident "sp" _) _) (Ident "pair" _) _) (P.Tuple [t1, t2] _) _ -> mk $ S.TTuple [parseType t1, parseType t2]
        Subscript (P.Var (Ident "tuple" _) _) (P.Tuple ts _) _ -> mk $ S.TTuple $ map parseType ts
        Subscript (Dot (P.Var (Ident "sp" _) _) (Ident "tuple" _) _) (P.Tuple ts _) _ -> mk $ S.TTuple $ map parseType ts
        Subscript (Dot (P.Var (Ident "sp" _) _) (Ident "ticket" _) _) t _ -> mk $ TTicket $ parseType t
        Call (Dot (P.Var (Ident "sp" _) _) (Ident "record" _) _) entries _ -> mk $ TRecord (parseRow entries) Nothing
        Call (Dot (P.Var (Ident "sp" _) _) (Ident "variant" _) _) entries _ -> mk $ TVariant (parseRow entries) Nothing
        Call (Dot (Call (Dot (P.Var (Ident "sp" _) _) (Ident "record" _) _) entries _) (Ident "layout" _) _) [ArgExpr layout _] _ -> mk $ TRecord (parseRow entries) (Just $ parseLayout layout)
        Call (Dot (Call (Dot (P.Var (Ident "sp" _) _) (Ident "variant" _) _) entries _) (Ident "layout" _) _) [ArgExpr layout _] _ -> mk $ TVariant (parseRow entries) (Just $ parseLayout layout)
        Call (Dot _ (Ident "layout" _) l) _ _ -> parseError l ".layout(...) can only be applied to record and variant types."
        Dot (P.Var (Ident mod _) _) (Ident name _) _ -> mk $ TAbbrev (Just mod) name
        P.Var (Ident name _) _ -> mk $ TAbbrev Nothing name
        x -> unexpected x "Not a type"

parseQualIdent :: ExprSpan -> QualIdent
parseQualIdent = \case
  P.Var (Ident name _) _ -> QualIdent Nothing name
  Dot (P.Var (Ident m _) _) (Ident i _) _ -> QualIdent (Just m) i
  x -> unexpected x "not an identifier"

parseStatement :: Context -> StatementSpan -> StatementL
parseStatement ctxt c =
  let expr = parseExpr ctxt
      statement = parseStatement ctxt
      loc = getSpan c
      mk = mkStatementL loc
      mkExpr = mkExprL loc
   in case c of
        P.StmtExpr (Call (Dot (P.Var (Ident "sp" _) _) (Ident "transfer" _) _) [ArgExpr arg _, ArgExpr amount _, ArgExpr dest _] _) _ ->
          -- TODO use keyword arguments here
          let t = mkExpr $ Prim3 Transfer (expr arg) (expr amount) (expr dest)
              ops = mkExpr Operations
           in mk $ S.Assign (ExprL loc $ Operations) $ mkExpr $ Cons t ops
        P.StmtExpr (Call (Dot (P.Var (Ident "sp" _) _) (Ident "send" _) _) [ArgExpr dest _, ArgExpr amount _] _) l ->
          let dest' = mkExprL l $ OpenVariant "Some" Nothing $ mkExprL l $ GetContract (mkTypeL l TUnit) (expr dest) Nothing
              t = mkExprL l $ Prim3 Transfer (mkExprL l $ Literal LUnit) (expr amount) dest'
              ops = mkExpr Operations
           in mk $ S.Assign (ExprL loc $ Operations) $ mkExprL l $ Cons t ops
        P.StmtExpr (Call (Dot (P.Var (Ident "sp" _) _) (Ident "set_delegate" _) _) [ArgExpr x _] _) l ->
          let t = mkExprL l $ Prim1 SetDelegate $ expr x
              ops = mkExpr Operations
           in mk $ S.Assign (ExprL loc $ Operations) $ mkExprL l $ Cons t ops
        P.StmtExpr (Call (Dot (P.Var (Ident "sp" _) _) (Ident "emit" _) _) (parseArgs -> ([x], kwArgs)) _) l ->
          case parseKeywords ["tag", "with_type"] kwArgs of
            [("tag", tag), ("with_type", _withType)] ->
              let _tag' = case tag of
                    Nothing -> Nothing
                    Just (readString -> Just (s, _)) -> Just s
                    Just _ -> unexpected tag "Not a tag"
                  _withType' = case _withType of
                    Nothing -> True
                    Just (Bool x _) -> x
                    Just x -> unexpected x "invalid argument to with_type"
               in let event = mkExprL l $ Emit _tag' _withType' (expr x)
                      ops = mkExprL l Operations
                   in mk $ S.Assign (ExprL loc $ Operations) $ mkExprL l $ Cons event ops
            _ -> error "invalid argument to sp.emit"
        P.Assign [Dot (Call (P.Var (Ident "record" _) _) args _) (Ident "match" _) _] rhs _ ->
          let f = \case
                ArgExpr (P.Var (Ident x _) _) _ -> (x, mkExpr $ Var x)
                ArgKeyword (Ident k _) (P.Var (Ident v _) _) _ -> (k, mkExpr $ Var v)
                x -> unexpected x "record match"
           in mk $ S.Assign (mkExpr $ Record (map f args)) $ expr rhs
        P.StmtExpr (Call (Dot s (Ident "add" _) _) [ArgExpr x _] _) _ -> mk $ UpdateSet (expr s) (expr x) True
        P.StmtExpr (Call (Dot s (Ident "remove" _) _) [ArgExpr x _] _) _ -> mk $ UpdateSet (expr s) (expr x) False
        Delete [Subscript m k _] _ -> mk $ DelItem (expr m) (expr k)
        P.StmtExpr (Call (Dot (P.Var (Ident "sp" _) _) (Ident "cast" _) _) [ArgExpr x _, ArgExpr t _] _) _ -> mk $ SetType (expr x) (parseType t)
        P.StmtExpr (Call (Dot (P.Var (Ident "sp" _) _) (Ident "never" _) _) [ArgExpr x _] _) _ -> mk $ Never (expr x)
        P.StmtExpr (Call (Dot xs (Ident "push" _) _) [ArgExpr x _] _) l ->
          let xs' = expr xs
           in mk $ S.Assign xs' $ mkExprL l $ Cons (expr x) xs'
        P.StmtExpr (Call (P.Var (Ident "trace" _) _) [ArgExpr e _] _) _ -> mk $ Trace (expr e)
        P.Assign [lhs] rhs _ -> mk $ S.Assign (expr lhs) $ expr rhs
        AugmentedAssign (lhs :: ExprSpan) op (rhs :: ExprSpan) l ->
          let op' = case op of
                PlusAssign _ -> AddHomo
                MinusAssign _ -> Sub
                MultAssign _ -> MulHomo
                DivAssign _ -> Div
                LeftShiftAssign _ -> LShiftInfix
                RightShiftAssign _ -> RShiftInfix
                _ -> unexpected op "Not an operator"
              lhs' = expr lhs
              rhs' = mkExprL l $ Prim2 op' lhs' (expr rhs)
           in mk $ S.Assign lhs' rhs'
        Assert [condition] _ -> mk $ Verify (expr condition) Nothing
        Assert [condition, message] _ -> mk $ Verify (expr condition) $ Just $ expr message
        Conditional [(c, t)] [] _ -> mk $ IfStmt (expr c) (mk $ Seq (map statement t)) Nothing
        Conditional [(c, t)] e _ -> mk $ IfStmt (expr c) (mk $ Seq (map statement t)) $ Just $ mk $ Seq $ map statement e
        Raise (RaiseV3 (Just (e, Nothing))) _ -> mk $ Failwith $ expr e
        Pass _ -> mk $ Seq []
        P.While cond body [] _ -> mk $ While (expr cond) $ mk $ Seq $ map statement body
        P.For [P.Var (P.Ident var _) _] gen body [] _ ->
          mk $ For var (expr gen) $ mk $ Seq $ map (parseStatement ctxt) body
        P.StmtExpr (Strings _ _) _ -> mk $ Seq [] -- For doc strings. TODO allow only at the beginning of method?
        Fun (Ident f _) [Param (Ident x _) Nothing Nothing _] Nothing body l ->
          mk $ Assign (ExprL l $ Var f) $ mkExprL l $ S.Lambda noEffects x $ map (parseStatement ctxt) body
        Decorated [Decorator [Ident "sp" _, Ident "effects" _] effects _] (Fun (Ident f _) [Param (Ident x _) Nothing Nothing _] Nothing body l) _ ->
          mk $ Assign (ExprL l $ Var f) $ mkExprL l $ S.Lambda (parseEffects effects) x $ map (parseStatement ctxt) body
        Return (Just x) _ -> mk $ S.Return $ expr x
        With [(Call (Dot (P.Var (Ident "sp" _) _) (Ident "match" _) _) [ArgExpr scrutinee _] _, Nothing)] cases _ ->
          let parseCase = \case
                With [pat] body loc ->
                  let pat' = case pat of
                        (None _, Nothing) -> VPVariant "None" Nothing
                        (Dot (Dot (P.Var (Ident "sp" _) _) (Ident "case" _) _) (Ident v _) _, as_) ->
                          let x' = case as_ of
                                Nothing -> Nothing
                                Just (P.Var (Ident x _) _) -> Just x
                                x -> unexpected x "Not a variable"
                           in VPVariant v x'
                        (x, _) -> unexpected x "not a pattern"
                   in (mkVariantPatternL loc pat', mkStatementL (getSpan body) $ Seq $ map statement body)
                x -> unexpected x "Not a variant case"
           in mk $ S.Match (expr scrutinee) (map parseCase cases)
        P.StmtExpr (Call (Dot (P.Var (Ident "self" _) _) (Ident "init" _) _) _ _) l ->
          unexpected l "self.init is not part of the new syntax.\nContract storage can be initialized with 'self.data.x = 1'."
        P.StmtExpr (Call (Dot superClass (Ident "__init__" _) _) args l) _ ->
          case args of
            ArgExpr (P.Var (Ident "self" _) _) _ : args -> do
              let args' = map (parseExpr ctxt) $ parsePositionalArgs args
              args' `seq` mk $ CallInit (parseQualIdent superClass) args'
            x : _ -> unexpected x "First argument to __init__ must be 'self'."
            [] -> unexpected l "First argument to __init__ must be 'self'."
        P.StmtExpr e _ -> mk $ S.StmtExpr $ parseExpr ctxt e
        x -> unexpected x "Not a statement"

parsePositionalArgs :: [ArgumentSpan] -> [ExprSpan]
parsePositionalArgs args = (`mapMaybe` args) $ \case
  ArgExpr x _ -> Just x
  x@(ArgKeyword _ _ _) -> unexpected x "Unexpected keyword argument"
  x@(ArgVarArgsPos _ _) -> unexpected x "Unexpected excess positional argument"
  x@(ArgVarArgsKeyword _ _) -> unexpected x "Unexpected execess keyword argument"

-- | Separate positional and keyword arguments.
parseArgs :: [ArgumentSpan] -> ([ExprSpan], String `Map` ExprSpan)
parseArgs args =
  let (posArgs, kwArgs) = unzip $
        (`map` args) $ \case
          ArgExpr x _ -> (Just x, Nothing)
          ArgKeyword (Ident kw _) x _ -> (Nothing, Just (kw, x))
          _ -> error "parseArgs"
   in -- Duplicate kw args are caught by the language-python parser.
      (catMaybes posArgs, Map.fromListWith (impossible "duplicate kw arg") $ catMaybes kwArgs)

parseKeywords :: [String] -> String `Map` ExprSpan -> [(String, Maybe ExprSpan)]
parseKeywords keywords args =
  let keywords' = Map.fromListWith (error "kwKeywords") $ map (\kw -> (kw, Nothing)) keywords
      args' = Map.map Just args
   in case Map.toList $ Map.difference args' keywords' of
        [] -> Map.toList $ Map.unionWith (\_ x -> x) keywords' args'
        (kw, _) : _ -> error $ "unexpected keyword arg: " <> show kw

-- TODO somehow protect reserved words, e.g. no local variable can be
-- called "self" or "range"
mkInt :: Integer -> Expr
mkInt i = Literal $ (if i >= 0 then LIntOrNat else LInt) i

data Context = Context
  { ctxtParams :: [String],
    _ctxtIsLambda :: Bool
  }

lintOrNat :: SrcSpan -> Integer -> ExprL
lintOrNat l i = mkExprL l $ Literal $ LIntOrNat i

readString :: ExprSpan -> Maybe (String, SrcSpan)
readString = \case
  Strings [x] l ->
    let c0 = head x
        c1 = head $ reverse x
     in if length x >= 2 && c0 `elem` ['\'', '"'] && c1 `elem` ['\'', '"'] -- TODO do proper string parsing
          then Just (reverse $ drop 1 $ reverse $ drop 1 x, l)
          else Nothing
  _ -> Nothing

parseSpIdent :: SrcSpan -> String -> Expr
parseSpIdent loc = \case
  "total_voting_power" -> Prim0 TotalVotingPower
  "source" -> Prim0 Source
  "sender" -> Prim0 Sender
  "amount" -> Prim0 Amount
  "balance" -> Prim0 Balance
  "now" -> Prim0 Now
  "chain_id" -> ChainId
  "level" -> Level
  "operations" -> Var "__operations__"
  x -> unexpected loc $ "sp." <> x <> " is undefined"

parseSpCall :: Context -> SrcSpan -> String -> [ArgumentSpan] -> Expr
parseSpCall ctxt loc f args =
  let expr = parseExpr ctxt
      mk = mkExprL loc
   in case (f, args) of
        -- TODO dispatch by number of arguments and give good error message if wrong number of arguments
        ("has_entry_point", [ArgExpr (readString -> Just (ep, _)) _]) -> Contains (mk $ Prim0 EntrypointMap) (mk $ EntrypointId ep)
        ("has_entrypoint", [ArgExpr (readString -> Just (ep, _)) _]) -> Contains (mk $ Prim0 EntrypointMap) (mk $ EntrypointId ep)
        ("contract", [ArgExpr t _, ArgExpr addr _]) -> GetContract (parseType t) (expr addr) Nothing
        ("contract", [ArgExpr t _, ArgExpr addr _, ArgExpr (readString -> Just (ep, _)) _]) -> GetContract (parseType t) (expr addr) $ Just ep
        ("contract", [ArgExpr t _, ArgExpr addr _, ArgKeyword (Ident "entrypoint" _) (readString -> Just (ep, _)) _]) -> GetContract (parseType t) (expr addr) $ Just ep
        ("self_address", []) -> Prim0 SelfAddress
        ("self_contract", []) -> Self $ Nothing
        ("self_entrypoint", [ArgExpr (readString -> Just (ep, _)) _]) -> Self $ Just ep
        ("hash_key", [ArgExpr x _]) -> Prim1 HashKey (expr x)
        ("voting_power", [ArgExpr x _]) -> Prim1 VotingPower (expr x)
        ("transfer_operation", [ArgExpr arg _, ArgExpr amount _, ArgExpr dest _]) -> Prim3 Transfer (expr arg) (expr amount) (expr dest)
        ("pack", [ArgExpr x _]) -> Prim1 Pack (expr x)
        ("unpack", [ArgExpr x _, ArgExpr t _]) -> Unpack (expr x) (parseType t) -- TODO use "cast" to determine type?
        ("split_tokens", [ArgExpr a _, ArgExpr q _, ArgExpr t _]) -> Prim3 SplitTokens (expr a) (expr q) (expr t)
        ("check_signature", [ArgExpr pk _, ArgExpr s _, ArgExpr msg _]) -> Prim3 CheckSignature (expr pk) (expr s) (expr msg)
        ("blake2b", [ArgExpr e _]) -> Prim1 Blake2b (expr e)
        ("sha256", [ArgExpr e _]) -> Prim1 Sha256 (expr e)
        ("sha512", [ArgExpr e _]) -> Prim1 Sha512 (expr e)
        ("sha3", [ArgExpr e _]) -> Prim1 Sha3 (expr e)
        ("keccak", [ArgExpr e _]) -> Prim1 Keccak (expr e)
        ("tez", [ArgExpr (Int _ i _) _]) -> Literal $ LMutez $ 1000 * 1000 * read i -- TODO handle 'read'
        ("mutez", [ArgExpr (Int _ i _) _]) -> Literal $ LMutez $ read i -- TODO handle 'read'
        ("big_map", []) -> MkMap True []
        ("big_map", [ArgExpr (Dictionary entries _) _]) -> MkMap True $ parseMap ctxt entries
        ("cast", [ArgExpr x _, ArgExpr t _]) -> SetTypeExpr (expr x) (parseType t)
        ("to_address", [ArgExpr x _]) -> Prim1 ToAddress $ expr x
        ("view", [ArgExpr (readString -> Just (name, _)) _, ArgExpr param _, ArgExpr addr _, ArgExpr t _]) -> View name (expr addr) (expr param) (parseType t) -- TODO arguments are inverted
        ("implicit_account", [ArgExpr x _]) -> Prim1 ImplicitAccount $ expr x
        ("michelson", (ArgExpr (readString -> Just (code, _)) _ : ArgExpr (P.List tsIn _) _ : ArgExpr (P.List tsOut _) _ : args)) ->
          let getArg = \case
                ArgExpr e _ -> expr e
                _ -> error "getArg"
           in InlineMichelson code (map parseType tsIn) (map parseType tsOut) (map getArg args)
        ("is_nat", [ArgExpr x _]) -> Prim1 IsNat $ expr x
        ("as_nat", [ArgExpr x _]) -> OpenVariant "Some" Nothing $ mk $ Prim1 IsNat $ expr x
        ("as_nat", [ArgExpr x _, ArgKeyword (Ident "error" _) msg _]) -> OpenVariant "Some" (Just $ expr msg) $ mk $ Prim1 IsNat $ expr x
        ("cons", [ArgExpr x _, ArgExpr xs _]) -> Cons (expr x) (expr xs)
        ("set", xs) -> Set $
          (`map` xs) $ \case
            ArgExpr x _ -> expr x
            _ -> parseError loc "unexpected argument"
        ("concat", [ArgExpr x _]) -> Prim1 Concat $ expr x
        ("ediv", [ArgExpr x _, ArgExpr y _]) -> Prim2 Ediv (expr x) (expr y)
        ("update_map", [ArgExpr k _, ArgExpr v _, ArgExpr m _]) -> UpdateMap (expr k) (expr v) (expr m)
        ("compare", [ArgExpr x _, ArgExpr y _]) -> Prim2 Compare (expr x) (expr y)
        ("fst", [ArgExpr e _]) -> Fst (expr e)
        ("snd", [ArgExpr e _]) -> Snd (expr e)
        ("to_int", [ArgExpr e _]) -> Prim1 ToInt (expr e)
        ("to_nat", [ArgExpr e _]) -> Prim1 ToNat (expr e)
        ("to_bytes", [ArgExpr e _]) -> Prim1 ToBytes (expr e)
        ("convert", [ArgExpr e _]) -> Convert (expr e)
        ("add_days", [ArgExpr x _, ArgExpr y _]) -> Prim2 AddSeconds (expr x) $ mk $ Prim2 MulHomo (expr y) $ lintOrNat loc $ 24 * 3600
        ("add_seconds", [ArgExpr x _, ArgExpr y _]) -> Prim2 AddSeconds (expr x) (expr y)
        ("sub_mutez", [ArgExpr x _, ArgExpr y _]) -> Prim2 SubMutez (expr x) (expr y)
        ("min", [ArgExpr x _, ArgExpr y _]) -> Prim2 Min (expr x) (expr y)
        ("max", [ArgExpr x _, ArgExpr y _]) -> Prim2 Max (expr x) (expr y)
        ("mod", [ArgExpr x _, ArgExpr y _]) -> Prim2 Mod (expr x) (expr y)
        ("add", [ArgExpr x _, ArgExpr y _]) -> Prim2 Add (expr x) (expr y)
        ("mul", [ArgExpr x _, ArgExpr y _]) -> Prim2 Mul (expr x) (expr y)
        ("invert_bytes", [ArgExpr x _]) -> Prim1 InvertBytes (expr x)
        ("lshift_bytes", [ArgExpr x _, ArgExpr y _]) -> Prim2 LShiftBytes (expr x) (expr y)
        ("rshift_bytes", [ArgExpr x _, ArgExpr y _]) -> Prim2 RShiftBytes (expr x) (expr y)
        ("and_bytes", [ArgExpr x _, ArgExpr y _]) -> Prim2 S.AndBytes (expr x) (expr y)
        ("or_bytes", [ArgExpr x _, ArgExpr y _]) -> Prim2 S.OrBytes (expr x) (expr y)
        ("xor_bytes", [ArgExpr x _, ArgExpr y _]) -> Prim2 S.XorBytes (expr x) (expr y)
        ("slice", [ArgExpr offset _, ArgExpr len _, ArgExpr buf _]) -> Prim3 Slice (expr offset) (expr len) (expr buf)
        ("get_and_update", [ArgExpr k _, ArgExpr v _, ArgExpr m _]) -> Prim3 GetAndUpdate (expr m) (expr k) (expr v)
        ("range", [ArgExpr to _]) -> Prim3 Range (lintOrNat loc 0) (expr to) (lintOrNat loc 1)
        ("range", [ArgExpr from _, ArgExpr to _]) -> Prim3 Range (expr from) (expr to) (lintOrNat loc 1)
        ("range", [ArgExpr from _, ArgExpr to _, ArgExpr step _]) -> Prim3 Range (expr from) (expr to) (expr step)
        ("map", [ArgExpr f _, ArgExpr xs _]) -> Prim2 Map (expr xs) (expr f)
        ("nat", [ArgExpr (Int _ i _) _]) -> Literal (LNat $ read i) -- TODO handle 'read'
        ("int", [ArgExpr (Int _ i _) _]) -> Literal (LInt $ read i) -- TODO handle 'read' failures, make sure it coincides with Python syntax
        ("int", [ArgExpr (UnaryOp (Minus _) (Int _ i _) _) _]) -> Literal (LInt $ -read i)
        ("timestamp", [ArgExpr (Int _ i _) _]) -> Literal (LTimestamp $ read i) -- TODO handle 'read'
        ("address", [ArgExpr (readString -> Just (s, _)) _]) -> Literal (LAddress s)
        ("chain_id", [ArgExpr (readString -> Just (s, _)) _]) -> Literal (LChainId s)
        ("bytes", [ArgExpr (readString -> Just (s, _)) _]) -> Literal (LBytes s)
        ("key", [ArgExpr (readString -> Just (s, _)) _]) -> Literal (LKey s)
        ("key_hash", [ArgExpr (readString -> Just (s, _)) _]) -> Literal (LKeyHash s)
        ("bls12_381_fr", [ArgExpr (readString -> Just (s, _)) _]) -> Literal (LBls12_381_fr s)
        ("bls12_381_g1", [ArgExpr (readString -> Just (s, _)) _]) -> Literal (LBls12_381_g1 s)
        ("bls12_381_g2", [ArgExpr (readString -> Just (s, _)) _]) -> Literal (LBls12_381_g2 s)
        ("record", entries) ->
          Record $
            sortBy (compare `on` fst) $
              (`map` entries) $ \case
                ArgKeyword (Ident name _) x _ -> (name, expr x)
                x -> unexpected x "non-keyword argument"
        ("Some", [ArgExpr x _]) -> Variant "Some" $ Just $ expr x
        ("Left", [ArgExpr x _]) -> Variant "Left" $ Just $ expr x
        ("Right", [ArgExpr x _]) -> Variant "Right" $ Just $ expr x
        ("len", [ArgExpr x _]) -> Prim1 Len (expr x)
        ("sum", [ArgExpr x _]) -> Prim1 Sum (expr x)
        ("create_contract", [ArgExpr qi _, ArgExpr baker _, ArgExpr amount _, ArgExpr storage _]) ->
          CreateContract True (parseQualIdent qi) (expr baker) (expr amount) (expr storage)
        ("create_contract_operation", [ArgExpr qi _, ArgExpr baker _, ArgExpr amount _, ArgExpr storage _]) ->
          CreateContract False (parseQualIdent qi) (expr baker) (expr amount) (expr storage)
        ("set_delegate_operation", [ArgExpr x _]) -> Prim1 SetDelegate (expr x)
        ("read_ticket", [ArgExpr t _]) -> Prim1 ReadTicket $ expr t
        ("join_tickets", [ArgExpr ts _]) -> Prim1 JoinTickets (expr ts)
        ("ticket", [ArgExpr content _, ArgExpr amount _]) -> Prim2 Ticket (expr content) (expr amount)
        ("split_ticket", [ArgExpr t _, ArgExpr amounts _]) -> Prim2 SplitTicket (expr t) (expr amounts)
        ("test_ticket", [ArgExpr ticketer _, ArgExpr content _, ArgExpr amount _]) -> Prim3 TestTicket (expr ticketer) (expr content) (expr amount)
        ("pairing_check", [ArgExpr pairs _]) -> Prim1 PairingCheck (expr pairs)
        (x, _) -> unexpected loc $ "sp." <> x <> " is undefined or wrong number of arguments" -- TODO be more specefic: undefined or wrong number of args?

parseMap :: Context -> [DictKeyDatumList SrcSpan] -> [(ExprL, ExprL)]
parseMap ctxt entries = (`map` entries) $ \case
  DictMappingPair k v -> (parseExpr ctxt k, parseExpr ctxt v)
  x@(DictUnpacking _) -> unexpected x "DictUnpacking" -- TODO what is this?

parseOperator :: SrcSpan -> Op SrcSpan -> Prim2
parseOperator loc op = case op of
  Divide _ -> Div
  Equality _ -> EqBin
  GreaterThan _ -> GtBin
  GreaterThanEquals _ -> GeBin
  LessThan _ -> LtBin
  LessThanEquals _ -> LeBin
  Minus _ -> Sub
  Modulo _ -> parseError loc $ "unknown operator: '" <> prettyText op <> " (Did you mean to use sp.mod?)"
  Multiply _ -> MulHomo
  NotEquals _ -> Neq
  P.And _ -> And
  P.BinaryAnd _ -> S.AndInfix
  P.BinaryOr _ -> S.OrInfix
  P.Or _ -> Or
  P.Xor _ -> XorInfix
  Plus _ -> AddHomo
  ShiftLeft _ -> LShiftInfix
  ShiftRight _ -> RShiftInfix
  _ -> parseError loc $ "unknown operator: '" <> prettyText op <> "'"

mkVar :: Context -> SrcSpan -> String -> Expr
mkVar ctxt loc name =
  let mk = mkExprL loc
   in if name `elem` ctxtParams ctxt
        then (if length (ctxtParams ctxt) > 1 then Attr (mk Params) name else Params)
        else Var name

parseExpr :: Context -> ExprSpan -> ExprL
parseExpr ctxt e =
  let expr = parseExpr ctxt
      loc = getSpan e
      mk = mkExprL loc
   in mk $ case e of
        P.Tuple [] _ -> Literal LUnit
        Paren (P.Tuple xs _) _ -> Tuple $ map expr xs
        Paren (expr -> ExprL _ e) _ -> e
        Dictionary entries _ -> MkMap False $ parseMap ctxt entries
        Dot (P.Var (Ident "self" _) _) (Ident "data" _) _ -> Storage
        Dot (P.Var (Ident "self" _) _) (Ident "private" _) _ -> Private
        Dot (P.Var (Ident "self" _) _) (Ident name _) _ -> S.PrivateMethod name
        Dot (P.Var (Ident "sp" _) _) (Ident x _) _ -> parseSpIdent loc x
        Call (P.Var (Ident "abs" _) _) [ArgExpr x _] _ -> Prim1 Abs $ expr x
        Call (P.Var (Ident "reversed" _) _) [ArgExpr xs _] _ -> Prim1 Reversed $ expr xs
        Call (Dot (P.Var (Ident "sp" _) _) (Ident f _) _) args _ -> parseSpCall ctxt loc f args
        Subscript c key _ -> GetItem (expr c) (expr key)
        Dot x (Ident attr _) _ -> Attr (expr x) attr
        Call (Dot (Dot (P.Var (Ident "sp" _) _) (Ident "variant" _) _) (Ident constructor _) _) [] _ -> Variant constructor Nothing
        Call (Dot (Dot (P.Var (Ident "sp" _) _) (Ident "variant" _) _) (Ident constructor _) _) [ArgExpr arg _] _ -> Variant constructor $ Just $ expr arg
        Call (Dot (Dot x (Ident "is_variant" _) _) (Ident v _) _) [] _ -> IsVariant v $ expr x
        Call (Dot x (Ident "is_none" _) _) [] _ -> IsVariant "None" $ expr x
        Call (Dot x (Ident "is_some" _) _) [] _ -> IsVariant "Some" $ expr x
        -- Call (Dot x (Ident "is_left" _) _) [] _ -> IsVariant "Left" $ expr x
        -- Call (Dot x (Ident "is_right" _) _) [] _ -> IsVariant "Right" $ expr x
        Call (Dot (Dot x (Ident "unwrap" _) _) (Ident v _) _) [] _ -> OpenVariant v Nothing $ expr x
        Call (Dot (Dot x (Ident "unwrap" _) _) (Ident v _) _) [ArgKeyword (Ident "error" _) msg _] _ -> OpenVariant v (Just $ expr msg) $ expr x
        Call (Dot x (Ident "unwrap_some" _) _) [] _ -> OpenVariant "Some" Nothing $ expr x
        Call (Dot x (Ident "unwrap_some" _) _) [ArgKeyword (Ident "error" _) msg _] _ -> OpenVariant "Some" (Just $ expr msg) $ expr x
        -- Call (Dot x (Ident "unwrap_left" _) _) [] _ -> OpenVariant "Left" Nothing $ expr x
        -- Call (Dot x (Ident "unwrap_left" _) _) [ArgKeyword (Ident "error" _) msg _] _ -> OpenVariant "Left" (Just $ expr msg) $ expr x
        -- Call (Dot x (Ident "unwrap_right" _) _) [] _ -> OpenVariant "Right" Nothing $ expr x
        -- Call (Dot x (Ident "unwrap_right" _) _) [ArgKeyword (Ident "error" _) msg _] _ -> OpenVariant "Right" (Just $ expr msg) $ expr x
        Call (Dot f (Ident "apply" _) _) [ArgExpr x _] _ -> Prim2 S.Apply (expr f) (expr x)
        Call (Dot c (Ident "get_opt" _) _) [ArgExpr key _] _ -> GetOpt (expr c) (expr key)
        Call (Dot c (Ident "items" _) _) [] _ -> Prim1 Items (expr c)
        Call (Dot c (Ident "values" _) _) [] _ -> Prim1 Values (expr c)
        Call (Dot c (Ident "keys" _) _) [] _ -> Prim1 Keys (expr c)
        Call (Dot c (Ident "elements" _) _) [] _ -> Prim1 Elements (expr c)
        Call (Dot c (Ident "contains" _) _) [ArgExpr key _] _ -> Contains (expr c) (expr key)
        Call (Dot c (Ident "get" _) _) [ArgExpr key _, ArgKeyword (Ident "default" _) dflt _] _ -> GetItemDefault (expr c) (expr key) (expr dflt)
        Call (Dot c (Ident "get" _) _) [ArgExpr key _, ArgKeyword (Ident "error" _) msg _] _ -> GetItemMessage (expr c) (expr key) (expr msg)
        Call (P.Var (Ident "len" _) _) [ArgExpr x _] _ -> Prim1 Len $ expr x
        Call (P.Var (Ident "range" _) _) [ArgExpr to _] _ -> Prim3 Range (lintOrNat loc 0) (expr to) (lintOrNat loc 1)
        Call (P.Var (Ident "range" _) _) [ArgExpr from _, ArgExpr to _] _ -> Prim3 Range (expr from) (expr to) (lintOrNat loc 1)
        Call (P.Var (Ident "range" _) _) [ArgExpr from _, ArgExpr to _, ArgExpr step _] _ -> Prim3 Range (expr from) (expr to) (expr step)
        Call f [] _ -> S.Call (expr f) Nothing
        Call f [ArgExpr x _] _ -> S.Call (expr f) $ Just $ expr x
        P.Var (Ident name _) _ -> mkVar ctxt loc name
        BinaryOp op lhs rhs _ -> let p = parseOperator loc op in Prim2 p (expr lhs) (expr rhs)
        UnaryOp (P.Minus _) x _ {- -x -} -> Prim1 S.Neg $ expr x
        UnaryOp (P.Not _) x _ {- not x -} -> Prim1 S.Not $ expr x
        UnaryOp (P.Invert _) x _ {- ~x -} -> Prim1 S.Invert $ expr x
        Int i _ _ -> mkInt i
        (readString -> Just (x, _)) -> Literal $ LString x
        Bool b _ -> Literal $ LBool b
        P.List xs _ -> List $ map expr xs
        CondExpr a c b _ -> Prim3 If (expr c) (expr a) (expr b)
        None _ -> Variant "None" Nothing
        Lambda [Param (Ident x _) Nothing Nothing _] body _ -> S.Lambda noEffects x $ [mkStatementL loc $ S.Return $ parseExpr ctxt body]
        P.ListComp
          ( Comprehension
              (ComprehensionExpr e)
              (CompFor False [P.Var (Ident x _) _] xs Nothing _)
              _
            )
          _ -> S.ListComprehension (expr e) x (expr xs)
        x -> unexpected x "Not an expression"
