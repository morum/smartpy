module Main where

import Control.Exception
import qualified Data.JSString
import Data.Maybe (fromJust)
import Data.Text.Lazy as T
import Data.Text.Lazy.Encoding as T
import GHCJS.Foreign.Callback (Callback, syncCallback2')
import GHCJS.Marshal (fromJSVal, toJSVal)
import GHCJS.Types (JSVal)
import qualified SmartPy

foreign import javascript unsafe "parsePythonModule = $1"
  set_parsePythonModule :: Callback (JSVal -> JSVal -> IO JSVal) -> IO ()

parsePythonModule :: JSVal -> JSVal -> IO JSVal
parsePythonModule fileName input = do
  fileName <- Data.JSString.unpack . fromJust <$> fromJSVal fileName
  input <- Data.JSString.unpack . fromJust <$> fromJSVal input
  r <- try $ do let r = SmartPy.parsePythonModule fileName input in r `seq` return r
  let r' = case r of
        Left (e :: SomeException) -> T.pack $ "ERROR " <> show e
        Right r -> T.decodeUtf8 r
  toJSVal $ T.toStrict r'

main :: IO ()
main = do
  set_parsePythonModule =<< syncCallback2' parsePythonModule
