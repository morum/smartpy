SHELL=bash

INSIDE_DOCKER ?= no

os := $(shell uname -s)
ifeq ($(os),Darwin)
  os := osx
else ifeq ($(os),Linux)
  os := linux
endif

arch := $(shell uname -m)
ifeq ($(arch),arm64)
  arch := aarch64
endif

GHC_VERSION=$(shell ./haskell/ghc-version)

deps-opam.ok:
	opam init --no-setup --bare $(shell [ $(INSIDE_DOCKER) = yes ] && echo --disable-sandboxing)
	opam switch create . ocaml-base-compiler.4.14.1
	opam switch import --yes env/switch.export
	touch $@

deps-cabal.ok:
	cabal update
	cd haskell && cabal build --enable-documentation --builddir ../_build/haskell --dependencies-only smartpy
	touch $@

parser.ok: deps-cabal.ok
	cd haskell && cabal build --enable-documentation --builddir ../_build/haskell smartpy-parser
	mkdir -p _build/bin
	cd _build/bin/; ln -sf ../haskell/build/$(arch)-$(os)/ghc-$(GHC_VERSION)/smartpy-parser-0.1.0.0/x/smartpy-parser/build/smartpy-parser/smartpy-parser

smartpyc.ok: deps-opam.ok
	opam exec -- dune build --root . smartML/smartpyc/smartpyc_unix.exe
	strip _build/default/smartML/smartpyc/smartpyc_unix.exe
	opam exec -- dune build --root . smartML/{michel,michelson_base,core,utils}.opam
	opam install -y smartML/utils.opam
	opam install -y smartML/michelson_base.opam
	opam install -y smartML/michel.opam
	opam install -y smartML/core.opam
	touch $@

DISTDIR=dist/smartpy-bin/smartpy

dist/smartpy-bin.tar.gz: smartpyc.ok parser.ok
	rm -rf $(DISTDIR)
	mkdir -p $(DISTDIR)
	cp -R \
	  python/templates \
	  python/smartpy.py \
	  python/smartpyio.py \
	  python/smartpyc.py \
	  python/sexp.py \
	  python/utils.py \
	  smartpy-cli/browser.py \
	  smartpy-cli/SmartPy.sh \
	  packages/frontend/public/theme.js \
	  packages/frontend/public/static/js/smart.js \
	  packages/frontend/public/static/css/typography.css \
	  packages/frontend/public/static/css/smart.css \
	  $(DISTDIR)
	cp _build/default/smartML/smartpyc/smartpyc_unix.exe $(DISTDIR)/smartpyc
	cp _build/bin/smartpy-parser $(DISTDIR)
	cp --parents \
	  _opam/lib/core/*.cmi \
	  _opam/lib/ocaml/*.cmi \
	  _opam/lib/ocaml/compiler-libs/*.cmi \
	  _opam/lib/utils/*.cmi \
	  $(DISTDIR)
	chmod -R go-w dist/smartpy-bin
	tar czf $@ -C dist/smartpy-bin smartpy

.PHONY: build dist
