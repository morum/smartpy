(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Core
open Utils

module Main (C : Cmd.S) (P : Primitives.Primitives) = struct
  type config = {
      script : string option
    ; output : string option
    ; config : Config.t
    ; script_args : string list
  }

  let config0 =
    {script = None; output = None; config = Config.default; script_args = []}

  let usage () =
    print_endline
      {|
Usage: smartpyc <options> <source file>

The <source file> is a python file containing a class that inherits
from sp.Contract.

OPTIONS

 -o | --output
    Specify a different output file (default: contract.smlse).

 -h | --help
    Print help.

EXAMPLE

smartpyc --kind test calculator.py --output calculator.smlse
|}

  let script_extensions = [".py"; ".json"]

  let rec parse c = function
    | [] -> c
    | p :: rest
      when List.mem (Filename.extension p) script_extensions && c.script = None
      -> parse {c with script = Some p} rest
    | ("-o" | "--output") :: o :: rest -> parse {c with output = Some o} rest
    | ("-h" | "--help") :: _ ->
        usage ();
        exit 0
    | "--" :: rest -> {c with script_args = rest}
    | x :: xs ->
        let err () =
          failwith (Printf.sprintf "Don't know what to do with %s." x)
        in
        if String.sub x 0 2 = "--"
        then
          let x = String.(sub x 2 (length x - 2)) in
          match Config.parse_flag [x] with
          | Some flag ->
              parse {c with config = Config.apply_flag c.config flag} xs
          | None -> (
              match xs with
              | [] -> err ()
              | arg :: rest -> (
                  match Config.parse_flag [x; arg] with
                  | None -> err ()
                  | Some flag ->
                      parse
                        {c with config = Config.apply_flag c.config flag}
                        rest))
        else err ()

  let mkdir_p x =
    let r = C.run "mkdir" ["-p"; x] in
    if r <> 0 then failwith (Printf.sprintf "mkdir failed: %d" r)

  let run_smartpyc_py args =
    let path = Option.default "" (Sys.getenv_opt "PATH") in
    let dir = Filename.dirname Sys.argv.(0) in
    let pythonpath =
      match Sys.getenv_opt "PYTHONPATH" with
      | None -> dir
      | Some pythonpath -> String.concat ":" [dir; pythonpath]
    in
    let env = [("PATH", path); ("PYTHONPATH", pythonpath)] in
    let r = C.run ~env "python3" (Filename.concat dir "smartpyc.py" :: args) in
    match r with
    | 0 -> ()
    | 1 -> exit 1
    | r -> failwith (Printf.sprintf "unexpected exit status from python: %d" r)

  let write_targets_py out_scenario_sc in_py script_args =
    run_smartpyc_py (["write_tests"; out_scenario_sc; in_py] @ script_args)

  let run_scenarios ~config fn_scenario out_dir =
    let scenarios =
      Yojson.Safe.Util.to_list (Yojson.Safe.from_file fn_scenario)
    in
    let primitives = (module P : Primitives.Primitives) in
    let read_scenario scenario =
      let scenario = Import.load_scenario ~primitives config scenario in
      let out_dir = Filename.concat out_dir scenario.scenario.shortname in
      (scenario, out_dir)
    in
    let scenarios = List.map read_scenario scenarios in
    let run_scenario (scenario, out_dir) =
      mkdir_p out_dir;
      Simulator.run ~config ~primitives ~browser:false
        ~all_scenarios:(List.map fst scenarios) ~output_dir:(Some out_dir)
        ~scenario
    in
    List.concat (List.map run_scenario scenarios)

  let is_error = function
    | `Error, _ -> true
    | _ -> false

  let run_targets ~config out_dir script script_args =
    let module Printer = (val Printer.get ~config : Printer.Printer) in
    let scenario_dir = out_dir in
    mkdir_p scenario_dir;
    let fn_scenario = Filename.concat scenario_dir "scenario.json" in
    let errors =
      try
        begin
          match Filename.extension script with
          | ".py" ->
              Io.write_file
                (Filename.concat scenario_dir "script_init.py")
                (Io.read_file script);
              write_targets_py fn_scenario script script_args
          | _ -> assert false
        end;
        run_scenarios ~config fn_scenario scenario_dir
      with Basics.SmartExcept l -> [(`Error, l)]
    in
    match List.partition is_error errors with
    | [], [] -> ()
    | (_ :: _ as errors), _ ->
        let f (_, x) =
          prerr_endline ("[error]\n" ^ Printer.pp_smart_except false x)
        in
        List.iter f errors;
        exit 1
    | [], (_ :: _ as warnings) ->
        let f (_, x) =
          prerr_endline ("[warning]\n" ^ Printer.pp_smart_except false x)
        in
        List.iter f warnings

  let run {script; output; config; script_args} =
    let script = Option.of_some ~msg:"source file required" script in
    let basefilename = Filename.chop_extension script in
    let out_dir = Option.default basefilename output in
    run_targets ~config out_dir script script_args

  let main =
    let env = parse config0 (List.tl (Array.to_list Sys.argv)) in
    let module Printer = (val Printer.get ~config:env.config : Printer.Printer)
    in
    (try run env
     with exn ->
       prerr_endline (Printer.exception_to_string false exn);
       exit 1);
    exit 0
end
