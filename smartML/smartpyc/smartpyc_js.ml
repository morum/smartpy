(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

include Smartpyc_lib.Smartpyc.Main (Utils.Cmd_js) (Smartml_js.Primitives)
