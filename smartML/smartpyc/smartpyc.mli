(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Core
open Utils

module Main (_ : Cmd.S) (_ : Primitives.Primitives) : sig
  val main : unit
end
