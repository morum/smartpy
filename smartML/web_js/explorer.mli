(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

val explore :
     config:Core.Config.t
  -> address:string
  -> json:string
  -> network:string
  -> unit

val exploreOperations :
     config:Core.Config.t
  -> address:string
  -> json:string
  -> operations:string
  -> unit

val parseStorage : config:Core.Config.t -> json:string -> string
