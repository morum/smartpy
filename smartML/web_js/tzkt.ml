(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Utils
open Core
open Utils.Misc
module Y = Yojson.Safe.Util

let parse_api ~config address operation_json =
  let module Printer = (val Printer.get ~config : Printer.Printer) in
  let operation = json_getter operation_json in
  let module M = (val operation : JsonGetter) in
  let get_protect field =
    try Y.to_string (Y.member "address" (M.get field))
    with _ ->
      assert (1 < 0);
      Printf.sprintf "Missing field '%s'" field
  in
  let target = get_protect "target" in
  let is_target = address = target in
  let render =
    Printer.tvalue_to_string ~options:Printer.Options.htmlStripStrings
  in
  let entry ?title fld parse =
    ( Option.default (String.capitalize_ascii fld) title
    , render (parse (Y.member fld operation_json)) )
  in
  let parse_string x = Value.Typed.string (Y.to_string x) in
  let parse_address x = Value.Typed.address (Y.to_string x) in
  let parse_address_record x =
    Value.Typed.address (Y.to_string (Y.member "address" x))
  in
  let metaData =
    [
      entry ~title:"Date and time" "timestamp" parse_string
    ; entry "sender" parse_address_record
    ; entry "target" parse_address_record
    ; ("Address", render (Value.Typed.address address))
    ; ("Internal", render (Value.Typed.bool is_target))
    ; entry "block" parse_address
    ; entry "hash" parse_address
    ]
  in
  let details =
    [
      entry "status" parse_string
      (* ; ("is_success", bool)
       * ; ("height", string)
       * ; ("cycle", string)
       * ; ("counter", string)
       * ; ("gas_limit", string)
       * ; ("gas_used", string)
       * ; ("gas_price", string)
       * ; ("storage_limit", string)
       * ; ("storage_size", string)
       * ; ("storage_paid", string)
       * ; ("volume", string)
       * ; ("fee", string)
       * ; ("reward", string)
       * ; ("deposit", string)
       * ; ("burned", string) *)
    ]
  in
  let parameters =
    let parameters = M.get "parameter" in
    match parameters with
    | `Null -> `Error "No parameters"
    | _ ->
        let module M = (val json_getter parameters : JsonGetter) in
        let entrypoint = M.string "entrypoint" in
        let value = M.string "value" in
        `OK (entrypoint, value)
  in
  let storage =
    if M.has_member "storage"
    then
      try `OK (M.string "storage")
      with exn -> `Error (Printer.exception_to_string true exn)
    else `Error "No storage"
  in
  let errors =
    match M.get "errors" with
    | `Null -> None
    | errors -> Some (Yojson.Safe.to_string errors)
  in
  Explorer_interface.
    {metaData; details; parameters; storage; errors; operation_json; is_target}
