(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

val parse_api :
     config:Core.Config.t
  -> string
  -> Yojson.Safe.t
  -> Explorer_interface.transaction
