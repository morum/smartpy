(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Core
open Js_of_ocaml
open Basics
open Utils

type exportToJs = {
    exportToJs : 'a 'b. string -> ('a -> 'b) -> unit
  ; exportToJsString : 'a. string -> ('a -> string) -> unit
  ; js_to_string : Js.js_string Js.t -> string
  ; string_to_js : string -> Js.js_string Js.t
  ; getText : checked:bool -> string -> string
  ; setText : string -> string -> unit
  ; setValue : string -> string -> unit
  ; setOutput : string -> unit
  ; setExplorerOutput : string -> string -> unit
  ; addOutput : string -> unit
  ; setOutputToMethod : string -> string -> unit
  ; isChecked : string -> bool
  ; parseDate : string -> string
}

let js_primitives = lazy (module Smartml_js.Primitives : Primitives.Primitives)

let buildTransfer ~config id t : unit =
  let module Printer = (val Printer.get ~config : Printer.Printer) in
  try
    let input = Value_gui.inputGuiR ~nextId:(Value.nextId id) t in
    let initialValue = input.get true in
    let initialValue = Value.typecheck t initialValue in
    let entrypoint, initialValue =
      match initialValue.tv with
      | Variant (entrypoint, initialValue) -> (Some entrypoint, initialValue)
      | _ -> (None, initialValue)
    in
    let compiled_parameter_full =
      let value =
        match entrypoint with
        | None -> initialValue
        | Some entrypoint -> Value.Typed.variant t entrypoint initialValue
      in
      Compiler.compile_value ~config ~scenario_vars:TBoundMap.empty value
    in
    let entrypoint = Option.default "default" entrypoint in
    let compiled_parameter =
      Compiler.compile_value ~config ~scenario_vars:TBoundMap.empty initialValue
    in
    let as_json = Builder.buildTransferParametersJSON ~compiled_parameter in
    let micheline =
      Builder.buildTransferParametersMicheline ~compiled_parameter
    in
    let as_json_full =
      Builder.buildTransferParametersJSON
        ~compiled_parameter:compiled_parameter_full
    in
    let micheline_full =
      Builder.buildTransferParametersMicheline
        ~compiled_parameter:compiled_parameter_full
    in
    SmartDom.setExplorerOutput "entrypoint" entrypoint;
    SmartDom.setExplorerOutput "paramsMicheline" micheline;
    SmartDom.setExplorerOutput "paramsJSON" as_json;
    SmartDom.setExplorerOutput "paramsMichelineFull" micheline_full;
    SmartDom.setExplorerOutput "paramsJSONFull" as_json_full
  with _ as e ->
    SmartDom.setExplorerOutput "errors"
      (Printf.sprintf "Error during execution: %s"
         (Printer.exception_to_string true e))

let michelson_view' sizes examples types_output_div simplified_types_output_div
    simplified_no_types_output_div raw_no_types_output_div json_output_div
    json_raw_output_div =
  let originationButton =
    {|
    <button class='centertextbutton extramarginbottom'
      onClick='smartpyContext.gotoOrigination(
        michelson_view_json_output.innerText,
        simplified_init_storage_json_0.innerText
      )'>
        Deploy Michelson Contract
    </button>
  |}
  in
  let tabs =
    [
      Html.tab ~active:() "Simplified"
        (Html.tabs ""
           [
             Html.tab "Michelson" ~active:()
               (Html.div ~args:"id='simplified_types_output'"
                  [Html.Raw originationButton; simplified_types_output_div])
           ; Html.tab "No Type"
               (Html.div ~args:"id='simplified_no_types_output'"
                  [simplified_no_types_output_div])
           ; Html.tab "JSON"
               (Html.div ~args:"id='json_output'" [json_output_div])
           ])
    ; Html.tab "Raw"
        (Html.tabs ""
           [
             Html.tab "Michelson" ~active:()
               (Html.div ~args:"id='types_output'" [types_output_div])
           ; Html.tab "No Type"
               (Html.div ~args:"id='raw_no_types_output'"
                  [raw_no_types_output_div])
           ; Html.tab "JSON"
               (Html.div ~args:"id='json_raw_output'" [json_raw_output_div])
           ])
    ; Html.tab "Sizes" (Html.div ~args:"id='sizes_output'" [sizes])
    ; Html.tab "Examples" (Html.div ~args:"id='examples_output'" [examples])
    ]
  in
  Html.tabs "Output" tabs |> Html.render

let update_michelson_view ~config micheline =
  let protocol = config.Config.protocol in
  SmartDom.setOutputToMethod "setFirstMessage" "";
  let raw_contract =
    try Some (Michelson.Of_micheline.contract micheline)
    with Failure msg ->
      SmartDom.setOutputToMethod "setFirstMessage" ("Error: " ^ msg);
      None
  in
  match raw_contract with
  | None -> ()
  | Some raw_contract ->
      let raw_contract = Michelson.typecheck_contract ~protocol raw_contract in
      let simplified_contract =
        Michelson_rewriter.(
          run_on_tcontract ~protocol (simplify ~protocol) raw_contract)
      in
      let first_message =
        let errors_simplifies =
          Michelson.has_error_tcontract ~accept_missings:false
            simplified_contract
        in
        let errors_raw =
          Michelson.has_error_tcontract ~accept_missings:false raw_contract
        in
        let pp_errors =
          if errors_simplifies = errors_raw
          then String.concat "; " errors_raw
          else ""
        in
        let pp_errors =
          if String.length pp_errors > 100
          then String.sub pp_errors 0 100 ^ "..."
          else pp_errors
        in
        match (errors_simplifies, errors_raw) with
        | [], [] -> None
        | [], _ -> Some ("Errors in raw contract. " ^ pp_errors)
        | _, [] -> Some ("Errors in simplified contract. " ^ pp_errors)
        | [e1], [e2]
          when e1 = Michelson.unexpected_final_stack_error
               && e2 = Michelson.unexpected_final_stack_error ->
            Some "Incomplete stack"
        | _ -> Some ("Errors in Michelson contract. " ^ pp_errors)
      in
      let simplifiedCodeSize, simplifiedJSon =
        let simplifiedCode =
          Michelson.to_micheline_tcontract ~protocol simplified_contract
        in
        ( Micheline_encoding.micheline_size_opt simplifiedCode
        , Format.asprintf "%a" (Micheline.pp_as_json ()) simplifiedCode )
      in
      let codeSize, initialJSon =
        let code = Michelson.to_micheline_tcontract ~protocol raw_contract in
        ( Micheline_encoding.micheline_size_opt code
        , Format.asprintf "%a" (Micheline.pp_as_json ()) code )
      in
      let sizes =
        Html.contract_sizes_html ~codeSize
          ~simplifiedCodeSize:
            (if initialJSon <> simplifiedJSon then simplifiedCodeSize else None)
          ~storageSize:None ~nb_bigmaps:0
      in
      let examples =
        let parameter_examples =
          Michelson.mtype_examples (fst raw_contract.tparameter)
        in
        let parameter_examples_m =
          List.map (Michelson.string_of_literal ~protocol) parameter_examples
        in
        let parameter_examples_j =
          List.map
            (fun parameter ->
              Format.asprintf "%a" (Micheline.pp_as_json ())
                (Michelson.To_micheline.literal ~protocol parameter))
            parameter_examples
        in
        let storage_examples = Michelson.mtype_examples raw_contract.tstorage in
        let storage_examples_m =
          List.map (Michelson.string_of_literal ~protocol) storage_examples
        in
        let storage_examples_j =
          List.map
            (fun storage ->
              Format.asprintf "%a" (Micheline.pp_as_json ())
                (Michelson.To_micheline.literal ~protocol storage))
            storage_examples
        in
        Html.div
          [
            Html.div
              [
                Html.Raw "<h2>Parameters</h2>"
              ; Html.copy_div ~id:"parameter_examples_" "parameter_examples"
                  (Html.Raw (String.concat "\n\n==\n\n" parameter_examples_m))
              ]
          ; Html.div
              [
                Html.Raw "<br><h2>Storage</h2>"
              ; Html.copy_div ~id:"storage_examples_" "storage_examples"
                  (Html.Raw (String.concat "\n\n==\n\n" storage_examples_m))
              ]
          ; Html.div
              [
                Html.Raw "<br><h2>Parameters JSON</h2>"
              ; Html.copy_div ~id:"parameter_examples_JSON" "parameter_examples"
                  (Html.Raw (String.concat "\n\n==\n\n" parameter_examples_j))
              ]
          ; Html.div
              [
                Html.Raw "<br><h2>Storage JSON</h2>"
              ; Html.copy_div ~id:"storage_examples_JSON" "storage_examples"
                  (Html.div
                     (List.fold_left
                        (fun acc example ->
                          (if List.length acc mod 2 == 1
                          then acc @ [Html.Raw "\n==\n\n"]
                          else acc)
                          @ [
                              Html.div
                                ~args:
                                  (Printf.sprintf
                                     "id='simplified_init_storage_json_%d'"
                                     (List.length acc))
                                [Html.Raw example]
                            ])
                        [] storage_examples_j))
              ]
          ]
      in
      (match first_message with
      | Some msg -> SmartDom.setOutputToMethod "setFirstMessage" msg
      | None -> ());
      let types_output_div =
        Html.copy_div ~id:"_types_output" "michelson_view"
          (Html.Raw (Michelson.render_tcontract ~protocol raw_contract))
      in
      let simplified_types_output_div =
        Html.copy_div ~id:"_simplified_types_output" "michelson_view"
          (Html.Raw (Michelson.render_tcontract ~protocol simplified_contract))
      in
      let simplified_no_types_output_div =
        Html.copy_div ~id:"_simplified_no_types_output" "michelson_view"
          (Html.Raw
             (Michelson.render_tcontract_no_types ~protocol simplified_contract))
      in
      let raw_no_types_output_div =
        Html.copy_div ~id:"_raw_no_types_output" "michelson_view"
          (Html.Raw (Michelson.render_tcontract_no_types ~protocol raw_contract))
      in
      let json_output_div =
        Html.copy_div ~id:"_json_output" "michelson_view"
          (Html.Raw
             (Format.asprintf "<div class='white-space-pre'>%a</div>"
                (Micheline.pp_as_json ())
                (Michelson.to_micheline_tcontract ~protocol simplified_contract)))
      in
      let json_raw_output_div =
        Html.copy_div ~id:"_json_raw_output" "michelson_view"
          (Html.Raw
             (Format.asprintf "<div class='white-space-pre'>%a</div>"
                (Micheline.pp_as_json ())
                (Michelson.to_micheline_tcontract ~protocol raw_contract)))
      in
      let module Printer = (val Printer.get ~config : Printer.Printer) in
      SmartDom.setOutput
        (michelson_view' sizes examples types_output_div
           simplified_types_output_div simplified_no_types_output_div
           raw_no_types_output_div json_output_div json_raw_output_div)

module type INTERFACE = sig
  open Js

  val importType : exportToJs -> js_string t -> Type.t

  val compileContractStorage : Basics.instance -> string

  val compileContract : Basics.instance -> Michelson.tcontract

  val update_michelson_view : exportToJs -> js_string t -> js_string t -> unit

  val buildTransfer : exportToJs -> js_string t -> Type.t -> unit

  val stringOfException : exportToJs -> bool -> exn -> js_string t

  val js_string : 'a -> 'a

  val explore : exportToJs -> js_string t -> js_string t -> js_string t -> unit

  val parseStorage : exportToJs -> js_string t -> string

  val exploreOperations :
       exportToJs
    -> Js.js_string Js.t
    -> Js.js_string Js.t
    -> Js.js_string Js.t
    -> unit

  val runScenarioInBrowser : exportToJs -> js_string t -> unit

  val lazy_tab : exportToJs -> int -> int -> js_string t
end

module Interface : INTERFACE = struct
  let config = Config.default

  let protocol = config.protocol

  let importType ctx s =
    Import.import_type (Import.init_env ())
      (Parsexp.Single.parse_string_exn
         (Base.String.substr_replace_all (ctx.js_to_string s) ~pattern:"***"
            ~with_:"\""))

  let compileContractStorage contract =
    let contract = Value.typecheck_instance contract in
    Option.cata "missing storage"
      (Michelson.string_of_tliteral ~protocol)
      (Compiler.compile_instance ~scenario_vars:TBoundMap.empty contract)
        .storage

  let compileContract c =
    let c = Value.typecheck_instance c in
    Compiler.compile_instance ~scenario_vars:TBoundMap.empty c

  let update_michelson_view ctx code protocol =
    let protocol = ctx.js_to_string protocol in
    let config = {config with protocol = Config.protocol_of_string protocol} in
    let micheline = Micheline_encoding.parse_node (ctx.js_to_string code) in
    update_michelson_view ~config micheline

  let buildTransfer ctx s t = buildTransfer ~config (ctx.js_to_string s) t

  let stringOfException ctx html exc =
    let module Printer = (val Printer.get ~config : Printer.Printer) in
    ctx.string_to_js (Printer.exception_to_string html exc)

  let js_string s = s

  let explore ctx address json network =
    Explorer.explore ~config ~address:(ctx.js_to_string address)
      ~json:(ctx.js_to_string json) ~network:(ctx.js_to_string network)

  let parseStorage ctx json =
    Explorer.parseStorage ~config ~json:(ctx.js_to_string json)

  let exploreOperations ctx address json operations =
    Explorer.exploreOperations ~config ~address:(ctx.js_to_string address)
      ~json:(ctx.js_to_string json)
      ~operations:(ctx.js_to_string operations)

  let runScenarioInBrowser ctx scenario =
    let module Printer = (val Printer.get ~config : Printer.Printer) in
    let primitives = Lazy.force js_primitives in
    let scenario = ctx.js_to_string scenario in
    let scenario =
      try
        Import.load_scenario ~primitives config
          (Yojson.Safe.from_string scenario)
      with exn -> failwith (Printer.exception_to_string true exn)
    in
    let errors =
      Simulator.run ~config ~primitives ~browser:true ~output_dir:None
        ~all_scenarios:[] ~scenario
    in
    match errors with
    | [] -> ()
    | l ->
        raise
          (SmartExcept
             [
               `Text "Error in Scenario"
             ; `Br
             ; `Rec (List.concat (List.map (fun (_, l) -> [`Rec l; `Br]) l))
             ])

  let lazy_tab ctx id global_id =
    ctx.string_to_js (Html.render (Html.call_tab id global_id))
end

let interface (ctx : exportToJs) =
  SmartDom.getTextRef := ctx.getText;
  SmartDom.setTextRef := ctx.setText;
  SmartDom.setValueRef := ctx.setValue;
  SmartDom.setOutputRef := ctx.setOutput;
  SmartDom.setExplorerOutputRef := ctx.setExplorerOutput;
  SmartDom.addOutputRef := ctx.addOutput;
  SmartDom.setOutputToMethodRef := ctx.setOutputToMethod;
  SmartDom.isCheckedRef := ctx.isChecked;
  SmartDom.parseDateRef := ctx.parseDate;
  let open Interface in
  ctx.exportToJs "importType" (importType ctx);
  ctx.exportToJsString "compileContractStorage" compileContractStorage;
  ctx.exportToJs "compileContract" compileContract;
  ctx.exportToJs "update_michelson_view" (update_michelson_view ctx);
  ctx.exportToJs "buildTransfer" (buildTransfer ctx);
  ctx.exportToJs "stringOfException" (stringOfException ctx);
  ctx.exportToJsString "js_string" js_string;
  ctx.exportToJs "explore" (explore ctx);
  ctx.exportToJs "parseStorage" (parseStorage ctx);
  ctx.exportToJs "exploreOperations" (exploreOperations ctx);
  ctx.exportToJs "runScenarioInBrowser" (runScenarioInBrowser ctx);
  ctx.exportToJs "lazy_tab" (lazy_tab ctx)
