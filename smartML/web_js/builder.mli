(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Core

val buildEntrypointCallJSON :
  entrypoint:string -> compiled_parameter:Michelson.literal -> string

val buildTransferParametersJSON : compiled_parameter:Michelson.literal -> string

val buildTransferParametersMicheline :
  compiled_parameter:Michelson.literal -> string

val buildTransferParametersCLI :
     entrypoint:string
  -> account:string
  -> destination:string
  -> compiled_parameter:Michelson.literal
  -> string
