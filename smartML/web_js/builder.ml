(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Core

let protocol = Core.Config.default.protocol

let buildEntrypointCallJSON ~entrypoint ~compiled_parameter =
  Yojson.Safe.to_string
    (`Assoc
      [
        ("entrypoint", `String entrypoint)
      ; ( "value"
        , Utils.Misc.json_to_json
            (Micheline.to_json
               (Michelson.To_micheline.literal ~protocol compiled_parameter)) )
      ])

let buildTransferParametersJSON ~compiled_parameter =
  Format.asprintf "%a" (Micheline.pp_as_json ())
    (Michelson.To_micheline.literal ~protocol compiled_parameter)

let buildTransferParametersMicheline ~compiled_parameter : string =
  Michelson.string_of_literal ~protocol compiled_parameter

let buildTransferParametersCLI ~entrypoint ~account ~destination
    ~compiled_parameter : string =
  Printf.sprintf
    "octez-client transfer 0 from %s to %s --entrypoint %s --arg '%s'" account
    destination entrypoint
    (buildTransferParametersMicheline ~compiled_parameter)
