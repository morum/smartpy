type protocol =
  | Kathmandu
  | Lima
  | Mumbai
[@@deriving eq, ord, show {with_path = false}, sexp]
