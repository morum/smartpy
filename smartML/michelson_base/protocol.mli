type protocol =
  | Kathmandu
  | Lima
  | Mumbai
[@@deriving eq, ord, show, sexp]
