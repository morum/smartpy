(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

module Blake2b_Hash_32 = Digestif.Make_BLAKE2B (struct
  let digest_size = 32
end)

module Timelock = struct
  type opening_result =
    | Correct of string
    | Bogus_cipher
    | Bogus_opening

  open Tezos_crypto

  let open_chest chest chest_key time =
    let chest =
      Data_encoding.Binary.of_bytes_exn Timelock.chest_encoding
        (Hex.to_bytes (`Hex chest))
    in
    let chest_key =
      Data_encoding.Binary.of_bytes_exn Timelock.chest_key_encoding
        (Hex.to_bytes (`Hex chest_key))
    in
    match Tezos_crypto.Timelock.open_chest chest chest_key ~time with
    | Correct s -> Correct Hex.(show (of_bytes s))
    | Bogus_cipher -> Bogus_cipher
    | Bogus_opening -> Bogus_opening
end

module Bls12 = struct
  open Bls12_381

  let g1_of_string x = G1.of_bytes_exn (Hex.to_bytes (`Hex x))

  let g1_to_string x = Hex.show (Hex.of_bytes (Bls12_381.G1.to_bytes x))

  let g2_of_string x = G2.of_bytes_exn (Hex.to_bytes (`Hex x))

  let g2_to_string x = Hex.show (Hex.of_bytes (Bls12_381.G2.to_bytes x))

  let fr_of_string x = Fr.of_bytes_exn (Hex.to_bytes (`Hex x))

  let fr_to_string x = Hex.show (Hex.of_bytes (Bls12_381.Fr.to_bytes x))

  let negateG1 x = g1_to_string (G1.negate (g1_of_string x))

  let addG1 x y = g1_to_string (G1.add (g1_of_string x) (g1_of_string y))

  let multiplyG1ByFr x y =
    g1_to_string (G1.mul (g1_of_string x) (fr_of_string y))

  let negateG2 x = g2_to_string (G2.negate (g2_of_string x))

  let addG2 x y = g2_to_string (G2.add (g2_of_string x) (g2_of_string y))

  let multiplyG2ByFr x y =
    g2_to_string (G2.mul (g2_of_string x) (fr_of_string y))

  let negateFr x = fr_to_string (Fr.negate (fr_of_string x))

  let addFr x y = fr_to_string (Fr.add (fr_of_string x) (fr_of_string y))

  let multiplyFrByFr x y =
    fr_to_string (Fr.mul (fr_of_string x) (fr_of_string y))

  let multiplyFrByInt x y =
    let y = Z.of_string y in
    fr_to_string (Fr.mul (fr_of_string x) (Fr.of_z y))

  let convertFrToInt x =
    let r = Fr.to_z (fr_of_string x) in
    Z.to_string r

  let pairingCheck xs =
    let xs = List.map (fun (x, y) -> (g1_of_string x, g2_of_string y)) xs in
    Pairing.pairing_check xs
end

module Crypto = struct
  open Tezos_crypto

  let account_of_seed seed =
    let hash = Digestif.SHA256.(digest_string seed |> to_raw_string) in
    let hash = Hex.(show (of_string hash)) in
    let seed = Bytes.of_string (String.sub hash 0 32) in
    let public_key_hash, public_key, secret_key =
      (* NB The secret key is 54 characters long. *)
      Ed25519.generate_key ~seed ()
    in
    let pk = Ed25519.Public_key.to_b58check public_key in
    let pkh = Ed25519.Public_key_hash.to_b58check public_key_hash in
    let sk = Ed25519.Secret_key.to_b58check secret_key in
    Core.Primitives.{pk; pkh; sk}

  let sign ~secret_key message =
    let secret_key = Ed25519.Secret_key.of_b58check_exn secret_key in
    Ed25519.(to_b58check (sign secret_key (Bytes.of_string message)))

  let hash_key x =
    let x = Ed25519.Public_key.of_b58check_exn x in
    let x = Ed25519.Public_key.hash x in
    Ed25519.Public_key_hash.to_b58check x

  let check_signature ~public_key ~signature message =
    let signature = Ed25519.of_b58check_exn signature in
    let public_key = Ed25519.Public_key.of_b58check_exn public_key in
    Ed25519.check public_key signature (Bytes.of_string message)
end
