(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Basics

type var = {
    typ : Type.t
  ; index : int
  ; occs : int (* TODO Use this to warn of unused definitions. *)
  ; derived : Checked.expr derived_var
}

type definition =
  | Def_variable of var
  | Def_module of Checked.module_
  | Def_type of Type.t
  | Def_contract of Checked.contract_def

val check_command :
     Config.t
  -> (Unchecked.bound * Type.t) list
  -> Unchecked.command
  -> Checked.command
(** Typechecks the given command in the given environment. *)

val check_expr :
  Config.t -> (Unchecked.bound * Type.t) list -> Unchecked.expr -> Checked.expr

val check_lambda :
     Config.t
  -> (Unchecked.bound * Type.t) list
  -> Unchecked.lambda
  -> Checked.lambda

val check_contract_def :
  Config.t -> Unchecked.contract_def -> Checked.contract_def

val check_action :
     Config.t
  -> definition Utils.String.Map.t
  -> Unchecked.action
  -> Checked.action * definition Utils.String.Map.t

val check_scenario :
     Config.t
  -> Unchecked.scenario
  -> Substitution.t * Checked.scenario * smart_except list list
