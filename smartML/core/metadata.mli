(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Basics

val for_contract :
     config:Config.t
  -> 'v instance_f
  -> Michelson.tcontract
  -> (string * Utils.Misc.json) list
