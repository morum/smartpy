(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Basics

type context = {
    primitives : (module Primitives.Primitives)
  ; browser : bool
  ; buffer : Buffer.t
  ; output_dir : string option
  ; out : out_channel option
  ; errors : ([ `Error | `Warning ] * smart_except list) list ref
  ; actions : (Config.t * Checked.action) list
  ; run_compiler : bool
}

val run_action :
     all_scenarios:Import.loaded_scenario list
  -> context
  -> Interpreter.scenario_state ref
  -> Config.t * Checked.action
  -> unit

val run :
     config:Config.t
  -> primitives:(module Primitives.Primitives)
  -> browser:bool
  -> output_dir:string option
  -> all_scenarios:Import.loaded_scenario list
  -> scenario:Import.loaded_scenario
  -> ([ `Warning | `Error ] * smart_except list) list
