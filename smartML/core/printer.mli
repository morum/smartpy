(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Basics
open Unchecked

(** {1 Target Printer Options} *)

module type Printer = sig
  module Options : sig
    type t = private {
        html : bool
      ; stripStrings : bool
      ; types : bool
      ; show_lambda_defs : bool
    }

    val string : t

    val stringNoLambdas : t

    val html : t

    val htmlStripStrings : t

    val types : t
  end

  (** {1 Types} *)

  val type_to_string : ?options:Options.t -> Type.t -> string

  val value_to_json : Basics.tvalue -> Yojson.Safe.t

  val type_to_json : Type.t -> Yojson.Safe.t

  val value_to_type_value_json : Basics.tvalue -> Yojson.Safe.t

  (** {1 Values} *)

  val render_storage_tvalue : Options.t -> tvalue -> string

  val tvalue_to_string :
    ?options:Options.t -> ?deep:bool -> ?noEmptyList:bool -> tvalue -> string

  val value_to_string : value -> string

  val variant_to_string :
       ?options:Options.t
    -> ?deep:bool
    -> ?noEmptyList:bool
    -> string
    -> tvalue
    -> string

  val literal_to_sp_string :
    html:bool -> ?protect:unit -> ?strip_strings:unit -> Literal.t -> string

  val literal_to_string :
    options:Options.t -> ?deep:bool -> Basics.Literal.t -> string

  val ppAmount : bool -> Utils.Bigint.t -> string

  (** {1 Expressions, Commands, Contracts} *)

  val layout_to_string : Layout.t Hole.t -> string

  val mprim0_to_string :
    Michelson.mtype Michelson_base.Primitive.prim0 -> string

  val mprim1_to_string :
    Michelson.mtype Michelson_base.Primitive.prim1 -> string

  val mprim2_to_string :
    Michelson.mtype Michelson_base.Primitive.prim2 -> string

  val mprim3_to_string : Michelson_base.Primitive.prim3 -> string

  val expr_to_string : ?options:Options.t -> ?protect:unit -> expr -> string

  val texpr_to_string :
    ?options:Options.t -> ?protect:unit -> Checked.expr -> string

  val command_to_string :
    ?options:Options.t -> ?indent:string -> command -> string

  val tcommand_to_string :
    ?options:Options.t -> ?indent:string -> Checked.command -> string

  val binding_to_string :
    options:Options.t -> ?indent:string -> expr * expr -> string

  val tbinding_to_string :
    options:Options.t -> ?indent:string -> Checked.expr * Checked.expr -> string

  val render_record_list :
    string list * 'a list list -> ('a -> string) -> string

  (** {1 Operations} *)

  val operation_to_string : ?options:Options.t -> toperation -> string

  (** {1 Exceptions} *)

  val wrong_condition_string : Checked.expr -> string

  val error_to_string : Execution.error -> string

  val exception_to_smart_except : exn -> Basics.smart_except list

  val exception_to_string : bool -> exn -> string

  val pp_smart_except : bool -> Basics.smart_except list -> string

  val string_of_contract_id : contract_id -> string

  val qual_ident : qual_ident -> string
end

val get : config:Config.t -> (module Printer)
