(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Writer
open Utils
open Control
open Basics
open Checked
open Printf
open Interpreter

type context = {
    primitives : (module Primitives.Primitives)
  ; browser : bool
  ; buffer : Buffer.t
  ; output_dir : string option
  ; out : out_channel option
  ; errors : ([ `Error | `Warning ] * smart_except list) list ref
  ; actions : (Config.t * Checked.action) list
  ; run_compiler : bool
}

type delayed_operation = {
    id : contract_id
  ; line_no : line_no
  ; source : string option
  ; sender : string option
  ; op : toperation
}

let noop _ = ()

let appendHtml {buffer} s = bprintf buffer "%s\n" s

let appendLogs {out} =
  let k s =
    match out with
    | None -> ()
    | Some h -> fprintf h "%s\n%!" s
  in
  ksprintf k

let appendError ctxt severity msg_out msg_full =
  ctxt.errors := (severity, msg_full) :: !(ctxt.errors);
  appendLogs ctxt "%s" msg_out

let closeOut ctxt =
  if ctxt.browser
  then fun () -> SmartDom.setOutput (Buffer.contents ctxt.buffer)
  else (*fun () -> close_out h*) noop

let with_file ~config ~id ctxt step name w x =
  let module Printer = (val Printer.get ~config : Printer.Printer) in
  match ctxt.output_dir with
  | None -> ()
  | Some od ->
      let name =
        sprintf "%s/step_%03d_cont_%s_%s" od step
          (Printer.string_of_contract_id id)
          name
      in
      let name, l = w name x in
      appendLogs ctxt " => %s %d" name l

let update_constants scenario_state hash constant =
  Hashtbl.replace scenario_state.constants hash constant

let parse_address ~config ~primitives ~scenario_state = function
  | Account x -> x.pkh
  | Address (address : Basics.Checked.expr) ->
      let pp () =
        [`Text "Computing address"; `Expr address; `Line address.line_no]
      in
      Value.unAddress ~pp
        (interpret_expr_external ~config ~primitives ~scenario_state address)

let create_instance_compilation ctxt scenario_state output_in step ~id ~line_no
    ~instance ~show =
  let derived = instance.template.derived in
  let config = derived.config in
  let protocol = config.Config.protocol in
  let module Printer = (val Printer.get ~config : Printer.Printer) in
  let _appendHtml = appendHtml ctxt in
  let appendLogs = appendLogs ctxt in
  let appendError = appendError ctxt in
  let with_file name = with_file ~id ctxt step name in
  let scenario_vars = TBoundMap.of_hashtbl scenario_state.variables in
  let compiled_contract = Compiler.compile_instance ~scenario_vars instance in
  let compiled_contract =
    if config.simplify_via_michel
    then (
      let michel, michelson =
        Compiler.simplify_via_michel ~config compiled_contract
      in
      if config.dump_michel
      then with_file "pre_michelson" ~config write_contract_michel michel;
      michelson)
    else compiled_contract
  in
  appendLogs " -> %s"
    (Option.cata "missing storage"
       (Michelson.string_of_tliteral ~protocol)
       compiled_contract.storage);
  (if ctxt.browser && show
  then
    let def = "SmartPy" in
    Html.full_html ~config ~contract:instance ~compiled_contract ~def
      ~onlyDefault:false
      ~id:(sprintf "%s_%d" (Printer.string_of_contract_id id) step)
      ~line_no ~accept_missings:false
    |> Html.render
    |> fun x -> output_in := x :: !output_in);
  let contract_micheline =
    Michelson.to_micheline_tcontract ~protocol compiled_contract
  in
  let storage_with_micheline =
    Option.map
      (fun x ->
        ( x
        , Michelson.(
            To_micheline.literal ~protocol (Michelson.erase_types_literal x)) ))
      compiled_contract.storage
  in
  if Option.is_some ctxt.output_dir
  then begin
    begin
      match storage_with_micheline with
      | Some (storage, storage_micheline) ->
          let name = "storage" in
          with_file name (write_mliteral ~protocol) ~config
            (Michelson.erase_types_literal storage);
          with_file name write_micheline ~config storage_micheline
      | None -> ()
    end;
    let sizes =
      [
        ("storage", Option.map snd storage_with_micheline)
      ; ("contract", Some contract_micheline)
      ]
    in
    let get_size (name, x) =
      try
        [
          name
        ; Option.fold ~none:"missing"
            ~some:(fun x ->
              Printf.sprintf "%i" (Micheline_encoding.micheline_size x))
            x
        ]
      with Failure s -> [name; s]
    in
    let name = "sizes" in
    with_file ~config name write_csv (List.map get_size sizes);
    (match instance.state.storage with
    | None -> ()
    | Some storage ->
        let name = "storage" in
        with_file ~config name (write_tvalue ~config) storage);
    let name = "types" in
    let instance = erase_types_instance instance in
    with_file ~config name (write_contract_types ~config) instance;
    let write_metadata_file (name, metadata) =
      with_file ~config ("metadata." ^ name) write_metadata metadata
    in
    List.iter write_metadata_file
      (Metadata.for_contract ~config instance compiled_contract);
    begin
      match
        Michelson.has_error_tcontract ~accept_missings:false compiled_contract
      with
      | [] -> ()
      | l ->
          let errors = String.concat " " (List.take 5 l) in
          appendError `Warning
            ("Error in generated contract: " ^ errors)
            [`Text ("Error in generated Michelson contract: " ^ errors)]
    end;
    (match instance.template.derived.unknown_parts with
    | Some msg ->
        ksprintf (appendError `Warning)
          "Warning: unknown types or type errors: %s" msg
          [`Text "Error (unknown) in generated Michelson contract"]
    | _ -> ());
    let name = "contract" in
    with_file ~config name
      (write_contract_michelson ~protocol)
      compiled_contract;
    with_file ~config name write_micheline contract_micheline
  end

let create_instance_old ?address ctxt scenario_state output_in step ~id ~line_no
    (template : Checked.contract_def) ~balance ~storage ~baker ~show =
  let derived = template.derived in
  let config = derived.config in
  let module Printer = (val Printer.get ~config : Printer.Printer) in
  let _appendHtml = appendHtml ctxt in
  let appendLogs = appendLogs ctxt in
  let state = {balance; storage; private_ = None; baker; metadata = []} in
  let instance = {template; state} in
  Interpreter.update_contract ?address scenario_state id
    (erase_types_instance instance);
  let instance = erase_types_instance instance in
  let instance = Value.typecheck_instance instance in
  appendLogs "Creating contract %s" (get_contract_address scenario_state id);
  if ctxt.run_compiler
  then
    create_instance_compilation ctxt scenario_state output_in step ~id ~line_no
      ~instance ~show

let create_instance_new ?address ({primitives} as ctxt) scenario_state output_in
    step ~id ~line_no ~instance ~show args =
  let derived = instance.template.derived in
  let config = derived.config in
  let module Printer = (val Printer.get ~config : Printer.Printer) in
  let _appendHtml = appendHtml ctxt in
  let appendLogs = appendLogs ctxt in
  Interpreter.update_contract ?address scenario_state id
    (erase_types_instance instance);
  Interpreter.exec_init ~config ~primitives ~scenario_state id args;
  let instance =
    match Hashtbl.find_opt scenario_state.contracts id with
    | Some i -> i
    | None -> assert false
  in
  let instance = Value.typecheck_instance instance in
  appendLogs "Creating contract %s" (get_contract_address scenario_state id);
  if ctxt.run_compiler
  then
    create_instance_compilation ctxt scenario_state output_in step ~id ~line_no
      ~instance ~show

let handle_set_delegate ~config scenario_state ~id ~line_no ~baker =
  let module Printer = (val Printer.get ~config : Printer.Printer) in
  let {template; state} =
    match Hashtbl.find_opt scenario_state.contracts id with
    | None ->
        raise
          (SmartExcept
             [
               `Text
                 (sprintf "Missing contract in scenario %s"
                    (Printer.string_of_contract_id id))
             ; `Line line_no
             ])
    | Some contract -> contract
  in
  update_contract scenario_state id {template; state = {state with baker}}

let handle_message ~config ~queue ~reverted ({primitives} as ctxt) output_in
    scenario_state step ~id ~(params : tvalue) ~params_expr ~line_no ~title
    ~sender ~source ~amount ~message ~show ~export =
  let protocol = config.Config.protocol in
  let _appendHtml = appendHtml ctxt in
  let appendLogs fmt = appendLogs ctxt fmt in
  let _appendError = appendError ctxt in
  let with_file = with_file ctxt step in
  let module Printer = (val Printer.get ~config : Printer.Printer) in
  let result =
    interpret_message_inner ~config ~primitives ~scenario_state ~title
      ~context:{context0 with sender; source; amount; line_no}
      ~channel:message ~id ~params ~line_no
  in
  if export
  then begin
    let name = "params" in
    with_file ~config ~id name (write_tvalue ~config) params;
    let scenario_vars = TBoundMap.of_hashtbl scenario_state.variables in
    let params = Compiler.compile_value ~config ~scenario_vars params in
    with_file ~config ~id name (write_mliteral ~protocol) params;
    let params = Michelson.To_micheline.literal ~protocol params in
    with_file ~config ~id name write_micheline params
  end;
  (match params_expr with
  | Some params_expr ->
      appendLogs "Executing %s(%s)..." message
        (Printer.texpr_to_string params_expr)
  | None ->
      appendLogs "Executing (queue) %s(%s)..." message
        (Printer.tvalue_to_string params));
  if show then output_in := result.html :: !output_in;
  match result.error with
  | None ->
      let contract = Option.of_some ~msg:"No contract" result.contract in
      update_contract scenario_state id contract;
      let contract = Value.typecheck_instance contract in
      let storage =
        let scenario_vars = TBoundMap.of_hashtbl scenario_state.variables in
        Option.cata "missing storage"
          (fun x ->
            Michelson.string_of_literal ~protocol
              (Compiler.compile_value ~config ~scenario_vars x))
          contract.state.storage
      in
      appendLogs " -> %s" storage;
      let sender = Some (get_contract_address scenario_state id) in
      let f op : delayed_operation =
        appendLogs "  + %s" (Printer.operation_to_string op);
        {id; line_no; source; sender; op}
      in
      let todo = List.map f result.operations in
      queue := todo @ !queue
  | Some error -> reverted := Some error

let update_context ~config ~primitives ~scenario_state
    ({sender; source; chain_id; time; level; voting_powers} :
      _ Basics.action_context) =
  let parse_chain_id (chain_id : Basics.Checked.expr) =
    let pp () =
      [`Text "Computing chain_id"; `Expr chain_id; `Line chain_id.line_no]
    in
    let chain_id =
      Value.unChain_id ~pp
        (interpret_expr_external ~config ~primitives ~scenario_state chain_id)
    in
    scenario_state.chain_id <- chain_id
  in
  let parse_time (time : Checked.expr) =
    let pp () = [`Text "Computing now"; `Expr time; `Line time.line_no] in
    let time =
      Value.unTimestamp ~pp
        (interpret_expr_external ~config ~primitives ~scenario_state time)
    in
    scenario_state.time <- time
  in
  let parse_level (level : Checked.expr) =
    let pp () = [`Text "Computing level"; `Expr level; `Line level.line_no] in
    let level =
      Value.unInt ~pp
        (interpret_expr_external ~config ~primitives ~scenario_state level)
    in
    scenario_state.level <- level
  in
  let parse_voting_powers (voting_powers : Checked.expr) =
    let pp () =
      [
        `Text "Computing voting powers"
      ; `Expr voting_powers
      ; `Line voting_powers.line_no
      ]
    in
    let voting_powers =
      List.map
        (fun (k, v) -> (Value.unKey_hash ~pp k, Value.unInt ~pp v))
        (Value.unMap ~pp
           (interpret_expr_external ~config ~primitives ~scenario_state
              voting_powers))
    in
    scenario_state.voting_powers <- voting_powers
  in
  Option.iter parse_chain_id chain_id;
  Option.iter parse_time time;
  Option.iter parse_level level;
  Option.iter parse_voting_powers voting_powers;
  let sender =
    Option.map (parse_address ~config ~primitives ~scenario_state) sender
  in
  let source =
    match
      Option.map (parse_address ~config ~primitives ~scenario_state) source
    with
    | None -> sender
    | source -> source
  in
  (source, sender)

let run_operation ~id ~line_no ~source ~sender ~queue ~reverted ~output_in
    ~config ctxt scenario_state step = function
  | Transfer {params; destination = {address; entrypoint}; amount} -> (
      let module Printer = (val Printer.get ~config : Printer.Printer) in
      let ok =
        (* Update sender's balance *)
        match sender with
        | None -> true
        | Some sender_address -> (
            match
              Hashtbl.find_opt scenario_state.rev_addresses sender_address
            with
            | None ->
                (* TODO - when we have balances on implicit accounts *) true
            | Some cid ->
                let c = Hashtbl.find scenario_state.contracts cid in
                let new_sender_balance =
                  Big_int.sub_big_int c.state.balance amount
                in
                if Big_int.compare_big_int new_sender_balance
                     Big_int.zero_big_int
                   < 0
                then begin
                  let message =
                    "Negative balance for "
                    ^ Aux.address_of_contract_id ~html:false cid None
                  in
                  reverted :=
                    Some
                      (Execution.Exec_failure
                         (Value.string message, [`Text message; `Line line_no]));
                  false
                end
                else begin
                  update_contract scenario_state cid
                    {c with state = {c.state with balance = new_sender_balance}};
                  true
                end)
      in
      if ok
      then
        match Hashtbl.find_opt scenario_state.rev_addresses address with
        | None -> ()
        | Some id ->
            let message = Option.default "default" entrypoint in
            let show_arg = Printer.tvalue_to_string params in
            let arg_shown =
              let the_max = 20 in
              if String.length show_arg > the_max
              then String.take the_max show_arg ^ "..."
              else show_arg
            in
            let title =
              sprintf "Follow-up-transfer: %s (%s)"
                (Aux.address_of_contract_id ~html:false id (Some message))
                arg_shown
            in
            let show = true in
            let export = false in
            handle_message ~config ~queue ~reverted ctxt output_in
              scenario_state step ~id ~params ~params_expr:None ~line_no ~title
              ~sender ~source ~amount ~message ~show ~export)
  | SetDelegate baker ->
      handle_set_delegate ~config scenario_state ~id ~line_no ~baker
  | CreateContract {id; template; balance; storage; baker} ->
      create_instance_old ctxt scenario_state output_in step ~id ~line_no
        template ~balance ~storage ~baker ~show:true ?address:None
  | Event _ -> (* TODO *) ()

let check_valid ({primitives} as ctxt) reverted scenario_state config action =
  let module Printer = (val Printer.get ~config : Printer.Printer) in
  let _appendHtml = appendHtml ctxt in
  let appendLogs = appendLogs ctxt in
  let appendError = appendError ctxt in
  let to_check =
    match action with
    | Message {message; valid; exception_; line_no} ->
        Some (Some valid, exception_, line_no, `Text message)
    | _ -> None
  in
  match to_check with
  | None -> `OK
  | Some (valid, (exception_ : Checked.expr option), line_no, message) -> (
      let exception_ =
        Option.map
          (fun exception_ ->
            ( interpret_expr_external ~config ~primitives ~scenario_state
                exception_
            , exception_.et ))
          exception_
      in
      let valid =
        match valid with
        | None -> false
        | Some valid ->
            let pp () = [`Text "Computing valid"; `Expr valid; `Line line_no] in
            Value.unBool ~pp
              (interpret_expr_external ~config ~primitives ~scenario_state valid)
      in
      match (reverted, valid, exception_) with
      | None, true, None -> `OK
      | None, true, Some (exception_, t) ->
          appendError `Error
            (sprintf
               " -> !!! Unexpected Exception declaration in valid operation !!!")
            [
              `Text
                "Unexpected Exception declaration in valid operation, please \
                 remove the exception declaration."
            ; `Br
            ; `Value (Value.typecheck t exception_)
            ; `Br
            ; `Line line_no
            ];
          `Error "Reverted transaction - unexpected error"
      | None, false, _ ->
          appendError `Error
            (sprintf " -> !!! Valid but expected ERROR !!!")
            [
              `Text "Expected error in transaction but valid."
            ; `Br
            ; message (*; `Expr params*)
            ; `Br
            ; `Line line_no
            ];
          `OK_but "Valid but expected ERROR"
      | Some error, true, _ ->
          appendError `Error
            (sprintf " -> !!! Unexpected ERROR !!! %s"
               (Printer.error_to_string error))
            [
              `Text
                "Unexpected error in transaction, please use .run(valid=False, \
                 ..)"
            ; `Br
            ; `Rec (Execution.to_smart_except error)
            ; `Br
            ; message (* ; `Expr params *)
            ; `Br
            ; `Line line_no
            ];
          `Error "Reverted transaction - unexpected error"
      | Some error, false, exception_ -> (
          match exception_ with
          | None ->
              appendLogs " -> --- Expected failure in transaction --- %s"
                (Printer.error_to_string error);
              `Error "Reverted transaction"
          | Some (exception_, t) ->
              let except =
                match error with
                | Exec_failure (except, _) -> except
              in
              if Printer.value_to_string except
                 = Printer.value_to_string exception_
              then begin
                appendLogs " -> --- Expected failure in transaction --- %s"
                  (Printer.error_to_string error);
                `Error "Reverted transaction"
              end
              else begin
                appendError `Error
                  (sprintf
                     " -> !!! Wrong or unsupported exception matching !!! \
                      Expected failure in transaction --- %s"
                     (Printer.error_to_string error))
                  [
                    `Text
                      "Wrong or unsupported exception matching (expected \
                       failure in transaction)"
                  ; `Br
                  ; `Rec (Execution.to_smart_except error)
                  ; `Br
                  ; `Text "Received:"
                  ; `UValue except
                  ; `Br
                  ; `Text "Expected:"
                  ; `Value (Value.typecheck t exception_)
                  ; `Br
                  ; `Line line_no
                  ];
                `Error
                  "Reverted transaction - wrong or unsupported exception \
                   matching"
              end))

let handle_action ~config ({primitives} as ctxt) scenario_state (step, action) =
  let module Printer = (val Printer.get ~config : Printer.Printer) in
  let protocol = config.Config.protocol in
  let appendHtml = appendHtml ctxt in
  let appendLogs fmt = appendLogs ctxt fmt in
  let appendError = appendError ctxt in
  let scenario_vars = TBoundMap.of_hashtbl scenario_state.variables in
  match action with
  | Add_module _ ->
      (* Module has already been typechecked. Nothing to do here. *) (None, [])
  | Instantiate_contract {id; line_no; derived} ->
      let {instantiate_derived_contract = contract; args} = derived in
      let {config} = contract.derived in
      let instance =
        Interpreter.instantiate_contract ~config ~primitives ~scenario_state
          contract
      in
      let instance = Value.typecheck_instance instance in
      let address = Aux.address_of_contract_id ~html:false id None in
      let args =
        Interpreter.interpret_exprs ~config ~primitives ~scenario_state
          Interpreter.context0 args
      in
      let output_in = ref [] in
      create_instance_new ctxt scenario_state output_in step ~id ~line_no
        ~instance ~show:true ~address args;
      (None, !output_in)
  | Compute {var; expression; context} ->
      let source, sender =
        update_context ~config ~primitives ~scenario_state context
      in
      let value =
        interpret_expr_external ~config ~primitives ?source ?sender
          ~scenario_state expression
      in
      Hashtbl.replace scenario_state.variables var
        (Value.typecheck expression.et value);
      (None, [])
  | Set_delegate {id; line_no; baker} ->
      let baker =
        Value.un_baker
          (interpret_expr_external ~config ~primitives ~scenario_state baker)
      in
      handle_set_delegate ~config scenario_state ~id ~line_no ~baker;
      (None, [])
  | Message {id; params; line_no; context; amount; message; show; export} ->
      let amount =
        let pp () =
          [`Text "Computing amount"; `Expr amount; `Line amount.line_no]
        in
        Value.unMutez ~pp
          (interpret_expr_external ~config ~primitives ~scenario_state amount)
      in
      let params_value =
        interpret_expr_external ~config ~primitives ~scenario_state params
      in
      let params_value = Value.typecheck params.et params_value in
      let source, sender =
        update_context ~config ~primitives ~scenario_state context
      in
      let queue = ref [] in
      let reverted = ref None in
      let output_in = ref [] in
      handle_message ~config ~queue ~reverted ctxt output_in scenario_state step
        ~id ~params:params_value ~params_expr:(Some params) ~line_no ~title:""
        ~sender ~source ~amount ~message ~show ~export;
      while
        match !queue with
        | [] -> false
        | {id; line_no; source; sender; op} :: rest ->
            queue := rest;
            run_operation ~id ~line_no ~source ~sender ~queue ~reverted
              ~output_in ~config ctxt scenario_state step op;
            true
      do
        ()
      done;
      (!reverted, !output_in)
  | ScenarioError {message} ->
      appendError `Error
        (sprintf " !!! Python Error: %s" message)
        [`Text "Python Error"; `Text message];
      (None, [])
  | Html {tag; inner} ->
      (match tag with
      | "h1" | "h2" | "h3" | "h4" ->
          appendHtml (sprintf "<span id='label%i'></span>" step)
      | _ -> ());
      appendLogs "Comment...";
      appendLogs " %s: %s" tag inner;
      appendHtml (sprintf "<%s>%s</%s>" tag inner tag);
      (None, [])
  | Verify {condition; line_no} ->
      appendLogs "Verifying %s..." (Printer.texpr_to_string condition);
      let value =
        interpret_expr_external ~config ~primitives ~scenario_state condition
      in
      let result = Value.bool_of_value value in
      if result
      then appendLogs " OK"
      else begin
        appendHtml
          (sprintf "Verification Error: <br>%s<br> is false."
             (Printer.texpr_to_string condition));
        appendError `Error " KO"
          [
            `Text "Verification Error"
          ; `Br
          ; `Expr condition
          ; `Br
          ; `Text "is false"
          ; `Line line_no
          ]
      end;
      (None, [])
  | Show {expression; html; stripStrings; compile} ->
      appendLogs "Computing %s..." (Printer.texpr_to_string expression);
      let value =
        interpret_expr_external ~config ~primitives ~scenario_state expression
      in
      let value = Value.typecheck expression.et value in
      let scenario_vars = TBoundMap.of_hashtbl scenario_state.variables in
      if compile
      then begin
        let name = "expression" in
        let expression = Compiler.compile_value ~config ~scenario_vars value in
        let expression_micheline =
          Michelson.To_micheline.literal ~protocol expression
        in
        let with_file name w x =
          match ctxt.output_dir with
          | None -> ()
          | Some od ->
              let name = sprintf "%s/step_%03d_%s" od step name in
              let name, l = w name x in
              appendLogs " => %s %d" name l
        in
        with_file name (write_tvalue ~config) value;
        with_file name (write_mliteral ~protocol) expression;
        with_file name write_micheline expression_micheline
      end;
      let result =
        Printer.tvalue_to_string ~options:Printer.Options.string value
      in
      let options =
        if html
        then
          if stripStrings
          then Printer.Options.htmlStripStrings
          else Printer.Options.html
        else Printer.Options.string
      in
      (match expression.e with
      | EVar id when id.derived = V_constant -> (
          match TBoundMap.find_opt id.name scenario_vars with
          | Some hash ->
              appendHtml
                (sprintf
                   "<div class='execMessage execMessageOK' \
                    style='margin-bottom: 1rem;'><h3>Constant: <span \
                    class='keyword'>%s</span><h3>Value: <div \
                    class='subtype'>%s</div></div>"
                   (Printer.tvalue_to_string ~options hash)
                   (Printer.tvalue_to_string ~options value))
          | None -> ())
      | _ ->
          appendHtml
            (sprintf "<div class='execMessage'>%s</div>"
               (Printer.tvalue_to_string ~options value)));
      appendLogs " => %s" result;
      (None, [])
  | DynamicContract {id; model_id; line_no} ->
      let lookup id =
        match Hashtbl.find_opt scenario_state.contracts id with
        | Some c -> (
            match c.template.derived.template_id with
            | None ->
                raise (SmartExcept [`Text "Missing template id"; `Line line_no])
            | Some cid -> cid.static_id)
        | None ->
            raise
              (SmartExcept
                 [
                   `Text "Contract id"
                 ; `Text (show_contract_id id)
                 ; `Text "not found"
                 ; `Line line_no
                 ])
      in
      let dynamic = lookup (C_dynamic id) in
      let model = lookup model_id in
      if dynamic <> model
      then
        raise
          (SmartExcept
             [
               `Text "Expected dynamic contract from template"
             ; `Text (string_of_int model)
             ; `Text ", got"
             ; `Text (string_of_int dynamic)
             ; `Line line_no
             ])
      else (None, [])
  | Add_flag _ -> (None, [] (* Handled in Scenario.acc_config. *))
  | Prepare_constant_value {var; hash; expression} ->
      let value =
        interpret_expr_external ~config ~primitives ~scenario_state expression
      in
      let hash =
        match hash with
        | Some hash -> hash
        | None -> (
            let primitives = primitives in
            let module P = (val primitives : Primitives.Primitives) in
            let interpreted_value =
              interpret_expr_external ~config ~primitives ~scenario_state
                expression
            in
            (*
          Steps:
          --------------
          1. Pack micheline value
          2. Chop the prefix that idenfies the value as packed ("0x05...")
          3. Hash the bytes with black2b (32 bytes)
          4. Encode the hash with base58 ("expr" prefixed)
        *)
            try
              interpreted_value
              |> Value.typecheck expression.et
              |> Compiler.pack_value ~config ~scenario_vars
              |> Base.String.chop_prefix_exn
                   ~prefix:Micheline_encoding.pack_prefix
              |> Utils.Crypto.blake2b |> Hex.of_string |> Hex.show
              |> Bs58.encode_expr
            with Data_encoding.Binary.Write_error write_error ->
              raise
                (ExecFailure
                   ( Value.string "Pack error when preparing constant value"
                   , [
                       `Text "Could not prepare constant due to invalid content"
                     ; `Br
                     ; `Expr expression
                     ; `Br
                     ; `Value (Value.typecheck expression.et interpreted_value)
                     ; `Text
                         (Format.asprintf "%a"
                            Data_encoding.Binary.pp_write_error write_error)
                     ; `Line expression.line_no
                     ] )))
      in
      (* Update table of constants *)
      update_constants scenario_state hash value;
      (* Register a variable that points
         to the generated hash *)
      Hashtbl.replace scenario_state.variables var (Value.Typed.string hash);
      (None, [])
  | Mutation_test _ -> assert false

let handle_action_result ~config ctxt reverted output_in state_ref new_state x =
  let appendHtml = appendHtml ctxt in
  let multiple = List.length output_in > 1 in
  let output_in = String.concat "\n" (List.rev output_in) in
  let pp msg =
    appendHtml
      (Printf.sprintf
         "<div style='border: double; padding: 5px; margin-bottom: \
          10px'>%s</br>%s</div>"
         msg output_in)
  in
  match check_valid ctxt reverted !state_ref config x with
  | `OK ->
      state_ref := new_state;
      if multiple then pp "Multiple operations" else appendHtml output_in
  | `OK_but msg ->
      state_ref := new_state;
      pp msg
  | `Error msg -> pp msg

let rec handle_action_general ~config ~all_scenarios ctxt scenario_state
    (step, action) =
  match action with
  | Mutation_test {scenarios; show_paths; line_no} ->
      mutation_test ~config ~all_scenarios ~ctxt ~line_no ~show_paths scenarios;
      (None, [])
  | _ -> handle_action ~config ctxt scenario_state (step, action)

and mutation_test ~config ~all_scenarios ~ctxt ~line_no ~show_paths scenarios =
  let module Printer = (val Printer.get ~config : Printer.Printer) in
  let scenarios =
    List.map
      (fun (name, cid) ->
        let f (s : Import.loaded_scenario) = s.scenario.longname = name in
        match List.find_opt f all_scenarios with
        | None -> failwith (sprintf "scenario %S not found" name)
        | Some s -> (s, cid))
      scenarios
  in
  let mutated_scenarios =
    (* List of mutations: each mutation has a list of scenarios. *)
    List.mapi pair
      (Mutator.mutate_loaded_scenarios ~config ~show_paths scenarios)
  in
  let run_scenario (s : Import.loaded_scenario) ~ctxt =
    let actions = s.scenario.actions in
    let ctxt = {ctxt with actions} in
    assert (!(ctxt.errors) = []);
    let errors = run ~config ~all_scenarios:[] ctxt s.scenario_state in
    ctxt.errors := [];
    appendLogs ctxt "%d warnings and errors" (List.length errors);
    if false
    then
      List.iteri
        (fun i (_, x) ->
          appendLogs ctxt "  Warning/error #%d: %s" i
            (Printer.pp_smart_except false x))
        errors;
    errors
  in
  (* Run the unmutated scenario first *)
  List.iter
    (fun (s, _) ->
      match run_scenario ~ctxt s with
      | (_, e) :: _ ->
          raise
            (SmartExcept
               ([`Text "Unmutated scenario failed."; `Line line_no; `Br] @ e))
      | [] -> ())
    scenarios;
  appendLogs ctxt "Mutating contract.";
  if !(ctxt.errors) <> []
  then
    raise
      (SmartExcept
         [
           `Text "Not running mutation test due to earlier errors."
         ; `Line line_no
         ]);
  let run_mutated_scenarios (i, (path, (c : Checked.contract_def), scenarios)) =
    let ctxt = {ctxt with output_dir = None; run_compiler = false} in
    appendLogs ctxt "  Running scenario on mutated contract #%d" i;
    let _c =
      let state =
        {
          balance = Bigint.of_int 0
        ; storage = None
        ; private_ = None
        ; baker = None
        ; metadata = []
        }
      in
      {template = c; state}
    in
    let success = List.for_all (fun s -> run_scenario ~ctxt s = []) scenarios in
    if success
    then
      raise
        (SmartExcept
           [
             `Text "A mutated contract passed all tests."
           ; `Line line_no
           ; `Br
           ; `Text ("Mutated path: " ^ path)
             (* ; `Br
                ; `Text ("Mutated contracted: " ^ Printer.tinstance_to_string c)*)
           ])
  in
  List.iter run_mutated_scenarios mutated_scenarios;
  appendLogs ctxt "Mutation testing successful."

and run_action ~all_scenarios ctxt global_state (config, x) =
  let step = !global_state.step in
  !global_state.step <- !global_state.step + 1;
  match !(ctxt.errors) with
  | _ :: _ -> ()
  | _ ->
      let new_state = copy_scenario_state !global_state in
      let reverted, output_in =
        handle_action_general ~config ~all_scenarios ctxt new_state (step, x)
      in
      handle_action_result ~config ctxt reverted output_in global_state
        new_state x

and run ~config ~all_scenarios ctxt initial_global_state =
  let module Printer = (val Printer.get ~config : Printer.Printer) in
  let global_state = ref initial_global_state in
  (try List.iter (run_action ~all_scenarios ctxt global_state) ctxt.actions
   with exn ->
     let except = Printer.exception_to_smart_except exn in
     let s = Printer.pp_smart_except ctxt.browser except in
     appendError ctxt `Error (" (Exception) " ^ s) except);
  closeOut ctxt ();
  List.rev !(ctxt.errors)

let run ~config ~primitives ~browser ~output_dir ~all_scenarios
    ~scenario:(s : Import.loaded_scenario) =
  let actions = s.scenario.actions in
  let context =
    {
      primitives
    ; browser
    ; buffer = Buffer.create 111024
    ; output_dir
    ; out =
        (match output_dir with
        | None -> None
        | Some od -> Some (open_out (Filename.concat od "log.txt")))
    ; errors = ref (List.map (fun x -> (`Warning, x)) s.warnings)
    ; actions
    ; run_compiler = true
    }
  in
  run ~config ~all_scenarios context s.scenario_state
