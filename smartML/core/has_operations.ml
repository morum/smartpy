(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Utils
open Basics
open Checked
open Control

(* TODO recognize ELambda with effects *)
type t =
  | HO_none
  | HO_at_most_one
  | HO_many
[@@deriving show {with_path = false}, eq, ord]

let or_ ho1 ho2 =
  match (ho1, ho2) with
  | HO_none, x | x, HO_none -> x
  | HO_at_most_one, x | x, HO_at_most_one -> x
  | _ -> HO_many

let add ho1 ho2 =
  match (ho1, ho2) with
  | HO_none, x | x, HO_none -> x
  | _ -> HO_many

let widen = function
  | HO_none -> HO_none
  | _ -> HO_many

let rec command c =
  match c.c with
  | CIf (_, x, y) -> or_ (command x) (command y)
  | CMatch (_, cases) ->
      List.fold_left
        (fun acc (_, _, body) -> or_ acc (command body))
        HO_none cases
  | CMatch_list {clause_cons; clause_nil} ->
      or_ (command clause_cons) (command clause_nil)
  | CExpr e -> expr e
  | CAssign
      {derived = Assign {e = EVar {name = {bound = "__operations__"}}}; rhs}
    -> (
      match rhs.e with
      | EList [] -> HO_none
      | EPrim2 (ECons, op, {e = EVar {name = {bound = "__operations__"}}}) ->
          add (expr op) HO_at_most_one
      | _ -> HO_many)
  | CAssign {rhs} -> expr rhs
  | CBlock cs -> List.fold_left add HO_none (List.map command cs)
  | CFor (_, e, c) | CWhile (e, c) -> add (expr e) (widen (command c))
  | ( CCall_init _
    | CModify_product _
    | CModify_record _
    | CSet_result_type _
    | CComment _
    | CSet_type _
    | CResult _
    | CUpdate_set _
    | CDel_item _
    | CNever _
    | CVerify _
    | CFailwith _
    | CTrace _ ) as c ->
      let c = map_command_f expr command id c in
      fold_command_f add add (fun x _ -> x) HO_none c

and expr e =
  let default () =
    let e = map_expr_f expr command id e.e in
    fold_expr_f add add (fun x _ -> x) HO_none e
  in
  match e.e with
  | ELambda _ -> HO_none
  | ECall ({et}, _) -> (
      match Type.unF et with
      | Lambda ({with_operations}, _, _) -> (
          match Hole.get with_operations with
          | Some false -> default ()
          | Some true -> HO_many
          | None -> assert false)
      | _ -> assert false)
  | ECreate_contract {push} when push -> add HO_at_most_one @@ default ()
  | _ -> default ()
