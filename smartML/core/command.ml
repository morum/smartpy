(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Basics
open Unchecked

type t = command

let build ~line_no c = {c; ct = U; line_no}

let ifte ~line_no c t e = build ~line_no (CIf (c, t, e))

let ifteSome ~line_no c t e =
  ifte ~line_no (Expr.is_variant ~line_no ~name:"Some" c) t e

let mk_match ~line_no scrutinee cases =
  let f (cons, bound, body) = (cons, mk_bound bound, body) in
  {c = CMatch (scrutinee, List.map f cases); ct = U; line_no}

let mk_match_cons ~line_no expr id clause_cons clause_nil =
  let id = mk_bound id in
  build ~line_no (CMatch_list {expr; id; clause_cons; clause_nil})

let mk_modify_product ~line_no s r p c =
  build ~line_no (CModify_product (s, r, p, c))

let mk_modify_record ~line_no s bound body =
  build ~line_no (CModify_record (s, mk_bound bound, body))

let sp_failwith ~line_no message = build ~line_no (CFailwith message)

let never ~line_no message = build ~line_no (CNever message)

let verify ~line_no e message = build ~line_no (CVerify (e, message))

let for_loop ~line_no x e c = {c = CFor (mk_bound x, e, c); ct = U; line_no}

let while_loop ~line_no e l = build ~line_no (CWhile (e, l))

let del_item ~line_no x y = build ~line_no (CDel_item (x, y))

let update_set ~line_no x y add = build ~line_no (CUpdate_set (x, y, add))

let block ~line_no = function
  | [c] -> c
  | xs -> build ~line_no (CBlock xs)

let result ~line_no x = build ~line_no (CResult x)

let comment ~line_no s = build ~line_no (CComment s)

let set_result_type ~line_no c t = build ~line_no (CSet_result_type (c, t))

let set_type ~line_no e t = build ~line_no (CSet_type (e, t))

let trace ~line_no x = build ~line_no (CTrace x)

let expr ~line_no e = build ~line_no @@ CExpr e

let assign ~line_no lhs (rhs : expr) =
  build ~line_no @@ CAssign {lhs; rhs; derived = U}
