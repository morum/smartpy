(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Utils
open Basics
open Checked

type scenario_state = {
    constants : (string, value) Hashtbl.t
  ; contracts : (contract_id, instance) Hashtbl.t
  ; variables : (Checked.bound, tvalue) Hashtbl.t
  ; addresses : (contract_id, string) Hashtbl.t
  ; rev_addresses : (string, contract_id) Hashtbl.t
  ; next_dynamic_address_id : int ref
  ; mutable step : int
  ; mutable time : Bigint.t
  ; mutable level : Bigint.t
  ; mutable chain_id : string
  ; mutable voting_powers : (string * Bigint.t) list
}

val scenario_state :
     ?time:Bigint.t
  -> ?level:Bigint.t
  -> chain_id:string
  -> ?voting_powers:(string * Big_int.big_int) list
  -> unit
  -> scenario_state

val copy_scenario_state : scenario_state -> scenario_state

val update_contract_address :
  ?address:string -> scenario_state -> contract_id -> unit

val update_contract :
  ?address:string -> scenario_state -> contract_id -> instance -> unit

val get_contract_address : scenario_state -> contract_id -> string

type in_contract

type context = {
    sender : string option
  ; source : string option
  ; amount : Bigint.t
  ; line_no : line_no
  ; contract_id : in_contract
  ; operations : Checked.bound option
  ; private_variables : (string * value) list
}

val context0 : context

val interpret_message :
     config:Config.t
  -> primitives:(module Primitives.Primitives)
  -> scenario_state:scenario_state
  -> context
  -> contract_id
  -> string * value
  -> tinstance option * toperation list * Execution.error option

val interpret_message_inner :
     config:Config.t
  -> primitives:(module Primitives.Primitives)
  -> scenario_state:scenario_state
  -> title:string
  -> context:context
  -> channel:string
  -> params:Basics.tvalue
  -> id:Ids.contract_id
  -> line_no:Basics.line_no
  -> string Basics.Execution.exec_message

val exec_init :
     config:Config.t
  -> primitives:(module Primitives.Primitives)
  -> scenario_state:scenario_state
  -> Ids.contract_id
  -> value list
  -> unit
(** Initializes a contract with zero storage and executes __init__ (if defined). *)

val interpret_expr :
     config:Config.t
  -> primitives:(module Primitives.Primitives)
  -> scenario_state:scenario_state
  -> context
  -> expr
  -> value

val interpret_exprs :
     config:Config.t
  -> primitives:(module Primitives.Primitives)
  -> scenario_state:scenario_state
  -> context
  -> expr list
  -> value list

val interpret_expr_external :
     config:Config.t
  -> primitives:(module Primitives.Primitives)
  -> scenario_state:scenario_state
  -> ?source:string
  -> ?sender:string
  -> expr
  -> value

val instantiate_contract :
     config:Config.t
  -> primitives:(module Primitives.Primitives)
  -> scenario_state:scenario_state
  -> Checked.contract_def
  -> instance
