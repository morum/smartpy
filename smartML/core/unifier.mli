(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Basics

exception Unification_failed of smart_except list

val equalize_holes :
     err:(unit -> smart_except list)
  -> eq:('a -> 'a -> bool)
  -> ('a Hole.t -> Substitution.substitute)
  -> Substitution.t
  -> 'a Hole.t
  -> 'a Hole.t
  -> unit

val unify :
     config:Config.t
  -> (unit -> smart_except list)
  -> Substitution.t
  -> Type.t
  -> Type.t
  -> typing_constraint list
(** throws Unification_failed *)
