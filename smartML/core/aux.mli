(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Basics

val address_of_contract_id : html:bool -> contract_id -> string option -> string
