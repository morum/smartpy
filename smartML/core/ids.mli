(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

type dynamic_id = {dynamic_id : int} [@@deriving eq, ord, show, sexp]

type static_id = {static_id : int} [@@deriving eq, ord, show, sexp]

type contract_id =
  | C_static of static_id
  | C_dynamic of dynamic_id
[@@deriving eq, ord, show, sexp]
