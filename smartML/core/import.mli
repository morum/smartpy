(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Basics
open Unchecked

type env

val init_env : unit -> env

val import_type : env -> Sexplib0.Sexp.t -> Type.t

val import_contract_id : Sexplib0.Sexp.t -> contract_id

val import_literal : Sexplib0.Sexp.t list -> Literal.t

val import_expr : env -> Sexplib0.Sexp.t -> expr

val import_command : env -> Sexplib0.Sexp.t -> command

val import_call : env -> Sexplib0.Sexp.t -> Sexplib0.Sexp.t -> call

val import_effects : Sexplib0.Sexp.t -> effects

type loaded_scenario = {
    scenario : Checked.scenario
  ; scenario_state : Interpreter.scenario_state
  ; warnings : smart_except list list
}

val action_of_json :
     primitives:(module Primitives.Primitives)
  -> env:env
  -> Yojson.Safe.t
  -> (Expr.t, Command.t, Type.t) Basics.Unchecked.action_f

val load_scenario :
     primitives:(module Primitives.Primitives)
  -> Config.t
  -> Yojson.Safe.t
  -> loaded_scenario
