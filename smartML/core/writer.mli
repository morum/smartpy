(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Basics

val write_contract_michelson :
  protocol:Config.protocol -> string -> Michelson.tcontract -> string * int

val write_micheline : string -> Micheline.t -> string * int

val write_contract_michel :
  string -> Michel.Expr.expr Michel.Expr.precontract -> string * int

val write_tvalue : config:Config.t -> string -> tvalue -> string * int

val write_mliteral :
  protocol:Config.protocol -> string -> Michelson.literal -> string * int

val pp_contract_types : config:Config.t -> instance -> string

val write_contract_types : config:Config.t -> string -> instance -> string * int

val write_metadata : string -> Utils.Misc.json -> string * int

val write_html : string -> string -> string * int

val write_csv : string -> string list list -> string * int

val wrap_html_document : install:string -> string -> string
