(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Basics
open Checked

type t

val empty : unit -> t

type substitute =
  | SType of Type.t
  | SRow of (Type.t Type.row * VarId.t option)
  | STupleRow of ((int * Type.t) list * VarId.t option)
  | SLayout of Layout.t Hole.t
  | SBool of bool Hole.t
  | SInt of int Hole.t
  | SWithStorage of (Type.access * Type.t) option Hole.t
[@@deriving show]

val dump : t -> unit

val on_hole : (substitute -> 'a Hole.t) -> t -> 'a Hole.t -> 'a Hole.t

val on_type : t -> Type.t -> Type.t

val on_open_row :
  t -> Type.t Type.row * VarId.t option -> Type.t Type.row * VarId.t option

val on_layout : t -> Layout.t Hole.t -> Layout.t Hole.t

val on_expr : t -> expr -> expr

val on_command : t -> command -> command

val on_lambda : t -> Checked.lambda -> Checked.lambda

val on_contract_def : t -> Checked.contract_def -> Checked.contract_def

val on_action : t -> Checked.action -> Checked.action

val on_scenario : t -> Checked.scenario -> Checked.scenario

val on_module : t -> Checked.module_ -> Checked.module_

val on_constraint : t -> typing_constraint -> typing_constraint

val find_opt : VarId.t -> t -> substitute option

val insert : (VarId.t * substitute) list -> t -> unit
