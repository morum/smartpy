(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

val getText : ?checked:unit -> string -> string

val getTextRef : (checked:bool -> string -> string) ref

val isChecked : string -> bool

val isCheckedRef : (string -> bool) ref

val parseDate : string -> string

val parseDateRef : (string -> string) ref

val setText : string -> string -> unit

val setTextRef : (string -> string -> unit) ref

val setValue : string -> string -> unit

val setValueRef : (string -> string -> unit) ref

val setOutput : string -> unit

val setExplorerOutput : string -> string -> unit

val addOutput : string -> unit

val setOutputToMethod : string -> string -> unit

val setOutputRef : (string -> unit) ref

val setExplorerOutputRef : (string -> string -> unit) ref

val addOutputRef : (string -> unit) ref

val setOutputToMethodRef : (string -> string -> unit) ref
