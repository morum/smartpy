(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Basics
open Unchecked

type t = expr [@@deriving show {with_path = false}]

type nullary_expr = line_no:line_no -> t

type unary_expr = line_no:line_no -> t -> t

type binary_expr = line_no:line_no -> t -> t -> t

type ternary_expr = line_no:line_no -> t -> t -> t -> t

let build ~line_no e = {e; et = U; line_no}

let invert_bytes ~line_no x = build ~line_no (EPrim1 (EInvert_bytes, x))

let lshift_infix ~line_no x y = build ~line_no (EPrim2 (ELshift_infix, x, y))

let rshift_infix ~line_no x y = build ~line_no (EPrim2 (ERshift_infix, x, y))

let lshift_bytes ~line_no x y = build ~line_no (EPrim2 (ELshift_bytes, x, y))

let rshift_bytes ~line_no x y = build ~line_no (EPrim2 (ERshift_bytes, x, y))

let sub_mutez ~line_no x y = build ~line_no (EMPrim2 (Sub_mutez, x, y))

let add ~line_no x y = build ~line_no (EPrim2 (EAdd, x, y))

let add_prim ~line_no x y = build ~line_no (EMPrim2 (Add, x, y))

let and_ ~line_no x y = build ~line_no (EPrim2 (EAnd, x, y))

let and_bytes ~line_no x y = build ~line_no (EPrim2 (EAnd_bytes, x, y))

let and_infix ~line_no x y = build ~line_no (EPrim2 (EAnd_infix, x, y))

let div ~line_no x y = build ~line_no (EPrim2 (EDiv, x, y))

let ediv ~line_no x y = build ~line_no (EPrim2 (EEDiv, x, y))

let eq ~line_no x y = build ~line_no (EPrim2 (EEq, x, y))

let ge ~line_no x y = build ~line_no (EPrim2 (EGe, x, y))

let gt ~line_no x y = build ~line_no (EPrim2 (EGt, x, y))

let le ~line_no x y = build ~line_no (EPrim2 (ELe, x, y))

let lt ~line_no x y = build ~line_no (EPrim2 (ELt, x, y))

let max ~line_no x y = build ~line_no (EPrim2 (EMax, x, y))

let min ~line_no x y = build ~line_no (EPrim2 (EMin, x, y))

let mod_ ~line_no x y = build ~line_no (EPrim2 (EMod, x, y))

let mul_homo ~line_no x y = build ~line_no (EPrim2 (EMul_homo, x, y))

let mul_prim ~line_no x y = build ~line_no (EMPrim2 (Mul, x, y))

let compare ~line_no x y = build ~line_no (EMPrim2 (Compare, x, y))

let neq ~line_no x y = build ~line_no (EPrim2 (ENeq, x, y))

let or_ ~line_no x y = build ~line_no (EPrim2 (EOr, x, y))

let or_bytes ~line_no x y = build ~line_no (EPrim2 (EOr_bytes, x, y))

let or_infix ~line_no x y = build ~line_no (EPrim2 (EOr_infix, x, y))

let sub ~line_no x y = build ~line_no (EPrim2 (ESub, x, y))

let xor_bytes ~line_no x y = build ~line_no (EPrim2 (EXor_bytes, x, y))

let xor_infix ~line_no x y = build ~line_no (EPrim2 (EXor_infix, x, y))

let attr ~name ~line_no x = build ~line_no (EAttr (name, x))

let variant ~name ~line_no x = build ~line_no (EVariant (name, x))

let is_variant ~name ~line_no x = build ~line_no (EPrim1 (EIs_variant name, x))

let var ~line_no name =
  {e = EVar {name = mk_bound name; derived = U}; et = U; line_no}

let open_variant ~line_no name x missing_message =
  build ~line_no (EOpen_variant (name, x, missing_message))

let update_map ~line_no key value map =
  build ~line_no (EPrim3 (EUpdate_map, key, value, map))

let private_ ~line_no name = {e = EPrivate name; et = U; line_no}

let item ~line_no items key default_value missing_message =
  build ~line_no (EItem {items; key; default_value; missing_message})

let contains ~line_no member items =
  build ~line_no (EPrim2 (EContains, member, items))

let sum ~line_no l = build ~line_no (EPrim1 (ESum, l))

let range ~line_no a b step = build ~line_no (EPrim3 (ERange, a, b, step))

let cons ~line_no x l = build ~line_no (EPrim2 (ECons, x, l))

let literal ~line_no x = build ~line_no (ELiteral x)

let is_failing ~line_no x = build ~line_no (EIs_failing x)

let catch_exception ~t ~line_no x = build ~line_no (ECatch_exception (t, x))

let unit = literal ~line_no:None Literal.unit

let type_annotation ~t ~line_no e =
  build ~line_no (EPrim1 (EType_annotation t, e))

let convert ~line_no x = build ~line_no (EPrim1 (EConvert, x))

let record ~line_no entries = build ~line_no (ERecord entries)

let list ~line_no ~elems = build ~line_no (EList elems)

let map ~line_no ~big ~entries = build ~line_no (EMap (big, entries))

let set ~line_no ~entries = build ~line_no (ESet entries)

let hash_key ~line_no e = build ~line_no (EMPrim1 (Hash_key, e))

let blake2b ~line_no e = {e = EMPrim1 (Blake2b, e); et = U; line_no}

let sha256 ~line_no e = {e = EMPrim1 (Sha256, e); et = U; line_no}

let sha512 ~line_no e = {e = EMPrim1 (Sha512, e); et = U; line_no}

let keccak ~line_no e = {e = EMPrim1 (Keccak, e); et = U; line_no}

let sha3 ~line_no e = {e = EMPrim1 (Sha3, e); et = U; line_no}

let pack ~line_no e = build ~line_no (EPrim1 (EPack, e))

let unpack ~line_no e t = build ~line_no (EPrim1 (EUnpack t, e))

let check_signature ~line_no message signature pk =
  build ~line_no (EMPrim3 (Check_signature, message, signature, pk))

let account_of_seed ~seed ~line_no =
  build ~line_no (EPrim0 (EAccount_of_seed {seed}))

let make_signature ~line_no ~secret_key ~message ~message_format =
  build ~line_no (EMake_signature {secret_key; message; message_format})

let constant ~line_no e t = build ~line_no (EPrim0 (EConstant (e, t)))

let split_tokens ~line_no mutez quantity total =
  build ~line_no (EPrim3 (ESplit_tokens, mutez, quantity, total))

let now = build (EMPrim0 Now)

let chain_id = build (EMPrim0 Chain_id)

let balance = build (EMPrim0 Balance)

let sender = build (EMPrim0 Sender)

let source = build (EMPrim0 Source)

let amount = build (EMPrim0 Amount)

let level = build (EMPrim0 Level)

let total_voting_power = build (EMPrim0 Total_voting_power)

let self = build (EMPrim0 (Self None))

let self_address = build (EMPrim0 Self_address)

let add_seconds ~line_no t s = build ~line_no (EPrim2 (EAdd_seconds, t, s))

let not ~line_no x = build ~line_no (EPrim1 (ENot, x))

let invert ~line_no x = build ~line_no (EPrim1 (EInvert, x))

let abs ~line_no x = build ~line_no (EMPrim1 (Abs, x))

let to_int ~line_no x = build ~line_no (EPrim1 (ETo_int, x))

let is_nat ~line_no x = build ~line_no (EMPrim1 (IsNat, x))

let neg ~line_no x = build ~line_no (EPrim1 (ENeg, x))

let sign ~line_no x = build ~line_no (EPrim1 (ESign, x))

let slice ~line_no ~offset ~length ~buffer =
  build ~line_no (ESlice {offset; length; buffer})

let concat_list ~line_no l = build ~line_no (EPrim1 (EConcat_list, l))

let size ~line_no s = build ~line_no (EPrim1 (ESize, s))

let self_entrypoint ~line_no name = build ~line_no (EMPrim0 (Self (Some name)))

let to_address ~line_no e =
  match e.e with
  | EMPrim0 (Self None) -> build ~line_no (EMPrim0 Self_address)
  | _ -> build ~line_no (EPrim1 (EAddress, e))

let implicit_account ~line_no e = build ~line_no (EPrim1 (EImplicit_account, e))

let voting_power ~line_no e = build ~line_no (EPrim1 (EVoting_power, e))

let list_rev ~line_no e = build ~line_no (EPrim1 (EList_rev, e))

let list_items ~line_no e rev = build ~line_no (EPrim1 (EList_items rev, e))

let list_keys ~line_no e rev = build ~line_no (EPrim1 (EList_keys rev, e))

let list_values ~line_no e rev = build ~line_no (EPrim1 (EList_values rev, e))

let list_elements ~line_no e rev =
  build ~line_no (EPrim1 (EList_elements rev, e))

let contract ~line_no entrypoint arg_type address =
  build ~line_no (EContract {entrypoint; arg_type; address})

let view ~line_no name param address return_type =
  build ~line_no (EPrim2 (EView (name, return_type), param, address))

let static_view ~line_no static_id name param =
  build ~line_no (EPrim1 (EStatic_view (static_id, name), param))

let tuple ~line_no es = build ~line_no (ETuple es)

let first ~line_no e = build ~line_no (EPrim1 (EFst, e))

let second ~line_no e = build ~line_no (EPrim1 (ESnd, e))

let none ~line_no = variant ~line_no ~name:"None" unit

let some ~line_no e = variant ~line_no ~name:"Some" e

let left ~line_no l = variant ~line_no ~name:"Left" l

let right ~line_no r = variant ~line_no ~name:"Right" r

let inline_michelson ~line_no michelson exprs =
  build ~line_no (EMichelson (michelson, exprs))

let map_function ~line_no l f = build ~line_no (EMap_function {l; f})

let call ~line_no lambda parameter =
  {e = ECall (lambda, parameter); et = U; line_no}

let apply_lambda ~line_no parameter lambda =
  build ~line_no (EPrim2 (EApply_lambda, parameter, lambda))

let lambda ~line_no lambda_var body ~with_storage ~with_operations ~recursive =
  build ~line_no
    (ELambda
       {
         lambda_var
       ; body
       ; with_storage
       ; with_operations
       ; recursive = Option.map mk_bound recursive
       ; derived = U
       })

let sapling_empty_state memo =
  build ~line_no:None (EMPrim0 (Sapling_empty_state {memo}))

let sapling_verify_update ~line_no state transaction =
  build ~line_no (ESapling_verify_update {state; transaction})

let set_delegate ~line_no e = build ~line_no (EPrim1 (ESet_delegate, e))

let transfer ~line_no ~arg ~amount ~destination =
  build ~line_no (ETransfer {arg; amount; destination})

let emit ~line_no tag with_type x =
  build ~line_no (EPrim1 (EEmit (tag, with_type), x))

let contract_address ~line_no entrypoint e =
  build ~line_no (EPrim0 (EContract_address (e, entrypoint)))

let contract_typed ~line_no entrypoint e =
  build ~line_no (EPrim0 (EContract_typed (e, entrypoint)))

let contract_data ~line_no e = build ~line_no (EPrim0 (EContract_data e))

let contract_private ~line_no e = build ~line_no (EPrim0 (EContract_private e))

let contract_balance ~line_no e = build ~line_no (EPrim0 (EContract_balance e))

let contract_baker ~line_no e = build ~line_no (EPrim0 (EContract_baker e))

let eif ~line_no cond a b = build ~line_no (EIf (cond, a, b))

let test_ticket ~line_no ticketer content amount =
  build ~line_no (EPrim3 (ETest_ticket, ticketer, content, amount))

let ticket ~line_no content amount =
  build ~line_no (EPrim2 (ETicket, content, amount))

let read_ticket ~line_no ticket = build ~line_no (EPrim1 (ERead_ticket, ticket))

let split_ticket ~line_no ticket decomposition =
  build ~line_no (EPrim2 (ESplit_ticket, ticket, decomposition))

let join_tickets ~line_no tickets =
  build ~line_no (EPrim1 (EJoin_tickets, tickets))

let pairing_check ~line_no pairs =
  build ~line_no (EPrim1 (EPairing_check, pairs))

let get_and_update ~line_no key value map =
  build ~line_no (EPrim3 (EGet_and_update, key, value, map))

let open_chest ~line_no chest_key chest time =
  build ~line_no (EMPrim3 (Open_chest, chest_key, chest, time))

let get_opt ~line_no k m = build ~line_no (EPrim2 (EGet_opt, k, m))

let of_value_f t v =
  let line_no = None in
  match v with
  | Literal l -> literal ~line_no l
  | Contract {address; entrypoint} -> (
      match Type.unF t with
      | T1 (Contract, t) ->
          {
            e = EPrim0 (ECst_contract {address; entrypoint; type_ = t})
          ; et = U
          ; line_no
          }
      | _ -> assert false)
  | Record entries -> build ~line_no (ERecord entries)
  | Variant (lbl, arg) -> build ~line_no (EVariant (lbl, arg))
  | List elems -> build ~line_no (EList elems)
  | Set entries -> set ~line_no ~entries
  | Map entries -> (
      match Type.unF t with
      | T2 (((Map | Big_map) as tm), _, _) ->
          let big = tm = Big_map in
          build ~line_no
            (EMap (big, entries |> List.map (fun (k, v) -> (k, v))))
      | _ -> assert false)
  | Tuple vs -> tuple ~line_no vs
  | Closure ({lambda_var; body; recursive}, args) ->
      let body = erase_types_command body in
      let recursive = Option.map (fun r -> r.Checked.bound) recursive in
      List.fold_left
        (fun f arg -> apply_lambda ~line_no f arg)
        (lambda ~line_no
           {bound = lambda_var.bound; index = U}
           body ~with_storage:None ~with_operations:false ~recursive)
        args
  | Operation _ -> failwith "TODO expr. Operation"
  | Ticket (ticketer, content, amount) ->
      let ticketer = literal ~line_no (Literal.address ticketer) in
      let content = content in
      let amount = literal ~line_no (Literal.int amount) in
      test_ticket ~line_no ticketer content amount

let of_value = cata_tvalue of_value_f
