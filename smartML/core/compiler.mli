(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Basics
open Utils
open Michelson

type scenario_vars = tvalue TBoundMap.t

(*val mtype_of_record : ('a -> mtype) -> (string * 'a) list -> mtype*)
(** Compile a record to Michelson, see {!Scenario.run}, it useful to
    reproduce the same binary tree structure as normal compilation. *)

val compile_value :
  config:Config.t -> scenario_vars:scenario_vars -> tvalue -> literal
(** Compile a value into a Michelson literal. *)

val pack_literal : protocol:Config.protocol -> tliteral -> string

val pack_value :
  config:Config.t -> scenario_vars:scenario_vars -> tvalue -> string
(** Serialize a value into the same binary format as Michelson in the
    protocol.  *)

val unpack_value : config:Config.t -> Type.t -> string -> value
(** Deserialize michelson bytes. *)

val compile_instance :
  scenario_vars:scenario_vars -> tinstance -> Michelson.tcontract
(** Compile a smart-contract to the Michelson intermediate representation. *)

val compile_literal : ?for_error:Checked.expr -> Literal.t -> Michelson.literal

val compile_record :
     layout:Layout.l Binary_tree.t
  -> (string * 'a) list
  -> (string * 'a) Binary_tree.t

val compile_variant :
     row:(string * 'a) list
  -> layout:Layout.l Utils.Binary_tree.t
  -> string
  -> 'a Utils.Binary_tree.context

val compile_tuple : ('a -> 'a -> 'a) -> 'a list -> 'a

val simplify_via_michel :
     config:Config.t
  -> tcontract
  -> Michel.Expr.expr Michel.Expr.precontract * tcontract
