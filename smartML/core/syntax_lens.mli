open Basics
open Checked

val entrypoints : (Checked.contract_def, Checked.entrypoint list) Lens.t

val entrypoint : string -> (Checked.contract_def, Checked.entrypoint) Lens.t

val entrypoint_body : (Checked.entrypoint, command) Lens.t

val views : (Checked.contract_def, Checked.view list) Lens.t

val view : string -> (Checked.contract_def, Checked.view) Lens.t

val view_body : (Checked.view, command) Lens.t

val seq_nth : int -> (command, command) Lens.t

val if_cond : (command, expr) Lens.t

val if_then : (command, command) Lens.t

val if_else : (command, command) Lens.t

val for_container : (command, expr) Lens.t

val for_body : (command, command) Lens.t

val arg1 : (expr, expr) Lens.t

val arg2 : (expr, expr) Lens.t

val arg3 : (expr, expr) Lens.t

val failwith_ : (command, expr) Lens.t
