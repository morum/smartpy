(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Basics
open Utils
open Unchecked
open Sexplib.Sexp
open Sexplib.Conv

type env = {
    unknowns : (string, Type.t) Hashtbl.t
  ; entrypoint : string option
}

let init_env () = {unknowns = Hashtbl.create 5; entrypoint = None}

let sort_strings = List.sort Stdlib.compare

let import_layout ~line_no l layout =
  match (l, layout) with
  | [], _ -> Hole.mk ()
  | _, List [Atom "Value"; Atom "right_comb"] ->
      Value (Type.comb_layout `Right l)
  | _ ->
      let r = Hole.t_of_sexp Layout.t_of_sexp layout in
      (match r with
      | Value layout ->
          let n =
            List.map (fun x -> x.Layout.source) (Binary_tree.to_list layout)
          in
          let l1 = sort_strings n in
          let l2 = sort_strings l in
          if l1 <> l2
          then
            raise
              (SmartExcept
                 [
                   `Text "Bad layout for type"
                 ; `Br
                 ; `Text (String.concat ", " l1)
                 ; `Br
                 ; `Text "!="
                 ; `Br
                 ; `Text (String.concat ", " l2)
                 ; `Br
                 ; `Line line_no
                 ])
      | _ -> ());
      r

let default_line_no_f =
  let id x = x in
  let f_expr line_no e default_line_no =
    let line_no = if line_no = None then default_line_no else line_no in
    let e = map_expr_f (fun e -> e line_no) id id e in
    {Unchecked.line_no; e; et = U}
  in
  let f_command line_no c =
    let c = map_command_f (fun e -> e line_no) id id c in
    {Unchecked.line_no; c; ct = U}
  in
  let f_type = id in
  {f_expr; f_command; f_type}

let _default_line_no_expr e line_no = cata_expr default_line_no_f e line_no

let string_of_line_no l = Option.cata "None" (fun (_, i) -> string_of_int i) l

let import_effects : _ -> effects = function
  | List [with_storage; with_operations] ->
      let with_storage = option_of_sexp Type.access_of_sexp with_storage in
      let with_operations = bool_of_sexp with_operations in
      {with_storage; with_operations}
  | x -> failwith ("import_effects: " ^ to_string x)

let rec import_literal =
  let open Literal in
  let chop_prefix n =
    Base.(String.chop_prefix n ~prefix:"0x" |> Option.value ~default:n)
  in
  function
  | [Atom "unit"] -> unit
  | [Atom "string"; Atom n] -> string n
  | [Atom "bytes"; Atom n] ->
      let n = chop_prefix n in
      bytes (Hex.to_string (`Hex n))
  | [Atom "bls12_381_g1"; Atom n] ->
      let n = chop_prefix n in
      bls12_381_g1 (Hex.to_string (`Hex n))
  | [Atom "bls12_381_g2"; Atom n] ->
      let n = chop_prefix n in
      bls12_381_g2 (Hex.to_string (`Hex n))
  | [Atom "bls12_381_fr"; Atom n] ->
      let n = chop_prefix n in
      bls12_381_fr (Hex.to_string (`Hex n))
  | [Atom "chest_key"; Atom n] ->
      let n = chop_prefix n in
      chest_key (Hex.to_string (`Hex n))
  | [Atom "chest"; Atom n] ->
      let n = chop_prefix n in
      chest (Hex.to_string (`Hex n))
  | [Atom "chain_id_cst"; Atom n] ->
      let n = chop_prefix n in
      chain_id (Hex.to_string (`Hex n))
  | [Atom "int"; Atom n] -> int (Bigint.of_string ~msg:"import" n)
  | [Atom "intOrNat"; Atom n] ->
      intOrNat (Hole.mk ()) (Bigint.of_string ~msg:"import" n)
  | [Atom "nat"; Atom n] -> nat (Bigint.of_string ~msg:"import" n)
  | [Atom "timestamp"; Atom n] -> timestamp (Bigint.of_string ~msg:"import" n)
  | [Atom "bool"; Atom "True"] -> bool true
  | [Atom "bool"; Atom "False"] -> bool false
  | [Atom "key"; Atom n] -> key n
  | [Atom "secret_key"; Atom n] -> secret_key n
  | [Atom "signature"; Atom n] -> signature n
  | [Atom "address"; Atom n] -> address n
  | [Atom "key_hash"; Atom n] -> key_hash n
  | [Atom "mutez"; List l] -> mutez (Option.of_some (unInt (import_literal l)))
  | [Atom "mutez"; Atom n] -> mutez (Bigint.of_string ~msg:"import" n)
  | [
      Atom "sapling_test_transaction"
    ; Atom memo
    ; Atom source
    ; Atom target
    ; Atom amount
    ; Atom boundData
    ] ->
      let source = if source = "" then None else Some source in
      let target = if target = "" then None else Some target in
      sapling_test_transaction (int_of_string memo) source target
        (Bigint.of_string ~msg:"import" amount)
        boundData
  | l ->
      Format.kasprintf failwith "Literal format error: %a" Sexplib.Sexp.pp
        (List l)

let import_type env =
  let open Type in
  let rec import_type = function
    | Atom "int" -> int
    | Atom "nat" -> nat
    | Atom "intOrNat" -> intOrNat ()
    | Atom "never" -> never
    | List [Atom "unknown"; Atom id] -> (
        let i = "sp:" ^ id in
        if i = ""
        then failwith "empty unknown"
        else
          match Hashtbl.find_opt env.unknowns i with
          | Some t -> t
          | None ->
              let r = unknown_raw i in
              Hashtbl.replace env.unknowns i r;
              r)
    | List [Atom "record"; List l; layout; line_no] ->
        let line_no = line_no_of_sexp line_no in
        let l = List.map importField l in
        record_or_unit (import_layout ~line_no (List.map fst l) layout) l
    | List [Atom "variant"; List l; layout; line_no] ->
        let line_no = line_no_of_sexp line_no in
        let l = List.map importField l in
        variant (import_layout ~line_no (List.map fst l) layout) l
    | List [Atom "lambda"; t1; t2] ->
        lambda no_effects (import_type t1) (import_type t2)
    | List [Atom "typeRef"; Atom name; line_no] ->
        let line_no = line_no_of_sexp line_no in
        abbrev ~line_no None name
    | List [Atom "typeRef"; Atom module_; Atom name; line_no] ->
        let line_no = line_no_of_sexp line_no in
        abbrev ~line_no (Some module_) name
    | List [Atom "lambda"; with_storage; with_operations; tstorage; t1; t2] ->
        let tstorage =
          match tstorage with
          | Atom "None" -> Type.unknown_raw "storage"
          | t -> import_type t
        in
        let with_storage = option_of_sexp Type.access_of_sexp with_storage in
        let with_storage = Option.map (fun a -> (a, tstorage)) with_storage in
        let with_storage = Hole.value with_storage in
        let with_operations = Hole.t_of_sexp bool_of_sexp with_operations in
        let effects = {with_storage; with_operations} in
        lambda effects (import_type t1) (import_type t2)
    | List [Atom "lambda"; (List _ as effects); t1; t2] ->
        let ({with_storage; with_operations} : Basics.effects) =
          import_effects effects
        in
        let with_storage =
          Hole.value
          @@ Option.map (fun x -> (x, Type.unknown_raw "storage")) with_storage
        in
        let with_operations = Hole.value with_operations in
        let effects = Type.{with_storage; with_operations} in
        lambda effects (import_type t1) (import_type t2)
    | ( List [Atom "sapling_state"; _]
      | List [Atom "sapling_transaction"; _]
      | List (Atom "tuple" :: _) ) as t -> Type.F (Type.f_of_sexp import_type t)
    | Atom "operation" -> Type.operation
    | Atom _ as t -> Type.F (T0 (Michelson_base.Type.type0_of_sexp t))
    | List [tc; t0] ->
        Type.F (T1 (Michelson_base.Type.type1_of_sexp tc, import_type t0))
    | List [tc; t0; t1] ->
        Type.F
          (T2
             ( Michelson_base.Type.type2_of_sexp tc
             , import_type t0
             , import_type t1 ))
    | t -> failwith ("Type format error: " ^ to_string_hum t)
  and importField = function
    | List [Atom name; v] -> (name, import_type v)
    | l -> failwith ("Type field format error " ^ to_string l)
  in
  import_type

let import_contract_id = function
  | List [_; Atom "static_id"; Atom id] ->
      C_static {static_id = int_of_string id}
  | List [_; Atom "dynamic_id"; Atom id] ->
      C_dynamic {dynamic_id = int_of_string id}
  | l ->
      Format.kasprintf failwith "Contract_id format error: %a" Sexplib.Sexp.pp l

let import_qual_ident = function
  | Atom id -> {qi_module = None; qi_ident = id}
  | List [Atom m; Atom id] -> {qi_module = Some m; qi_ident = id}
  | l -> Format.kasprintf failwith "import_qual_ident: %a" Sexplib.Sexp.pp l

let rec import_expr_inline_michelson env =
  let import_type t = import_type env t in
  let rec import_expr_inline_michelson = function
    | List [Atom "call_michelson"; instr; Atom _line_no] ->
        import_expr_inline_michelson instr
    | List (Atom "op" :: Atom name :: args) ->
        let parsed = Micheline_encoding.parse_node name in
        let rec extractTypes acc = function
          | [] -> assert false
          | Atom "out" :: out -> (List.rev acc, List.map import_type out)
          | t :: args -> extractTypes (import_type t :: acc) args
        in
        let typesIn, typesOut = extractTypes [] args in
        {name; parsed; typesIn; typesOut}
    | input ->
        failwith
          (Printf.sprintf "Cannot parse inline michelson %s" (to_string input))
  in
  import_expr_inline_michelson

and import_expr env =
  let import_type t = import_type env t in
  let import_expr_inline_michelson = import_expr_inline_michelson env in
  let open Expr in
  let rec import_expr = function
    | List (line_no :: Atom f :: args) as input -> (
        let line_no = line_no_of_sexp line_no in
        let mk e = {e; et = U; line_no} in
        match (f, args) with
        | "storage", [] -> Expr.var ~line_no "__storage__"
        | "private", [] -> Expr.var ~line_no "__private__"
        | "operations", [] -> Expr.var ~line_no "__operations__"
        | "parameter", [] -> Expr.var ~line_no "__parameter__"
        | "var", [Atom name] -> Expr.var ~line_no name
        | "sender", [] -> sender ~line_no
        | "source", [] -> source ~line_no
        | "amount", [] -> amount ~line_no
        | "balance", [] -> balance ~line_no
        | "now", [] -> now ~line_no
        | "self", [] -> self ~line_no
        | "self", [Atom name] ->
            let name =
              if name = ""
              then
                match env.entrypoint with
                | None -> failwith "import: params type yet unknown"
                | Some name -> name
              else name
            in
            self_entrypoint ~line_no name
        | "self_address", [] -> self_address ~line_no:None
        | "chain_id", [] -> chain_id ~line_no:None
        | "total_voting_power", [] -> total_voting_power ~line_no:None
        | "sapling_empty_state", [Atom memo] ->
            sapling_empty_state (int_of_string memo)
        | "level", [] -> level ~line_no:None
        | "eq", [e1; e2] -> Expr.eq ~line_no (import_expr e1) (import_expr e2)
        | "neq", [e1; e2] -> Expr.neq ~line_no (import_expr e1) (import_expr e2)
        | "le", [e1; e2] -> Expr.le ~line_no (import_expr e1) (import_expr e2)
        | "lt", [e1; e2] -> Expr.lt ~line_no (import_expr e1) (import_expr e2)
        | "ge", [e1; e2] -> Expr.ge ~line_no (import_expr e1) (import_expr e2)
        | "gt", [e1; e2] -> Expr.gt ~line_no (import_expr e1) (import_expr e2)
        | "add", [e1; e2] -> Expr.add ~line_no (import_expr e1) (import_expr e2)
        | "sub", [e1; e2] -> mk @@ EPrim2 (ESub, import_expr e1, import_expr e2)
        | "sub_mutez", [e1; e2] ->
            Expr.sub_mutez ~line_no (import_expr e1) (import_expr e2)
        | "ADD", [e1; e2] ->
            Expr.add_prim ~line_no (import_expr e1) (import_expr e2)
        | "mul_homo", [e1; e2] ->
            Expr.mul_homo ~line_no (import_expr e1) (import_expr e2)
        | "MUL", [e1; e2] ->
            Expr.mul_prim ~line_no (import_expr e1) (import_expr e2)
        | "ediv", [e1; e2] ->
            Expr.ediv ~line_no (import_expr e1) (import_expr e2)
        | "div", [e1; e2] -> Expr.div ~line_no (import_expr e1) (import_expr e2)
        | "mod", [e1; e2] ->
            Expr.mod_ ~line_no (import_expr e1) (import_expr e2)
        | "or", [e1; e2] -> Expr.or_ ~line_no (import_expr e1) (import_expr e2)
        | "and", [e1; e2] ->
            Expr.and_ ~line_no (import_expr e1) (import_expr e2)
        | "and_bytes", [e1; e2] ->
            Expr.and_bytes ~line_no (import_expr e1) (import_expr e2)
        | "and_infix", [e1; e2] ->
            Expr.and_infix ~line_no (import_expr e1) (import_expr e2)
        | "or_bytes", [e1; e2] ->
            Expr.or_bytes ~line_no (import_expr e1) (import_expr e2)
        | "or_infix", [e1; e2] ->
            Expr.or_infix ~line_no (import_expr e1) (import_expr e2)
        | "COMPARE", [e1; e2] ->
            compare ~line_no (import_expr e1) (import_expr e2)
        | "max", [e1; e2] -> Expr.max ~line_no (import_expr e1) (import_expr e2)
        | "min", [e1; e2] -> Expr.min ~line_no (import_expr e1) (import_expr e2)
        | "sum", [a] -> sum ~line_no (import_expr a)
        | "to_address", [e] -> to_address ~line_no (import_expr e)
        | "implicit_account", [e] -> implicit_account ~line_no (import_expr e)
        | "cons", [e1; e2] -> cons ~line_no (import_expr e1) (import_expr e2)
        | "range", [e1; e2; e3] ->
            range ~line_no (import_expr e1) (import_expr e2) (import_expr e3)
        | "literal", [List l] -> literal ~line_no (import_literal l)
        | "is_failing", [x] -> is_failing ~line_no (import_expr x)
        | "catch_exception", [x; t] ->
            catch_exception ~line_no (import_expr x) ~t:(import_type t)
        | "convert", [x] -> convert ~line_no (import_expr x)
        | "list", l -> list ~line_no ~elems:(List.map import_expr l)
        | "list_comprehension", [e; Atom x; xs] ->
            let e = import_expr e in
            let x = mk_bound x in
            let xs = import_expr xs in
            {e = EList_comprehension (e, x, xs); et = U; line_no}
        | "test_ticket", [ticketer; content; amount] ->
            test_ticket ~line_no (import_expr ticketer) (import_expr content)
              (import_expr amount)
        | "ticket", [content; amount] ->
            ticket ~line_no (import_expr content) (import_expr amount)
        | "read_ticket", [ticket] -> read_ticket ~line_no (import_expr ticket)
        | "split_ticket", [ticket; decomposition] ->
            split_ticket ~line_no (import_expr ticket)
              (import_expr decomposition)
        | "join_tickets", [tickets] ->
            join_tickets ~line_no (import_expr tickets)
        | "pairing_check", [pairs] -> pairing_check ~line_no (import_expr pairs)
        | "voting_power", [e] -> voting_power ~line_no (import_expr e)
        | "first", [e] -> first ~line_no (import_expr e)
        | "second", [e] -> second ~line_no (import_expr e)
        | "tuple", es -> tuple ~line_no (List.map import_expr es)
        | "neg", [e] -> mk @@ EPrim1 (ENeg, import_expr e)
        | "abs", [e1] -> abs ~line_no (import_expr e1)
        | "to_int", [e] -> mk @@ EPrim1 (ETo_int, import_expr e)
        | "to_nat", [e] -> mk @@ EPrim1 (ETo_nat, import_expr e)
        | "to_bytes", [e] -> mk @@ EPrim1 (ETo_bytes, import_expr e)
        | "is_nat", [e1] -> is_nat ~line_no (import_expr e1)
        | "sign", [e1] -> sign ~line_no (import_expr e1)
        | "not", [e1] -> not ~line_no (import_expr e1)
        | "invert", [e1] -> invert ~line_no (import_expr e1)
        | "contract_address", [id; Atom entrypoint] ->
            let entrypoint =
              match entrypoint with
              | "" -> None
              | x -> Some x
            in
            contract_address ~line_no entrypoint (import_contract_id id)
        | "contract_typed", [id; Atom entrypoint] ->
            let entrypoint =
              match entrypoint with
              | "" -> None
              | x -> Some x
            in
            contract_typed ~line_no entrypoint (import_contract_id id)
        | "contract_balance", [id] ->
            contract_balance ~line_no (import_contract_id id)
        | "contract_baker", [id] ->
            contract_baker ~line_no (import_contract_id id)
        | "contract_data", [id] ->
            contract_data ~line_no (import_contract_id id)
        | "contract_private", [id] ->
            contract_private ~line_no (import_contract_id id)
        | "contract", [Atom entrypoint; t; addr] ->
            let entrypoint =
              match entrypoint with
              | "" -> None
              | _ -> Some entrypoint
            in
            let address = import_expr addr in
            contract ~line_no entrypoint (import_type t) address
        | "view", [Atom name; param; addr; t] ->
            let param = import_expr param in
            let address = import_expr addr in
            let ty = import_type t in
            view ~line_no name param address ty
        | "static_view", [Atom name; contract_id; param] ->
            let static_id =
              match import_contract_id contract_id with
              | C_static static_id -> static_id
              | C_dynamic _ -> assert false
            in
            let param = import_expr param in
            static_view ~line_no static_id name param
        | "attr", [x; Atom name] -> attr ~line_no (import_expr x) ~name
        | "is_variant", [x; Atom name] ->
            is_variant ~line_no ~name (import_expr x)
        | "open_variant", [x; Atom name; missing_message] ->
            let missing_message =
              match missing_message with
              | Atom "None" -> None
              | _ -> Some (import_expr missing_message)
            in
            open_variant ~line_no name (import_expr x) missing_message
        | "variant", [Atom name; x] -> variant ~line_no ~name (import_expr x)
        | "blake2b", [e] -> blake2b ~line_no (import_expr e)
        | "sha256", [e] -> sha256 ~line_no (import_expr e)
        | "sha512", [e] -> sha512 ~line_no (import_expr e)
        | "keccak", [e] -> keccak ~line_no (import_expr e)
        | "sha3", [e] -> sha3 ~line_no (import_expr e)
        | "hash_key", [e] -> hash_key ~line_no (import_expr e)
        | "pack", [e] ->
            let e = import_expr e in
            pack ~line_no e
        | "unpack", [e; t] -> unpack ~line_no (import_expr e) (import_type t)
        | "update_map", [map; key; v] ->
            update_map ~line_no (import_expr key) (import_expr v)
              (import_expr map)
        | "get_and_update", [map; key; v] ->
            get_and_update ~line_no (import_expr key) (import_expr v)
              (import_expr map)
        | "open_chest", [chest_key; chest; time] ->
            open_chest ~line_no (import_expr chest_key) (import_expr chest)
              (import_expr time)
        | "get_item", [a; pos] ->
            item ~line_no (import_expr a) (import_expr pos) None None
        | "get_item_default", [a; pos; def] ->
            item ~line_no (import_expr a) (import_expr pos)
              (Some (import_expr def))
              None
        | "get_item_message", [a; pos; message] ->
            item ~line_no (import_expr a) (import_expr pos) None
              (Some (import_expr message))
        | "get_opt", [m; k] -> get_opt ~line_no (import_expr k) (import_expr m)
        | "add_seconds", [t; s] ->
            add_seconds ~line_no (import_expr t) (import_expr s)
        | "rev", [e] -> list_rev ~line_no (import_expr e)
        | "items", [e] -> list_items ~line_no (import_expr e) false
        | "keys", [e] -> list_keys ~line_no (import_expr e) false
        | "values", [e] -> list_values ~line_no (import_expr e) false
        | "elements", [e] -> list_elements ~line_no (import_expr e) false
        | "rev_items", [e] -> list_items ~line_no (import_expr e) true
        | "rev_keys", [e] -> list_keys ~line_no (import_expr e) true
        | "rev_values", [e] -> list_values ~line_no (import_expr e) true
        | "rev_elements", [e] -> list_elements ~line_no (import_expr e) true
        | "contains", [items; x] ->
            contains ~line_no (import_expr x) (import_expr items)
        | "check_signature", [pk; s; msg] ->
            check_signature ~line_no (import_expr pk) (import_expr s)
              (import_expr msg)
        | "constant", [Atom e; t] ->
            let tExpr = import_type t in
            constant ~line_no e tExpr
        | "make_signature", [sk; msg; Atom fmt] ->
            let secret_key = import_expr sk in
            let message = import_expr msg in
            let message_format =
              match String.lowercase_ascii fmt with
              | "raw" -> `Raw
              | "hex" -> `Hex
              | other ->
                  Format.kasprintf failwith
                    "make_signature: Wrong message format : %S (l. %s)" other
                    (string_of_line_no line_no)
            in
            make_signature ~secret_key ~message ~message_format ~line_no
        | "account_of_seed", [Atom seed] -> account_of_seed ~seed ~line_no
        | "split_tokens", [mutez; quantity; total] ->
            split_tokens ~line_no (import_expr mutez) (import_expr quantity)
              (import_expr total)
        | "slice", [ofs; len; buf] ->
            let offset = import_expr ofs in
            let length = import_expr len in
            let buffer = import_expr buf in
            slice ~offset ~length ~buffer ~line_no
        | "concat", [l] -> concat_list (import_expr l) ~line_no
        | "size", [s] -> size (import_expr s) ~line_no
        | "type_annotation", [e; t] ->
            let e = import_expr e in
            let t = import_type t in
            type_annotation ~line_no e ~t
        | "map", entries ->
            map ~line_no ~big:false ~entries:(List.map import_map_entry entries)
        | "set", entries -> set ~line_no ~entries:(List.map import_expr entries)
        | "big_map", entries ->
            map ~line_no ~big:true ~entries:(List.map import_map_entry entries)
        | "record", l ->
            let import_exprField = function
              | List [Atom name; e] -> (name, import_expr e)
              | l -> failwith ("Expression field format error " ^ to_string l)
            in
            record ~line_no (List.map import_exprField l)
        | "eif", [cond; a; b] ->
            let cond = import_expr cond in
            let a = import_expr a in
            let b = import_expr b in
            eif ~line_no cond a b
        | "call_michelson", instr :: args ->
            inline_michelson ~line_no
              (import_expr_inline_michelson instr)
              (List.map import_expr args)
        | "private_method", [Atom name] -> private_ ~line_no name
        | "map_function", [l; f] ->
            map_function ~line_no (import_expr l) (import_expr f)
        | "emit", [Atom tag; with_type; x] ->
            let tag = if tag = "" then None else Some tag in
            emit ~line_no tag (bool_of_sexp with_type) (import_expr x)
        | "lambda", [Atom name; List commands] ->
            import_lambda name line_no commands ~with_storage:None
              ~with_operations:false ~recursive:None
        | "lambda", [(List _ as effects); Atom name; List commands] ->
            let ({with_storage; with_operations} : effects) =
              import_effects effects
            in
            let with_storage = Option.map (fun x -> (x, None)) with_storage in
            import_lambda name line_no commands ~with_storage ~with_operations
              ~recursive:None
        | "lambda", [Atom id; Atom name; List commands] ->
            let name = if name = "" then "_x" ^ id else name in
            import_lambda name line_no commands ~with_storage:None
              ~with_operations:false ~recursive:None
        | ( "lambda_general"
          , [
              with_storage
            ; with_operations
            ; tstorage
            ; Atom name
            ; Atom recursive
            ; List commands
            ] ) ->
            let tstorage =
              match tstorage with
              | Atom "None" -> None
              | t -> Some (import_type t)
            in
            let with_storage =
              option_of_sexp Type.access_of_sexp with_storage
            in
            let with_storage =
              Option.map (fun a -> (a, tstorage)) with_storage
            in
            let recursive =
              match recursive with
              | "" -> None
              | f -> Some f
            in
            import_lambda name line_no commands ~with_storage
              ~with_operations:(bool_of_sexp with_operations)
              ~recursive
        | ( "lambda"
          , [
              Atom id
            ; with_storage
            ; with_operations
            ; tstorage
            ; Atom name
            ; List commands
            ] ) ->
            let name = if name = "" then "_x" ^ id else name in
            let tstorage =
              match tstorage with
              | Atom "None" -> None
              | t -> Some (import_type t)
            in
            let with_storage =
              option_of_sexp Type.access_of_sexp with_storage
            in
            let with_storage =
              Option.map (fun a -> (a, tstorage)) with_storage
            in
            import_lambda name line_no commands ~with_storage
              ~with_operations:(bool_of_sexp with_operations)
              ~recursive:None
        | "call", [lambda] -> call ~line_no (import_expr lambda) None
        | "call", [lambda; parameter] ->
            call ~line_no (import_expr lambda) (Some (import_expr parameter))
        | "apply_lambda", [lambda; parameter] ->
            apply_lambda ~line_no (import_expr parameter) (import_expr lambda)
        | "lshift_infix", [expression; shift] ->
            lshift_infix ~line_no (import_expr expression) (import_expr shift)
        | "rshift_infix", [expression; shift] ->
            rshift_infix ~line_no (import_expr expression) (import_expr shift)
        | "invert_bytes", [x] -> invert_bytes ~line_no (import_expr x)
        | "lshift_bytes", [expression; shift] ->
            lshift_bytes ~line_no (import_expr expression) (import_expr shift)
        | "rshift_bytes", [expression; shift] ->
            rshift_bytes ~line_no (import_expr expression) (import_expr shift)
        | "xor_infix", [e1; e2] ->
            Expr.xor_infix ~line_no (import_expr e1) (import_expr e2)
        | "xor_bytes", [e1; e2] ->
            Expr.xor_bytes ~line_no (import_expr e1) (import_expr e2)
        | "set_delegate", [x] -> set_delegate ~line_no (import_expr x)
        | "sapling_verify_update", [state; transaction] ->
            sapling_verify_update ~line_no (import_expr state)
              (import_expr transaction)
        | "transfer", [e1; e2; e3] ->
            transfer ~line_no ~arg:(import_expr e1) ~amount:(import_expr e2)
              ~destination:(import_expr e3)
        | "create_contract", [push; template_ref; baker; amount; storage] ->
            let push = bool_of_sexp push in
            let template_ref = import_qual_ident template_ref in
            let baker = import_expr baker in
            let amount = import_expr amount in
            let storage = import_expr storage in
            let e =
              ECreate_contract
                {
                  push
                ; template_ref
                ; baker
                ; balance = amount
                ; storage
                ; derived = U
                }
            in
            {e; line_no; et = U}
        | _, l ->
            failwith
              (Printf.sprintf "Expression format error (a %i) %s"
                 (List.length l) (to_string_hum input)))
    | x -> failwith ("Expression format error (b) " ^ to_string_hum x)
  and import_map_entry = function
    | List [k; v] -> (import_expr k, import_expr v)
    | e -> failwith (Printf.sprintf "import_map_entry: '%s'" (to_string e))
  and import_lambda name line_no body ~with_storage ~with_operations ~recursive
      =
    lambda ~line_no (mk_bound name)
      (import_block ~line_no env body)
      ~with_storage ~with_operations ~recursive
  in
  import_expr

and _import_meta_expr env =
  let import_expr = import_expr env in
  let rec import_meta_expr = function
    | List (Atom f :: args) as input -> (
        match (f, args) with
        | "meta_list", _loc :: l -> Meta.List (List.map import_meta_expr l)
        | "meta_map", _loc :: l ->
            let import_elem = function
              | List [Atom "elem"; k; v] -> (import_expr k, import_meta_expr v)
              | _ -> assert false
            in
            Meta.Map (List.map import_elem l)
        | "meta_expr", [e] -> Meta.Other (import_expr e)
        | "meta_view", [Atom name; _line_no] -> Meta.View name
        | _ -> failwith ("Meta expression format error " ^ to_string_hum input))
    | input -> failwith ("Meta expression format error " ^ to_string_hum input)
  in
  import_meta_expr

and import_command env =
  let import_type t = import_type t in
  let import_expr e = import_expr env e in
  let import_block ~line_no = import_block ~line_no env in
  let open Command in
  function
  | List (line_no :: Atom f :: args) as input -> (
      let line_no = line_no_of_sexp line_no in
      match (f, args) with
      | "call_init", [name; args; kargs] ->
          let name = import_qual_ident name in
          let call = import_call env args kargs in
          {c = CCall_init (name, call, U); ct = U; line_no}
      | "result", [x] -> result ~line_no (import_expr x)
      | "failwith", [x] -> sp_failwith ~line_no (import_expr x)
      | "never", [x] -> never ~line_no (import_expr x)
      | "verify", [x] -> verify ~line_no (import_expr x) None
      | "verify", [x; message] ->
          verify ~line_no (import_expr x) (Some (import_expr message))
      | "while_block", [e; List l] ->
          while_loop ~line_no (import_expr e) (import_block ~line_no l)
      | "for_group", [Atom name; e; List l] ->
          let e = import_expr e in
          for_loop ~line_no name e (import_block ~line_no l)
      | "match", [scrutinee; Atom constructor; Atom arg_name; List body] ->
          mk_match ~line_no (import_expr scrutinee)
            [(constructor, arg_name, import_block ~line_no body)]
      | "match_cases", [scrutinee; _arg; List cases] ->
          let parse_case = function
            | List
                [
                  line_no
                ; Atom "match"
                ; _
                ; Atom constructor
                ; Atom arg_name
                ; List body
                ] ->
                let line_no = line_no_of_sexp line_no in
                (constructor, arg_name, import_block ~line_no body)
            | input -> failwith ("Bad case parsing: " ^ to_string input)
          in
          let cases = List.map parse_case cases in
          mk_match ~line_no (import_expr scrutinee) cases
      | "set_type", [e; t] ->
          set_type ~line_no (import_expr e) (import_type env t)
      | "set_result_type", [List cs; t] ->
          set_result_type ~line_no (import_block ~line_no cs)
            (import_type env t)
      | "del_item", [e; k] -> del_item ~line_no (import_expr e) (import_expr k)
      | "update_set", [e; k; b] ->
          update_set ~line_no (import_expr e) (import_expr k) (bool_of_sexp b)
      | "trace", [e] -> trace ~line_no (import_expr e)
      | "bind", _ ->
          raise
            (SmartExcept
               [
                 `Text
                   "Command format error. sp.bind_block() can be used as \
                    follows:"
               ; `Br
               ; `Text "x = sp.bind_block('x'):"
               ; `Br
               ; `Text "with x:"
               ; `Br
               ; `Text "    ..."
               ; `Br
               ; `Text "... x ..."
               ; `Br
               ; `Text (to_string input)
               ])
      | _ -> failwith ("Command format error (a) " ^ to_string input))
  | List cs -> import_block ~line_no:None cs
  | input -> failwith ("Command format error (b) " ^ to_string input)

and import_call env args kargs =
  let args, kargs =
    match (args, kargs) with
    | List args, List kargs -> (args, kargs)
    | _ -> assert false
  in
  let args = List.map (import_expr env) args in
  let kargs =
    let f = function
      | List [Atom k; v] -> (k, import_expr env v)
      | _ -> assert false
    in
    List.map f kargs
  in
  match (args, kargs) with
  | args, [] -> Call_positional args
  | [], kargs -> Call_keyword kargs
  | _ :: _, _ :: _ -> assert false

and import_block ~line_no env cs =
  let import_expr = import_expr env in
  let import_command = import_command env in
  let import_block ~line_no = import_block ~line_no env in
  let open Command in
  let f = function
    | List [line_no; Atom "expr"; e] ->
        let line_no = line_no_of_sexp line_no in
        expr ~line_no @@ import_expr e
    | List [line_no; Atom "assign"; lhs; rhs] ->
        let line_no = line_no_of_sexp line_no in
        assign ~line_no (import_expr lhs) @@ import_expr rhs
    | List [line_no; Atom "if_block"; e; List tBlock] ->
        let line_no = line_no_of_sexp line_no in
        ifte ~line_no (import_expr e)
          (import_block ~line_no tBlock)
          (block ~line_no [])
    | List [line_no; Atom "if_then_else"; c; t; e] ->
        let line_no = line_no_of_sexp line_no in
        ifte ~line_no (import_expr c) (import_command t) (import_command e)
    | List [line_no; Atom "if_some_block"; e; Atom _; List tBlock] ->
        let line_no = line_no_of_sexp line_no in
        ifteSome ~line_no (import_expr e)
          (import_block ~line_no tBlock)
          (block ~line_no [])
    | List [line_no; Atom "set"; lhs; rhs] ->
        let line_no = line_no_of_sexp line_no in
        let lhs = import_expr lhs in
        let lhs = {lhs with line_no} in
        assign ~line_no lhs @@ import_expr rhs
    | x -> import_command x
  in
  Command.block ~line_no (List.map f cs)

type loaded_scenario = {
    scenario : Checked.scenario
  ; scenario_state : Interpreter.scenario_state
  ; warnings : smart_except list list
}

let import_kind = function
  | Atom "init" -> Init U
  | List [Atom "entrypoint"; Atom name] -> Entrypoint (name, U)
  | List [Atom "private"; Atom name; effects] ->
      let ({with_storage; with_operations} : effects) =
        import_effects effects
      in
      Private (name, with_storage, with_operations, U)
  | List [Atom "onchain_view"; Atom name] -> Onchain_view (name, U)
  | List [Atom "offchain_view"; Atom name] -> Offchain_view (name, U)
  | x -> failwith ("import_kind: " ^ to_string x)

let import_string ~err = function
  | Atom name -> name
  | x -> failwith ("import_string: " ^ err ^ ": " ^ to_string x)

let import_method = function
  | List [l; Atom "method"; kind; List params; body] ->
      let env = init_env () in
      {
        kind = import_kind kind
      ; parameters = List.map (import_string ~err:"method") params
      ; body = import_command env body
      ; line_no = line_no_of_sexp l
      }
  | x -> failwith ("import_method " ^ to_string x)

let import_module_elt = function
  | List [line_no; Atom "expr_def"; Atom name; rhs] ->
      let env = init_env () in
      Expr_def
        {
          name = mk_bound name
        ; rhs = import_expr env rhs
        ; line_no = line_no_of_sexp line_no
        }
  | List [line_no; Atom "fun_def"; Atom name; List params; effects; body] ->
      let env = init_env () in
      Fun_def
        {
          name = mk_bound name
        ; params = List.map (import_string ~err:"fun_def") params
        ; effects = import_effects effects
        ; body = import_command env body
        ; line_no = line_no_of_sexp line_no
        }
  | List [line_no; Atom "type_def"; Atom name; t] ->
      let env = init_env () in
      Type_def
        {name; rhs = import_type env t; line_no = line_no_of_sexp line_no}
  | List [line_no; Atom "contract_def"; Atom name; List parents; List methods]
    ->
      Contract_def
        {
          name
        ; parents =
            List.map
              (fun x -> (import_qual_ident x, Basics.Deco_unchecked.U))
              parents
        ; methods = List.map import_method methods
        ; derived = U
        ; line_no = line_no_of_sexp line_no
        }
  | x -> failwith ("import_module_elt " ^ to_string x)

let import_module = function
  | List (l :: Atom "module" :: Atom module_name :: elts) ->
      let elts = List.map import_module_elt elts in
      {
        module_name
      ; elts
      ; defs = U
      ; type_defs = U
      ; contract_defs = U
      ; mod_line_no = line_no_of_sexp l
      }
  | x -> failwith ("import_module " ^ to_string x)

let action_of_json ~primitives ~env x =
  let open Misc in
  let module M = (val json_getter x : JsonGetter) in
  let open M in
  let default f s v =
    match get s with
    | `Null -> v
    | _ -> f s
  in
  let import_expr_string s =
    import_expr env (Parsexp.Single.parse_string_exn (string s))
  in
  let _import_type_string s =
    import_type env (Parsexp.Single.parse_string_exn (string s))
  in
  let import_contract_id_string s =
    import_contract_id (Parsexp.Single.parse_string_exn (string s))
  in
  let parse_line_no s =
    line_no_of_sexp (Parsexp.Single.parse_string_exn (string s))
  in
  let of_seed id =
    match string id with
    | "none" -> None
    | seed ->
        if String.is_prefix "seed:" seed
        then
          let module P = (val primitives : Primitives.Primitives) in
          Some
            (Account
               (P.Crypto.account_of_seed
                  (String.sub seed 5 (String.length seed - 5))))
        else Some (Address (import_expr_string id))
  in
  let chain_id s =
    if string s = "" then None else Some (import_expr_string "chain_id")
  in
  let default_import_expr name =
    default (fun s -> Some (import_expr_string s)) name None
  in
  let import_context () =
    {
      sender = default of_seed "sender" None
    ; source = default of_seed "source" None
    ; chain_id = default chain_id "chain_id" None
    ; time = default_import_expr "time"
    ; level = default_import_expr "level"
    ; voting_powers = default_import_expr "voting_powers"
    }
  in
  match string "action" with
  | "instantiateContract" ->
      let id = import_contract_id_string "id" in
      let qi_module = Some (string "module") in
      let qi_ident = string "name" in
      let class_ = {qi_module; qi_ident} in
      let line_no = parse_line_no "line_no" in
      let args = Parsexp.Single.parse_string_exn (string "args") in
      let kargs = Parsexp.Single.parse_string_exn (string "kargs") in
      let call = import_call env args kargs in
      let balance = Parsexp.Single.parse_string_exn (string "balance") in
      let balance =
        match balance with
        | Atom "None" -> None
        | l -> Some (import_expr env l)
      in
      Instantiate_contract {id; class_; call; balance; line_no; derived = U}
  | "compute" ->
      let expression = import_expr_string "expression" in
      Compute
        {
          var = mk_bound @@ string "id"
        ; expression
        ; context = import_context ()
        ; line_no = parse_line_no "line_no"
        }
  | "message" ->
      let params = import_expr_string "params" in
      let line_no = parse_line_no "line_no" in
      let message = string "message" in
      let id = import_contract_id_string "id" in
      Message
        {
          id
        ; valid =
            default import_expr_string "valid"
              (Expr.literal ~line_no:None (Literal.bool true))
        ; exception_ =
            default (fun s -> Some (import_expr_string s)) "exception" None
        ; params
        ; line_no
        ; context = import_context ()
        ; amount =
            default import_expr_string "amount"
              (Expr.literal ~line_no:None (Literal.mutez Big_int.zero_big_int))
        ; message
        ; show = default bool "show" true
        ; export = default bool "export" true
        }
  | "error" -> ScenarioError {message = string "message"}
  | "html" ->
      Html
        {
          tag = string "tag"
        ; inner = string "inner"
        ; line_no = parse_line_no "line_no"
        }
  | "verify" ->
      Verify
        {
          condition = import_expr_string "condition"
        ; line_no = parse_line_no "line_no"
        }
  | "show" ->
      Show
        {
          expression = import_expr_string "expression"
        ; html = bool "html"
        ; stripStrings = bool "stripStrings"
        ; compile = bool "compile"
        ; line_no = parse_line_no "line_no"
        }
  | "dynamic_contract" ->
      DynamicContract
        {
          id =
            (match import_contract_id_string "dynamic_id" with
            | C_dynamic dyn -> dyn
            | _ -> assert false)
        ; model_id = import_contract_id_string "model_id"
        ; line_no = parse_line_no "line_no"
        }
  | "constant" -> (
      let primitives = primitives in
      let module P = (val primitives : Primitives.Primitives) in
      (* Get the constant hash if any was provided *)
      let hash =
        match Parsexp.Single.parse_string_exn (string "hash") |> to_string with
        | "None" -> None
        | hash -> Some hash
      in
      match Parsexp.Single.parse_string_exn (string "kind") |> to_string with
      | "value" ->
          Prepare_constant_value
            {
              var = mk_bound @@ string "id"
            ; hash
            ; expression = import_expr_string "expression"
            ; line_no = parse_line_no "line_no"
            }
      | s -> failwith ("Unexpected constant kind: " ^ s))
  | "flag" -> (
      let flags = string_list "flag" in
      match Config.parse_flag flags with
      | None ->
          raise
            (SmartExcept
               [
                 `Text "Flag parse errors"
               ; `Text
                   (String.concat "; "
                      (List.map (fun s -> Printf.sprintf "%S" s) flags))
               ; `Line (parse_line_no "line_no")
               ])
      | Some flag -> Add_flag {flag; line_no = parse_line_no "line_no"})
  | "mutation_test" ->
      let open Yojson.Safe in
      let f x =
        match x with
        | `List [`String name; `Int cid] -> (name, cid)
        | _ -> failwith "parse error:"
      in
      let scenarios =
        match Util.member "scenarios" x with
        | `List scenarios -> scenarios
        | x -> failwith ("parse error: not a list: " ^ to_string x)
      in
      let scenarios = List.map f scenarios in
      Mutation_test
        {
          scenarios
        ; show_paths = bool "show_paths"
        ; line_no = parse_line_no "line_no"
        }
  | "add_module" ->
      let module_ = Parsexp.Single.parse_string_exn (string "module") in
      let module_ = import_module module_ in
      Add_module {module_; line_no = parse_line_no "line_no"}
  | action -> failwith ("Unknown action: '" ^ action ^ "'")

let check_close_scenario config s =
  let _subst, s, warnings = Checker.check_scenario config s in
  let scenario_state = Interpreter.scenario_state ~chain_id:"" () in
  {scenario = s; scenario_state; warnings}

let acc_config config actions =
  let initial_config, _, actions =
    List.fold_left
      (fun (initial_config, config, actions) action ->
        let initial_config, config =
          match action with
          | Add_flag {flag; line_no} ->
              let config = Config.apply_flag config flag in
              let initial_config =
                if List.length actions <= 1
                then config
                else (
                  check_initial_flag ~line_no flag;
                  initial_config)
              in

              (initial_config, config)
          | _ -> (initial_config, config)
        in
        (initial_config, config, (config, action) :: actions))
      (config, config, []) actions
  in
  (initial_config, List.rev actions)

let load_scenario ~primitives config j =
  let open Yojson.Safe.Util in
  let module Printer = (val Printer.get ~config : Printer.Printer) in
  let j_actions = member "scenario" j in
  let shortname = Yojson.Safe.Util.(to_string (member "shortname" j)) in
  let longname = Yojson.Safe.Util.(to_string (member "longname" j)) in
  let env = init_env () in
  let actions =
    List.map
      (action_of_json ~primitives ~env)
      (Yojson.Safe.Util.to_list j_actions)
  in
  let config, actions = acc_config config actions in
  let s = {shortname; longname; actions} in
  check_close_scenario config s
