(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Basics
open Checked
open Utils

let collect_type subst =
  let open Type in
  let f t0 =
    match t0 with
    | Int {isNat = Variable x} ->
        Substitution.insert [(x, SBool (Hole.Variable {var_id = 0}))] subst
    | Record {row; var; layout} -> (
        (match layout with
        | Variable x ->
            let layout = comb_layout `Right (List.map fst row) in
            Substitution.insert [(x, SLayout (Value layout))] subst
        | Value _ -> ());
        match var with
        | Some x -> Substitution.insert [(x, SRow ([], None))] subst
        | _ -> ())
    | Variant {row; var; layout} -> (
        (match layout with
        | Variable x ->
            let layout = tree_layout (List.map fst row) in
            Substitution.insert [(x, SLayout (Value layout))] subst
        | Value _ -> ());
        match var with
        | Some x -> Substitution.insert [(x, SRow ([], None))] subst
        | _ -> ())
    | Lambda ({with_storage; with_operations}, _, _) -> (
        (match with_storage with
        | Variable x ->
            Substitution.insert [(x, SWithStorage (Value None))] subst
        | Value _ -> ());
        match with_operations with
        | Variable x -> Substitution.insert [(x, SBool (Value false))] subst
        | Value _ -> ())
    | _ -> ()
  in
  Type.cata f

let collect_talg subst =
  let f_texpr _line_no et e =
    collect_type subst et;
    match e with
    | ELiteral (Int {is_nat = Variable x}) ->
        Substitution.insert [(x, SBool (Hole.Variable {var_id = 0}))] subst
    | _ -> ()
  in
  let f_tcommand _line_no ct _c = collect_type subst ct in
  {f_texpr; f_tcommand; f_ttype = collect_type subst}

let collect_expr subst = cata_texpr (collect_talg subst)

let collect_command subst = cata_tcommand (collect_talg subst)

let on_type e =
  let subst = Substitution.empty () in
  collect_type subst e;
  Substitution.on_type subst e

let on_expr e =
  let subst = Substitution.empty () in
  collect_expr subst e;
  Substitution.on_expr subst e

let on_command c =
  let subst = Substitution.empty () in
  collect_command subst c;
  Substitution.on_command subst c

let on_contract_def (c : Checked.contract_def) =
  let subst = Substitution.empty () in
  collect_type subst @@ c.derived.tparameter;
  Substitution.on_contract_def subst c

let on_action s =
  let subst = Substitution.empty () in
  let _ =
    Checked.map_action_f (collect_expr subst) (collect_command subst)
      (collect_type subst) s
  in
  Substitution.on_action subst s
(*
let on_module  e =
  let subst = Substitution.empty () in
  collect_module  subst e;
  Substitution.on_module subst e
 *)

let on_scenario s =
  let subst = Substitution.empty () in
  let _ =
    Checked.map_scenario_f (collect_expr subst) (collect_command subst)
      (collect_type subst) s
  in
  Substitution.on_scenario subst s
