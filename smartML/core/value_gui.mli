(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

(** {1 Automatic User Interface Generation from Types} *)

type inputGuiResult = {
    gui : string
  ; get : bool -> Value.t
}

val inputGuiR :
  ?path:string list -> nextId:(unit -> string) -> Type.t -> inputGuiResult
