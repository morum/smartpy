(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

include module type of Michelson_base.Protocol

type exceptions =
  | FullDebug
  | Message
  | VerifyOrLine
  | DefaultLine
  | Line
  | DefaultUnit
  | Unit
[@@deriving eq, ord, show]

type t = {
    simplify : bool
  ; simplify_via_michel : bool
  ; erase_comments : bool
  ; disable_dup_check : bool
  ; contract_check_exception : bool
  ; view_check_exception : bool
  ; protocol : protocol
  ; exceptions : exceptions
  ; dump_michel : bool
  ; single_entrypoint_annotation : bool
  ; warn_unused : bool
  ; default_check_no_incoming_transfer : bool
}
[@@deriving eq, ord, show {with_path = false}, sexp]

val default : t

type bool_flag =
  | Contract_check_exception
  | Disable_dup_check
  | Dump_michel
  | Erase_comments
  | Simplify
  | Simplify_via_michel
  | Single_entrypoint_annotation
  | Warn_unused
  | View_check_exception
  | Default_check_no_incoming_transfer
  | Native
[@@deriving eq, ord, show]

type flag =
  | Bool_Flag of bool_flag * bool
  | Exceptions of exceptions
  | Protocol of protocol
[@@deriving eq, ord, show, sexp]

val parse_flag : string list -> flag option

val apply_flag : t -> flag -> t

val protocol_of_string : string -> protocol

val is_initial_flag : flag -> bool

val string_of_bool_flag : bool_flag -> string

val string_of_flag : flag -> string list
