(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Utils

type t = {var_id : int} [@@deriving eq, ord, show, sexp]

val mk : unit -> t

module Set : Set.S with type elt = t

module Map : Map.S with type key = t
