open Basics
open Checked

module TExpr = struct
  let unit = {e = ELiteral Literal.unit; et = Type.unit; line_no = None}
end

type _ kind =
  | Kind_contract : Checked.contract_def kind
  | Kind_entrypoint : Checked.entrypoint kind
  | Kind_view : Checked.view kind
  | Kind_command : command kind
  | Kind_expr : expr kind

type (_, _) step =
  | Step_entrypoint : string -> (Checked.contract_def, command) step
  | Step_view : string -> (Checked.contract_def, command) step
  | Step_seq_nth : int -> (command, command) step
  | Step_if_cond : (command, expr) step
  | Step_if_then : (command, command) step
  | Step_if_else : (command, command) step
  | Step_for_container : (command, expr) step
  | Step_for_body : (command, command) step
  | Step_arg1 : (expr, expr) step
  | Step_arg2 : (expr, expr) step
  | Step_arg3 : (expr, expr) step
  | Step_failwith : (command, expr) step

type (_, _) path =
  | Path_nil : ('a, 'a) path
  | Path_cons : ('a, 'b) step * ('b, 'c) path -> ('a, 'c) path

type _ some_path = Some_path : 'b kind * ('a, 'b) path -> 'a some_path

let show_step : type a b. (a, b) step -> string = function
  | Step_entrypoint name -> Printf.sprintf "entrypoint %S" name
  | Step_view name -> Printf.sprintf "view %S" name
  | Step_seq_nth n -> Printf.sprintf "seq_nth %d" n
  | Step_if_cond -> "if_cond"
  | Step_if_then -> "if_then"
  | Step_if_else -> "if_else"
  | Step_for_container -> "for_container"
  | Step_for_body -> "for_body"
  | Step_arg1 -> "arg1"
  | Step_arg2 -> "arg2"
  | Step_arg3 -> "arg3"
  | Step_failwith -> "failwith"

let rec show_path : type a b. (a, b) path -> string = function
  | Path_nil -> "."
  | Path_cons (s, p) -> show_step s ^ " > " ^ show_path p

let show_some_path (Some_path (_, p)) = show_path p

let path_cons : ('a, 'b) step -> ('b, 'c) path -> ('a, 'c) path =
 fun x y -> Path_cons (x, y)

let some_path_nil k = Some_path (k, Path_nil)

let some_path_cons : ('a, 'b) step -> 'b some_path -> 'a some_path =
 fun x (Some_path (k, y)) -> Some_path (k, path_cons x y)

let lens_step : type a b. (a, b) step -> (a, b) Lens.t =
  let open Syntax_lens in
  function
  | Step_entrypoint name -> Lens.(entrypoint name @. entrypoint_body)
  | Step_view name -> Lens.(view name @. view_body)
  | Step_seq_nth n -> seq_nth n
  | Step_if_cond -> if_cond
  | Step_if_then -> if_then
  | Step_if_else -> if_else
  | Step_for_container -> for_container
  | Step_for_body -> for_body
  | Step_arg1 -> arg1
  | Step_arg2 -> arg2
  | Step_arg3 -> arg3
  | Step_failwith -> failwith_

let rec lens_path : type a b. (a, b) path -> (a, b) Lens.t = function
  | Path_nil -> Lens.id
  | Path_cons (s, p) -> Lens.(lens_step s @. lens_path p)

let rec generate_paths_expr e =
  let ( / ) s ps = List.map (some_path_cons s) ps in
  let id x = x in
  let e' = map_expr_f generate_paths_expr generate_paths_command id e.e in
  let ps =
    match e' with
    | EPrim1 (_, x1) | EMPrim1 (_, x1) -> Step_arg1 / x1
    | EPrim2 (_, x1, x2) | EMPrim2 (_, x1, x2) ->
        (Step_arg1 / x1) @ (Step_arg2 / x2)
    | EPrim3 (_, x1, x2, x3) | EMPrim3 (_, x1, x2, x3) ->
        (Step_arg1 / x1) @ (Step_arg2 / x2) @ (Step_arg3 / x3)
    | _ -> []
  in
  some_path_nil Kind_expr :: ps

and generate_paths_command c =
  let ( / ) s ps = List.map (some_path_cons s) ps in
  let id x = x in
  let c' = map_command_f generate_paths_expr generate_paths_command id c.c in
  let ps =
    match c' with
    | CBlock cs ->
        let f i x = Step_seq_nth i / x in
        List.concat (List.mapi f cs)
    | CFor (_, c, body) -> (Step_for_container / c) @ (Step_for_body / body)
    | CIf (c, x, y) ->
        (Step_if_cond / c) @ (Step_if_then / x) @ (Step_if_else / y)
    | CFailwith x -> Step_failwith / x
    | _ -> []
  in
  let ps =
    ps
    @
    match c' with
    | CSet_type _ -> []
    | CSet_result_type _ -> []
    | CTrace _ -> []
    | _ -> [some_path_nil Kind_command]
  in
  ps

let generate_paths_contract (c : Checked.contract_def) =
  let on_ep (ep : Checked.entrypoint) =
    List.map
      (some_path_cons (Step_entrypoint ep.channel))
      (generate_paths_command ep.body)
  in
  let on_view (v : Checked.view) =
    List.map
      (some_path_cons (Step_view v.view_name))
      (generate_paths_command v.body)
  in
  List.flatten (List.map on_ep c.derived.entrypoints)
  @ List.flatten (List.map on_view c.derived.views)

let rec simplify_command = function
  | {c = CResult {e = ELiteral Unit}; ct; line_no} ->
      {c = CBlock []; ct; line_no}
  | {c = CBlock [x]} -> simplify_command x
  | x -> x

let mutate_command_at p x =
  let l = lens_path p in
  let c = Lens.get l x in
  let cs = [] in
  let cs = cs @ if Type.(equal c.ct unit) then [CBlock []] else [] in
  let cs = cs @ [CFailwith TExpr.unit] in
  let cs = List.map (fun x -> {c with c = x}) cs in
  let same c1 c2 =
    match (simplify_command c1, simplify_command c2) with
    | _ ->
        equal_command_modulo_line_nos (erase_types_command c1)
          (erase_types_command c2)
  in
  let cs = List.filter (fun x -> not (same x c)) cs in
  List.map (fun c -> Lens.set l c x) cs

let mutate_expr_at p x =
  let l = lens_path p in
  let e = Lens.get l x in
  let es =
    match e.e with
    | ELiteral (Int {i; is_nat}) ->
        let open Big_int in
        let zero = big_int_of_int 0 in
        let one = big_int_of_int 1 in
        let neg = sub_big_int zero in
        let xs = [zero; one; neg one; neg i] in
        let negative_allowed =
          match is_nat with
          | Value false -> true
          | _ -> false
        in
        let xs =
          if negative_allowed
          then xs
          else List.filter (fun x -> ge_big_int x zero_big_int) xs
        in
        List.map (fun i -> ELiteral (Literal.intOrNat is_nat i)) xs
    | _ when Type.(equal e.et int) ->
        let add x y =
          {e = EPrim2 (EAdd, x, y); et = Type.int; line_no = None}
        in
        let sub x y =
          {e = EPrim2 (ESub, x, y); et = Type.int; line_no = None}
        in
        let mk_int i =
          {
            e =
              ELiteral
                (Literal.intOrNat (Value false) (Big_int.big_int_of_int i))
          ; et = Type.int
          ; line_no = None
          }
        in
        [(add e (mk_int 1)).e; (sub e (mk_int 1)).e]
    | _ when Type.(equal e.et int) ->
        let add x y =
          {e = EPrim2 (EAdd, x, y); et = Type.int; line_no = None}
        in
        let sub x y =
          {e = EPrim2 (ESub, x, y); et = Type.int; line_no = None}
        in
        let mk_int i =
          {
            e =
              ELiteral
                (Literal.intOrNat (Value false) (Big_int.big_int_of_int i))
          ; et = Type.int
          ; line_no = None
          }
        in
        [(add e (mk_int 1)).e; (sub e (mk_int 1)).e]
    | _ when Type.(equal e.et bool) ->
        [
          ELiteral (Literal.bool false)
        ; ELiteral (Literal.bool true)
        ; EMPrim1 (Not, e)
        ]
    | _ -> []
  in
  let es =
    es
    @
    match e.e with
    | EPrim1 (_, x) when Type.equal x.et e.et -> [x.e]
    | EPrim2 (_, x, _) when Type.equal x.et e.et -> [x.e]
    | EPrim3 (_, x, _, _) when Type.equal x.et e.et -> [x.e]
    | EMPrim1 (_, x) when Type.equal x.et e.et -> [x.e]
    | EMPrim2 (_, x, _) when Type.equal x.et e.et -> [x.e]
    | EMPrim3 (_, x, _, _) when Type.equal x.et e.et -> [x.e]
    | _ -> []
  in
  let es =
    es
    @
    match e.e with
    | EPrim2 (_, _, x) when Type.equal x.et e.et -> [x.e]
    | EPrim3 (_, _, x, _) when Type.equal x.et e.et -> [x.e]
    | EMPrim2 (_, _, x) when Type.equal x.et e.et -> [x.e]
    | EMPrim3 (_, _, x, _) when Type.equal x.et e.et -> [x.e]
    | _ -> []
  in
  let es =
    es
    @
    match e.e with
    | EPrim3 (_, _, _, x) when Type.equal x.et e.et -> [x.e]
    | EMPrim3 (_, _, _, x) when Type.equal x.et e.et -> [x.e]
    | _ -> []
  in
  let es = List.map (fun x -> {e with e = x}) es in
  let same e1 e2 =
    equal_expr_modulo_line_nos (erase_types_expr e1) (erase_types_expr e2)
  in
  let es = List.filter (fun x -> not (same x e)) es in
  List.map (fun e -> Lens.set l e x) es

let mutate_at : type a. a some_path -> a -> a list =
 fun (Some_path (kind, p)) x ->
  match kind with
  | Kind_command -> mutate_command_at p x
  | Kind_expr -> mutate_expr_at p x
  | _ -> assert false

let mutate_contract_def ~show_paths (c : Checked.contract_def) =
  let ps = generate_paths_contract c in
  let f p = (show_some_path p, mutate_at p c) in
  let ps = List.map f ps in
  if show_paths
  then (
    Printf.printf "%d paths generated:\n" (List.length ps);
    List.iter
      (fun (p, cs) -> Printf.printf "  %3d mutations: %s\n" (List.length cs) p)
      ps);
  List.flatten (List.map (fun (p, xs) -> List.map (fun x -> (p, x)) xs) ps)

(* Finds the *first* contract in the scenario that matches the id. *)
let find_contract_in_scenario _contract_id (s : Import.loaded_scenario) =
  let f = function
    (* TODO Adapt for new syntax:
       | _config, New_contract {id = C_static i; contract}
         when i.static_id = contract_id -> Some {tcontract = contract}
    *)
    | _ -> None
  in
  match List.find_map f s.scenario.actions with
  | None -> failwith "find_contract_in_scenario: contract not found"
  | Some c -> c

let same_syntax c1 c2 =
  (* c1.template_id = c2.template_id *)
  Checked.equal_contract_def c1 c2

let find_contract_in_scenarios scenarios =
  let cs = List.map (fun (s, id) -> find_contract_in_scenario id s) scenarios in
  match cs with
  | [] -> failwith "mutation testing: empty scenario list"
  | c :: cs ->
      if List.for_all (same_syntax c) cs
      then c
      else
        failwith
          "mutation testing: must refer to same contract across scenarios"

let substitute_contract_in_scenario _cid _c_new (s : Import.loaded_scenario) =
  let f = function
    (* TODO Adapt for new syntax:
       | config, New_contract ({id = C_static i} as c) when i.static_id = cid ->
           (config, New_contract {c with contract = c_new.tcontract})
    *)
    | x -> x
  in
  {s with scenario = {s.scenario with actions = List.map f s.scenario.actions}}

let substitute_contract_in_scenarios c_new scenarios =
  let f (scenario, cid) = substitute_contract_in_scenario cid c_new scenario in
  List.map f scenarios

let _state0 =
  {
    balance = Big_int.big_int_of_int 0
  ; storage = None
  ; private_ = None
  ; baker = None
  ; metadata = []
  }

(** Mutate a list of scenarios uniformly, i.e. mutate the same
      contract in all of them. *)
let mutate_loaded_scenarios ~config ~show_paths scenarios =
  let module Printer = (val Printer.get ~config : Printer.Printer) in
  let c_old = find_contract_in_scenarios scenarios in
  let cs = mutate_contract_def ~show_paths c_old in
  let f (p, (c_new : Checked.contract_def)) =
    (if false
    then
      (* TODO print only the modified entrypoint or substituted expression/command. *)
      let _template =
        {
          name = "mutated"
        ; parents = []
        ; methods = []
        ; derived = c_new.derived
        ; line_no = None
        }
      in
      Printf.printf "<mutated instance>\n"
      (* Printf.printf "%s\n%s\n\n" p
         (Printer.tinstance_to_string {template; state = state0}));*));
    (p, c_new, substitute_contract_in_scenarios c_new scenarios)
  in
  List.map f cs

let _failwithk = Printf.kprintf failwith

(*
let parse_step : type a b. a kind -> b kind -> string list -> (a, b) Lens.t =
 fun a b xs ->
  match (a, b, xs) with
  | Kind_contract, Kind_entrypoint, "ep" :: xs -> Step_entrypoint "ep"
  | _, _, [] -> assert false
 *)

let _ = Kind_contract

let _ = Kind_entrypoint

let _ = Kind_view
