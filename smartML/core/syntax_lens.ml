open Basics
open Checked

let entrypoints =
  Lens.make (fun (c : Checked.contract_def) ->
      let derived = c.derived in
      let focus = derived.entrypoints in
      let zip entrypoints = {c with derived = {derived with entrypoints}} in
      {focus; zip})

let entrypoint name =
  let open Lens in
  let f ({channel} : Checked.entrypoint) = channel = name in
  let err = Printf.sprintf "No such entrypoint: %S" name in
  entrypoints @. find f @. some ~err

let entrypoint_body =
  Lens.make (fun (ep : Checked.entrypoint) ->
      {focus = ep.body; zip = (fun body -> {ep with body})})

let views =
  Lens.make (fun (c : Checked.contract_def) ->
      let derived = c.derived in
      let focus = derived.views in
      let zip views = {c with derived = {derived with views}} in
      {focus; zip})

let view name' =
  let open Lens in
  let f ({view_name} : Checked.view) = view_name = name' in
  let err = Printf.sprintf "No such view: %S" name' in
  views @. find f @. some ~err

let view_body =
  Lens.make (fun (v : Checked.view) ->
      {focus = v.body; zip = (fun body -> {v with body})})

let seq =
  Lens.make (function
    | {c = CBlock focus} as c ->
        {focus; zip = (fun focus -> {c with c = CBlock focus})}
    | _ -> failwith "Syntax_lens.seq")

let seq_nth n =
  let open Lens in
  seq @. nth n @. some ~err:"seq_nth"

let if_cond =
  Lens.make (function
    | {c = CIf (focus, a, b)} as c ->
        {focus; zip = (fun focus -> {c with c = CIf (focus, a, b)})}
    | _ -> failwith "if_cond")

let if_then =
  Lens.make (function
    | {c = CIf (a, focus, b)} as c ->
        {focus; zip = (fun focus -> {c with c = CIf (a, focus, b)})}
    | _ -> failwith "if_then")

let if_else =
  Lens.make (function
    | {c = CIf (a, b, focus)} as c ->
        {focus; zip = (fun focus -> {c with c = CIf (a, b, focus)})}
    | _ -> failwith "if_else")

let for_container =
  Lens.make (function
    | {c = CFor (x, focus, body)} as c ->
        {focus; zip = (fun focus -> {c with c = CFor (x, focus, body)})}
    | _ -> failwith "for_container")

let for_body =
  Lens.make (function
    | {c = CFor (x, xs, focus)} as c ->
        {focus; zip = (fun focus -> {c with c = CFor (x, xs, focus)})}
    | _ -> failwith "for_body")

let failwith_ =
  Lens.make (function
    | {c = CFailwith focus} as c ->
        {focus; zip = (fun focus -> {c with c = CFailwith focus})}
    | _ -> failwith "failwith")

let arg1 =
  Lens.make (function
    | {e = EMPrim1 (p, focus)} as e ->
        {focus; zip = (fun focus -> {e with e = EMPrim1 (p, focus)})}
    | {e = EMPrim2 (p, focus, x2)} as e ->
        {focus; zip = (fun focus -> {e with e = EMPrim2 (p, focus, x2)})}
    | {e = EMPrim3 (p, focus, x2, x3)} as e ->
        {focus; zip = (fun focus -> {e with e = EMPrim3 (p, focus, x2, x3)})}
    | {e = EPrim1 (p, focus)} as e ->
        {focus; zip = (fun focus -> {e with e = EPrim1 (p, focus)})}
    | {e = EPrim2 (p, focus, x2)} as e ->
        {focus; zip = (fun focus -> {e with e = EPrim2 (p, focus, x2)})}
    | {e = EPrim3 (p, focus, x2, x3)} as e ->
        {focus; zip = (fun focus -> {e with e = EPrim3 (p, focus, x2, x3)})}
    | _ -> failwith "arg1")

let arg2 =
  Lens.make (function
    | {e = EMPrim2 (p, x1, focus)} as e ->
        {focus; zip = (fun focus -> {e with e = EMPrim2 (p, x1, focus)})}
    | {e = EMPrim3 (p, x1, focus, x2)} as e ->
        {focus; zip = (fun focus -> {e with e = EMPrim3 (p, x1, focus, x2)})}
    | {e = EPrim2 (p, x1, focus)} as e ->
        {focus; zip = (fun focus -> {e with e = EPrim2 (p, x1, focus)})}
    | {e = EPrim3 (p, x1, focus, x2)} as e ->
        {focus; zip = (fun focus -> {e with e = EPrim3 (p, x1, focus, x2)})}
    | _ -> failwith "arg2")

let arg3 =
  Lens.make (function
    | {e = EMPrim3 (p, x1, x2, focus)} as e ->
        {focus; zip = (fun focus -> {e with e = EMPrim3 (p, x1, x2, focus)})}
    | {e = EPrim3 (p, x1, x2, focus)} as e ->
        {focus; zip = (fun focus -> {e with e = EPrim3 (p, x1, x2, focus)})}
    | _ -> failwith "arg3")
