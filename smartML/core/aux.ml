(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Basics

let address_of_contract_id ~html contract_id entrypoint =
  let address =
    match contract_id with
    | C_static {static_id} ->
        Utils.Bs58.address_of_contract_id ~static:true static_id entrypoint
    | C_dynamic {dynamic_id} ->
        Utils.Bs58.address_of_contract_id ~static:false dynamic_id entrypoint
  in
  if html
  then Printf.sprintf "<span class='address'>%s</span>" address
  else address
