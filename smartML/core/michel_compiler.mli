(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Michelson

type stack =
  | Stack_ok of string option list
  | Stack_failed
[@@deriving show]

val compile_expr : stack -> Michel.Expr.expr -> instr list * stack

val compile_contract :
     protocol:Config.protocol
  -> ?storage:literal
  -> views:tinstr Michelson.view list
  -> Michel.Typing.checked_precontract
  -> Michelson.tcontract
