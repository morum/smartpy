(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

val of_mtype : Michelson.mtype -> Michel.Type.ty

val decompile_contract :
     Michel.Transformer.state
  -> Michelson.tcontract
  -> Michel.Expr.expr Michel.Expr.precontract
