(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Utils
open Control
open Basics
open Checked
open Primitives

type scenario_state = {
    constants :
      (string (* constant hash *), value (* constant value *)) Hashtbl.t
  ; contracts : (contract_id, instance) Hashtbl.t
  ; variables : (Checked.bound, tvalue) Hashtbl.t
  ; addresses : (contract_id, string) Hashtbl.t
  ; rev_addresses : (string, contract_id) Hashtbl.t
  ; next_dynamic_address_id : int ref
  ; mutable step : int
  ; mutable time : Bigint.t
  ; mutable level : Bigint.t
  ; mutable chain_id : string
  ; mutable voting_powers : (string * Bigint.t) list
}

let scenario_state ?(time = Bigint.zero_big_int) ?(level = Bigint.zero_big_int)
    ~chain_id ?(voting_powers = []) () =
  {
    constants = Hashtbl.create 5
  ; contracts = Hashtbl.create 5
  ; variables = Hashtbl.create 5
  ; addresses = Hashtbl.create 5
  ; rev_addresses = Hashtbl.create 5
  ; next_dynamic_address_id = ref 0
  ; step = 0
  ; time
  ; level
  ; chain_id
  ; voting_powers
  }

let copy_scenario_state
    {
      constants
    ; contracts
    ; variables
    ; addresses
    ; rev_addresses
    ; next_dynamic_address_id
    ; step
    ; time
    ; level
    ; chain_id
    ; voting_powers
    } =
  let contracts = Hashtbl.copy contracts in
  let variables = Hashtbl.copy variables in
  let addresses = Hashtbl.copy addresses in
  let rev_addresses = Hashtbl.copy rev_addresses in
  let next_dynamic_address_id = ref !next_dynamic_address_id in
  {
    constants
  ; contracts
  ; variables
  ; addresses
  ; rev_addresses
  ; next_dynamic_address_id
  ; step
  ; time
  ; level
  ; chain_id
  ; voting_powers
  }

let update_contract_address ?address scenario_state id =
  let address =
    match address with
    | None -> Aux.address_of_contract_id ~html:false id None
    | Some address -> address
  in
  Hashtbl.replace scenario_state.addresses id address;
  Hashtbl.replace scenario_state.rev_addresses address id;
  address

let get_contract_address scenario_state id =
  match Hashtbl.find_opt scenario_state.addresses id with
  | Some addr -> addr
  | None -> update_contract_address scenario_state id

let update_contract_address ?address scenario_state id =
  let (_ : string) = update_contract_address ?address scenario_state id in
  ()

let update_contract ?address scenario_state id contract =
  Hashtbl.replace scenario_state.contracts id contract;
  update_contract_address ?address scenario_state id

let impossible fmt =
  let fail s = failwith ("Internal error. Please report.\n" ^ s) in
  Printf.ksprintf fail fmt

let fail fmt =
  let fail s = failwith ("Interpreter error:\n" ^ s) in
  Printf.ksprintf fail fmt

let exec_fail_string msg xs = raise (ExecFailure (Value.string msg, xs))

type in_contract =
  | No_contract
  | In_contract of contract_id

type context = {
    sender : string option
  ; source : string option
  ; amount : Bigint.t
  ; line_no : line_no
  ; contract_id : in_contract
  ; operations : Checked.bound option
  ; private_variables : (string * value) list
}

let context0 =
  {
    sender = None
  ; source = None
  ; amount = Bigint.zero_big_int
  ; line_no = None
  ; contract_id = No_contract
  ; operations = None
  ; private_variables = []
  }

module type Args = sig
  val config : Config.t

  module P : Primitives.Primitives

  val scenario_state : scenario_state

  module Printer : Printer.Printer
end

module Main (A : Args) = struct
  open A

  let protocol = config.protocol

  let primitives = (module P : Primitives.Primitives)

  type root = R_local of Checked.bound [@@deriving show {with_path = false}]

  type step =
    | S_attr of string
    | S_item_map of value
    | S_item_list of int
  [@@deriving show {with_path = false}]

  type path = {
      root : root
    ; steps : step list
  }
  [@@deriving show {with_path = false}]

  let string_of_step = function
    | S_attr attr -> "." ^ attr
    | S_item_list i -> Printf.sprintf "[%d]" i
    | S_item_map key -> Printf.sprintf "[%s]" (Printer.value_to_string key)

  let _string_of_steps steps = String.concat "" (List.map string_of_step steps)

  let extend_path {root; steps} steps' = {root; steps = steps @ steps'}

  let ( @. ) = Lens.( @. )

  type variable_value =
    | Iter of (value * path option)
    | Var of value
    | Constant of value
  [@@deriving show {with_path = false}]

  type variables = (Checked.bound * variable_value) list [@@deriving show]

  let _ = show_variables

  let find_instance id =
    match Hashtbl.find_opt scenario_state.contracts id with
    | Some c -> c
    | None ->
        fail "Cannot find contract for id %s" (Printer.string_of_contract_id id)

  let get_current_contract_id env : contract_id =
    match env.contract_id with
    | No_contract -> fail "No current contract."
    | In_contract id -> id

  let get_current_address env =
    let id = get_current_contract_id env in
    get_contract_address scenario_state id

  let get_current_instance env : instance =
    find_instance @@ get_current_contract_id env

  let lens_var =
    Lens.bi
      (function
        | Var x -> x
        | _ -> impossible "not a heap reference")
      (fun x -> Var x)

  let lens_of_root : root -> (variables, value) Lens.t =
    let open Lens in
    function
    | R_local x ->
        assoc_exn ~equal:( = ) ~key:x ~err:("local not found: " ^ x.bound)
        @. lens_var

  (** Convert a step into a lens that points to an optional value,
   accompanied by an error message for when the value is missing. *)
  let lens_of_step ~line_no =
    let line_no = string_of_line_no line_no in
    function
    | S_item_list i ->
        ( Value.lens_list_nth i
        , Printf.sprintf "Line %s: Index '%d' out of range." line_no i )
    | S_item_map key ->
        ( Value.lens_map_at ~key
        , Printf.sprintf "Line %s: Key '%s' not found." line_no
            (Printer.value_to_string key) )
    | S_attr attr ->
        ( Value.lens_record_at ~attr
        , Printf.sprintf "Line %s: Impossible: attribute not found" line_no )

  let rec lens_of_steps ~line_no acc = function
    | [s] ->
        let l, err = lens_of_step ~line_no s in
        (acc @. l, err)
    | s :: steps ->
        let l, err = lens_of_step ~line_no s in
        lens_of_steps ~line_no (acc @. l @. Lens.some ~err) steps
    | [] -> (Lens.option ~err:"lens_of_steps", "lens_of_steps")

  let lens_of_path ~line_no {root; steps} =
    let l, err = lens_of_steps ~line_no Lens.id steps in
    (lens_of_root root @. l, err)

  let add_variable (x : Checked.bound) value (vars : variables) : variables =
    match List.assoc_opt x vars with
    | Some _ -> impossible "Variable %S already defined" x.bound
    | None -> (x, value) :: vars

  let add_variables vs = List.fold_right (uncurry add_variable) vs

  let assert_unit = function
    | {v = Literal Unit} -> ()
    | _ -> impossible "unexpected non-unit value from command"

  let mk_key_value k v = Value.record [("key", k); ("value", v)]

  let get_constant_value hash =
    match Hashtbl.find_opt scenario_state.constants hash with
    | Some value -> value
    | None ->
        raise
          (SmartExcept
             [`Text "Could not find any constant with hash: "; `Text hash])

  let un_int err = function
    | {v = Literal (Int {i})} -> i
    | _ -> err ()

  let un_bool err = function
    | {v = Literal (Bool x)} -> x
    | _ -> err ()

  let un_bytes err = function
    | {v = Literal (Bytes x)} -> x
    | _ -> err ()

  let un_tuple err = function
    | {v = Tuple vs} -> vs
    | _ -> err ()

  let un_record err = function
    | {v = Record vs} -> vs
    | _ -> err ()

  let un_operation = function
    | {v = Operation op} -> op
    | _ -> impossible "not an operation"

  let interpret_mprim0 env e0 : _ Michelson_base.Primitive.prim0 -> _ = function
    | Sender -> (
        match env.sender with
        | Some address -> Value.literal (Literal.address address)
        | None -> raise (SmartExcept [`Text "Sender is undefined "; `Expr e0]))
    | Source -> (
        match env.source with
        | Some address -> Value.literal (Literal.address address)
        | None -> raise (SmartExcept [`Text "Source is undefined "; `Expr e0]))
    | Amount -> Value.mutez env.amount
    | Balance -> Value.mutez (get_current_instance env).state.balance
    | Level -> Value.nat scenario_state.level
    | Now -> Value.timestamp scenario_state.time
    | Self None -> Value.contract (get_current_address env)
    | Self (Some entrypoint) ->
        Value.contract ~entrypoint (get_current_address env)
    | Self_address -> Value.literal (Literal.address (get_current_address env))
    | Chain_id -> Value.chain_id scenario_state.chain_id
    | Total_voting_power ->
        Value.nat
          (List.fold_left
             (fun acc (_, v) -> Big_int.add_big_int acc v)
             Big_int.zero_big_int scenario_state.voting_powers)
    | Sapling_empty_state {memo} ->
        Value.literal (Literal.sapling_test_state memo [])
    | Unit_ -> Value.literal Literal.unit
    | None_ _ -> Value.none
    | Nil _ -> Value.build_list []
    | Empty_set _ -> Value.set []
    | Empty_map _ -> Value.map []
    | Empty_bigmap _ -> Value.map []

  let interpret_mprim1 p x =
    let impossible () =
      impossible "interpret_mprim1 %s %s"
        Michelson_base.(Primitive.show_prim1 Type.pp_mtype p)
        (show_value x)
    in
    let un_int = un_int impossible in
    let hash_algo h x =
      match x.v with
      | Literal (Bytes b) -> Value.bytes (h b)
      | _ -> impossible ()
    in
    match p with
    | Abs -> Value.nat (Big_int.abs_big_int (un_int x))
    | IsNat ->
        let x = un_int x in
        if 0 <= Big_int.compare_big_int x Big_int.zero_big_int
        then Value.some (Value.nat x)
        else Value.none
    | Hash_key ->
        let public_key =
          match x.v with
          | Literal (Key k) -> k
          | _ -> impossible ()
        in
        Value.key_hash (P.Crypto.hash_key public_key)
    | Blake2b -> hash_algo Utils.Crypto.blake2b x
    | Sha256 -> hash_algo Utils.Crypto.sha256 x
    | Sha512 -> hash_algo Utils.Crypto.sha512 x
    | Keccak -> hash_algo Utils.Crypto.keccak x
    | Sha3 -> hash_algo Utils.Crypto.sha3 x
    | _ -> impossible ()

  let interpret_mprim2 p (x1, tx1) (x2, _tx2) =
    let impossible () =
      impossible "interpret_mprim2 %s %s %s"
        Michelson_base.(Primitive.show_prim2 Type.pp_mtype p)
        (show_value x1) (show_value x2)
    in
    match (p : _ Michelson_base.Primitive.prim2) with
    | Lsl -> Value.shift_left x1 x2
    | Lsr -> Value.shift_right x1 x2
    | Sub_mutez -> Value.sub_mutez x1 x2
    | Mul -> Value.mul ~primitives tx1 x1 x2
    | Add -> Value.add ~primitives tx1 x1 x2
    | Compare ->
        Value.int (Big_int.big_int_of_int (Value.compare_typed tx1 x1 x2))
    | _ -> impossible ()

  let interpret_mprim3 p x1 x2 x3 =
    let impossible () =
      impossible "interpret_mprim3 %s %s %s %s"
        Michelson_base.(Primitive.show_prim3 p)
        (show_value x1) (show_value x2) (show_value x3)
    in
    match (p : Michelson_base.Primitive.prim3) with
    | Check_signature ->
        let pk_expr, sig_expr, msg_expr = (x1, x2, x3) in
        Misc.Dbg.on := false;
        let public_key =
          match pk_expr.v with
          | Literal (Key k) -> k
          | _ -> impossible ()
        in
        let signature =
          match sig_expr.v with
          | Literal (Signature s) -> s
          | _ -> impossible ()
        in
        let msg =
          match msg_expr.v with
          | Literal (Bytes b) -> b
          | _ -> impossible ()
        in
        Value.bool (P.Crypto.check_signature ~public_key ~signature msg)
    | Open_chest -> (
        let chest_key, chest, time = (x1, x2, x3) in
        let chest_key =
          match chest_key.v with
          | Literal (Chest_key b) -> b
          | _ -> impossible ()
        in
        let chest =
          chest.v |> function
          | Literal (Chest b) -> b
          | _ -> impossible ()
        in
        let time =
          time.v |> function
          | Literal (Int i) -> Bigint.int_of_big_int i.i
          | _ -> impossible ()
        in
        let result =
          P.Timelock.open_chest
            Hex.(show (of_string chest))
            Hex.(show (of_string chest_key))
            time
        in
        match result with
        | Correct x -> Value.(variant "Left" (bytes (Hex.to_string (`Hex x))))
        | Bogus_cipher -> Value.(variant "Right" (bool false))
        | Bogus_opening -> Value.(variant "Right" (bool true)))
    | _ -> impossible ()

  let interpret_prim0 (e0 : expr) p =
    match p with
    | ECst_contract {address; entrypoint} -> Value.contract ?entrypoint address
    | EConstant (hash, _) -> get_constant_value hash
    | EContract_address (id, entrypoint) ->
        Value.address ?entrypoint (get_contract_address scenario_state id)
    | EContract_balance id -> (
        match Hashtbl.find_opt scenario_state.contracts id with
        | Some t -> Value.mutez t.state.balance
        | None ->
            fail "Cannot find contract for id %s"
              (Printer.string_of_contract_id id))
    | EContract_baker id -> (
        let {state = {baker}} = find_instance id in
        match baker with
        | Some baker -> Value.some (Value.key_hash baker)
        | None -> Value.none)
    | EContract_data id -> (
        let t = find_instance id in
        match t.state.storage with
        | Some storage -> storage
        | None ->
            raise
              (SmartExcept
                 [
                   `Text "Missing contract storage for id: "
                 ; `Text (Printer.string_of_contract_id id)
                 ; `Br
                 ; `Line e0.line_no
                 ]))
    | EContract_private id -> (
        let t = find_instance id in
        match t.state.private_ with
        | Some p -> p
        | None ->
            raise
              (SmartExcept
                 [
                   `Text "Missing contract private for id: "
                 ; `Text (Printer.string_of_contract_id id)
                 ; `Br
                 ; `Line e0.line_no
                 ]))
    | EContract_typed (id, entrypoint) ->
        Value.contract ?entrypoint (get_contract_address scenario_state id)
    | EAccount_of_seed {seed} ->
        let account = P.Crypto.account_of_seed seed in
        let seed = Value.string seed in
        let pkh = Value.key_hash account.pkh in
        let address = Value.address account.pkh in
        let pk = Value.key account.pk in
        let sk = Value.secret_key account.sk in
        Value.record
          [
            ("seed", seed)
          ; ("address", address)
          ; ("public_key", pk)
          ; ("public_key_hash", pkh)
          ; ("secret_key", sk)
          ]

  let not_char c = Char.chr @@ (255 - Char.code c)

  let not_bytes = Bytes.map not_char

  let not_string x = Bytes.to_string @@ not_bytes @@ Bytes.of_string x

  let bytes_to_nat x =
    let open Big_int in
    let r = ref zero_big_int in
    let step c =
      r := add_int_big_int (int_of_char c) (mult_int_big_int 256 !r)
    in
    Bytes.iter step x;
    !r

  let sign_of_bytes x =
    let l = Bytes.length x in
    if l > 0
    then if int_of_char (Bytes.get x 0) land 128 <> 0 then `Neg else `NonNeg
    else `NonNeg

  let bytes_to_int x =
    let open Big_int in
    let l = Bytes.length x in
    let sign = sign_of_bytes x in
    match sign with
    | `NonNeg -> bytes_to_nat x
    | `Neg ->
        let a = power_int_positive_int 2 (8 * l) in
        Big_int.sub_big_int (bytes_to_nat x) a

  let bytes_cons x xs = Bytes.(cat (make 1 x) xs)

  let nat_to_bytes n =
    let open Big_int in
    let big256 = big_int_of_int 256 in
    let rec aux x acc =
      if sign_big_int x = 0
      then acc
      else
        let q, r = quomod_big_int x big256 in
        let r = char_of_int @@ int_of_big_int r in
        aux q (bytes_cons r acc)
    in
    aux n Bytes.empty

  let bytes_ones_complement =
    Bytes.map @@ fun c -> char_of_int (255 - int_of_char c)

  let bytes_succ x = nat_to_bytes @@ Big_int.succ_big_int @@ bytes_to_nat x

  let twos_complement n = bytes_succ @@ bytes_ones_complement n

  let int_to_bytes x =
    let open Big_int in
    if sign_big_int x >= 0
    then
      let r = nat_to_bytes x in
      if sign_of_bytes r = `NonNeg then r else bytes_cons (char_of_int 0) r
    else
      let r = nat_to_bytes @@ Big_int.abs_big_int x in
      twos_complement r

  let interpret_prim1 e0 p (x, tx) =
    let scenario_vars = TBoundMap.of_hashtbl scenario_state.variables in
    let impossible () =
      impossible "interpret_prim1 %s %s" (show_prim1 Type.pp p) (show_value x)
    in
    let un_int = un_int impossible in
    let un_bool = un_bool impossible in
    let un_bytes = un_bytes impossible in
    match p with
    | ENot -> Value.bool (not (un_bool x))
    | EInvert ->
        let open Big_int in
        Value.int @@ sub_big_int (big_int_of_int (-1)) @@ un_int x
    | EInvert_bytes -> Value.bytes @@ not_string @@ un_bytes x
    | ETo_nat -> (
        match x.v with
        | Literal (Bytes b) -> Value.nat @@ bytes_to_nat @@ Bytes.of_string b
        | _ -> impossible ())
    | ETo_int -> (
        match x.v with
        | Literal (Bls12_381_fr hex) ->
            Value.int
              (Bigint.of_string ~msg:"interpreter"
                 (try P.Bls12.convertFrToInt Hex.(show (of_string hex))
                  with _ -> impossible ()))
        | Literal (Int {i}) -> Value.int i
        | Literal (Bytes b) -> Value.int @@ bytes_to_int @@ Bytes.of_string b
        | _ -> impossible ())
    | ETo_bytes -> (
        match (x.v, Type.unF tx) with
        | Literal (Int {i}), Int {isNat = Hole.Value false} ->
            Value.bytes @@ Bytes.to_string @@ int_to_bytes i
        | Literal (Int {i}), Int {isNat = Hole.Value true} ->
            Value.bytes @@ Bytes.to_string @@ nat_to_bytes i
        | _ -> impossible ())
    | ENeg -> (
        match x.v with
        | Literal (Bls12_381_g1 hex) ->
            Value.bls12_381_g1
              (Hex.to_string
                 (`Hex
                   (try P.Bls12.negateG1 Hex.(show (of_string hex))
                    with _ -> impossible ())))
        | Literal (Bls12_381_g2 hex) ->
            Value.bls12_381_g2
              (Hex.to_string
                 (`Hex
                   (try P.Bls12.negateG2 Hex.(show (of_string hex))
                    with _ -> impossible ())))
        | Literal (Bls12_381_fr hex) ->
            Value.bls12_381_fr
              (Hex.to_string
                 (`Hex
                   (try P.Bls12.negateFr Hex.(show (of_string hex))
                    with _ -> impossible ())))
        | Literal (Int {i}) -> Value.int (Big_int.minus_big_int i)
        | _ -> impossible ())
    | ESign ->
        let x = un_int x in
        let result = Big_int.compare_big_int x Big_int.zero_big_int in
        Value.int (Bigint.of_int result)
    | ESum -> (
        let t =
          match Type.unF tx with
          | T1 (List, t) -> t
          | _ -> impossible ()
        in
        match x.v with
        | List l | Set l ->
            List.fold_left
              (fun x y -> Value.plus ~primitives t x y)
              (Value.zero_of_type e0.et) l
        | Map l ->
            List.fold_left
              (fun x (_, y) -> Value.plus ~primitives t x y)
              (Value.zero_of_type e0.et) l
        | _ -> impossible ())
    | EList_rev -> (
        match x.v with
        | Basics.List l -> Value.build_list (List.rev l)
        | _ -> impossible ())
    | EList_items rev -> (
        match x.v with
        | Basics.Map l ->
            Value.build_list
              ((if rev then List.rev_map else List.map)
                 (fun (k, v) -> Value.record [("key", k); ("value", v)])
                 l)
        | _ -> impossible ())
    | EList_keys rev -> (
        match x.v with
        | Basics.Map l ->
            Value.build_list ((if rev then List.rev_map else List.map) fst l)
        | _ -> impossible ())
    | EList_values rev -> (
        match x.v with
        | Basics.Map l ->
            Value.build_list ((if rev then List.rev_map else List.map) snd l)
        | _ -> impossible ())
    | EList_elements rev -> (
        match x.v with
        | Basics.Set l -> Value.build_list (if rev then List.rev l else l)
        | _ -> impossible ())
    | EPack -> (
        try
          x |> Value.typecheck tx
          |> Compiler.pack_value ~config ~scenario_vars
          |> Value.bytes
        with Data_encoding.Binary.Write_error write_error ->
          exec_fail_string "Pack error"
            [
              `Text "Could not pack bytes due to invalid content"
            ; `Br
            ; `Expr e0
            ; `Text
                (Format.asprintf "%a " Data_encoding.Binary.pp_write_error
                   write_error)
            ; `Line e0.line_no
            ])
    | EUnpack t -> (
        match x.v with
        | Literal (Bytes b) -> (
            try Compiler.unpack_value ~config t b |> Value.some
            with Data_encoding.Binary.Read_error read_error ->
              exec_fail_string "Unpack error"
                [
                  `Text
                    (Format.asprintf
                       "Could not unpack bytes (%s) due to invalid content: %a."
                       Hex.(show (of_string b))
                       Data_encoding.Binary.pp_read_error read_error)
                ; `Br
                ; `Expr e0
                ; `Line e0.line_no
                ])
        | _ -> impossible ())
    | EConcat_list -> (
        let vals =
          match x.v with
          | List sl ->
              List.map
                (fun sb ->
                  match sb.v with
                  | Literal (String s) | Literal (Bytes s) -> s
                  | _ -> impossible ())
                sl
          | _ -> impossible ()
        in
        match Type.unF e0.et with
        | T0 String -> Value.string (String.concat "" vals)
        | T0 Bytes -> Value.bytes (String.concat "" vals)
        | _ -> impossible ())
    | ESize -> (
        let result length = Value.nat (Bigint.of_int length) in
        match x.v with
        | Literal (String s) | Literal (Bytes s) -> result (String.length s)
        | List l | Set l -> result (List.length l)
        | Map l -> result (List.length l)
        | _ -> impossible ())
    | EFst ->
        let vs = un_tuple impossible x in
        assert (List.length vs = 2);
        List.nth vs 0
    | ESnd ->
        let vs = un_tuple impossible x in
        assert (List.length vs = 2);
        List.nth vs 1
    | EAddress -> (
        match x.v with
        | Contract {address; entrypoint} ->
            Value.literal (Literal.address ?entrypoint address)
        | _ -> impossible ())
    | EImplicit_account -> (
        match x.v with
        | Literal (Key_hash key_hash) -> Value.contract key_hash
        | _ -> impossible ())
    | ESet_delegate ->
        let baker = Value.un_baker x in
        (* The delegate must be registered *)
        (match baker with
        | None -> ()
        | Some key_hash ->
            let registered =
              List.exists
                (fun (k, _) -> k = key_hash)
                scenario_state.voting_powers
            in
            if not registered
            then failwith (Printf.sprintf "Unregistered delegate (%s)" key_hash));
        Value.operation (SetDelegate baker)
    | EType_annotation _ -> x
    | EIs_variant name -> (
        match x.v with
        | Variant (cons, _) -> Value.bool (cons = name)
        | _ -> impossible ())
    | ERead_ticket -> (
        match x.v with
        | Ticket (address, content, amount) ->
            let address = Value.literal (Literal.address address) in
            let amount = Value.nat amount in
            Value.tuple [Value.tuple [address; content; amount]; x]
        | _ -> impossible ())
    | EJoin_tickets -> (
        match x.v with
        | Tuple
            [
              {v = Ticket (ticketer1, content1, amount1)}
            ; {v = Ticket (ticketer2, content2, amount2)}
            ] ->
            if ticketer1 = ticketer2 && Value.equal content1 content2
            then
              Value.some
                (Value.ticket ticketer1 content1
                   (Bigint.add_big_int amount1 amount2))
            else impossible ()
        | _ -> impossible ())
    | EPairing_check ->
        let vals =
          match x.v with
          | List sl ->
              List.map
                (fun sb ->
                  match sb.v with
                  | Tuple
                      [
                        {v = Literal (Bls12_381_g1 i1)}
                      ; {v = Literal (Bls12_381_g2 i2)}
                      ] -> (Hex.(show (of_string i1)), Hex.(show (of_string i2)))
                  | _ -> impossible ())
                sl
          | _ -> impossible ()
        in
        Value.bool (try P.Bls12.pairingCheck vals with _ -> impossible ())
    | EVoting_power ->
        let key_hash =
          match x.v with
          | Literal (Key_hash key_hash) -> key_hash
          | _ -> impossible ()
        in
        let res =
          List.find_opt
            (fun (k, _) ->
              Value.equal (Value.key_hash k) (Value.key_hash key_hash))
            scenario_state.voting_powers
        in
        Value.nat
          (match res with
          | Some (_, v) -> v
          | None -> Big_int.zero_big_int)
    | EConvert ->
        let open Michelson_interpreter in
        let x = mvalue_of_value ~protocol tx x in
        value_of_mvalue config e0.et x
    | EStatic_view _ -> impossible ()
    | EEmit (tag, _with_type) -> Value.operation (Event (tag, tx, x))

  let interpret_prim2 env (_e : Checked.expr) p (x1, tx1) (x2, tx2) =
    let impossible () =
      impossible "interpret_prim2 %s %s %s" (show_prim2 Type.pp p)
        (show_value x1) (show_value x2)
    in
    let un_int = un_int impossible in
    match p with
    | EGet_opt -> (
        match x2.v with
        | Map m -> (
            match List.assoc_opt ~equal:Value.equal x1 m with
            | Some v -> Value.some v
            | None -> Value.none)
        | _ -> impossible ())
    | EEq -> Value.bool (Value.equal x1 x2)
    | ENeq -> Value.bool (not (Value.equal x1 x2))
    | EAdd -> Value.plus ~primitives tx1 x1 x2
    | EMul_homo -> Value.mul ~primitives tx1 x1 x2
    | EMod -> Value.e_mod tx1 x1 x2
    | EDiv -> Value.div x1 x2
    | ESub -> Value.minus x1 x2
    | ELt -> Value.bool (Value.lt tx1 x1 x2)
    | ELe -> Value.bool (Value.le tx1 x1 x2)
    | EGt -> Value.bool (Value.lt tx1 x2 x1)
    | EGe -> Value.bool (Value.le tx1 x2 x1)
    | EAnd | EOr -> impossible ()
    | EAnd_bytes -> Value.logical_and x1 x2
    | EAnd_infix -> Value.logical_and x1 x2
    | EOr_bytes -> Value.logical_or x1 x2
    | EOr_infix -> Value.logical_or x1 x2
    | EXor_bytes -> Value.xor x1 x2
    | EXor_infix -> Value.xor x1 x2
    | ELshift_infix -> Value.shift_left x1 x2
    | ELshift_bytes -> Value.shift_left x1 x2
    | ERshift_infix -> Value.shift_right x1 x2
    | ERshift_bytes -> Value.shift_right x1 x2
    | EEDiv -> Value.ediv tx1 tx2 x1 x2
    | ECons -> Value.cons x1 x2
    | EMax ->
        let isNat =
          match Type.unF tx1 with
          | Int {isNat} -> isNat
          | _ -> impossible ()
        in
        Value.intOrNat isNat (Big_int.max_big_int (un_int x1) (un_int x2))
    | EMin ->
        let isNat =
          match Type.unF tx1 with
          | Int {isNat} -> isNat
          | _ -> impossible ()
        in
        Value.intOrNat isNat (Big_int.min_big_int (un_int x1) (un_int x2))
    | EAdd_seconds -> (
        match (x1.v, x2.v) with
        | Literal (Timestamp t), Literal (Int {i = s}) ->
            Value.timestamp (Big_int.add_big_int t s)
        | _ -> impossible ())
    | EContains -> (
        match x2.v with
        | Map l -> Value.bool (List.exists (fun (x, _) -> Value.equal x x1) l)
        | Set l -> Value.bool (List.exists (fun x -> Value.equal x x1) l)
        | _ -> impossible ())
    | EApply_lambda -> Value.closure_apply x2 x1
    | ETicket -> (
        let ticketer = get_current_address env in
        match x2.v with
        | Literal (Int {i}) -> Value.ticket ticketer x1 i
        | _ -> impossible ())
    | ESplit_ticket -> (
        match (x1.v, x2.v) with
        | ( Ticket (ticketer, content, amount)
          , Tuple [{v = Literal (Int {i = a1})}; {v = Literal (Int {i = a2})}] )
          ->
            if Bigint.equal (Bigint.add_big_int a1 a2) amount
            then
              let t1 = Value.ticket ticketer content a1 in
              let t2 = Value.ticket ticketer content a2 in
              Value.some (Value.tuple [t1; t2])
            else Value.none
        | _ -> impossible ())
    | EView _ -> impossible ()

  let interpret_prim3 p (x1, _tx1) (x2, _tx2) (x3, tx3) =
    let impossible () =
      impossible "interpret_prim3 %s %s %s %s" (show_prim3 p) (show_value x1)
        (show_value x2) (show_value x3)
    in
    match p with
    | ESplit_tokens -> (
        match (x1.v, x2.v, x3.v) with
        | Literal (Mutez x1), Literal (Int {i = x2}), Literal (Int {i = x3}) ->
            Value.mutez (Big_int.div_big_int (Big_int.mult_big_int x1 x2) x3)
        | _ -> impossible ())
    | ERange -> (
        match (x1.v, x2.v, x3.v) with
        | Literal (Int {i = a}), Literal (Int {i = b}), Literal (Int {i = step})
          ->
            let a = Big_int.int_of_big_int a in
            let b = Big_int.int_of_big_int b in
            let step = Big_int.int_of_big_int step in
            if step = 0
            then failwith (Printf.sprintf "Range with 0 step")
            else if step * (b - a) < 0
            then Value.build_list []
            else
              let isNat =
                match Type.unF tx3 with
                | Int {isNat} -> isNat
                | _ -> impossible ()
              in
              let rec aux a acc =
                if (b - a) * step <= 0
                then List.rev acc
                else
                  aux (a + step) (Value.intOrNat isNat (Bigint.of_int a) :: acc)
              in
              Value.build_list (aux a [])
        | _ -> impossible ())
    | EUpdate_map -> (
        match x3.v with
        | Map xs ->
            let value = Value.unOption x2 in
            let m' =
              Lens.set (Lens.assoc ~equal:Value.equal ~key:x1) value xs
            in
            Value.map m'
        | _ -> impossible ())
    | EGet_and_update -> (
        match x3.v with
        | Map xs ->
            let value' = Value.unOption x2 in
            let old, m' =
              Lens.get_and_set (Lens.assoc ~equal:Value.equal ~key:x1) value' xs
            in
            let map = Value.map m' in
            Value.tuple [Value.option old; map]
        | _ -> impossible ())
    | ETest_ticket -> (
        match (x1.v, x3.v) with
        | Literal (Address {address}), Literal (Int {i}) ->
            Value.ticket address x2 i
        | _ -> impossible ())

  let get_var (x : Checked.bound) (vs : variables) : value =
    match List.assoc_opt x vs with
    | Some (Var s) -> s
    | _ -> impossible "get_var: %s" x.bound

  let add_var ((x : Checked.bound), (v : value)) (vs : variables) : variables =
    (x, Var v) :: vs

  let remove_var (x : Checked.bound) (vs : variables) : variables =
    match List.partition (fun (x', _) -> Checked.equal_bound x x') vs with
    | [_], rest -> rest
    | [], _ -> impossible "remove_var: not found: %s" x.bound
    | _ :: _ :: _, _ -> impossible "remove_var: duplicate: %s" x.bound

  let replace_var x v (vs : variables) : variables =
    (x, v) :: List.remove_assoc x vs

  let rec exec_init_internal id
      {init_params; init_body; init_storage; init_private} args
      (storage, private_) : value * value =
    let params =
      let f (k, _t) x = (k, Var x) in
      List.map2 f init_params args
    in
    let variables =
      params @ [(init_storage, Var storage); (init_private, Var private_)]
    in
    let env = {context0 with contract_id = In_contract id} in
    let fail err =
      raise
        (SmartExcept
           ([`Text "Error during execution of __init__: "; `Br]
           @ Execution.to_smart_except err))
    in
    try
      let r, variables = interpret_command env variables init_body in
      assert_unit r;
      let s = get_var init_storage variables in
      let p = get_var init_private variables in
      (s, p)
    with
    | Failure f ->
        fail
          (Execution.Exec_failure (Value.string f, [`Text "Failure: "; `Text f]))
    | ExecFailure (value, message) ->
        fail (Execution.Exec_failure (value, message))
    | SmartExcept l -> fail (Execution.Exec_failure (Value.string "Error", l))

  and interpret_expr env variables : Checked.expr -> value * variables =
    let rec interp (variables : variables) e =
      let unexpected () =
        impossible "interpret_expr: %s" (Printer.texpr_to_string e)
      in
      let un_bool = un_bool (fun () -> impossible "un_bool") in
      match e.e with
      | ELiteral x -> (Value.literal x, variables)
      | EVar {derived = V_defined x} ->
          let r, _ = interp [] x (* inlining *) in
          (r, variables)
      | EVar x when x.derived = V_match_cons -> (
          match List.assoc_opt x.name variables with
          | Some (Constant v) -> (v, variables)
          | _ -> impossible "match cons variable not found")
      | EVar {name; derived} when derived = V_constant -> (
          match Hashtbl.find_opt scenario_state.variables name with
          | Some {tv = Literal (String s)} -> (get_constant_value s, variables)
          | _ -> impossible "scenario variable not found")
      | EVar {name; derived} when derived = V_scenario -> (
          match Hashtbl.find_opt scenario_state.variables name with
          | Some x -> (Value.erase_types x, variables)
          | None ->
              raise
                (SmartExcept
                   [
                     text "Missing scenario variable for id: '%s'" name.bound
                   ; `Br
                   ; `Line e.line_no
                   ]))
      | EVar {name} -> (
          match List.assoc_opt name variables with
          | Some (Var x) -> (x, variables)
          | Some (Iter (v, _)) -> (v, variables)
          | Some (Constant v) -> (v, variables)
          | None -> impossible "variable not found: %S" name.bound)
      | EPrivate id -> (
          match List.assoc_opt id env.private_variables with
          | Some v -> (v, variables)
          | None -> impossible "private not found")
      | EMPrim1 (Emit (tag, _ty), x) ->
          let x_t = x.et in
          let x, variables = interp variables x in
          (Value.operation (Event (tag, x_t, x)), variables)
      | EMPrim0 p -> (interpret_mprim0 env e p, variables)
      | EMPrim1 (p, x) ->
          let x, variables = interp variables x in
          (interpret_mprim1 p x, variables)
      | EMPrim1_fail _ -> impossible "prim fail"
      | EMPrim2 (p, x1, x2) ->
          let tx1 = x1.et in
          let tx2 = x2.et in
          let x2, variables = interp variables x2 in
          let x1, variables = interp variables x1 in
          (interpret_mprim2 p (x1, tx1) (x2, tx2), variables)
      | EMPrim3 (p, x1, x2, x3) ->
          let x3, variables = interp variables x3 in
          let x2, variables = interp variables x2 in
          let x1, variables = interp variables x1 in
          (interpret_mprim3 p x1 x2 x3, variables)
      | EPrim0 p -> (interpret_prim0 e p, variables)
      | EIs_failing x -> (
          try
            let (_ : value), variables = interp variables x in
            (Value.bool false, variables)
          with _ -> (Value.bool true, variables))
      | ECatch_exception (_, x) -> (
          try
            let (_ : value), variables = interp variables x in
            (Value.none, variables)
          with
          | Failure f -> (Value.some (Value.string f), variables)
          | ExecFailure (value, _) -> (Value.some value, variables)
          | SmartExcept _ -> (Value.some (Value.string "Error"), variables)
          | _ -> impossible "exception during catch")
      | EPrim1 (EStatic_view (static_id, name), param) ->
          let param_t = param.et in
          let param, variables = interp variables param in
          let id = C_static static_id in
          let return_type =
            match Type.unF e.et with
            | T1 (Option, t) -> t
            | _ -> impossible "static view"
          in
          ( eval_view ~name ~id ~line_no:e.line_no ~e ~return_type
              ~types_ok:true env (param, param_t)
          , variables )
      | EVariant (name, x) ->
          let x, variables = interp variables x in
          (Value.variant name x, variables)
      | EAttr (name, x) -> (
          let x, variables = interp variables x in
          match x.v with
          | Record l -> (
              match List.assoc_opt name l with
              | Some v -> (v, variables)
              | None -> impossible "attr")
          | _ -> impossible "attr")
      | EPrim1 (p, x) ->
          let tx = x.et in
          let x, variables = interp variables x in
          (interpret_prim1 e p (x, tx), variables)
      | EPrim2 (EOr, x, y) ->
          let pp () = [] in
          let x, variables = interp variables x in
          if Value.unBool ~pp x
          then (Value.bool true, variables)
          else interp variables y
      | EPrim2 (EAnd, x, y) ->
          let pp () = [] in
          let x, variables = interp variables x in
          if Value.unBool ~pp x
          then interp variables y
          else (Value.bool false, variables)
      | ECall (lambda, parameter) -> (
          let lambda, variables = interpret_expr env variables lambda in
          let parameter, variables =
            match parameter with
            | None -> (Value.unit, variables)
            | Some parameter -> interpret_expr env variables parameter
          in
          match lambda.v with
          | Closure (l, args) -> (
              match l.recursive with
              | None ->
                  (* TODO Instead of recovering storage and operations refs
                     here, have them derived by the checker for each
                     ECall. *)
                  let storage =
                    Option.map fst
                    @@ List.find_opt
                         (fun (n, _) -> n.bound = "__storage__")
                         variables
                  in
                  let operations =
                    Option.map fst
                    @@ List.find_opt
                         (fun (n, _) -> n.bound = "__operations__")
                         variables
                  in
                  let r, vs =
                    call_clean_closure ~storage ~operations env variables
                      (l, args) parameter
                  in
                  (r, vs)
              | Some fname ->
                  let r, variables =
                    call_rec_closure env variables (l, args) parameter
                      (fname, lambda)
                  in
                  (r, variables))
          | _ -> unexpected ())
      | EPrim2 (EView (name, return_type), param, y) -> (
          let param_t = param.et in
          let param, variables = interp variables param in
          let y, variables = interp variables y in
          let address =
            match y with
            | {v = Literal (Address {address})} -> address
            | _ -> raise (SmartExcept [`Text "View address is invalid"])
          in
          match Hashtbl.find_opt scenario_state.rev_addresses address with
          | None ->
              if config.view_check_exception
              then
                exec_fail_string "Missing contract for view"
                  [`Expr e; `Line e.line_no]
              else (Value.none, variables)
          | Some id ->
              ( eval_view ~name ~id ~line_no:e.line_no ~e ~return_type
                  ~types_ok:false env (param, param_t)
              , variables ))
      | EPrim2 (p, x1, x2) ->
          let tx1 = x1.et in
          let tx2 = x2.et in
          let x2, variables = interp variables x2 in
          let x1, variables = interp variables x1 in
          (interpret_prim2 env e p (x1, tx1) (x2, tx2), variables)
      | EIf (c, t, e) ->
          let c, variables = interp variables c in
          if un_bool c then interp variables t else interp variables e
      | EPrim3 (p, x1, x2, x3) ->
          let tx1 = x1.et in
          let tx2 = x2.et in
          let tx3 = x3.et in
          let x3, variables = interp variables x3 in
          let x2, variables = interp variables x2 in
          let x1, variables = interp variables x1 in
          (interpret_prim3 p (x1, tx1) (x2, tx2) (x3, tx3), variables)
      | EOpen_variant (name, x, missing_message) -> (
          let x, variables = interp variables x in
          match x.v with
          | Variant (cons, v) ->
              if cons <> name
              then
                let default_message =
                  Printf.sprintf
                    "Not the proper variant constructor [%s] != [%s]" name cons
                in
                let missing_message, message =
                  match missing_message with
                  | Some e ->
                      let v, _variables = interpret_expr env variables e in
                      (v, [`Br; `Text "Message: "; `UValue v])
                  | None -> (Value.string default_message, [])
                in
                raise
                  (ExecFailure
                     ( missing_message
                     , [
                         `Text default_message
                       ; `Br
                       ; `Expr e
                       ; `Rec message
                       ; `Line e.line_no
                       ] ))
              else (v, variables)
          | _ -> impossible "opened non-variant")
      | EItem {items; key; default_value; missing_message} -> (
          let pp () =
            Printf.sprintf "%s[%s]"
              (Printer.texpr_to_string items)
              (Printer.texpr_to_string key)
          in
          let items', variables = interp variables items in
          let key', variables = interp variables key in
          match items'.v with
          | Map map -> (
              match
                (List.assoc_opt ~equal:Value.equal key' map, default_value)
              with
              | Some v, _ -> (v, variables)
              | None, Some def -> interp variables def
              | _ ->
                  let default_message = "Missing item in map" in
                  let missing_message =
                    match missing_message with
                    | None -> Value.string default_message
                    | Some missing_message ->
                        let x, _variables =
                          interpret_expr env variables missing_message
                        in
                        x
                  in
                  raise
                    (ExecFailure
                       ( missing_message
                       , [
                           `Text (default_message ^ ": ")
                         ; `Value (Value.typecheck key.et key')
                         ; `Text " is not in "
                         ; `Value (Value.typecheck items.et items')
                         ; `Text " while evaluating "
                         ; `Text (pp ())
                         ] )))
          | _ ->
              raise
                (SmartExcept
                   [
                     `Text "Bad getItem: "
                   ; `Value (Value.typecheck items.et items')
                   ; `Text "["
                   ; `Value (Value.typecheck key.et key')
                   ; `Text "]"
                   ]))
      | ERecord [] -> (Value.unit, variables)
      | ERecord l ->
          let variables = ref variables in
          let f (s, e) =
            let r, vs = interp !variables e in
            variables := vs;
            (s, r)
          in
          let r = Value.record (List.map f l) in
          (r, !variables)
      | EMap_function {l; f} -> (
          let f, variables = interpret_expr env variables f in
          let l, variables = interpret_expr env variables l in
          match (l.v, f.v) with
          | Variant ("None", _), _ -> (l, variables)
          | Variant ("Some", x), Closure (f_code, f_args) ->
              let r, variables =
                call_dirty_closure env variables (f_code, f_args) x
              in
              (Value.some r, variables)
          | List l, Closure (f_code, f_args) ->
              let variables = ref variables in
              let f x =
                let r, vs =
                  call_dirty_closure env !variables (f_code, f_args) x
                in
                variables := vs;
                r
              in
              let r = Value.build_list (List.map f l) in
              (r, !variables)
          | Map l, Closure (f_code, f_args) ->
              let variables = ref variables in
              let f (k, v) =
                let v, vars =
                  call_dirty_closure env !variables (f_code, f_args)
                    (mk_key_value k v)
                in
                variables := vars;
                (k, v)
              in
              let l = List.map f l in
              let r = Value.map l in
              (r, !variables)
          | _ -> impossible "not mappable")
      | ESlice {offset; length; buffer} -> (
          let buffer, variables = interp variables buffer in
          let length, variables = interp variables length in
          let offset, variables = interp variables offset in
          Basics.(
            match (offset.v, length.v, buffer.v) with
            | ( Literal (Int {i = ofs_bi})
              , Literal (Int {i = len_bi})
              , Literal (String s) ) ->
                ( (try
                     Value.some
                       (Value.string
                          (String.sub s
                             (Big_int.int_of_big_int ofs_bi)
                             (Big_int.int_of_big_int len_bi)))
                   with _ -> Value.none)
                , variables )
            | ( Literal (Int {i = ofs_bi})
              , Literal (Int {i = len_bi})
              , Literal (Bytes s) ) ->
                ( (try
                     Value.some
                       (Value.bytes
                          (String.sub s
                             (Big_int.int_of_big_int ofs_bi)
                             (Big_int.int_of_big_int len_bi)))
                   with _ -> Value.none)
                , variables )
            | _ -> impossible "not sliceable"))
      | EMap (_, entries) ->
          let variables = ref variables in
          let f entries (key, value) =
            let value, vs = interp !variables value in
            let key, vs = interp vs key in
            variables := vs;
            (key, value)
            :: Base.List.Assoc.remove ~equal:Value.equal entries key
          in
          let r = Value.map (List.fold_left f [] entries) in
          (r, !variables)
      | EList items ->
          let variables = ref variables in
          let f x =
            let r, vs = interp !variables x in
            variables := vs;
            r
          in
          let r = Value.build_list (List.rev_map f (List.rev items)) in
          (r, !variables)
      | EList_comprehension (e, x, xs) ->
          let xs, variables = interpret_expr env variables xs in
          let xs =
            match xs.v with
            | List xs -> xs
            | _ -> impossible "not a list"
          in
          let f v =
            let y, variables' =
              interpret_expr env (add_variable x (Constant v) variables) e
            in
            let variables' =
              match variables' with
              | _ :: vs -> vs
              | [] -> assert false
            in
            if false
            then assert (show_variables variables = show_variables variables');
            y
          in
          let r = Value.build_list (List.map f xs) in
          (r, variables)
      | ESet l ->
          let variables = ref variables in
          let f x =
            let r, vs = interp !variables x in
            variables := vs;
            r
          in
          let r = Value.set (List.rev_map f (List.rev l)) in
          (r, !variables)
      | EContract {arg_type; address; entrypoint} -> (
          let address, variables = interp variables address in
          match (entrypoint, address) with
          | ( entrypoint
            , {v = Literal (Address {address = addr; entrypoint = None})} )
          | None, {v = Literal (Address {address = addr; entrypoint})} -> (
              let ok () =
                (Value.some (Value.contract ?entrypoint addr), variables)
              in
              let contract =
                match Hashtbl.find_opt scenario_state.rev_addresses addr with
                | None -> None
                | Some id ->
                    Option.map
                      (fun c -> (id, c))
                      (Hashtbl.find_opt scenario_state.contracts id)
              in
              match contract with
              | None -> ok ()
              | Some (id, contract) -> (
                  let extra = contract.template.derived in
                  let raise_or_none msg xs =
                    if extra.config.contract_check_exception
                    then exec_fail_string msg xs
                    else (Value.none, variables)
                  in
                  let check_type t =
                    if Type.equal arg_type t
                    then ok ()
                    else if extra.config.contract_check_exception
                    then
                      raise_or_none "Wrong type for contract"
                        [
                          `Text "Wrong type for contract("
                        ; `Text (Printer.string_of_contract_id id)
                        ; `Text ")."
                        ; `Br
                        ; `Expr e
                        ; `Br
                        ; `Type t
                        ; `Br
                        ; `Text " instead of "
                        ; `Br
                        ; `Type arg_type
                        ; `Br
                        ; `Text "Please set a stable type."
                        ; `Br
                        ; `Line e.line_no
                        ]
                    else (Value.none, variables)
                  in
                  match entrypoint with
                  | None -> (
                      match Type.unF extra.tparameter with
                      | Variant {row = [(_, t)]} -> check_type t
                      | Variant {row = []} | T0 Unit -> check_type Type.unit
                      | Variant {row} -> (
                          match List.assoc_opt "default" row with
                          | None ->
                              raise_or_none
                                "Missing entrypoint target in contract"
                                [
                                  `Text "Missing entrypoint target in contract ("
                                ; `Text (Printer.string_of_contract_id id)
                                ; `Text ")."
                                ; `Br
                                ; `Line e.line_no
                                ]
                          | Some t -> check_type t)
                      | _ -> impossible "EContract: tparameter not a variant")
                  | Some entrypoint -> (
                      match Type.unF extra.tparameter with
                      | Variant {row = [(ep_name, t)]} ->
                          if ep_name = entrypoint
                             && extra.config.single_entrypoint_annotation
                          then check_type t
                          else
                            raise_or_none "No annotation in contract"
                              [
                                `Text "Entrypoint annotation ("
                              ; `Text entrypoint
                              ; `Text ") for contract ("
                              ; `Text (Printer.string_of_contract_id id)
                              ; `Text
                                  ") with a single entrypoint and no \
                                   annotation."
                              ; `Br
                              ; `Line e.line_no
                              ]
                      | Variant {row} -> (
                          match List.assoc_opt entrypoint row with
                          | None ->
                              raise_or_none
                                "Missing entrypoint target in contract"
                                [
                                  `Text "Missing entrypoint target ( "
                                ; `Text entrypoint
                                ; `Text " ) for contract ("
                                ; `Text (Printer.string_of_contract_id id)
                                ; `Text ")."
                                ; `Br
                                ; `Line e.line_no
                                ]
                          | Some t -> check_type t)
                      | _ -> impossible "EContract: tparameter not a variant")))
          | _ -> impossible "not an address")
      | ETuple es ->
          let variables = ref variables in
          let f x =
            let r, vs = interp !variables x in
            variables := vs;
            r
          in
          let r = Value.tuple (List.rev_map f (List.rev es)) in
          (r, !variables)
      | ELambda l -> (Value.closure_init l, variables)
      | EMake_signature {secret_key; message; message_format} ->
          let secret_key, variables = interp variables secret_key in
          let message, variables = interp variables message in
          let secret_key =
            secret_key.v |> function
            | Literal (Secret_key k) -> k
            | _ -> impossible "not a secret key"
          in
          let message =
            match message.v with
            | Literal (Bytes k) -> (
                match message_format with
                | `Raw -> k
                | `Hex ->
                    Hex.to_string
                      (`Hex
                        (Option.default k
                           (Base.String.chop_prefix k ~prefix:"0x"))))
            | _ -> impossible "not a bytes value"
          in
          (P.Crypto.sign ~secret_key message |> Value.signature, variables)
      | EMichelson ({parsed; typesIn; typesOut}, es) -> (
          let open Michelson_interpreter in
          let code = Michelson.Of_micheline.instruction parsed in
          let ts =
            List.map (fun e -> Typing.mtype_of_type ~with_annots:true e.et) es
          in
          let code =
            Michelson.typecheck_instr ~protocol
              ~tparameter:(Michelson.mt_unit, None)
              (* TODO: rely on the Checker and treat tparameter *)
              (Stack_ok ts) code
          in
          let variables = ref variables in
          let stack =
            List.map2
              (fun t x ->
                let r, vs = interp !variables x in
                variables := vs;
                mvalue_of_value ~protocol t r)
              typesIn es
          in
          let module MI = M (P) in
          let context =
            let amount = env.amount in
            let balance = (get_current_instance env).state.balance in
            ({config; balance; amount} : context)
          in
          let stack = MI.interpret context code stack in
          match (stack, typesOut) with
          | Ok [x], [t] ->
              let mt = Typing.mtype_of_type ~with_annots:false t in
              let x = typecheck_mvalue mt x in
              let x = value_of_tmvalue config x in
              let x = Value.smartMLify t x in
              let _ = Value.typecheck t x in
              (x, !variables)
          | Failed v, _ ->
              let v = value_of_tmvalue config v in
              raise
                (ExecFailure
                   (v, [`Text "Failure: "; `UValue v; `Br; `Line e.line_no]))
          | Error msg, _ -> failwith msg
          | _ -> impossible "unexpected inline Michelson error")
      | ETransfer {destination; arg = params; amount} ->
          let params_t = params.et in
          let destination, variables = interp variables destination in
          let amount, variables = interp variables amount in
          let params, variables = interp variables params in
          let destination =
            match destination with
            | {v = Contract {address; entrypoint}} ->
                {address; entrypoint; type_ = params_t}
            | _ -> impossible "destination not a contract value"
          in
          let amount =
            match amount with
            | {v = Literal (Mutez x)} -> x
            | _ -> impossible "amount not a mutez value"
          in
          (Value.operation (Transfer {params; destination; amount}), variables)
      | ECreate_contract {push; baker; balance; storage; derived} ->
          let storage, variables = interp variables storage in
          let balance, variables = interp variables balance in
          let baker, variables = interp variables baker in
          let baker = Value.un_baker baker in
          let balance =
            match balance with
            | {v = Literal (Mutez x)} -> x
            | _ -> impossible "balance not a mutez value"
          in
          let {template} =
            let contract_template = derived in
            (* TODO Why do we instantiate here and not later during
               operation processing? *)
            instantiate_contract contract_template
          in
          let dynamic_id = !(scenario_state.next_dynamic_address_id) in
          incr scenario_state.next_dynamic_address_id;
          let id = C_dynamic {dynamic_id} in
          let address = get_contract_address scenario_state id in
          let op =
            Value.operation
              (CreateContract
                 {id; template; baker; balance; storage = Some storage})
          in
          if push
          then
            let operations =
              match env.operations with
              | None -> assert false
              | Some operations -> operations
            in
            let ops = get_var operations variables in
            let variables =
              replace_var operations (Var (Value.cons op ops)) variables
            in
            (Value.literal (Literal.address address), variables)
          else
            ( Value.record
                [
                  ("operation", op)
                ; ("address", Value.literal (Literal.address address))
                ]
            , variables )
      | ESapling_verify_update {state; transaction} -> (
          let state, variables = interp variables state in
          let transaction, variables = interp variables transaction in
          match (state.v, transaction.v) with
          | ( Literal (Sapling_test_state {memo; elements})
            , Literal
                (Sapling_test_transaction {source; target; amount; boundData}) )
            -> (
              let output =
                match (source, target) with
                | None, None -> impossible "no source, no target"
                | None, Some _ -> Bigint.minus_big_int amount
                | Some _, None -> amount
                | Some _, Some _ -> Bigint.zero_big_int
              in
              let state =
                match source with
                | None -> Some elements
                | Some source -> (
                    match List.assoc_opt source elements with
                    | Some x when Bigint.ge_big_int x amount ->
                        Some
                          ((source, Bigint.sub_big_int x amount)
                          :: List.remove_assoc source elements)
                    | _ -> None)
              in
              let state =
                match (state, target) with
                | None, _ -> None
                | Some state, None -> Some state
                | Some state, Some target -> (
                    match List.assoc_opt target state with
                    | Some x ->
                        Some
                          ((target, Bigint.add_big_int x amount)
                          :: List.remove_assoc target state)
                    | _ -> Some ((target, amount) :: state))
              in
              match state with
              | None -> (Value.none, variables)
              | Some state ->
                  ( Value.some
                      (Value.tuple
                         [
                           Value.bytes boundData
                         ; Value.int output
                         ; Value.literal (Literal.sapling_test_state memo state)
                         ])
                  , variables ))
          | _ -> impossible "not expected sapling values")
    in
    interp variables

  and interpret_exprs env (es : Checked.expr list) variables :
      value list * variables =
    let variables = ref variables in
    let f x =
      let r, vs = interpret_expr env !variables x in
      variables := vs;
      r
    in
    let r = List.map f es in
    (r, !variables)

  and path_of_expr env =
    let rec of_expr acc variables e =
      match e.e with
      | EVar {name = {bound = "__parameter__"} as p} ->
          (Some {root = R_local p; steps = acc}, variables)
      | EVar {name} -> (
          match List.assoc_opt name variables with
          | Some (Var _) -> (Some {root = R_local name; steps = acc}, variables)
          | Some (Iter (_, Some p)) -> (Some (extend_path p acc), variables)
          | Some (Iter (_, None)) ->
              (None, variables (* Iteratee not an l-expression. *))
          | Some (Constant _) -> (None, variables)
          | None -> impossible "variable %s not found" name.bound)
      | EItem {items = cont; key; default_value = None; missing_message = None}
        ->
          let key, variables = interpret_expr env variables key in
          of_expr (S_item_map key :: acc) variables cont
      | EAttr (name, expr) -> of_expr (S_attr name :: acc) variables expr
      | _ -> (None, variables)
    in
    of_expr []

  and scoped env variables extra c =
    let variables' = add_variables extra variables in
    let r, variables' = interpret_command env variables' c in
    let variables', extra' =
      List.partition (fun (x, _) -> List.mem_assoc x variables) variables'
    in
    assert (
      List.sort compare (List.map fst variables)
      = List.sort compare (List.map fst variables'));
    assert (
      List.sort compare (List.map fst extra)
      = List.sort compare (List.map fst extra'));
    (r, variables', extra')

  and scoped_ env variables xs extra =
    let r, vs, _extra = scoped env variables xs extra in
    (r, vs)

  and interpret_binding ~line_no env variables =
    let impossible () = impossible "interpret_binding" in
    function
    | {c = CAssign {derived = Assign lhs; rhs}} ->
        let lhs, vs = path_of_expr env !variables lhs in
        variables := vs;
        let lhs =
          match lhs with
          | None -> assert false
          | Some lhs -> lhs
        in
        let lhs, _err = lens_of_path ~line_no lhs in
        let rhs, vs = interpret_expr env !variables rhs in
        variables := vs;
        let vs = Lens.set lhs (Some rhs) !variables in
        variables := vs;
        Value.unit
    | {c = CExpr rhs} ->
        let r, vs = interpret_expr env !variables rhs in
        variables := vs;
        r
    | {c = CAssign {rhs; derived = Var_def x}} ->
        let y, vs = interpret_expr env !variables rhs in
        variables := vs;
        if x.bound <> "_" then variables := add_variable x (Var y) !variables;
        Value.unit
    | {c = CAssign {derived = Product (Pattern_tuple ns); rhs}} ->
        let rhs, vs = interpret_expr env !variables rhs in
        variables :=
          add_variables
            (List.map2 ~err:"match"
               (fun n v -> (n, Var v))
               ns (un_tuple impossible rhs))
            vs;
        Value.unit
    | {c = CAssign {derived = Product (Pattern_record bs); rhs}} ->
        let rhs, vs = interpret_expr env !variables rhs in
        let rhs = un_record impossible rhs in
        let bs =
          bs
          |> List.map @@ fun {var; field} ->
             match List.assoc_opt field rhs with
             | Some v -> (var, Var v)
             | None -> assert false
        in
        variables := add_variables bs vs;
        Value.unit
    | c ->
        let r, vs = interpret_command env !variables c in
        variables := vs;
        r

  and interpret_command env (variables : variables)
      ({line_no} as initialCommand : Checked.command) : value * variables =
    let impossible () =
      impossible "interpret_command %S" (Checked.show_command initialCommand)
    in
    let un_bool = un_bool impossible in
    match initialCommand.c with
    | CCall_init (_name, Call_positional args, derived) -> (
        let {storage; private_; super} = derived in
        let {template = _} = get_current_instance env in
        match super with
        | None ->
            assert (args = []);
            (Value.unit, variables)
        | Some init ->
            let args, variables = interpret_exprs env args variables in
            let id = get_current_contract_id env in
            let s = get_var storage variables in
            let p = get_var private_ variables in
            let s, p = exec_init_internal id init args (s, p) in
            let variables = remove_var storage variables in
            let variables = add_var (storage, s) variables in
            let variables = remove_var private_ variables in
            let variables = add_var (private_, p) variables in
            (Value.unit, variables))
    | CCall_init (_name, Call_keyword _args, _derived) ->
        assert false
        (* TODO derive this lookup and use check-call-provided 'args'. rename 'args' to 'ordered args' *)
    | CNever message ->
        let _r, _varibales = interpret_expr env variables message in
        assert false
    | CFailwith message ->
        let value, _variables = interpret_expr env variables message in
        raise
          (ExecFailure
             ( value
             , [`Text "Failure: "; `UValue value]
               @ (match message.e with
                 | ELiteral _ -> []
                 | _ -> [`Br; `Text "("; `Expr message; `Text ")"])
               @ [`Br; `Line line_no] ))
    | CIf (c, t, e) ->
        let condition, variables = interpret_expr env variables c in
        if un_bool condition
        then interpret_command env variables t
        else interpret_command env variables e
    | CMatch_list {expr; id; clause_cons; clause_nil} -> (
        let expr, variables = interpret_expr env variables expr in
        match expr.v with
        | List [] -> interpret_command env variables clause_nil
        | List (head :: tail) ->
            let v =
              Constant
                (Value.record [("head", head); ("tail", Value.build_list tail)])
            in
            scoped_ env variables [(id, v)] clause_cons
        | _ -> assert false)
    | CModify_record (lhs, name, c) -> (
        let path, variables =
          path_of_expr env variables lhs
          (* FIXME The LHS is evaluated again below, so effects are run twice. *)
        in
        match path with
        | None -> assert false
        | Some lhs_path ->
            let lhs_lens, _err = lens_of_path ~line_no lhs_path in
            let v, variables = interpret_expr env variables lhs in
            let vs = [(name, Var v)] in
            let r, variables, new_variables = scoped env variables vs c in
            assert_unit r;
            let r =
              match List.assoc_opt name new_variables with
              | Some (Var x) -> x
              | _ -> assert false
            in
            let variables = Lens.set lhs_lens (Some r) variables in
            (Value.unit, variables))
    | CModify_product (lhs, name, p, c) -> (
        let path, variables =
          path_of_expr env variables lhs
          (* FIXME The LHS is evaluated again below, so effects are run twice. *)
        in
        match path with
        | None -> assert false
        | Some lhs_path ->
            let lhs_lens, _err = lens_of_path ~line_no lhs_path in
            let v, variables = interpret_expr env variables lhs in
            let vs =
              match (name, p) with
              | None, Pattern_single x -> [(x, Var v)]
              | None, Pattern_tuple ns -> (
                  match v.v with
                  | Tuple vs ->
                      List.map2 ~err:"match" (fun n v -> (n, Var v)) ns vs
                  | _ -> assert false)
              | Some name, Pattern_record _bs -> [(name, Var v)]
              | _ -> assert false
            in
            let r, variables, new_variables = scoped env variables vs c in
            let r =
              match (name, p) with
              | _, Pattern_single _ | _, Pattern_tuple _ -> r
              | Some name, Pattern_record _bs -> (
                  match List.assoc_opt name new_variables with
                  | Some (Var x) -> x
                  | _ -> assert false)
              | _ -> assert false
            in
            let variables = Lens.set lhs_lens (Some r) variables in
            (Value.unit, variables))
    | CMatch (scrutinee, cases) -> (
        let scrutinee', variables = interpret_expr env variables scrutinee in
        match scrutinee'.v with
        | Variant (cons, arg) -> (
            match
              List.find_opt
                (fun (constructor, _, _) -> constructor = cons)
                cases
            with
            | None -> (Value.unit, variables)
            | Some (_constructor, arg_name, body) ->
                scoped_ env variables [(arg_name, Constant arg)] body)
        | _ -> assert false)
    | CExpr e -> interpret_expr env variables e
    | CAssign _ ->
        let vars0 = List.map fst variables in
        let variables = ref variables in
        let r = interpret_binding ~line_no env variables initialCommand in
        variables := List.filter (fun (n, _) -> List.mem n vars0) !variables;
        (r, !variables)
    | CBlock [] -> (Value.unit, variables)
    | CBlock cs ->
        let vars0 = List.map fst variables in
        let variables = ref variables in
        let cs', last = List.unsnoc cs in
        List.iter
          (fun x -> assert_unit (interpret_binding ~line_no env variables x))
          cs';
        let r = interpret_binding ~line_no env variables last in
        variables := List.filter (fun (n, _) -> List.mem n vars0) !variables;
        (r, !variables)
    | CDel_item (map, key) ->
        let map, variables = path_of_expr env variables map in
        let path =
          match map with
          | None -> assert false
          | Some path -> path
        in
        let key, variables = interpret_expr env variables key in
        let l, _err =
          lens_of_path ~line_no (extend_path path [S_item_map key])
        in
        let variables = Lens.set l None variables in
        (Value.unit, variables)
    | CUpdate_set (set, elem, add) ->
        let set, variables = path_of_expr env variables set in
        let path =
          match set with
          | None -> assert false
          | Some path -> path
        in
        let elem, variables = interpret_expr env variables elem in
        let l, err = lens_of_path ~line_no path in
        let variables =
          Lens.set
            (l @. Lens.some ~err @. Value.lens_set_at ~elem)
            add variables
        in
        (Value.unit, variables)
    | CFor (name, iteratee, body) ->
        let iteratee_v, variables = interpret_expr env variables iteratee in
        let iteratee_l, variables = path_of_expr env variables iteratee in
        let elems =
          match iteratee_v.v with
          | List elems -> elems
          | _ -> assert false
        in
        let variables = ref variables in
        let step i v =
          let path =
            Option.map (fun p -> extend_path p [S_item_list i]) iteratee_l
          in
          let r, variables' =
            scoped_ env !variables [(name, Iter (v, path))] body
          in
          variables := variables';
          assert_unit r
        in
        List.iteri step elems;
        (Value.unit, !variables)
    | CWhile (e, cmd) ->
        let variables = ref variables in
        while
          let e, vs = interpret_expr env !variables e in
          variables := vs;
          un_bool e
        do
          let vs = interpret_unit_command env !variables cmd in
          variables := vs
        done;
        (Value.unit, !variables)
    | CVerify (x, message) -> (
        let v, variables = interpret_expr env variables x in
        match v.v with
        | Literal (Bool true) -> (Value.unit, variables)
        | Literal (Bool false) ->
            let except, message =
              match message with
              | Some e ->
                  let v, _variables = interpret_expr env variables e in
                  (v, [`Br; `Text "Message: "; `UValue v])
              | None -> (Value.string (Printer.wrong_condition_string x), [])
            in
            raise
              (ExecFailure
                 ( except
                 , with_loc ~line_no
                     ([`Text "Wrong condition: "; `Expr x] @ message) ))
        | _ -> assert false)
    | CResult e -> interpret_expr env variables e
    | CComment _ -> (Value.unit, variables)
    | CSet_type _ -> (Value.unit, variables)
    | CSet_result_type (c, _) -> interpret_command env variables c
    | CTrace e ->
        let e, variables = interpret_expr env variables e in
        print_endline (Printer.value_to_string e);
        (Value.unit, variables)

  and interpret_unit_command env (variables : variables) c =
    let r, variables = interpret_command env variables c in
    assert_unit r;
    variables

  and call_clean_closure env ~operations ~storage (variables : variables)
      (lambda, args) parameter =
    let caller_operations = operations in
    let caller_storage = storage in
    let ({with_operations; with_storage} : _ lambda_derived) = lambda.derived in
    let parameter =
      List.fold_left (fun acc v -> Value.tuple [v; acc]) parameter args
    in
    let lookup x variables =
      match x with
      | None -> impossible "effect lookup failed"
      | Some x -> (
          match List.assoc_opt x variables with
          | None -> impossible "effect lookup failed"
          | Some r -> (x, r))
    in
    let storage_spec =
      with_storage
      |> Option.map @@ fun (_, _, body_storage) ->
         let caller_storage, s = lookup caller_storage variables in
         (caller_storage, body_storage, s)
    in
    let operations_spec =
      with_operations
      |> Option.map @@ fun body_operations ->
         let caller_operations, s = lookup caller_operations variables in
         (caller_operations, body_operations, s)
    in
    let lambda_vars =
      let f = Option.cata [] (fun (_, body_var, x) -> [(body_var, x)]) in
      (lambda.lambda_var, Constant parameter)
      :: (f operations_spec @ f storage_spec)
    in
    let r, lambda_vars =
      let env =
        let operations = lambda.derived.with_operations in
        {env with operations}
      in
      interpret_command env lambda_vars lambda.body
    in
    let transfer spec variables =
      (* Transfer storage/operations from lambda_vars to variables.  *)
      match spec with
      | None -> variables
      | Some (caller, body, _) -> (
          match List.assoc_opt body lambda_vars with
          | None -> impossible "no effect result"
          | Some s -> replace_var caller s variables)
    in
    let variables = transfer storage_spec variables in
    let variables = transfer operations_spec variables in
    (r, variables)

  and call_dirty_closure env (variables : variables) (lambda, args) parameter =
    let ({with_operations; with_storage} : _ lambda_derived) = lambda.derived in
    assert (Option.is_none with_storage);
    assert (Option.is_none with_operations);
    let parameter =
      List.fold_left (fun acc v -> Value.tuple [v; acc]) parameter args
    in
    let original_var_names = List.map fst variables in
    let variables = (lambda.lambda_var, Constant parameter) :: variables in
    let r, variables = interpret_command env variables lambda.body in
    let variables =
      List.filter (fun (x, _) -> List.mem x original_var_names) variables
    in
    (r, variables)

  and call_rec_closure env variables (l, args) parameter (fname, f) =
    let parameter =
      List.fold_left (fun acc v -> Value.tuple [v; acc]) parameter args
    in
    let vars = [(l.lambda_var, Constant parameter)] in
    let vars = (fname, Constant f) :: vars in
    let r, _vars = interpret_command env vars l.body in
    (* For now recursive closures cannot return operations, hence no
       variables returned here. *)
    (r, variables)

  and eval_view ~name ~id ~line_no ~e ~return_type ~types_ok env
      (parameters, param_t) =
    let optional_return_type = Type.option return_type in
    match Hashtbl.find_opt scenario_state.contracts id with
    | None ->
        Printf.ksprintf failwith "Missing contract %s for view %s"
          (address_of_contract_id id)
          name
    | Some contract -> (
        let view =
          List.find_opt
            (fun (view : Checked.view) -> view.view_name = name)
            contract.template.derived.views
        in
        match view with
        | None ->
            if config.view_check_exception
            then
              Printf.ksprintf failwith "Missing view %s in contract %s" name
                (address_of_contract_id id)
            else Value.none
        | Some view ->
            let env =
              let ({private_variables} : _ contract_derived) =
                contract.template.derived
              in
              let private_variables =
                List.map
                  (map_snd (fun x -> interpret_expr_external x))
                  private_variables
              in
              {env with private_variables; contract_id = In_contract id}
            in
            let tparameter =
              match view.derived.tparameter with
              | None -> Type.unit
              | Some parameter -> parameter
            in
            let input_ok, output_ok =
              ( types_ok || Type.equal tparameter param_t
              , types_ok
                || Type.equal (Type.option view.body.ct) optional_return_type )
            in
            if input_ok && output_ok
            then
              let env =
                let sender =
                  match env.contract_id with
                  | No_contract -> env.sender
                  | In_contract id -> Some (address_of_contract_id id)
                in
                {
                  env with
                  contract_id = In_contract id
                ; line_no
                ; amount = Bigint.zero_big_int
                ; sender
                }
              in
              let {parameter; storage} = view.derived in
              let storage_contents =
                match contract.state.storage with
                | None -> failwith "no storage!!"
                | Some s -> s
              in
              let variables =
                [
                  (parameter, Constant parameters)
                ; (storage, Var storage_contents)
                ]
              in
              let r, _variables = interpret_command env variables view.body in
              Value.some r
            else if config.view_check_exception
            then
              let message =
                match (input_ok, output_ok) with
                | true, true -> assert false
                | true, false -> "Wrong output type"
                | false, true -> "Wrong input type"
                | false, false -> "Wrong input and output type"
              in
              exec_fail_string "Type error in view"
                [
                  `Text message
                ; `Expr e
                ; `Br
                ; `Text "Expected input type (in the view): "
                ; `Br
                ; `Type tparameter
                ; `Br
                ; `Text "Actual input type: "
                ; `Br
                ; `Type param_t
                ; `Br
                ; `Text "Actual output type (in the view): "
                ; `Br
                ; `Type view.body.ct
                ; `Br
                ; `Text "Expected output type: "
                ; `Br
                ; `Type optional_return_type
                ; `Line line_no
                ]
            else Value.none)

  and interpret_expr_external ?source ?sender x =
    let env = {context0 with source; sender} in
    let r, _variables = interpret_expr env [] x in
    r

  and instantiate_contract (c : Checked.contract_def) =
    let derived = c.derived in
    let interp = interpret_expr_external in
    let balance =
      match Option.map interp derived.initial_balance with
      | None -> Big_int.zero_big_int
      | Some {v = Literal (Mutez x)} -> x
      | _ -> impossible "instantiate_contract: balance not a mutez"
    in
    let storage = Some (Value.zero_of_type derived.tstorage) in
    let private_ = Some (Value.zero_of_type derived.tprivate) in
    let baker = None in
    let metadata =
      c.derived.initial_metadata |> List.map @@ map_snd @@ Meta.map interp
    in
    {template = c; state = {balance; storage; private_; baker; metadata}}

  let update_storage id storage =
    let i = find_instance id in
    let state = {i.state with storage = Some storage} in
    let i = {i with state} in
    update_contract scenario_state id i

  let update_private id private_ =
    let i = find_instance id in
    let state = {i.state with private_ = Some private_} in
    let i = {i with state} in
    update_contract scenario_state id i

  let exec_init id args =
    let instance = find_instance id in
    let {init} = instance.template.derived in
    match init with
    | None -> ()
    | Some init ->
        let s =
          match instance.state.storage with
          | None -> impossible "exec_init: no storage"
          | Some s -> s
        in
        let p =
          match instance.state.private_ with
          | None -> impossible "exec_init: no private"
          | Some p -> p
        in
        let s, p = exec_init_internal id init args (s, p) in
        update_storage id s;
        update_private id p

  let interpret_message env id (channel, params) =
    assert (env.contract_id = No_contract);
    assert (env.private_variables = []);
    let instance = find_instance id in
    let {template} = instance in
    let derived = template.derived in
    let {entrypoints; private_variables} = derived in
    let contract_config = derived.config in
    let ep =
      List.find_opt
        (fun ({channel = x} : _ entrypoint_f) -> channel = x)
        entrypoints
    in
    let ep =
      match (ep, channel, entrypoints) with
      | None, "default", [ep] -> Some ep
      | _ -> ep
    in
    match ep with
    | None ->
        ( None
        , []
        , Some
            (Execution.Exec_failure
               (Printf.ksprintf Value.string "Channel not found: %s" channel, []))
        )
    | Some ({body; derived} as ep) -> (
        let {parameter; operations; storage; private_} = derived in
        let variables1, has_storage =
          match instance.state.storage with
          | None -> ([], false)
          | Some s -> ([(storage, Var s)], true)
        in
        let variables2, has_private =
          match instance.state.private_ with
          | None -> ([], false)
          | Some p -> ([(private_, Var p)], true)
        in
        let variables =
          (parameter, Constant params)
          :: (operations, Var (Value.build_list []))
          :: (variables1 @ variables2)
        in
        if Basics.check_no_incoming_transfer ~config:contract_config ep
           && not (Bigint.eq_big_int env.amount Bigint.zero_big_int)
        then
          let err = [`Text "Check-no-incoming-transfer-failure"] in
          ( None
          , []
          , Some
              (Execution.Exec_failure
                 ( Value.string
                     "Incoming transfer in check-no-incoming-transfer mode"
                 , err )) )
        else
          let env =
            let private_variables =
              List.map (map_snd interpret_expr_external) private_variables
            in
            {env with private_variables; contract_id = In_contract id}
          in
          try
            let (_ : value), variables =
              let env =
                let ops = derived.operations in
                {env with operations = Some ops}
              in
              interpret_command env variables body
            in
            let storage =
              match List.assoc_opt storage variables with
              | Some (Var storage) -> Some storage
              | Some _ -> impossible "unexpected __storage__ content"
              | None ->
                  if has_storage
                  then impossible "__storage__ not found"
                  else None
            in
            let private_ =
              match List.assoc_opt private_ variables with
              | Some (Var private_) -> Some private_
              | Some _ -> impossible "unexpected __private__ content"
              | None ->
                  if has_private
                  then impossible "__private__ not found"
                  else None
            in
            let ops =
              match List.assoc_opt operations variables with
              | Some (Var {v = List ops}) -> List.map un_operation ops
              | _ -> impossible "__operations__ not found"
            in
            let ops = List.map Value.typecheck_operation ops in
            let instance = get_current_instance env in
            let state = instance.state in
            let state = {state with storage; private_} in
            let instance = {instance with state} in
            let state = (Value.typecheck_instance instance).state in
            (* NB We return an instance here. The updating in the
               scenario_state is done elesewhere. TODO Move it here.*)
            (Some {template; state}, List.rev ops, None)
          with
          | Failure f ->
              ( None
              , []
              , Some
                  (Execution.Exec_failure
                     (Value.string f, [`Text "Failure: "; `Text f])) )
          | ExecFailure (value, message) ->
              (None, [], Some (Execution.Exec_failure (value, message)))
          | SmartExcept l ->
              (None, [], Some (Execution.Exec_failure (Value.string "Error", l)))
        )

  let interpret_message_inner ~title ~context ~channel ~params ~id ~line_no =
    let init_contract =
      match Hashtbl.find_opt scenario_state.contracts id with
      | None ->
          raise
            (SmartExcept
               [
                 `Text
                   (Printf.sprintf "Missing contract in scenario %s"
                      (Printer.string_of_contract_id id))
               ; `Line line_no
               ])
      | Some contract -> contract
    in
    let initial_balance = init_contract.state.balance in
    let init_contract =
      let state = init_contract.state in
      let state =
        {state with balance = Big_int.add_big_int context.amount state.balance}
      in
      {init_contract with state}
    in
    Hashtbl.add scenario_state.contracts id init_contract;
    let contract, operations, error =
      interpret_message context id (channel, Value.erase_types params)
    in
    let balance, contract =
      match contract with
      | None -> (initial_balance, None)
      | Some contract -> (contract.state.balance, Some contract)
    in
    let html ok output =
      let transaction =
        let hd_line_no =
          match context.line_no with
          | Some x -> x
          | None -> ("", -1)
        in
        Printf.sprintf
          "<div class='execMessage execMessage%s'>%s<h3>Transaction [%s] by \
           [%s] at time [timestamp(%s)] to %s</h3>%s<h3>Balance: \
           %s</h3>%s</div>%s(<button class='text-button' \
           onClick='showLine(%i)'>line %i</button>)"
          ok title ok
          (match context.sender with
          | None -> ""
          | Some x ->
              Printer.tvalue_to_string ~options:Printer.Options.html
                (Value.Typed.address x))
          (Big_int.string_of_big_int scenario_state.time)
          (Aux.address_of_contract_id ~html:true id None)
          (Printer.variant_to_string ~options:Printer.Options.html channel
             params)
          (Printer.ppAmount true balance)
          output "" (snd hd_line_no) (snd hd_line_no)
      in
      let michelson =
        let initContract = init_contract.template.derived in
        match
          List.find_opt
            (fun (x : _ entrypoint_f) -> x.channel = channel)
            initContract.entrypoints
        with
        | None -> "Missing entrypoint."
        | Some _ ->
            let params =
              match initContract.entrypoints with
              | [_] -> params
              | _ -> Value.Typed.variant initContract.tparameter channel params
            in
            let scenario_vars = TBoundMap.of_hashtbl scenario_state.variables in
            let params = Compiler.compile_value ~config ~scenario_vars params in
            let mich =
              Html.render
                (Html.copy_div ~id:"transaction_params"
                   ~className:"white-space-pre-wrap" "paramsCode"
                   (Raw
                      (Michelson.string_of_literal ~protocol:config.protocol
                         params)))
            in
            let json =
              Html.render
                (Html.copy_div ~id:"transaction_params_json" "paramsCodeJson"
                   (Raw
                      (Format.asprintf "<div class='white-space-pre'>%a</div>"
                         (Micheline.pp_as_json ())
                         (Michelson.To_micheline.literal
                            ~protocol:config.protocol params))))
            in
            let result =
              let myTabs =
                [
                  Html.tab "Parameter Michelson" ~active:() (Raw mich)
                ; Html.tab "Parameter JSON" (Raw json)
                ]
              in
              Html.render (Html.tabs "Generated Michelson" myTabs)
            in
            result
      in
      let myTabs =
        [
          Html.tab "Summary" ~active:() (Raw transaction)
        ; Html.tab "Michelson" (Raw michelson)
        ; Html.tab "X" (Raw "")
        ]
      in
      Html.render (Html.tabs "Transaction" myTabs)
    in
    match contract with
    | Some ({state = {storage}} as contract) ->
        let storage =
          match storage with
          | None -> failwith "Cannot simulate contract without a storage"
          | Some storage -> storage
        in
        let output =
          let operations =
            List.map
              (Printer.operation_to_string ~options:Printer.Options.html)
              operations
          in
          Printf.sprintf
            "<h3>Operations:</h3>%s<h3>Storage:</h3><div \
             class='subtype'>%s</div>"
            (String.concat " " operations)
            (Printer.render_storage_tvalue Printer.Options.html storage)
        in
        {
          Execution.ok = true
        ; contract = Some (map_instance_f Value.erase_types contract)
        ; operations
        ; error
        ; html = html "OK" output
        ; storage = Value.erase_types storage
        }
    | None ->
        let errorMessage =
          match error with
          | Some (Exec_failure (_, message)) ->
              Printer.pp_smart_except true message
          | None -> ""
        in
        let output = Printf.sprintf "<h3>Error:</h3>%s" errorMessage in
        {
          ok = false
        ; contract = None
        ; operations
        ; error
        ; html = html "KO" output
        ; storage = Value.unit
        }
end

let mkArgs ~config ~primitives ~scenario_state : (module Args) =
  (module struct
    let config = config

    module P = (val primitives : Primitives)

    let scenario_state = scenario_state

    module Printer = (val Printer.get ~config : Printer.Printer)
  end)

let interpret_message ~config ~primitives ~scenario_state =
  let module M = Main ((val mkArgs ~config ~primitives ~scenario_state : Args)) in
  M.interpret_message

let interpret_message_inner ~config ~primitives ~scenario_state =
  let module M = Main ((val mkArgs ~config ~primitives ~scenario_state : Args)) in
  M.interpret_message_inner

let interpret_expr ~config ~primitives ~scenario_state =
  let module M = Main ((val mkArgs ~config ~primitives ~scenario_state : Args)) in
  fun context e ->
    let r, _ = M.interpret_expr context [] e in
    r

let interpret_exprs ~config ~primitives ~scenario_state =
  let module M = Main ((val mkArgs ~config ~primitives ~scenario_state : Args)) in
  fun context e ->
    let r, _ = M.interpret_exprs context e [] in
    r

let interpret_expr_external ~config ~primitives ~scenario_state =
  let module M = Main ((val mkArgs ~config ~primitives ~scenario_state : Args)) in
  M.interpret_expr_external

let exec_init ~config ~primitives ~scenario_state =
  let module M = Main ((val mkArgs ~config ~primitives ~scenario_state : Args)) in
  M.exec_init

let instantiate_contract ~config ~primitives ~scenario_state =
  let module M = Main ((val mkArgs ~config ~primitives ~scenario_state : Args)) in
  M.instantiate_contract
