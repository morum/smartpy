(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Utils
open Control

type 't row = (string * 't) list [@@deriving eq, show, ord, map, fold]

val sort_row : 't row -> 't row

type access =
  | Read_only
  | Read_write
[@@deriving eq, ord, show, sexp]

type 't effects_f = {
    with_storage : (access * 't) option Hole.t
  ; with_operations : bool Hole.t
}
[@@deriving eq, ord, show {with_path = false}, map, fold]

type 't f =
  | T0 of Michelson_base.Type.type0
  | T1 of Michelson_base.Type.type1 * 't
  | T2 of Michelson_base.Type.type2 * 't * 't
  | Int of {isNat : bool Hole.t}
  | Lambda of 't effects_f * 't * 't
  | Record of {
        row : 't row
      ; var : VarId.t option
      ; layout : Layout.t Hole.t
    }
  | Variant of {
        row : 't row
      ; var : VarId.t option
      ; layout : Layout.t Hole.t
    }
  | Unknown of VarId.t * string
  | Tuple of 't list
  | Secret_key
  | Sapling_state of int Hole.t
  | Sapling_transaction of int Hole.t
  | Abbrev of {
        module_ : string option
      ; name : string
      ; line_no : (string * int) option
    }
[@@deriving eq, show, ord, map, fold, sexp]

include UNTIED with type 'a f := 'a f

include EQ with type t := t

include ORD with type t := t

include SHOW with type t := t

val t_of_sexp : Sexplib.Sexp.t -> t

val sexp_of_t : t -> Sexplib.Sexp.t

type effects = t effects_f

val no_effects : effects

val unknown_effects : unit -> effects

type tvariable = string * t [@@deriving eq, show]

val tree_layout : string list -> Layout.t

val comb_layout : [ `Left | `Right ] -> string list -> Layout.t

val mk0 : Michelson_base.Type.type0 -> t

val mk1 : Michelson_base.Type.type1 -> t -> t

val mk2 : Michelson_base.Type.type2 -> t -> t -> t

val unit : t

val address : t

val contract : t -> t

val bool : t

val bytes : t

val key_hash : t

val int : t

val nat : t

val intOrNat : unit -> t

val key : t

val key_value : t -> t -> t

val head_tail : t -> t -> t

val chain_id : t

val secret_key : t

val operation : t

val sapling_state : int option -> t

val sapling_transaction : int option -> t

val never : t

val map : big:bool -> tkey:t -> tvalue:t -> t

val set : telement:t -> t

val record : Layout.t Hole.t -> (string * t) list -> t

val variant : Layout.t Hole.t -> (string * t) list -> t

val record_default_layout : (string * t) list -> t

val variant_default_layout : (string * t) list -> t

val record_or_unit : Layout.t Hole.t -> (string * t) list -> t

val signature : t

val option : t -> t

val tor : t -> t -> t

val string : t

val timestamp : t

val mutez : t

val uvariant : string -> t -> t

val urecord : (string * t) list -> t

val unknown_raw : string -> t

val account : t

val pair : t -> t -> t

val tuple : t list -> t

val list : t -> t

val ticket : t -> t

val lambda : effects -> t -> t -> t

val has_unknowns : t -> bool

val has_holes : t -> bool

val bls12_381_g1 : t

val bls12_381_g2 : t

val bls12_381_fr : t

val chest_key : t

val chest : t

val is_hot : t -> Ternary.t

val view_variant : t -> (Layout.t Hole.t * t row) option

val of_mtype : Michelson_base.Type.mtype -> t

val type_of_literal : Literal.t -> t

val rawify_lambda :
  with_storage:t option -> with_operations:bool -> t -> t -> t * t

val has_effects : effects -> bool

val frees : t -> VarId.Set.t

val abbrev : line_no:(string * int) option -> string option -> string -> t
