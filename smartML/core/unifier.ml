(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Basics
open Utils
open Printf

exception Unification_failed of smart_except list

let fail e = raise (Unification_failed e)

let unify_holes ~unify_values wrap rsubst h1 h2 =
  let open Hole in
  match (h1, h2) with
  | Value x1, Value x2 -> unify_values x1 x2
  | Variable x1, _ ->
      Substitution.(insert [(x1, wrap h2)] rsubst);
      []
  | _, Variable x2 ->
      Substitution.(insert [(x2, wrap h1)] rsubst);
      []

(* Only checks equality of values, doesn't generate any
   constraints. *)
let equalize_holes ~err ~eq wrap rsubst h1 h2 =
  let unify_values v1 v2 = if not (eq v1 v2) then fail (err ()) else [] in
  let cs = unify_holes ~unify_values wrap rsubst h1 h2 in
  assert (cs = [])

let unify_layouts ~config ~err_msg rsubst l1 l2 =
  let open Substitution in
  let module Printer = (val Printer.get ~config : Printer.Printer) in
  let err () =
    [
      `Text "Non matching layouts "
    ; `Text (Printer.layout_to_string l1)
    ; `Text " and "
    ; `Text (Printer.layout_to_string l2)
    ; `Text err_msg
    ]
  in
  equalize_holes ~err ~eq:Layout.equal (fun x -> SLayout x) rsubst l1 l2

let unify_rows ~pp =
  let rec unify (cs, r1, r2) row1 row2 =
    match (row1, row2) with
    | (lbl1, t1) :: row1', (lbl2, t2) :: row2' -> (
        let c = compare lbl1 lbl2 in
        match c with
        | 0 -> unify (AssertEqual (t1, t2, pp) :: cs, r1, r2) row1' row2'
        | c when c < 0 -> unify (cs, (lbl1, t1) :: r1, r2) row1' row2
        | _ -> unify (cs, r1, (lbl2, t2) :: r2) row1 row2')
    | row1, row2 ->
        (cs, List.sort compare (r1 @ row1), List.sort compare (r2 @ row2))
  in
  fun row1 row2 ->
    unify ([], [], []) (List.sort compare row1) (List.sort compare row2)

let unify ~config pp rsubst t1 t2 : typing_constraint list =
  let module Printer = (val Printer.get ~config : Printer.Printer) in
  let emit r = Substitution.(insert r rsubst) in
  match (Type.unF t1, Type.unF t2) with
  | T0 (Nat | Int | Sapling_transaction _ | Sapling_state _), _
  | _, T0 (Nat | Int | Sapling_transaction _ | Sapling_state _) -> assert false
  | T0 c, T0 c' when Michelson_base.Type.equal_type0 c c' -> []
  | T1 (c, t1), T1 (c', t1') when Michelson_base.Type.equal_type1 c c' ->
      [AssertEqual (t1, t1', pp)]
  | T2 (c, t1, t2), T2 (c', t1', t2') when Michelson_base.Type.equal_type2 c c'
    -> [AssertEqual (t1, t1', pp); AssertEqual (t2, t2', pp)]
  | Secret_key, Secret_key -> []
  | Sapling_transaction m1, Sapling_transaction m2
  | Sapling_state m1, Sapling_state m2 ->
      let err () = [`Text "memo_size mismatch"] in
      equalize_holes ~err ~eq:( = ) (fun i -> Substitution.SInt i) rsubst m1 m2;
      []
  | Int {isNat = isNat1}, Int {isNat = isNat2} ->
      let tint = Printer.type_to_string Type.int in
      let tnat = Printer.type_to_string Type.nat in
      let err () = [`Text (sprintf "Type %s / %s mismatch" tint tnat)] in
      equalize_holes ~err ~eq:( = )
        (fun i -> Substitution.SBool i)
        rsubst isNat1 isNat2;
      []
  | Unknown (i1, _), Unknown (i2, _) when i1 = i2 -> []
  | Tuple r1, Tuple r2 ->
      if List.length r1 = List.length r2
      then List.map2 (fun t1 t2 -> AssertEqual (t1, t2, pp)) r1 r2
      else
        fail
          [
            `Text "Cannot unify tuples of different lengths: "
          ; `Type t1
          ; `Text " and "
          ; `Type t2
          ]
  | (T1 (Set, _) | T2 ((Map | Big_map), _, _)), Unknown _ ->
      [AssertEqual (t2, t1, pp)]
  | Unknown (i, _), _ ->
      emit [(i, SType t2)];
      []
  | _, Unknown (i, _) ->
      emit [(i, SType t1)];
      []
  | ( Variant {row = r1; var = i1; layout = l1}
    , Variant {row = r2; var = i2; layout = l2} )
  | ( Record {row = r1; var = i1; layout = l1}
    , Record {row = r2; var = i2; layout = l2} ) -> (
      let err_msg =
        match Type.unF t1 with
        | Variant _ -> "Different layouts for variant types"
        | Record _ -> "Different layouts for record types"
        | _ -> assert false
      in
      let err_missing lbl t =
        let s =
          match Type.unF t1 with
          | Variant _ -> "variant"
          | Record _ -> "field"
          | _ -> assert false
        in
        fail [text "Missing %s '%s' in type " s lbl; `Type t]
      in
      unify_layouts ~config ~err_msg rsubst l2 l1;
      match (i1, i2, unify_rows ~pp r1 r2) with
      | None, None, (cs, [], []) -> cs
      | None, Some i2, (cs, r1', []) ->
          emit [(i2, SRow (r1', None))];
          cs
      | Some i1, None, (cs, [], r2') ->
          emit [(i1, SRow (r2', None))];
          cs
      | Some i1, Some i2, (cs, r1', r2') ->
          let i3 = VarId.mk () in
          emit [(i1, SRow (r2', Some i3)); (i2, SRow (r1', Some i3))];
          cs
      | _, None, (_, (lbl, _) :: _, _) -> err_missing lbl t2
      | None, _, (_, _, (lbl, _) :: _) -> err_missing lbl t1)
  | Lambda (es1, t11, t12), Lambda (es2, t21, t22) ->
      let err () =
        [
          `Text "Lambdas have different effects: "
        ; `Type t1
        ; `Text " and "
        ; `Type t2
        ]
      in
      let with_storage_constraints =
        let unify_values ws1 ws2 =
          match (ws1, ws2) with
          | Some (a1, t1), Some (a2, t2) when Type.equal_access a1 a2 ->
              [AssertEqual (t1, t2, err)]
          | None, None -> []
          | _ -> fail (err ())
        in
        unify_holes ~unify_values
          (fun i -> Substitution.SWithStorage i)
          rsubst es1.with_storage es2.with_storage
      in
      equalize_holes ~err ~eq:( = )
        (fun i -> Substitution.SBool i)
        rsubst es1.with_operations es2.with_operations;
      [AssertEqual (t11, t21, pp); AssertEqual (t12, t22, pp)]
      @ with_storage_constraints
  | _ ->
      fail
        [
          `Text "Expected type '"
        ; `Type t2
        ; `Text "', but got '"
        ; `Type t1
        ; `Text "'."
        ]

let unify ~config pp rsubst t1 t2 =
  let cs = unify ~config pp rsubst t1 t2 in
  cs
