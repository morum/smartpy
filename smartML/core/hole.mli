(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

type 'a t =
  | Variable of VarId.t
  | Value of 'a
[@@deriving eq, ord, show, map, fold, sexp]

val variable : VarId.t -> 'a t

val value : 'a -> 'a t

val mk : unit -> 'a t

val get : 'a t -> 'a option

val get_value : 'a t -> 'a
