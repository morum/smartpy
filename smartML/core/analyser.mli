(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Basics
open Checked

val find_unassigned_no_init : Type.t -> string list option

val find_unassigned :
     [ `Private | `Storage ]
  -> Checked.bound * Type.t
  -> command
  -> string list option

val find_non_terminal_return : command -> line_no option

val collect_init_calls : command -> Qual_ident.Set.t
