(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

val export_type : Type.t -> string
