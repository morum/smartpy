(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Utils
open Control
open Basics
open Checked

let lens_string_map_at key =
  let open Lens in
  let unzip map =
    let focus = String.Map.find_opt key map in
    let zip = function
      | None -> String.Map.remove key map
      | Some v -> String.Map.add key v map
    in
    {focus; zip}
  in
  Lens.make unzip

let collect_init_calls =
  let open Qual_ident.Set in
  let f_tcommand _line_no _t : (unit, t, unit) command_f -> t = function
    | CCall_init (c, _, _) -> singleton c
    | CIf (_, a, b) -> inter a b
    | CMatch (_, branches) -> unions @@ List.map (fun (_, _, x) -> x) branches
    | CBlock bindings -> unions bindings
    | CExpr _
    | CAssign _
    | CVerify _
    | CNever _
    | CFailwith _
    | CFor _
    | CWhile _
    | CResult _
    | CComment _
    | CSet_type _
    | CSet_result_type _
    | CModify_product _
    | CModify_record _
    | CMatch_list _
    | CDel_item _
    | CUpdate_set _
    | CTrace _ -> empty
  in
  let alg =
    {f_texpr = (fun _line_no _t _c -> ()); f_tcommand; f_ttype = (fun _ -> ())}
  in
  cata_tcommand alg

let ( ||| ) x y =
  match x with
  | Some _ -> x
  | None -> y

let find_non_terminal_return =
  let f_tcommand line_no _t :
      (unit, bool -> line_no option, unit) command_f -> bool -> line_no option =
   fun c (return_ok : bool) ->
    match c with
    | CResult _ -> if return_ok then None else Some line_no
    | CFor (_, _, body) -> body false
    | CWhile (_, body) -> body false
    | CBlock [] -> None
    | CBlock bindings ->
        let bindings, b = List.unsnoc bindings in
        List.fold_left (fun r b -> r ||| b false) None bindings ||| b true
    | c ->
        let f x _ = x in
        fold_command_f f (fun r x -> r ||| x return_ok) f None c
  in
  let alg =
    {f_texpr = (fun _line_no _t _c -> ()); f_tcommand; f_ttype = (fun _ -> ())}
  in
  fun x -> cata_tcommand alg x true

type skeleton =
  | Leaf of bool
  | Node of skeleton String.Map.t

let rec map2_skeleton f x y =
  match (x, y) with
  | Leaf x, Leaf y -> Leaf (f x y)
  | Node xs, Node ys ->
      let f = function
        | Some x, Some y -> Some (map2_skeleton f x y)
        | _ -> assert false
      in
      Node (String.Map.merge (fun _ -> curry f) xs ys)
  | _ -> assert false

let rec skeleton_of_type = function
  | Type.F (Record {row; var = _}) ->
      (* TODO after refactoring closer call: assert (Option.is_none var); *)
      let row = List.map (map_snd skeleton_of_type) row in
      Node (String.Map.of_list row)
  | _ -> Leaf false

let path_of_expr root =
  let rec f = function
    | {e = EAttr (attr, x)} -> Option.map (fun x -> attr :: x) @@ f x
    | {e = EVar {name}} when Checked.equal_bound name root -> Some []
    | _ -> None
  in
  fun xs -> Option.map List.rev @@ f xs

let rec define_skeleton_all = function
  | Leaf _ -> Leaf true
  | Node xs -> Node (String.Map.map define_skeleton_all xs)

let rec define_skeleton_at (path : string list) s =
  match (path, s) with
  | [], x -> define_skeleton_all x
  | fld :: flds, Node xs ->
      let f = Option.map @@ define_skeleton_at flds in
      Node (Lens.modify xs (lens_string_map_at fld) f)
  | _ :: _, Leaf _ -> assert false

let simulate_assignments kind =
  let rec f var s c =
    match c.c with
    | CCall_init (_, _, d) -> (
        let {super} = d in
        match super with
        | None -> s
        | Some {init_storage; init_private; init_body} ->
            let var =
              match kind with
              | `Storage -> init_storage
              | `Private -> init_private
            in
            f var s init_body)
    | CAssign {lhs} -> (
        match path_of_expr var lhs with
        | None -> s
        | Some p -> define_skeleton_at p s)
    | CBlock bindings -> List.fold_left (f var) s bindings
    | CIf (_, x, y) ->
        let x = f var s x in
        let y = f var s y in
        map2_skeleton ( && ) x y
    | CExpr _
    | CNever _
    | CFailwith _
    | CVerify _
    | CMatch _
    | CModify_product _
    | CModify_record _
    | CMatch_list _
    | CDel_item _
    | CUpdate_set _
    | CFor _
    | CWhile _
    | CResult _
    | CComment _
    | CSet_type _
    | CSet_result_type _
    | CTrace _ -> s
  in
  f

let find_undefined =
  let rec find p = function
    | Leaf true -> None
    | Leaf false -> Some p
    | Node xs ->
        let f fld x r = Option.if_none r @@ fun () -> find (fld :: p) x in
        String.Map.fold_with_key f xs None
  in
  find []

let find_unassigned_no_init t =
  let s = skeleton_of_type t in
  Option.map List.rev @@ find_undefined s

let find_unassigned kind (var, t) c =
  let s = skeleton_of_type t in
  let s = simulate_assignments kind var s c in
  Option.map List.rev @@ find_undefined s
