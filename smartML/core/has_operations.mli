(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Basics
open Checked

type t =
  | HO_none
  | HO_at_most_one
  | HO_many
[@@deriving show, eq, ord]

val or_ : t -> t -> t

val add : t -> t -> t

val widen : t -> t

val command : command -> t

val expr : expr -> t
