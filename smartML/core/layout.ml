(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Utils
open Sexplib.Std

(* A layout explains where the leaves of a record type are
   positioned. This does *not* include the layout of any of its fields
   that are records. *)
type l = {
    source : string
  ; target : string
}
[@@deriving eq, ord, show {with_path = false}, sexp_of]

let l_of_sexp : Sexplib.Sexp.t -> l = function
  | Atom name -> (
      match Base.String.split (Base.String.strip name) ~on:' ' with
      | [source] -> {source; target = source}
      | [source; "as"; target] -> {source; target}
      | [source; "as"] -> {source; target = source}
      | _ -> Printf.ksprintf failwith "Bad attribute format %S" name)
  | _ -> failwith "Layout.l_of_sexp"

type t = l Binary_tree.t [@@deriving eq, ord, show {with_path = false}, sexp]

let leaf source target = Binary_tree.Leaf {source; target}

let on_row r =
  Binary_tree.map (fun l ->
      match List.assoc_opt l.source r with
      | Some t -> (l, t)
      | None -> assert false)
