(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Utils
open Basics
open Checked
open Control

let err ~line_no fmt =
  Printf.ksprintf
    (fun s ->
      raise (SmartExcept (with_loc ~line_no (`Text "error: " :: [`Text s]))))
    fmt

let build_expr ~line_no e et = {e; et; line_no}

let build_command ~line_no c ct = {c; ct; line_no}

type var = {
    typ : Type.t
  ; index : int
  ; occs : int (* TODO Use this to warn of unused definitions. *)
  ; derived : Checked.expr derived_var
}

type definition =
  | Def_variable of var
  | Def_module of Checked.module_
  | Def_type of Type.t
  | Def_contract of Checked.contract_def

let get_module definitions name =
  match String.Map.find_opt name definitions with
  | Some (Def_module m) -> Some m
  | _ -> None

let _dump_definitions definitions =
  let f x rhs =
    let rhs =
      match rhs with
      | Def_variable _ -> "variable"
      | Def_module _ -> "module"
      | Def_type _ -> "type"
      | Def_contract _ -> "contract"
    in
    Printf.printf "  %s = %s\n" x rhs
  in
  Printf.printf "definitions begin\n";
  String.Map.iter f definitions;
  Printf.printf "definitions end\n"

let unfold_abbrevs_type definitions =
  (* TODO Add line number information to parsed types. *)
  Type.cata (function
    | Abbrev {module_ = None; name; line_no} -> (
        match String.Map.find_opt name definitions with
        | Some (Def_type t) -> t
        | _ -> err ~line_no "Type abbreviation '%s' undefined." name)
    | Abbrev {module_ = Some module_; name; line_no} -> (
        match get_module definitions module_ with
        | Some m -> (
            match String.Map.find_opt name m.type_defs with
            | Some t -> t
            | None ->
                err ~line_no "Type abbreviation '%s.%s' undefined." module_ name
            )
        | None -> err ~line_no "Module '%s' undefined." module_)
    | t -> Type.F t)

let unfold_abbrevs_alg definitions =
  let open Unchecked in
  let f_expr line_no e = {e; et = U; line_no} in
  let f_command line_no c = {c; ct = U; line_no} in
  let f_type t = unfold_abbrevs_type definitions t in
  {f_expr; f_command; f_type}

let unfold_abbrevs_expr definitions = cata_expr (unfold_abbrevs_alg definitions)

let unfold_abbrevs_command definitions =
  cata_command (unfold_abbrevs_alg definitions)

let unfold_abbrevs_action definitions : Unchecked.action -> Unchecked.action =
  Unchecked.map_action_f
    (unfold_abbrevs_expr definitions)
    (unfold_abbrevs_command definitions)
    (unfold_abbrevs_type definitions)

let unfold_abbrevs_method definitions : Unchecked.method_ -> Unchecked.method_ =
  function
  | {kind; parameters; body; line_no} ->
      {
        kind
      ; parameters
      ; body = unfold_abbrevs_command definitions body
      ; line_no
      }

let string_of_static_id {static_id} =
  Printf.sprintf "static_contract%d" static_id

let string_of_dynamic_id {dynamic_id} =
  Printf.sprintf "dynamic_contract%d" dynamic_id

let string_of_contract_id = function
  | C_static id -> string_of_static_id id
  | C_dynamic id -> string_of_dynamic_id id

type state = {
    config : Config.t
  ; tvar_counter : int
  ; index_counter : int
  ; definitions : definition String.Map.t
  ; privates : Type.t String.Map.t
  ; constraints : (line_no * typing_constraint) list
  ; warnings : smart_except list list
}

module type ACTION = sig
  include MONAD_CORE

  val set : state -> unit t

  val get : state t

  val warn : smart_except list -> unit t

  val run : 'a t -> state -> 'a * state

  val map_list : ('a -> 'b t) -> 'a list -> 'b list t
end

module Action : ACTION = struct
  type 'a t = state -> 'a * state

  let run x s = x s

  let return (x : 'a) : 'a t = fun s -> (x, s)

  let bind (x : 'a t) (f : 'a -> 'b t) : 'b t =
   fun s ->
    let y, s = x s in
    f y s

  let map f x = bind x (fun x -> return (f x))

  let apply f x = bind f (fun f -> bind x (fun x -> return (f x)))

  let set s _ = ((), s)

  let get s = (s, s)

  let warn w s = ((), {s with warnings = w :: s.warnings})

  let map_list f =
    let rec map_list acc s = function
      | [] -> (List.rev acc, s)
      | x :: xs ->
          let y, s = f x s in
          map_list (y :: acc) s xs
    in
    flip (map_list [])
end

module ActionM = struct
  include Action
  include Monad (Action)
end

open ActionM

let get_definitions =
  let* s = get in
  return s.definitions

let get_config =
  let* s = get in
  return s.config

let modify f =
  let* s = get in
  set (f s)

let set_config config = modify (fun s -> {s with config})

let set_definitions definitions = modify (fun s -> {s with definitions})

let modify_definitions f =
  modify (fun s -> {s with definitions = f s.definitions})

let fresh_tvar =
  let* ({tvar_counter = i} as s) = get in
  let* () = set {s with tvar_counter = i + 1} in
  return (Type.unknown_raw ("a" ^ string_of_int i))

let next_index =
  let* ({index_counter = i} as s) = get in
  let* () = set {s with index_counter = i + 1} in
  return i

let add_constraint ~line_no c =
  modify (fun st -> {st with constraints = (line_no, c) :: st.constraints})

let assertEqual ~line_no ~pp t1 t2 =
  if Type.unF t1 != Type.unF t2
  then add_constraint ~line_no (AssertEqual (t1, t2, pp))
  else return ()

let checkComparable ~line_no x y =
  let pp () =
    with_loc ~line_no [text "comparison between"; `Expr x; `Text ", "; `Expr y]
  in
  let* () = assertEqual ~line_no x.et y.et ~pp in
  let* () = add_constraint ~line_no (IsComparable x) in
  let* () = add_constraint ~line_no (IsComparable y) in
  return ()

let solve_constraints =
  let* s = get in
  let* config = get_config in
  let subst = Solver.run ~config s.constraints in
  let* () = modify (fun s -> {s with constraints = []}) in
  return subst

let err ~line_no fmt =
  Printf.ksprintf
    (fun s ->
      raise (SmartExcept (with_loc ~line_no (`Text "error: " :: [`Text s]))))
    fmt

let add_variable ~line_no name rhs =
  modify (fun s ->
      let definitions =
        match String.Map.find_opt name s.definitions with
        | None -> String.Map.add name rhs s.definitions
        | Some _ -> err ~line_no "'%s' already defined." name
      in
      {s with definitions})

let add_var_general ~line_no ~kind ({bound} : Unchecked.bound) typ :
    (Checked.bound * Checked.expr derived_var) t =
  let* index = next_index in
  let derived = kind in
  let* () =
    modify_definitions (fun definitions ->
        match String.Map.find_opt bound definitions with
        | None ->
            String.Map.add bound
              (Def_variable {typ; index; occs = 0; derived})
              definitions
        | Some _ -> err ~line_no "Variable '%s' already defined." bound)
  in
  return ({bound; index}, derived)

let add_var_general' ~line_no ~kind bound typ =
  map fst @@ add_var_general ~line_no ~kind bound typ

let add_var ~line_no ~is_mutable =
  let kind = V_simple {is_mutable} in
  add_var_general' ~line_no ~kind

let add_local_var ~line_no ~is_mutable =
  let kind = V_simple {is_mutable} in
  add_var_general' ~line_no ~kind

let add_scenario_var ~line_no = add_var_general' ~line_no ~kind:V_scenario

let add_var_binding ~line_no (x : Unchecked.bound) typ ~is_mutable =
  let kind = V_simple {is_mutable} in
  if x.bound = "_"
  then
    let* index = next_index in
    return ({bound = x.bound; index}, kind)
  else add_var_general ~line_no ~kind x typ

let check_var_occ ~line_no ({bound} : Unchecked.bound) =
  let* {definitions} = get in
  match String.Map.find_opt bound definitions with
  | Some (Def_variable {typ; index; derived = V_scenario}) ->
      (* Don't count occurrences for scenario variables. *)
      let x = {bound; index} in
      return (typ, x, V_scenario)
  | Some (Def_variable ({typ; index; derived; occs} as v)) ->
      let* () =
        if occs = 1 && derived <> V_exploded
        then add_constraint ~line_no (IsNotHot (bound, typ))
        else return ()
      in
      let* () =
        set_definitions
          (String.Map.add bound
             (Def_variable {v with occs = occs + 1})
             definitions)
      in
      let x = {bound; index} in
      return (typ, x, derived)
  | Some _ -> err ~line_no "Variable '%s' is defined, but not a variable" bound
  | None -> err ~line_no "Variable '%s' is not defined" bound

let check_var ~line_no var =
  let* t, _index, _derived = check_var_occ ~line_no var in
  return t

let get_var_type_opt var =
  let* {definitions} = get in
  match String.Map.find_opt var definitions with
  | Some (Def_variable {typ}) -> return @@ Some typ
  | Some _ ->
      err ~line_no:None "Variable '%s' is defined, but not a variable" var
  | None -> return None

let add_definition ~line_no lhs rhs =
  modify (fun s ->
      let definitions =
        match String.Map.find_opt lhs s.definitions with
        | None -> String.Map.add lhs rhs s.definitions
        | Some _ -> err ~line_no "'%s' already defined." lhs
      in
      {s with definitions})

let _dump_contract_defs contract_defs =
  (*let contract_defs =   (String.Map.to_list contract_defs) in*)
  let f (n, _) = Printf.printf "%S = ..\n" n in
  Printf.printf "contract_defs begin\n";
  List.iter f contract_defs;
  Printf.printf "contract_defs end\n"

let remove_definition ~line_no ({bound} : Checked.bound) =
  modify_definitions (fun definitions ->
      match String.Map.find_opt bound definitions with
      | None -> err ~line_no "Missing variable '%s'." bound
      | Some _ ->
          String.Map.filter
            (fun x' _ -> not (String.equal x' bound))
            definitions)

let scoped_definitions f x =
  let* {definitions = original_vars} = get in
  let* () = modify (fun s -> {s with definitions = f original_vars}) in
  let* r = x in
  let* () = modify (fun s -> {s with definitions = original_vars}) in
  return r

let scoped x =
  let* {definitions; privates} = get in
  let* r = x in
  let* () = modify (fun s -> {s with definitions; privates}) in
  return r

let scoped_no_occs x =
  let* old_definitions = get_definitions in
  let f = function
    | Def_variable v -> Def_variable {v with occs = 0}
    | other -> other
  in
  let* () = set_definitions (String.Map.map f old_definitions) in
  let* r = x in
  let* () = set_definitions old_definitions in
  return r

let scoped_branch x =
  let* old_definitions = get_definitions in
  let* r = x in
  let* new_definitions = get_definitions in
  let* () = set_definitions old_definitions in
  return
    ( String.Map.map snd (String.Map.intersect old_definitions new_definitions)
    , r )

let max_occs b1 b2 =
  let f _ x y =
    match (x, y) with
    | Some (Def_variable x), Some (Def_variable y) ->
        Some (Def_variable {x with occs = max x.occs y.occs})
    | Some a, _ | _, Some a -> Some a
    | None, None -> None
  in
  String.Map.merge f b1 b2

let no_new_occs ~line_no old_definitions new_definitions =
  let f name x y =
    match (x, y) with
    | Some (Def_variable {typ; occs = 0}), Some (Def_variable {occs = 1}) ->
        Some (add_constraint ~line_no (IsNotHot (name, typ)))
        (* TODO better error message *)
    | _ -> None
  in
  iter_list id String.Map.(values (merge f old_definitions new_definitions))

let for_variant ~line_no name t =
  let open Type in
  match name with
  | "None" ->
      let* () =
        assertEqual ~line_no t Type.unit ~pp:(fun () ->
            [text "Argument to None must be unit."])
      in
      let* t = fresh_tvar in
      return (option t)
  | "Some" -> return (option t)
  | "Left" ->
      let* u = fresh_tvar in
      return (tor t u)
  | "Right" ->
      let* u = fresh_tvar in
      return (tor u t)
  | _ -> return (uvariant name t)

let check_assignable line_no (vars : definition String.Map.t) =
  let f x y =
    match (x, y) with
    | Some x, _ | _, Some x -> Some x
    | None, None -> None
  in
  let alg = monoid_talg f None in
  let f_texpr ln t = function
    | EVar {name} -> (
        match String.Map.find_opt name.bound vars with
        | None -> Some [text "Variable '%s' is not defined." name.bound]
        | Some (Def_variable {derived = kind}) -> (
            match kind with
            | V_simple {is_mutable = false}
            | V_scenario | V_constant | V_match_cons | V_defined _ ->
                Some [text "Variable '%s' is not mutable." name.bound]
            | V_simple {is_mutable = true} | V_exploded -> None)
        | Some _ -> Some [text "'%s' is not mutable." name.bound])
    | EItem {items; key = _} -> items
    | e -> alg.f_texpr ln t e
  in
  let alg = {alg with f_texpr} in
  fun e ->
    match cata_texpr alg e with
    | None -> ()
    | Some err -> raise (SmartExcept (with_loc ~line_no err))

let err_undefined_module ~line_no module_name =
  raise
    (SmartExcept
       [
         `Text (Printf.sprintf "Module '%s' is undefined." module_name)
       ; `Br
       ; `Text "Did you forget it to add it to the scenario?"
       ; `Br
       ; `Text
           (Printf.sprintf
              "Please use either '<scenario> = sp.test_scenario(%s)' or \
               '<scenario>.add_module(%s)'."
              module_name module_name)
       ; `Line line_no
       ])

let err_undefined_contract ~line_no module_name name =
  let module_name = Option.cata "" (fun x -> x ^ ".") module_name in
  raise
    (SmartExcept
       [
         `Text
           (Printf.sprintf "Contract class %s%s has not been defined."
              module_name name)
       ; `Line line_no
       ])

let find_contract_def ~line_no {qi_module; qi_ident} : Checked.contract_def t =
  let* {definitions} = get in
  match qi_module with
  | None -> (
      match String.Map.find_opt qi_ident definitions with
      | Some (Def_contract cd) -> return cd
      | _ -> err_undefined_contract ~line_no None qi_ident)
  | Some module_name -> (
      let module_ : Checked.module_ =
        match String.Map.find_opt module_name definitions with
        | Some (Def_module m) -> m
        | _ -> err_undefined_module ~line_no module_name
      in
      match String.Map.find_opt qi_ident module_.contract_defs with
      | Some cd -> return cd
      | None -> err_undefined_contract ~line_no (Some module_name) qi_ident)

let check_returns body =
  let ntr = Analyser.find_non_terminal_return body in
  match ntr with
  | None -> ()
  | Some line_no -> err ~line_no "'return' in non-terminal position."

type context = {
    current_init_supers :
      (qual_ident * (Checked.command, Type.t) init_f option) list option
}

let context0 = {current_init_supers = None}

let rec check_command context ({c; line_no} : Unchecked.command) :
    Checked.command t =
  let* config = get_config in
  let module Printer = (val Printer.get ~config : Printer.Printer) in
  let remove_var = remove_definition ~line_no in
  let add_constraint = add_constraint ~line_no in
  let check_command =
    check_command context (* TODO the context should change under lambdas *)
  in
  match c with
  | CCall_init (name, call, _) ->
      let init_supers =
        match context.current_init_supers with
        | None ->
            err ~line_no
              "Can call __init__ only from the __init__  of an inheriting class"
        | Some s -> s
      in
      let init_super =
        match List.assoc_opt name init_supers with
        | Some i -> i
        | None ->
            err ~line_no "%s is not a superclass" (Printer.qual_ident name)
      in
      let params = Option.cata [] (fun x -> x.init_params) init_super in
      let* call, _args =
        check_call ~line_no
          {qi_module = name.qi_module; qi_ident = name.qi_ident ^ ".__init__"}
          params call
      in
      let* _t, storage, _derived =
        check_var_occ ~line_no (mk_bound "__storage__")
      in
      let* _t, private_, _derived =
        check_var_occ ~line_no (mk_bound "__private__")
      in
      let derived = {super = init_super; storage; private_} in
      return {c = CCall_init (name, call, derived); ct = Type.unit; line_no}
  | CMatch (scrutinee, cases) ->
      let* scrutinee = check_expr scrutinee in
      let* rt = fresh_tvar in
      let check_case (constructor, param, body) =
        scoped_branch
          (let* at = fresh_tvar in
           let* vt = for_variant ~line_no constructor at in
           let* () =
             assertEqual ~line_no scrutinee.et vt ~pp:(fun () ->
                 [
                   `Text "Match scrutinee has incompatible type:"
                 ; `Expr scrutinee
                 ; `Br
                 ; `Text "Expected type:"
                 ; `Type vt
                 ])
           in
           let* param = add_var ~line_no ~is_mutable:false param at in
           let* body = check_command body in
           let* () =
             assertEqual ~line_no body.ct rt ~pp:(fun () ->
                 [
                   `Text "Match branch has type "
                 ; `Type body.ct
                 ; `Text " instead of "
                 ; `Type rt
                 ])
           in
           let* () = remove_var param in
           return (constructor, param, body))
      in
      let* cases = map_list check_case cases in
      let* () =
        let cases = List.map fst cases in
        set_definitions @@ List.(fold_left max_occs String.Map.empty cases)
      in
      let c = CMatch (scrutinee, List.map snd cases) in
      return (build_command ~line_no c rt)
  | CMatch_list {expr; id; clause_cons; clause_nil} ->
      let* expr = check_expr expr in
      let* at = fresh_tvar in
      let vt = Type.list at in
      let* () =
        assertEqual ~line_no expr.et vt ~pp:(fun () ->
            [
              `Text "match list scrutinee is not a list: "
            ; `Expr expr
            ; `Br
            ; `Text "Expected type: "
            ; `Type vt
            ])
      in
      let* vars1, (id, clause_cons) =
        scoped_branch
          (let t = Type.record_default_layout [("head", at); ("tail", vt)] in
           let* id = add_var_general' ~line_no ~kind:V_match_cons id t in
           let* r = check_command clause_cons in
           return (id, r))
      in
      let* vars2, clause_nil = scoped_branch (check_command clause_nil) in
      let* () = set_definitions @@ max_occs vars1 vars2 in
      let* () =
        assertEqual ~line_no clause_cons.ct clause_nil.ct ~pp:(fun () ->
            [`Text "sp.match_cons: cannot unify branches"])
      in
      let c = CMatch_list {expr; id; clause_cons; clause_nil} in
      return (build_command ~line_no c clause_cons.ct)
  | CModify_product (_, _, Pattern_record _, _) -> assert false
  | CModify_record (e, x, body) ->
      let* e = check_expr e in
      let t = Type.urecord [] in
      let* () =
        assertEqual ~line_no e.et t ~pp:(fun () ->
            [
              `Text "sp.match_record scrutinee is not a record: "
            ; `Expr e
            ; `Br
            ; `Text "Expected type: "
            ; `Type t
            ])
      in
      let* x = add_var_general' ~line_no ~kind:V_exploded x e.et in
      let* body = check_command body in
      let* () =
        assertEqual ~line_no body.ct Type.unit ~pp:(fun () ->
            [`Text "sp.modify_record result type must not have an sp.result"])
      in
      let c' = CModify_record (e, x, body) in
      return (build_command ~line_no c' Type.unit)
  | CModify_product (_, Some _, Pattern_tuple _, _) -> assert false
  | CModify_product (lhs, None, Pattern_tuple vars, body) ->
      if List.length vars < 2
      then err ~line_no "sp.modify_tuple requires at least two arguments ";
      let* lhs = check_expr lhs in
      let* vars =
        map_list
          (fun n ->
            let* t = fresh_tvar in
            return (n, t))
          vars
      in
      let t = Type.tuple (List.map snd vars) in
      let* () =
        assertEqual ~line_no lhs.et t ~pp:(fun () ->
            [
              `Text "scrutinee is not a tuple: "
            ; `Expr lhs
            ; `Br
            ; `Text "Expected type: "
            ; `Type t
            ])
      in
      let* body, vars =
        scoped
          (let* vars =
             map_list
               (fun (x, t) -> add_local_var ~line_no ~is_mutable:true x t)
               vars
           in
           let* body = check_command body in
           return (body, vars))
      in
      let* () =
        assertEqual ~line_no body.ct t ~pp:(fun () ->
            [
              `Text "sp.match_record result type: "
            ; `Type body.ct
            ; `Br
            ; `Text "Expected type: "
            ; `Type t
            ])
      in
      let c' = CModify_product (lhs, None, Pattern_tuple vars, body) in
      return (build_command ~line_no c' Type.unit)
  | CModify_product (_, Some _, Pattern_single _, _body) -> assert false
  | CModify_product (lhs, None, Pattern_single x, body) ->
      (* TODO check whether lhs is assignable *)
      let* lhs = check_expr lhs in
      let* body, x =
        scoped
          (let* x = add_local_var ~line_no ~is_mutable:true x lhs.et in
           let* body = check_command body in
           return (body, x))
      in
      let* () =
        assertEqual ~line_no body.ct lhs.et ~pp:(fun () ->
            [
              `Text "sp.match result type: "
            ; `Type body.ct
            ; `Br
            ; `Text "Expected type: "
            ; `Type lhs.et
            ])
      in
      let c' = CModify_product (lhs, None, Pattern_single x, body) in
      return (build_command ~line_no c' Type.unit)
  | CExpr e ->
      let* e = check_expr e in
      let* () =
        assertEqual ~line_no e.et Type.unit ~pp:(fun () ->
            [
              `Text "Expression of type "
            ; `Type e.et
            ; `Text " instead of sp.unit."
            ])
      in
      return {c = CExpr e; ct = Type.unit; line_no}
  | CAssign {lhs = {e = ETuple ns; line_no}; rhs} ->
      let* rhs = check_expr rhs in
      let scrutinee = rhs in
      let* ns =
        ns
        |> map_list @@ function
           | ({e = EVar x} : Unchecked.expr) ->
               let* t = fresh_tvar in
               return (x.name, t)
           | _ -> failwith "not a variable"
      in
      if List.length ns < 2
      then err ~line_no "Matched tuple must have at least two elements";
      let t = Type.tuple @@ List.map snd ns in
      let* () =
        assertEqual ~line_no scrutinee.et t ~pp:(fun () ->
            [
              `Text "match tuple scrutinee is not a tuple: "
            ; `Expr scrutinee
            ; `Br
            ; `Text "Expected type: "
            ; `Type t
            ])
      in
      let* (ns : (bound * Type.t) list) =
        ns
        |> map_list (fun (x, t) ->
               let* x = add_var ~line_no ~is_mutable:true x t in
               return (x, t))
      in
      let derived = Product (Pattern_tuple (List.map fst ns)) in
      let xs =
        ns
        |> List.map @@ fun (name, et) ->
           let e = EVar {name; derived = V_simple {is_mutable = true}} in
           {e; et; line_no}
      in
      let lhs = {e = ETuple xs; et = t; line_no} in
      return @@ {c = CAssign {lhs; rhs; derived}; ct = Type.unit; line_no}
  | CAssign {lhs = {e = ERecord bs; line_no}; rhs} ->
      let* rhs = check_expr rhs in
      let scrutinee = rhs in
      if List.length bs < 1
      then err ~line_no "Must match at least one record field";
      let bs =
        bs
        |> List.map @@ function
           | fld, Unchecked.{e = EVar {name}} -> (fld, name, line_no)
           | _ -> failwith "not a variable"
      in
      let* bs =
        bs
        |> map_list @@ fun (fld, name, line_no) ->
           let* et = fresh_tvar in
           let* name = add_var ~line_no ~is_mutable:true name et in
           let e = EVar {name; derived = V_simple {is_mutable = true}} in
           return (fld, name, {e; et; line_no})
      in
      let fields = List.map (fun (field, _, e) -> (field, e.et)) bs in
      let t = Type.urecord fields in
      let* () =
        assertEqual ~line_no scrutinee.et t ~pp:(fun () ->
            [
              `Text "match tuple scrutinee is not a record:"
            ; `Expr scrutinee
            ; `Br
            ; `Text "Expected type:"
            ; `Type t
            ])
      in
      let derived =
        let bs = List.map (fun (field, var, _) -> Checked.{var; field}) bs in
        Product (Pattern_record bs)
      in
      let lhs =
        let e = ERecord (List.map (fun (field, _, e) -> (field, e)) bs) in
        {e; et = t; line_no}
      in
      return {c = CAssign {lhs; rhs; derived}; ct = Type.unit; line_no}
  | CAssign {lhs; rhs} -> (
      let* rhs = check_expr rhs in
      let* {definitions} = get in
      match lhs with
      | {e = EVar {name; derived = U}; line_no} ->
          let* lhs, d =
            match String.Map.find_opt name.bound definitions with
            | None ->
                let* name, derived =
                  add_var_binding ~line_no ~is_mutable:true name rhs.et
                in
                let e = EVar {name; derived} in
                let lhs = {e; et = rhs.et; line_no} in
                return (lhs, Var_def name)
            | Some (Def_variable {typ; index; derived}) ->
                let name = Checked.{bound = name.bound; index} in
                let* () = assertEqual ~line_no typ rhs.et ~pp:(fun () -> []) in
                let e = EVar {name; derived} in
                let lhs = {e; et = rhs.et; line_no} in
                check_assignable line_no definitions lhs;
                return (lhs, Assign lhs)
            | Some _ -> err ~line_no "Cannot assign to '%s'." name.bound
          in
          return {c = CAssign {lhs; rhs; derived = d}; ct = Type.unit; line_no}
      | {line_no} ->
          let* lhs = scoped_no_occs (check_expr lhs) in
          check_assignable line_no definitions lhs;
          let* () = assertEqual ~line_no lhs.et rhs.et ~pp:(fun () -> []) in
          return
            {
              c = CAssign {lhs; rhs; derived = Assign lhs}
            ; ct = Type.unit
            ; line_no
            })
  | CBlock [] -> return (build_command ~line_no (CBlock []) Type.unit)
  | CBlock bindings ->
      scoped
        (let bindings', last = List.unsnoc bindings in
         let* bindings' =
           map_list
             (fun x ->
               let* x = check_command x in
               let pp () = [`Text "Unexpected non-unit return type."] in
               let* () = assertEqual ~line_no ~pp x.ct Type.unit in
               return x)
             bindings'
         in
         let* last = check_command last in
         return (build_command ~line_no (CBlock (bindings' @ [last])) last.ct))
  | CFor (name, e, body) ->
      let* e = check_expr e in
      let* t = fresh_tvar in
      let* () =
        assertEqual ~line_no e.et (Type.list t) ~pp:(fun () ->
            [
              text "for (%s : %s) in " name.bound (Printer.type_to_string t)
            ; `Expr e
            ])
      in
      let* old_definitions = get_definitions in
      let* new_definitions, (name, body) =
        scoped_branch
          (let* name = add_var ~line_no ~is_mutable:true name t in
           let* body = check_command body in
           return (name, body))
      in
      let* () = no_new_occs ~line_no old_definitions new_definitions in
      let* () =
        assertEqual ~line_no body.ct Type.unit ~pp:(fun () ->
            [`Text "for-loop body has non-unit type "; `Type body.ct])
      in
      return (build_command ~line_no (CFor (name, e, body)) Type.unit)
  | CIf (cond, c1, c2) ->
      let* cond = check_expr cond in
      let* definitions1, c1 = scoped_branch (check_command c1) in
      let* definitions2, c2 = scoped_branch (check_command c2) in
      let* () = set_definitions (max_occs definitions1 definitions2) in
      let* () =
        assertEqual ~line_no cond.et Type.bool ~pp:(fun () ->
            [`Text "sp.if "; `Expr cond])
      in
      let* () =
        assertEqual ~line_no c1.ct c2.ct ~pp:(fun () ->
            [`Text "sp.if: cannot unify branches"])
      in
      return (build_command ~line_no (CIf (cond, c1, c2)) c1.ct)
  | CSet_result_type (c, t) ->
      let* c = check_command c in
      let* () =
        assertEqual ~line_no c.ct t ~pp:(fun () ->
            [`Type c.ct; `Text " is not compatible with type "; `Type t])
      in
      return (build_command ~line_no (CSet_result_type (c, t)) t)
  | CWhile (e, body) ->
      let* e = check_expr e in
      let* () =
        assertEqual ~line_no e.et Type.bool ~pp:(fun () ->
            [`Text "while "; `Expr e])
      in
      let* old_definitions = get_definitions in
      let* new_definitions, body = scoped_branch (check_command body) in
      let* () = no_new_occs ~line_no old_definitions new_definitions in
      let* () =
        assertEqual ~line_no body.ct Type.unit ~pp:(fun () ->
            [`Text "while body has non-unit type "; `Type body.ct])
      in
      return (build_command ~line_no (CWhile (e, body)) Type.unit)
  | CSet_type (e, t) ->
      let* e = scoped_no_occs (check_expr e) in
      let* () =
        assertEqual ~line_no e.et t ~pp:(fun () ->
            [`Expr e; `Text " is not compatible with type "; `Type t])
      in
      return (build_command ~line_no (CSet_type (e, t)) Type.unit)
  | CVerify (e, message) ->
      let* e = check_expr e in
      let* message = sequence_option (Option.map check_expr message) in
      let* () =
        assertEqual ~line_no e.et Type.bool ~pp:(fun () ->
            [`Text "not a boolean expression"])
      in
      return (build_command ~line_no (CVerify (e, message)) Type.unit)
  | CDel_item (x, y) ->
      let* x = check_expr x in
      let* y = check_expr y in
      let* tvalue = fresh_tvar in
      let* () = add_constraint (IsAnyMap (y.et, tvalue, x)) in
      return (build_command ~line_no (CDel_item (x, y)) Type.unit)
  | CUpdate_set (x, y, add) ->
      let* x = check_expr x in
      let* y = check_expr y in
      let pp () =
        [
          `Expr x
        ; `Text "."
        ; `Text (if add then "add" else "remove")
        ; `Text "("
        ; `Expr y
        ; `Text ")"
        ]
      in
      let* () = assertEqual ~line_no x.et (Type.set ~telement:y.et) ~pp in
      return (build_command ~line_no (CUpdate_set (x, y, add)) Type.unit)
  | CFailwith x ->
      let* x = check_expr x in
      let* t = fresh_tvar in
      return (build_command ~line_no (CFailwith x) t)
  | CNever e ->
      let* e = check_expr e in
      let* () =
        assertEqual ~line_no e.et Type.never ~pp:(fun () ->
            [`Text "Bad type in sp.never "; `Expr e])
      in
      let* t = fresh_tvar in
      return (build_command ~line_no (CNever e) t)
  | CResult e ->
      let* e = check_expr e in
      return (build_command ~line_no (CResult e) e.et)
  | CComment x -> return (build_command ~line_no (CComment x) Type.unit)
  | CTrace x ->
      let* x = check_expr x in
      return (build_command ~line_no (CTrace x) Type.unit)

and check_view ~line_no ~tstorage ~tprivate
    ({view_kind; view_name; has_param; pure; body; doc} : Unchecked.view) :
    Checked.view t =
  scoped_no_occs
    (let storage = mk_bound "__storage__" in
     let parameter = mk_bound "__parameter__" in
     let private_ = mk_bound "__private__" in
     let* storage = add_local_var ~line_no ~is_mutable:true storage tstorage in
     let* t = fresh_tvar in
     let* parameter = add_local_var ~line_no ~is_mutable:true parameter t in
     let* _private_ =
       add_local_var ~line_no ~is_mutable:false private_ tprivate
     in
     let* body = check_command context0 body in
     check_returns body;
     let derived =
       {parameter; storage; tparameter = (if has_param then Some t else None)}
     in
     return {view_kind; view_name; has_param; pure; body; doc; derived})

and check_entrypoint ~config ~tparameter ~tstorage ~tprivate
    ({channel; tparameter_ep; check_no_incoming_transfer; line_no; body} :
      Unchecked.entrypoint) : Checked.entrypoint t =
  let is_param = function
    | {e = EVar {name = {bound = "__parameter__"}}} -> true
    | _ -> false
  in
  let uses_param =
    exists_command ~exclude_create_contract:false is_param (fun _ -> false)
  in
  scoped_no_occs
    (let parameter = mk_bound "__parameter__" in
     let storage = mk_bound "__storage__" in
     let private_ = mk_bound "__private__" in
     let operations = mk_bound "__operations__" in
     let contract = mk_bound "__contract__" in
     let* operations =
       add_var ~line_no ~is_mutable:true operations (Type.list Type.operation)
     in
     let* storage = add_local_var ~line_no ~is_mutable:true storage tstorage in
     let* private_ =
       add_local_var ~line_no ~is_mutable:false private_ tprivate
     in
     let* tparameter_ep' =
       match tparameter_ep with
       | `Absent -> return Type.unit
       | `Present -> fresh_tvar
       | `Annotated t -> return t
     in
     let* parameter =
       add_local_var ~line_no ~is_mutable:true parameter tparameter_ep'
     in
     let* _contract =
       add_var ~line_no ~is_mutable:false contract (Type.contract tparameter)
     in
     let* body = check_command context0 body in
     let* () =
       assertEqual ~line_no body.ct Type.unit ~pp:(fun () ->
           [`Text "Entrypoint "; `Text channel; `Text " has a sp.result."])
     in
     let* () =
       if config.Config.warn_unused && tparameter_ep = `Present
          && not (uses_param body)
       then
         warn
           (with_loc ~line_no
              [text "Entrypoint '%s' has unused parameter." channel])
       else return ()
     in
     let derived =
       {
         parameter
       ; storage
       ; private_
       ; operations
       ; tparameter_ep = tparameter_ep'
       }
     in
     check_returns body;
     return
       {
         channel
       ; tparameter_ep
       ; check_no_incoming_transfer
       ; line_no
       ; body
       ; derived
       })

and check_mprim0 ~line_no :
    Michelson_base.Type.mtype Michelson_base.Primitive.prim0 -> _ = function
  | Self None ->
      let parameter_contract = mk_bound "__parameter_contract__" in
      check_var ~line_no parameter_contract
  | Self (Some name) ->
      let contract = mk_bound "__contract__" in
      let* t_contract = check_var ~line_no contract in
      let* t_ep = fresh_tvar in
      let* () =
        assertEqual ~line_no
          (Type.contract (Type.uvariant name t_ep))
          t_contract
          ~pp:(fun () -> [`Text "sp.self_entrypoint: incompatible type"])
      in
      return (Type.contract t_ep)
  | p ->
      let* config = get_config in
      let module Printer = (val Printer.get ~config : Printer.Printer) in
      let t = Michelson_base.Typing.type_prim0 p in
      return (Type.of_mtype t)

and check_mprim1 ~line_no p x =
  let* config = get_config in
  let module Printer = (val Printer.get ~config : Printer.Printer) in
  let name = Printer.mprim1_to_string p in
  let t1, t = Michelson_base.Typing.type_prim1 p in
  let t1 = Type.of_mtype t1 in
  let* () =
    assertEqual ~line_no x.et t1 ~pp:(fun () ->
        [text "'%s' expects a " name; `Type t1; `Text ", but got: "; `Type x.et])
  in
  return (Type.of_mtype t)

and check_mprim2 ~line_no p x1 x2 =
  let* config = get_config in
  let module Printer = (val Printer.get ~config : Printer.Printer) in
  let name = Printer.mprim2_to_string p in
  match Michelson_base.Typing.type_prim2 p with
  | [((t1, t2), t)] ->
      let t1 = Type.of_mtype t1 in
      let t2 = Type.of_mtype t2 in
      let* () =
        assertEqual ~line_no x1.et t1 ~pp:(fun () ->
            [
              text "'%s' expects a " name
            ; `Type t1
            ; `Text " as its first argument, but got: "
            ; `Type x1.et
            ])
      in
      let* () =
        assertEqual ~line_no x2.et t2 ~pp:(fun () ->
            [
              text "'%s' expects a " name
            ; `Type t2
            ; `Text " as its second argument, but got: "
            ; `Type x2.et
            ])
      in
      return (Type.of_mtype t)
  | instances ->
      let* t = fresh_tvar in
      let e = build_expr ~line_no (EMPrim2 (p, x1, x2)) t in
      let instances =
        List.map
          (fun ((t1, t2), t) ->
            ((Type.of_mtype t1, Type.of_mtype t2), Type.of_mtype t))
          instances
      in
      let* () =
        add_constraint ~line_no
          (IsInstance2
             ( Michelson_base.(Primitive.show_prim2 Type.pp_mtype p)
             , e
             , (x1.et, x2.et)
             , instances ))
      in
      return t

and check_mprim3 ~line_no p x1 x2 x3 =
  let* config = get_config in
  let module Printer = (val Printer.get ~config : Printer.Printer) in
  let name = Printer.mprim3_to_string p in
  let t1, t2, t3, t = Michelson_base.Typing.type_prim3 p in
  let t1 = Type.of_mtype t1 in
  let t2 = Type.of_mtype t2 in
  let t3 = Type.of_mtype t3 in
  let* () =
    assertEqual ~line_no x1.et t1 ~pp:(fun () ->
        [
          text "'%s' expects a " name
        ; `Type t1
        ; `Text " as its first argument, but got: "
        ; `Type x1.et
        ])
  in
  let* () =
    assertEqual ~line_no x2.et t2 ~pp:(fun () ->
        [
          text "'%s' expects a '" name
        ; `Type t2
        ; `Text " as its second argument, but got: "
        ; `Type x2.et
        ])
  in
  let* () =
    assertEqual ~line_no x3.et t3 ~pp:(fun () ->
        [
          text "'%s' expects a " name
        ; `Type t3
        ; `Text " as its third argument, but got: "
        ; `Type x3.et
        ])
  in
  return (Type.of_mtype t)

and name_view view id = view ^ "_view_" ^ string_of_contract_id id

and check_prim0 ~line_no =
  let check_var = check_var ~line_no in
  let assertEqual = assertEqual ~line_no in
  function
  | ECst_contract {type_} -> return (Type.contract type_)
  | EContract_data id ->
      let name = mk_bound @@ string_of_contract_id id ^ "_storage" in
      check_var name
  | EContract_private id ->
      let name = mk_bound @@ string_of_contract_id id ^ "_private" in
      check_var name
  | EAccount_of_seed {seed = _} -> return Type.account
  | EContract_address _ -> return Type.address
  | EContract_balance _ -> return Type.mutez
  | EContract_baker _ -> return (Type.option Type.key_hash)
  | EContract_typed (id, entrypoint) -> (
      let name = mk_bound @@ string_of_contract_id id in
      let* t = check_var name in
      match entrypoint with
      | None -> return t
      | Some entrypoint ->
          let* t_ep = fresh_tvar in
          let* () =
            assertEqual
              (Type.contract (Type.uvariant entrypoint t_ep))
              t
              ~pp:(fun () ->
                [
                  `Text "sp.contract: incompatible type or missing entrypoint "
                ; `Text entrypoint
                ; `Type t_ep
                ; `Type t
                ])
          in
          return (Type.contract t_ep))
  | EConstant (_, t) -> return t

and check_prim1 ~line_no prim =
  let assertEqual = assertEqual ~line_no in
  let add_constraint = add_constraint ~line_no in
  match prim with
  | ESum ->
      fun x ->
        let t = Type.intOrNat () in
        let* () =
          assertEqual x.et (Type.list t) ~pp:(fun () -> [`Text "sum "; `Expr x])
        in
        return t
  | ESet_delegate ->
      fun x ->
        let* () =
          assertEqual x.et (Type.option Type.key_hash) ~pp:(fun () ->
              [`Text "not an optional hash"])
        in
        return Type.operation
  | EUnpack t ->
      fun x ->
        let pp () = [`Text "unpack "; `Expr x; `Type t] in
        let* () = add_constraint (IsPackable t) in
        let* () = assertEqual x.et Type.bytes ~pp in
        return (Type.option t)
  | EPack ->
      fun x ->
        let* () = add_constraint (IsPackable x.et) in
        return Type.bytes
  | EImplicit_account ->
      fun x ->
        let* () =
          assertEqual x.et Type.key_hash ~pp:(fun () ->
              [`Text "sp.implicit_account("; `Expr x; `Text ")"])
        in
        return (Type.contract Type.unit)
  | ETo_nat ->
      fun x ->
        let* () = assertEqual x.et Type.bytes ~pp:(fun () -> []) in
        return Type.nat
  | ETo_int ->
      fun x ->
        let* () = add_constraint (HasInt x) in
        return Type.int
  | ETo_bytes ->
      fun x ->
        let* () = assertEqual x.et (Type.intOrNat ()) ~pp:(fun () -> []) in
        return Type.bytes
  | ENot ->
      fun x ->
        let* () = assertEqual x.et Type.bool ~pp:(fun () -> []) in
        return Type.bool
  | EInvert ->
      fun x ->
        let* () = assertEqual x.et Type.int ~pp:(fun () -> []) in
        return Type.int
  | EInvert_bytes ->
      fun x ->
        let* () = assertEqual x.et Type.bytes ~pp:(fun () -> []) in
        return Type.bytes
  | ENeg ->
      fun x ->
        let* t = fresh_tvar in
        let* () = add_constraint (HasNeg (x, t)) in
        return t
  | ESign ->
      fun x ->
        let pp () = [text "Incompatible argument to sp.sign."] in
        let* () = assertEqual x.et Type.int ~pp in
        return Type.int
  | EConcat_list ->
      fun x ->
        let* t = fresh_tvar in
        let pp () = [`Text "Incompatible argument to sp.concat."] in
        let* () = assertEqual x.et (Type.list t) ~pp in
        let* () = add_constraint (IsStringOrBytes t) in
        return t
  | ESize ->
      fun x ->
        let* () = add_constraint (HasSize x) in
        return Type.nat
  | EList_rev ->
      fun x ->
        let* t = fresh_tvar in
        let* () =
          assertEqual x.et (Type.list t) ~pp:(fun () ->
              [`Expr x; `Text ".rev()"])
        in
        return (Type.list t)
  | EList_items _rev ->
      fun x ->
        let* tkey = fresh_tvar in
        let* tvalue = fresh_tvar in
        let* () =
          assertEqual x.et (Type.map ~big:false ~tkey ~tvalue) ~pp:(fun () ->
              [`Expr x; `Text ".items()"])
        in
        return (Type.list (Type.key_value tkey tvalue))
  | EList_keys _rev ->
      fun x ->
        let* tkey = fresh_tvar in
        let* tvalue = fresh_tvar in
        let* () =
          assertEqual x.et (Type.map ~big:false ~tkey ~tvalue) ~pp:(fun () ->
              [`Expr x; `Text ".keys()"])
        in
        return (Type.list tkey)
  | EList_values _rev ->
      fun x ->
        let* tkey = fresh_tvar in
        let* tvalue = fresh_tvar in
        let* () =
          assertEqual x.et (Type.map ~big:false ~tkey ~tvalue) ~pp:(fun () ->
              [`Expr x; `Text ".values()"])
        in
        return (Type.list tvalue)
  | EList_elements _rev ->
      fun x ->
        let* telement = fresh_tvar in
        let* () =
          assertEqual x.et (Type.set ~telement) ~pp:(fun () ->
              [`Expr x; `Text ".elements()"])
        in
        return (Type.list telement)
  | EAddress ->
      fun x ->
        let* t = fresh_tvar in
        let* () =
          assertEqual x.et (Type.contract t) ~pp:(fun () ->
              [`Text "sp.to_address("; `Expr x; `Text ")"])
        in
        return Type.address
  | EType_annotation t ->
      fun x ->
        let pp () =
          [
            `Text "Mismatch type annotation"
          ; `Expr x
          ; `Br
          ; `Text "Expected type:"
          ; `Br
          ; `Type t
          ]
        in
        let* () = assertEqual x.et t ~pp in
        return t
  | EIs_variant name ->
      fun x ->
        let* t = fresh_tvar in
        let tvariant =
          match name with
          | "None" | "Some" -> Type.option t
          | _ -> Type.uvariant name t
        in
        let* () =
          assertEqual x.et tvariant ~pp:(fun () ->
              [
                `Text "Incompatible variant types: "
              ; `Type x.et
              ; `Text " and "
              ; `Type tvariant
              ])
        in
        return Type.bool
  | EFst ->
      fun x ->
        let* t1 = fresh_tvar in
        let* t2 = fresh_tvar in
        let* () =
          assertEqual x.et (Type.pair t1 t2) ~pp:(fun () ->
              [`Expr x; `Text " is not a pair"])
        in
        return t1
  | ESnd ->
      fun x ->
        let* t1 = fresh_tvar in
        let* t2 = fresh_tvar in
        let* () =
          assertEqual x.et (Type.pair t1 t2) ~pp:(fun () ->
              [`Expr x; `Text " is not a pair"])
        in
        return t2
  | ERead_ticket ->
      fun x ->
        let* t = fresh_tvar in
        let* () =
          assertEqual x.et (Type.ticket t) ~pp:(fun () ->
              [`Expr x; `Text " is not a valid ticket"])
        in
        let t_data = Type.tuple [Type.address; t; Type.nat] in
        return (Type.pair t_data x.et)
  | EJoin_tickets ->
      fun x ->
        let* t = fresh_tvar in
        let ticket_t = Type.ticket t in
        let* () =
          assertEqual x.et (Type.pair ticket_t ticket_t) ~pp:(fun () ->
              [`Expr x; `Text " is not a valid ticket list"])
        in
        return (Type.option ticket_t)
  | EPairing_check ->
      fun x ->
        let* () =
          assertEqual x.et
            (Type.list (Type.pair Type.bls12_381_g1 Type.bls12_381_g2))
            ~pp:(fun () ->
              [`Expr x; `Text " is not a valid list of BLS12 pairs"])
        in
        return Type.bool
  | EVoting_power ->
      fun x ->
        let* () =
          assertEqual x.et Type.key_hash ~pp:(fun () ->
              [
                `Expr x
              ; `Text "voting_power requires tz1[1-3] address as parameter"
              ])
        in
        return Type.nat
  | EConvert ->
      fun x ->
        let* u = fresh_tvar in
        let* () = add_constraint (IsConvertible (x.et, u)) in
        return u
  | EStatic_view (static_id, name) ->
      fun params ->
        let* t =
          check_var ~line_no @@ mk_bound @@ name_view name (C_static static_id)
        in
        let* return_type = fresh_tvar in
        let* () =
          assertEqual t
            Type.(lambda no_effects params.et return_type)
            ~pp:(fun () ->
              [
                `Text "Wrong view type"
              ; `Br
              ; `Type t
              ; `Br
              ; `Type params.et
              ; `Br
              ; `Type return_type
              ; `Br
              ])
        in
        return (Type.option return_type)
  | EEmit (_tag, _with_type) -> fun _x -> return Type.operation

and check_prim2 ~line_no p x y =
  let assertEqual = assertEqual ~line_no in
  let add_constraint = add_constraint ~line_no in
  let pp_bin op x y () =
    with_loc ~line_no [text op; `Expr x; `Text ", "; `Expr y]
  in
  let pp_bin_op op x y () = with_loc ~line_no [`Expr x; `Text op; `Expr y] in
  let e = EPrim2 (p, x, y) in
  match p with
  | ELe | ELt | EGe | EGt | EEq | ENeq ->
      let* () = checkComparable ~line_no x y in
      return Type.bool
  | EAdd ->
      let* () = assertEqual x.et y.et ~pp:(pp_bin_op "+" x y) in
      let e = build_expr ~line_no e x.et in
      let* () = add_constraint (HasAdd (e, x, y)) in
      return x.et
  | EMod ->
      let pp = pp_bin_op "%" x y in
      let* () = assertEqual x.et y.et ~pp in
      let* () = assertEqual x.et (Type.intOrNat ()) ~pp in
      return Type.nat
  | EEDiv ->
      let* a = fresh_tvar in
      let* b = fresh_tvar in
      let et = Type.option (Type.pair a b) in
      let* () = add_constraint (HasDiv (et, x, y)) in
      return et
  | EDiv ->
      let pp () =
        [
          `Text "in expression "
        ; `Expr x
        ; `Text " / "
        ; `Expr y
        ; `Br
        ; `Text "Both expressions must be of type "
        ; `Type Type.nat
        ]
      in
      let* () = assertEqual x.et y.et ~pp in
      let* a = fresh_tvar in
      let* b = fresh_tvar in
      let et = Type.option (Type.pair a b) in
      let* () = add_constraint (HasDiv (et, x, y)) in
      return a
  | ESub ->
      let pp = pp_bin_op "-" x y in
      let* () = assertEqual x.et y.et ~pp in
      let* et = fresh_tvar in
      let e = build_expr ~line_no e et in
      let* () = add_constraint (HasSub (e, x, y)) in
      return et
  | ELshift_infix ->
      let pp () =
        [
          `Text "bad type for left shift:"
        ; `Expr x
        ; `Text " << "
        ; `Expr y
        ; `Text " is not a nat"
        ]
      in
      let* () = assertEqual x.et Type.nat ~pp in
      let* () = assertEqual y.et Type.nat ~pp in
      return Type.nat
  | ERshift_infix ->
      let pp () =
        [
          `Text "bad type for right shift:"
        ; `Expr x
        ; `Text " >> "
        ; `Expr y
        ; `Text " is not a nat"
        ]
      in
      let* () = assertEqual x.et Type.nat ~pp in
      let* () = assertEqual y.et Type.nat ~pp in
      return Type.nat
  | ELshift_bytes ->
      let pp () =
        [
          `Text "bad type for left shift:"
        ; `Expr x
        ; `Text " lshift "
        ; `Expr y
        ; `Text " is not bytes"
        ]
      in
      let* () = assertEqual x.et Type.bytes ~pp in
      let* () = assertEqual y.et Type.nat ~pp in
      return Type.bytes
  | ERshift_bytes ->
      let pp () =
        [
          `Text "bad type for right shift:"
        ; `Expr x
        ; `Text " rshift "
        ; `Expr y
        ; `Text " is not bytes"
        ]
      in
      let* () = assertEqual x.et Type.bytes ~pp in
      let* () = assertEqual y.et Type.nat ~pp in
      return Type.bytes
  | EAnd | EOr ->
      let pp () = [] in
      let* () = assertEqual x.et Type.bool ~pp in
      let* () = assertEqual y.et Type.bool ~pp in
      return Type.bool
  | EAnd_bytes | EOr_bytes ->
      let pp () = [] in
      let* () = assertEqual x.et Type.bytes ~pp in
      let* () = assertEqual y.et Type.bytes ~pp in
      return Type.bytes
  | EAnd_infix | EOr_infix ->
      let pp () = [] in
      let* () = assertEqual x.et Type.nat ~pp in
      let* () = assertEqual y.et Type.nat ~pp in
      return Type.nat
  | EXor_bytes ->
      let pp () = [] in
      let* () = assertEqual x.et Type.bytes ~pp in
      let* () = assertEqual y.et Type.bytes ~pp in
      return Type.bytes
  | EXor_infix ->
      let pp () = [] in
      let* () = assertEqual x.et Type.nat ~pp in
      let* () = assertEqual y.et Type.nat ~pp in
      return Type.nat
  | EMax ->
      let pp = pp_bin "max" x y in
      let* () = assertEqual x.et y.et ~pp in
      let* () = assertEqual x.et (Type.intOrNat ()) ~pp in
      return x.et
  | EMin ->
      let pp = pp_bin "min" x y in
      let* () = assertEqual x.et y.et ~pp in
      let* () = assertEqual x.et (Type.intOrNat ()) ~pp in
      return x.et
  | EMul_homo ->
      let* () = assertEqual x.et y.et ~pp:(pp_bin_op "*" x y) in
      let e = build_expr ~line_no e x.et in
      let* () = add_constraint (HasMul (e, x, y)) in
      return x.et
  | EContains ->
      let* () = add_constraint (HasContains (y, x, line_no)) in
      return Type.bool
  | ECons ->
      let x, l = (x, y) in
      let* () =
        assertEqual l.et (Type.list x.et) ~pp:(fun () ->
            [`Text "cannot cons "; `Exprs [x; l]])
      in
      return l.et
  | EGet_opt ->
      let* r = fresh_tvar in
      let* () = add_constraint (IsAnyMap (x.et, r, y)) in
      return (Type.option r)
  | EAdd_seconds ->
      let t, s = (x, y) in
      let pp () = [`Text "sp.add_seconds("; `Exprs [t; s]; `Text ")"] in
      let* () = assertEqual Type.timestamp t.et ~pp in
      let* () = assertEqual Type.int s.et ~pp in
      return Type.timestamp
  | EApply_lambda ->
      let parameter, lambda = (x, y) in
      let* t1 = fresh_tvar in
      let* t2 = fresh_tvar in
      let* () =
        assertEqual lambda.et
          (Type.lambda Type.no_effects (Type.pair parameter.et t1) t2)
          ~pp:(fun () ->
            [`Expr lambda; `Text " cannot be applied to "; `Expr parameter])
      in
      return Type.(lambda no_effects t1 t2)
  | ETicket ->
      let content, amount = (x, y) in
      let* () =
        assertEqual amount.et Type.nat ~pp:(fun () ->
            [`Expr amount; `Text " is not a valid ticket amount"])
      in
      return (Type.ticket content.et)
  | ESplit_ticket ->
      let ticket, decomposition = (x, y) in
      let* t = fresh_tvar in
      let ticket_t = Type.ticket t in
      let* () =
        assertEqual ticket.et ticket_t ~pp:(fun () ->
            [`Expr ticket; `Text " is not a ticket"])
      in
      let* () =
        assertEqual decomposition.et (Type.pair Type.nat Type.nat)
          ~pp:(fun () ->
            [
              `Expr decomposition
            ; `Text " is not a ticket decomposition (nat * nat) for"
            ; `Expr ticket
            ])
      in
      return (Type.option (Type.pair ticket_t ticket_t))
  | EView (_name, return_type) ->
      let _params, address = (x, y) in
      let* () =
        assertEqual address.et Type.address ~pp:(fun () -> [`Expr address])
      in
      return (Type.option return_type)

and check_prim3 ~line_no p =
  let assertEqual = assertEqual ~line_no in
  let add_constraint = add_constraint ~line_no in
  match p with
  | ERange ->
      fun a b step ->
        let pp () =
          [
            `Text "Range takes integers, it cannot be applied to"
          ; `Exprs [a; b; step]
          ]
        in
        let* () = assertEqual a.et b.et ~pp in
        let* () = assertEqual a.et step.et ~pp in
        let* () = add_constraint (IsInt (a.et, pp)) in
        return (Type.list a.et)
  | ESplit_tokens ->
      fun mutez quantity total ->
        let pp () =
          [`Text "sp.split_tokens("; `Exprs [mutez; quantity; total]; `Text ")"]
        in
        let* () = assertEqual Type.mutez mutez.et ~pp in
        let* () = assertEqual Type.nat quantity.et ~pp in
        let* () = assertEqual Type.nat total.et ~pp in
        return Type.mutez
  | EUpdate_map ->
      fun key value map ->
        let* tvalue = fresh_tvar in
        let pp () =
          [
            `Expr value
          ; `Text " is of type "
          ; `Type value.et
          ; `Text ", but should be an option."
          ]
        in
        let* () = assertEqual value.et (Type.option tvalue) ~pp in
        let* () = add_constraint (IsAnyMap (key.et, tvalue, map)) in
        return map.et
  | EGet_and_update ->
      fun key value map ->
        let* tvalue = fresh_tvar in
        let pp () =
          [
            `Expr value
          ; `Text " is of type "
          ; `Type value.et
          ; `Text ", but should be an option."
          ]
        in
        let* () = assertEqual value.et (Type.option tvalue) ~pp in
        let* () = add_constraint (IsAnyMap (key.et, tvalue, map)) in
        return (Type.pair value.et map.et)
  | ETest_ticket ->
      fun ticketer content amount ->
        let* () =
          assertEqual ticketer.et Type.address ~pp:(fun () ->
              [
                `Expr ticketer
              ; `Text " is not a valid ticketer (it should be an address)"
              ])
        in
        let* () =
          assertEqual amount.et Type.nat ~pp:(fun () ->
              [`Expr amount; `Text " is not a valid ticket amount"])
        in
        return (Type.ticket content.et)

and check_lambda ~line_no
    Unchecked.{lambda_var; body; with_storage; with_operations; recursive} =
  let* tstorage_in_scope = get_var_type_opt "__storage__" in
  let f =
    String.Map.filter @@ fun _name -> function
    | Def_variable (v : var) -> (
        match v.derived with
        | V_scenario | V_defined _ -> true
        | _ -> false)
    | Def_contract _ -> true
    | Def_module _ -> true
    | Def_type _ -> true
  in
  scoped_definitions f
    (let* derived_with_storage =
       match with_storage with
       | None -> return None
       | Some (access, tstorage_explicit) ->
           let* tstorage =
             match (tstorage_explicit, tstorage_in_scope) with
             | Some t, _ -> return t
             | None, Some t -> return t
             | None, None ->
                 let* t = fresh_tvar in
                 let* () = add_constraint ~line_no (DefaultsToUnit t) in
                 return t
           in
           let is_mutable =
             match access with
             | Read_write -> true
             | Read_only -> false
           in
           let storage = mk_bound "__storage__" in
           let* storage = add_local_var ~line_no ~is_mutable storage tstorage in
           return (Some (access, tstorage, storage))
     in
     let* derived_with_operations =
       if with_operations
       then
         let ops = mk_bound "__operations__" in
         let* ops =
           add_var ~line_no ~is_mutable:true ops (Type.list Type.operation)
         in
         return (Some ops)
       else return None
     in
     let* tParams = fresh_tvar in
     let* () = add_constraint ~line_no (DefaultsToUnit tParams) in
     let* tResult = fresh_tvar in
     let* lambda_var = add_var ~line_no ~is_mutable:false lambda_var tParams in
     let* recursive =
       match recursive with
       | None -> return None
       | Some recursive ->
           let msg = "Recursive lambdas cannot have effects." in
           if Option.is_some with_storage || with_operations
           then raise (SmartExcept [`Text msg]);
           let* r =
             add_var ~line_no ~is_mutable:false recursive
               Type.(lambda no_effects tParams tResult)
           in
           return (Some r)
     in
     let* body = check_command context0 body in
     let* () =
       assertEqual ~line_no body.ct tResult ~pp:(fun () ->
           [
             `Text "lambda result type "
           ; `Type body.ct
           ; `Text " is not "
           ; `Type tResult
           ])
     in
     let derived =
       let with_operations = derived_with_operations in
       let with_storage = derived_with_storage in
       {tParams; tResult; with_operations; with_storage}
     in
     check_returns body;
     return
       {lambda_var; body; with_storage; with_operations; recursive; derived})

and check_expr ({line_no} as ee : Unchecked.expr) : Checked.expr t =
  let* config = get_config in
  let protocol = config.protocol in
  let assertEqual = assertEqual ~line_no in
  let add_constraint = add_constraint ~line_no in
  let k e' t = return (build_expr ~line_no e' t) in
  let* {definitions} = get in
  match ee.e with
  | ELambda l ->
      let* l = check_lambda ~line_no l in
      let derived = l.derived in
      let t =
        Type.(
          lambda
            {
              with_storage =
                Value
                  (Option.map
                     (fun (access, t, _) -> (access, t))
                     derived.with_storage)
            ; with_operations = Value l.with_operations
            }
            derived.tParams derived.tResult)
      in
      return (build_expr ~line_no (ELambda l) t)
  | ECreate_contract {push; template_ref; baker; balance; storage} ->
      let* cd = find_contract_def ~line_no template_ref in
      let c = cd.derived in
      let* baker = check_expr baker in
      let* balance = check_expr balance in
      let* storage = check_expr storage in
      let* () =
        assertEqual baker.et (Type.option Type.key_hash) ~pp:(fun () ->
            [`Expr baker; `Text " is not a valid optional baker in sp.contract"])
      in
      let* () =
        assertEqual balance.et Type.mutez ~pp:(fun () ->
            [
              `Expr balance; `Text " is not a valid amount in sp.create_contract"
            ])
      in
      let* () =
        assertEqual storage.et c.tstorage ~pp:(fun () ->
            [`Text "sp.create_contract: incompatible storage type"])
      in
      let e =
        ECreate_contract
          {push; template_ref; baker; balance; storage; derived = cd}
      in
      let et =
        if push
        then Type.address
        else
          Type.record_default_layout
            [("operation", Type.operation); ("address", Type.address)]
      in
      return (build_expr ~line_no e et)
  | EAttr (d, Unchecked.{e = EVar {name = {bound = mod_name}}})
    when Option.is_some @@ get_module definitions mod_name ->
      let m = Option.get @@ get_module definitions mod_name in
      let x =
        match String.Map.find_opt d m.defs with
        | None ->
            err ~line_no "Module '%s' does not have definition '%s'." mod_name d
        | Some x -> x
      in
      let name : Checked.bound =
        {bound = Printf.sprintf "%s.%s" mod_name d; index = -1}
      in
      let derived = V_defined x in
      k (EVar {name; derived}) x.et
  | EVar {name} ->
      let* t, name, derived = check_var_occ ~line_no name in
      k (EVar {name; derived}) t
  | EPrivate name -> (
      let* {privates} = get in
      match String.Map.find_opt name privates with
      | Some t -> k (EPrivate name) t
      | None -> err ~line_no "Private variable 'self.%s' is not defined." name)
  | EList xs ->
      let* t = fresh_tvar in
      let item x =
        let* x = check_expr x in
        let* () =
          assertEqual x.et t ~pp:(fun () ->
              [
                `Text "bad type for list item "
              ; `Expr x
              ; `Text " is not a"
              ; `Type t
              ])
        in
        return x
      in
      let* xs = map_list item xs in
      k (EList xs) (Type.list t)
  | EList_comprehension (e, x, xs) ->
      let* xs = check_expr xs in
      scoped
      @@ let* a = fresh_tvar in
         let* () = assertEqual (Type.list a) xs.et ~pp:(fun () -> []) in
         let* x = add_var ~line_no ~is_mutable:false x a in
         let* e = check_expr e in
         k (EList_comprehension (e, x, xs)) (Type.list e.et)
  | EMPrim0 p ->
      let* t = check_mprim0 ~line_no p in
      k (EMPrim0 p) t
  | EMPrim1 (p, x) ->
      let* x = check_expr x in
      let* t = check_mprim1 ~line_no p x in
      k (EMPrim1 (p, x)) t
  | EMPrim1_fail _ -> assert false
  | EMPrim2 (Compare, x1, x2) ->
      let* x1 = check_expr x1 in
      let* x2 = check_expr x2 in
      let* () = checkComparable ~line_no x1 x2 in
      k (EMPrim2 (Compare, x1, x2)) Type.int
  | EMPrim2 (p, x1, x2) ->
      let* x1 = check_expr x1 in
      let* x2 = check_expr x2 in
      let* t = check_mprim2 ~line_no p x1 x2 in
      k (EMPrim2 (p, x1, x2)) t
  | EMPrim3 (p, x1, x2, x3) ->
      let* x1 = check_expr x1 in
      let* x2 = check_expr x2 in
      let* x3 = check_expr x3 in
      let* t = check_mprim3 ~line_no p x1 x2 x3 in
      k (EMPrim3 (p, x1, x2, x3)) t
  | ELiteral (Int {i; is_nat} as x) -> (
      match Hole.get is_nat with
      | Some true when Big_int.sign_big_int i < 0 ->
          err ~line_no "sp.nat cannot take negative values"
      | _ -> k (ELiteral x) (Type.type_of_literal x))
  | ELiteral (Mutez i) when Big_int.sign_big_int i < 0 ->
      err ~line_no "sp.tez/sp.mutez cannot take negative values"
  | ELiteral x -> k (ELiteral x) (Type.type_of_literal x)
  | EPrim0 p ->
      let* t = check_prim0 ~line_no p in
      k (EPrim0 p) t
  | EPrim1 (p, x) ->
      let* x = check_expr x in
      let* t = check_prim1 ~line_no p x in
      k (EPrim1 (p, x)) t
  | EPrim2 (p, x, y) ->
      let* x = check_expr x in
      let* y = check_expr y in
      let* t = check_prim2 ~line_no p x y in
      k (EPrim2 (p, x, y)) t
  | EPrim3 (p, x, y, z) ->
      let* x = check_expr x in
      let* y = check_expr y in
      let* z = check_expr z in
      let* t = check_prim3 ~line_no p x y z in
      k (EPrim3 (p, x, y, z)) t
  | EAttr (name, x) ->
      let* x = check_expr x in
      let* t = fresh_tvar in
      let* () =
        assertEqual x.et
          (Type.urecord [(name, t)])
          ~pp:(fun () ->
            [`Expr x; text " has no field '%s' of type " name; `Type t; `Br])
      in
      k (EAttr (name, x)) t
  | EVariant (name, x) ->
      let* x = check_expr x in
      let* t = for_variant ~line_no name x.et in
      k (EVariant (name, x)) t
  | ECall (l, arg) ->
      let* arg = map_option check_expr arg in
      let* l = check_expr l in
      let* r = fresh_tvar in
      let* with_operations =
        let* toperations = get_var_type_opt "__operations__" in
        match toperations with
        | None -> return @@ Hole.value false
        | Some _ -> return @@ Hole.mk ()
      in
      let* with_storage =
        let* tstorage = get_var_type_opt "__storage__" in
        match tstorage with
        | None -> return @@ Hole.value None
        | Some t ->
            let h = Hole.mk () in
            let* () = add_constraint @@ WithStorage (t, h) in
            return h
      in
      let effects : _ Type.effects_f = {with_storage; with_operations} in
      let* () =
        match arg with
        | None ->
            assertEqual l.et (Type.lambda effects Type.unit r) ~pp:(fun () ->
                [`Expr l; `Text " expects an argument."])
        | Some arg ->
            assertEqual l.et (Type.lambda effects arg.et r) ~pp:(fun () ->
                [`Expr l; `Text " cannot be applied to "; `Expr arg])
      in
      k (ECall (l, arg)) r
  | EOpen_variant (name, x, missing_message) ->
      let* x = check_expr x in
      let* missing_message =
        sequence_option (Option.map check_expr missing_message)
      in
      let* et =
        match name with
        | "None" -> return Type.unit
        | _ -> fresh_tvar
      in
      let tv =
        match name with
        | "Some" | "None" -> Type.option et
        | _ -> Type.uvariant name et
      in
      let* () =
        assertEqual x.et tv ~pp:(fun () ->
            [`Expr x; `Text "has no variant "; `Text name; `Br])
      in
      k (EOpen_variant (name, x, missing_message)) et
  | ESlice {offset; length; buffer} ->
      let* offset = check_expr offset in
      let* length = check_expr length in
      let* buffer = check_expr buffer in
      let pp expr () = [`Text "sp.slice "; `Expr expr] in
      let* () = assertEqual offset.et Type.nat ~pp:(pp offset) in
      let* () = assertEqual length.et Type.nat ~pp:(pp length) in
      let* () = add_constraint (IsStringOrBytes buffer.et) in
      k (ESlice {offset; length; buffer}) (Type.option buffer.et)
  | EItem {items; key; default_value; missing_message} ->
      (* TODO Fix data type to enforce that at most one of
         'default_value' and 'missing_message' is defined. *)
      let* items = check_expr items in
      let* key = check_expr key in
      let* default_value =
        sequence_option (Option.map check_expr default_value)
      in
      let* missing_message =
        sequence_option (Option.map check_expr missing_message)
      in
      let* et = fresh_tvar in
      let* () = add_constraint (HasGetItem (items, key, et)) in
      let* () =
        match default_value with
        | None -> return ()
        | Some e ->
            assertEqual e.et et ~pp:(fun () ->
                [`Expr items; `Text "bad type for default value."; `Expr e])
      in
      k (EItem {items; key; default_value; missing_message}) et
  | ERecord entries ->
      let layout = Hole.mk () in
      let f (x, e) =
        let* e = check_expr e in
        return (x, e)
      in
      let* entries = map_list f entries in
      let row = List.map (fun (x, {et}) -> (x, et)) entries in
      k (ERecord entries) (Type.record_or_unit layout row)
  | EMap (big, entries) ->
      let* tkey = fresh_tvar in
      let* tvalue = fresh_tvar in
      let item (k, v) =
        let* k = check_expr k in
        let* v = check_expr v in
        let* () =
          assertEqual k.et tkey ~pp:(fun () ->
              [
                `Text "bad type for map key "
              ; `Expr k
              ; `Text " is not a "
              ; `Type tkey
              ])
        in
        let* () =
          assertEqual v.et tvalue ~pp:(fun () ->
              [
                `Text "bad type for map value "
              ; `Expr v
              ; `Text " is not a "
              ; `Type tvalue
              ])
        in
        return (k, v)
      in
      let* entries = map_list item entries in
      k (EMap (big, entries)) (Type.map ~big ~tkey ~tvalue)
  | ESet entries ->
      let* telement = fresh_tvar in
      let check k =
        let* k = check_expr k in
        let* () =
          assertEqual k.et telement ~pp:(fun () ->
              [
                `Text "bad type for set element"
              ; `Expr k
              ; `Text "is not an element of"
              ; `Type (Type.set ~telement)
              ])
        in
        return k
      in
      let* entries = map_list check entries in
      k (ESet entries) (Type.set ~telement)
  | ETransfer {arg; amount; destination} ->
      let* arg = check_expr arg in
      let* amount = check_expr amount in
      let* destination = check_expr destination in
      let pp () =
        [`Text "transfer("; `Exprs [arg; amount; destination]; `Text ")"]
      in
      let* () = assertEqual destination.et (Type.contract arg.et) ~pp in
      let* () = assertEqual amount.et Type.mutez ~pp in
      k (ETransfer {arg; amount; destination}) Type.operation
  | EMake_signature {secret_key; message; message_format} ->
      let* secret_key = check_expr secret_key in
      let* message = check_expr message in
      let pp e () = [`Text "sp.make_signature"; `Expr e] in
      let* () = assertEqual Type.secret_key secret_key.et ~pp:(pp secret_key) in
      let* () = assertEqual Type.bytes message.et ~pp:(pp message) in
      k (EMake_signature {secret_key; message; message_format}) Type.signature
  | EContract {arg_type; address; entrypoint} ->
      let* address = check_expr address in
      let* () =
        assertEqual address.et Type.address ~pp:(fun () -> [`Expr address])
      in
      k
        (EContract {arg_type; address; entrypoint})
        (Type.option (Type.contract arg_type))
  | EMap_function {l; f} ->
      let* l = check_expr l in
      let* f = check_expr f in
      let* s = fresh_tvar in
      let* t = fresh_tvar in
      let* target = fresh_tvar in
      let* () =
        assertEqual f.et
          Type.(lambda no_effects s t)
          ~pp:(fun () -> [`Text "map"; `Expr f])
      in
      let e = build_expr ~line_no (EMap_function {l; f}) target in
      let* () = add_constraint (HasMap (e, l, f)) in
      k (EMap_function {l; f}) target
  | EMichelson ({name; parsed; typesIn; typesOut}, args) ->
      let* args = map_list check_expr args in
      let pp () = [`Text "sp.michelson"; `Text name; `Exprs args] in
      let err message = raise (SmartExcept [`Text message; `Rec (pp ())]) in
      let code = Michelson.Of_micheline.instruction parsed in
      let ts = List.map (Typing.mtype_of_type ~with_annots:true) typesIn in
      let code =
        Michelson.typecheck_instr ~protocol
          ~tparameter:(Michelson.mt_unit, None)
          (* TODO: tparameter *)
          (Stack_ok ts) code
      in
      let* t_out =
        match (typesOut, code.stack_out) with
        | _, Error msg ->
            raise
              (SmartExcept
                 [`Text "Error in inlined Michelson code: "; `Text msg])
        | [t], Ok Stack_failed ->
            return t (* FAILED is compatible with anything *)
        | [t], Ok (Stack_ok [t_result]) ->
            let t_specified = Typing.mtype_of_type ~with_annots:false t in
            if Michelson.unifiable_types t_result t_specified
            then return t
            else
              raise
                (SmartExcept
                   [
                     `Text "Output from Michelson code is of type "
                   ; `Type (Type.of_mtype t_result)
                   ; `Text "Expected: "
                   ; `Type t
                   ])
        | [_], Ok (Stack_ok _) ->
            raise
              (SmartExcept
                 [`Text "Non-singleton output stack from Michelson code"])
        | _ -> assert false
      in
      let rec matchTypes tin targs =
        match (tin, targs) with
        | [], [] -> return ()
        | [], _ -> err "Too many arguments"
        | _, [] -> err "Too few arguments"
        | t1 :: tin, t2 :: targs ->
            let* () = assertEqual t1 t2 ~pp in
            matchTypes tin targs
      in
      let* () = matchTypes (List.map (fun e -> e.et) args) typesIn in
      k (EMichelson ({name; parsed; typesIn; typesOut}, args)) t_out
  | ETuple es ->
      let* es = map_list check_expr es in
      k (ETuple es) (Type.tuple (List.map (fun {et} -> et) es))
  | ESapling_verify_update {state; transaction} ->
      let* state = check_expr state in
      let* transaction = check_expr transaction in
      let* () =
        assertEqual transaction.et (Type.sapling_transaction None)
          ~pp:(fun () ->
            [
              `Expr transaction; `Text "is expected to be a sapling transaction "
            ])
      in
      let* () =
        assertEqual state.et (Type.sapling_state None) ~pp:(fun () ->
            [`Expr state; `Text "is expected to be a sapling state "])
      in
      let* () = add_constraint (SaplingVerify (state, transaction)) in
      k
        (ESapling_verify_update {state; transaction})
        (Type.option (Type.tuple [Type.bytes; Type.int; state.et]))
  | EIf (cond, a, b) ->
      let* cond = check_expr cond in
      let* a = check_expr a in
      let* b = check_expr b in
      let* () =
        assertEqual a.et b.et ~pp:(fun () ->
            [`Text "incompatible result types in if-then-else"])
      in
      let* () =
        assertEqual cond.et Type.bool ~pp:(fun () ->
            [`Text "non-boolean condition in if-then-else"])
      in
      k (EIf (cond, a, b)) a.et
  | EIs_failing e ->
      let* e = check_expr e in
      k (EIs_failing e) Type.bool
  | ECatch_exception (t, e) ->
      let* e = check_expr e in
      k (ECatch_exception (t, e)) (Type.option t)

and check_call_positional ~line_no name (params : (string * Type.t) list) args =
  let l_actual = List.length args in
  let numeralish adjective = function
    | 0 -> if adjective then "no" else "none"
    | 1 -> "one"
    | 2 -> "two"
    | n -> string_of_int n
  in
  let argument_s = function
    | 1 -> "argument"
    | _ -> "arguments"
  in
  let l_formal = List.length params in
  if l_actual <> l_formal
  then
    err ~line_no "%s expects %s %s, but got %s." name (numeralish true l_formal)
      (argument_s l_formal)
      (numeralish false l_actual);
  let f (_, formal) actual =
    let pp () =
      [
        `Text "Expected argument of type "
      ; `Type formal
      ; `Text ", but got "
      ; `Type actual.et
      ; `Line line_no
      ]
    in
    assertEqual ~line_no ~pp formal actual.et
  in
  iter2_list f params args

and check_call_keyword ~line_no name (formal_args : (string * Type.t) list)
    kargs =
  let fail fmt =
    Printf.ksprintf
      (fun err -> raise (SmartExcept [`Text err; `Line line_no]))
      fmt
  in
  let (_ : String.Set.t) =
    let f kargs (k, _) =
      if String.Set.mem k kargs then fail "Keyword '%s' given twice." k;
      String.Set.add k kargs
    in
    List.fold_left f String.Set.empty kargs
  in
  let () =
    let f (k, _) =
      if not (List.mem_assoc k formal_args)
      then fail "'%s' does not expect argument '%s'." name k
    in
    List.iter f kargs
  in
  let* r =
    let f (k, t) =
      let e =
        match List.assoc_opt k kargs with
        | None -> fail "'%s' requires argument '%s'." name k
        | Some e -> e
      in
      let* () = assertEqual ~line_no ~pp:(fun () -> []) e.et t in
      return e
    in
    map_list f formal_args
  in
  return r

and check_call ~line_no {qi_module; qi_ident} (formal_args : _ list) =
  let formal_args = List.map (fun (x, t) -> (x.bound, t)) formal_args in
  let name = Option.cata "" (fun x -> x ^ ".") qi_module ^ qi_ident in
  function
  | Call_positional args ->
      let* args = map_list check_expr args in
      let* () = check_call_positional ~line_no name formal_args args in
      return (Call_positional args, args)
  | Call_keyword kargs ->
      let f (k, v) =
        let* v = check_expr v in
        return (k, v)
      in
      let* kargs = map_list f kargs in
      let* args = check_call_keyword ~line_no name formal_args kargs in
      return (Call_keyword kargs, args)

let check_context {source; sender; chain_id; time; level; voting_powers} =
  let check_aa = function
    | Address a ->
        let* a = check_expr a in
        return (Address a)
    | Account a -> return (Account a)
  in
  let* source = map_option check_aa source in
  let* sender = map_option check_aa sender in
  let* chain_id = map_option check_expr chain_id in
  let* time = map_option check_expr time in
  let* level = map_option check_expr level in
  let* voting_powers = map_option check_expr voting_powers in
  return {source; sender; chain_id; time; level; voting_powers}

let check_new_entrypoint ~tparameter ~tstorage ~tprivate name parameters
    (body : Unchecked.command) line_no : Checked.entrypoint t =
  let* {config} = get in
  let ep : _ Unchecked.entrypoint_f =
    {
      channel = name
    ; tparameter_ep = (if List.length parameters > 0 then `Present else `Absent)
    ; check_no_incoming_transfer = None
    ; line_no
    ; body
    ; derived = U
    }
  in
  check_entrypoint ~config ~tparameter ~tstorage ~tprivate ep

let _method_name (m : method_) =
  match m.kind with
  | Init _ -> "__init__"
  | Entrypoint (name, _)
  | Private (name, _, _, _)
  | Onchain_view (name, _)
  | Offchain_view (name, _) -> name

let is_init : Unchecked.method_ -> bool = function
  | {kind = Init _} -> true
  | _ -> false

let is_init_checked : Checked.method_ -> bool = function
  | {kind = Init _} -> true
  | _ -> false

let extract_init ~line_no (methods : Checked.method_ list) =
  match List.filter is_init_checked methods with
  | [] -> None
  | [{kind = Init _; parameters; body; line_no}] ->
      Some (parameters, body, line_no)
  | _ -> err ~line_no "A contract class can have only one __init__ method."

let rec check_init tstorage tprivate parents
    (parameters, (body : Unchecked.command), line_no) : init t =
  let module Printer = (val Printer.get ~config:Config.default : Printer.Printer)
  in
  let* init_supers =
    parents
    |> map_list @@ fun (name, cd) ->
       let* init =
         match extract_init ~line_no cd.methods with
         | None -> return None
         | Some (parameters, body, line_no) ->
             let body = erase_types_command body in
             let* r =
               check_init tstorage tprivate cd.parents
                 (parameters, body, line_no)
             in
             return @@ Some r
       in
       return (name, init)
  in
  scoped
    (let* storage =
       add_var ~line_no ~is_mutable:true (mk_bound "__storage__") tstorage
     in
     let* private_ =
       add_var ~line_no ~is_mutable:true (mk_bound "__private__") tprivate
     in
     let f p =
       let* t = fresh_tvar in
       let* p = add_var ~line_no ~is_mutable:true (mk_bound p) t in
       return (p, t)
     in
     let* params = map_list f parameters in
     let context = {current_init_supers = Some init_supers} in
     let* body = check_command context body in
     let called = Analyser.collect_init_calls body in
     let not_called =
       let open Qual_ident.Set in
       elements @@ diff (of_list @@ List.map fst init_supers) called
     in
     let* () =
       match not_called with
       | [] -> return ()
       | x :: _ ->
           err ~line_no "'%s.__init__' may not have been called."
             (Printer.qual_ident x)
     in
     return
       {
         init_params = params
       ; init_body = body
       ; init_storage = storage
       ; init_private = private_
       ; init_supers
       ; line_no
       })

let check_generic_fun_def ~line_no ~with_storage ~with_operations params body =
  let* s = get in
  let body = unfold_abbrevs_command s.definitions body in
  let p, body =
    match params with
    | [] -> (mk_bound "__parameter__", body)
    | [p] -> (mk_bound p, body)
    | _ ->
        let p = mk_bound "__parameter__" in
        let lhs =
          Expr.record ~line_no
          @@ List.map (fun fld -> (fld, Expr.var ~line_no fld)) params
        in
        let body =
          Command.block ~line_no
            [
              Command.assign ~line_no lhs (Expr.var ~line_no "__parameter__")
            ; body
            ]
        in
        (p, body)
  in
  let rhs =
    Expr.lambda ~line_no p body ~with_storage ~with_operations ~recursive:None
  in
  let* rhs = check_expr rhs in
  match rhs.e with
  | ELambda {body} -> return (body, rhs)
  | _ -> assert false

let elim_dups defs =
  let f (used, r) (name, rhs) =
    if String.Set.mem name used
    then (used, r)
    else (String.Set.add name used, (name, rhs) :: r)
  in
  let _, r = List.fold_left f (String.Set.empty, []) defs in
  List.rev r

let get_private_name : Unchecked.method_ -> string option = function
  | {kind = Private (name, _, _, _)} -> Some name
  | _ -> None

let check_method ~tparameter ~tstorage ~tprivate ~tprivates parents
    ({kind; parameters; body; line_no} : Unchecked.method_) : method_ t =
  scoped
  @@
  match kind with
  | Init _ ->
      let* i =
        check_init tstorage tprivate parents (parameters, body, line_no)
      in
      let kind = Init i in
      let body = i.init_body in
      return {kind; parameters; body; line_no}
  | Entrypoint (name, _) ->
      let* () = modify @@ fun s -> {s with privates = tprivates} in
      let* ep =
        check_new_entrypoint ~tparameter ~tstorage ~tprivate name parameters
          body line_no
      in
      let body = ep.body in
      let kind = Entrypoint (name, ep) in
      return {kind; parameters; body; line_no}
  | Private (name, with_storage, with_operations, _) ->
      let* body, rhs =
        let with_storage =
          Option.map (fun a -> (a, Some tstorage)) with_storage
        in
        check_generic_fun_def ~line_no ~with_storage ~with_operations parameters
          body
      in
      let pp () = with_loc ~line_no [text "wrong private type"] in
      let t =
        match String.Map.find_opt name tprivates with
        | None -> assert false
        | Some t -> t
      in
      (* TODO Move this outside of checking. *)
      let* () = assertEqual ~line_no ~pp rhs.et t in
      let kind = Private (name, with_storage, with_operations, rhs) in
      return {kind; parameters; body; line_no}
  | Onchain_view (name, _) ->
      let* () = modify @@ fun s -> {s with privates = tprivates} in
      let view_kind = Onchain in
      let has_param = parameters <> [] in
      let* c =
        check_view ~line_no ~tstorage ~tprivate
          {
            view_kind
          ; view_name = name
          ; has_param
          ; pure = false
          ; body
          ; doc = "\"\""
          ; derived = U
          }
      in
      let kind = Onchain_view (name, c) in
      let body = c.body in
      return {kind; parameters; body; line_no}
  | Offchain_view (name, _) ->
      let* () = modify @@ fun s -> {s with privates = tprivates} in
      let view_kind = Offchain in
      let has_param = parameters <> [] in
      let* c =
        check_view ~line_no ~tstorage ~tprivate
          {
            view_kind
          ; view_name = name
          ; has_param
          ; pure = false
          ; body
          ; doc = "\"\""
          ; derived = U
          }
      in
      let kind = Offchain_view (name, c) in
      let body = c.body in
      return {kind; parameters; body; line_no}

let check_contract_def (cd : Unchecked.contract_def) : contract_def t =
  let Unchecked.{name; parents; methods; line_no} = cd in
  let* parents =
    map_list
      (fun (x, Basics.Deco_unchecked.U) ->
        let* c = find_contract_def ~line_no x in
        return (x, c))
      parents
  in
  let* {config} = get in
  let* tparameter = fresh_tvar in
  let* tstorage = fresh_tvar in
  let* tprivate = fresh_tvar in
  let* () = add_constraint ~line_no (DefaultsToUnit tstorage) in
  let* () = add_constraint ~line_no (DefaultsToUnit tprivate) in
  let methods =
    let parent_methods = List.concat_map (fun (_, cd) -> cd.methods) parents in
    let parent_methods = List.map erase_types_method parent_methods in
    let all_non_inits =
      methods @ parent_methods (* without our __init__ *)
      |> List.filter_map @@ fun (m : Unchecked.method_) ->
         match m.kind with
         | Init _ -> None
         | Entrypoint (name, _)
         | Private (name, _, _, _)
         | Onchain_view (name, _)
         | Offchain_view (name, _) -> Some (name, m)
    in
    let xs = List.map snd @@ elim_dups all_non_inits in
    List.filter is_init methods
    @ xs (* add in our __init__, needed by check_init *)
  in
  let privates0 = List.filter_map get_private_name methods in
  let* tprivates =
    let f x =
      let* p = fresh_tvar in
      return (x, p)
    in
    map_list f privates0
  in
  let tprivates = String.Map.of_list tprivates in
  let* methods' =
    scoped
      (map_list
         (check_method ~tparameter ~tstorage ~tprivate ~tprivates parents)
         methods)
  in
  let init =
    match List.filter is_init_checked methods' with
    | [] -> None
    | [{kind = Init i}] -> Some i
    | _ -> err ~line_no "A contract class can have only one __init__ method."
  in
  let privates =
    List.sort (fun (n1, _) (n2, _) -> Stdlib.compare n1 n2)
    @@ List.filter_map
         (function
           | ({kind = Private (name, _, _, r)} : Checked.method_) ->
               Some (name, r)
           | _ -> None)
         methods'
  in
  let onchain_views =
    List.filter_map
      (function
        | ({kind = Onchain_view (_name, r)} : Checked.method_) -> Some r
        | _ -> None)
      methods'
  in
  let offchain_views =
    List.filter_map
      (function
        | ({kind = Offchain_view (_name, r)} : Checked.method_) -> Some r
        | _ -> None)
      methods'
  in
  let entrypoints =
    List.filter_map
      (function
        | ({kind = Entrypoint (_name, r)} : Checked.method_) -> Some r
        | _ -> None)
      methods'
  in
  let tparameter' =
    match entrypoints with
    | [] -> Type.unit
    | eps ->
        let row =
          List.map
            (fun (ep : _ entrypoint_f) ->
              (ep.channel, ep.derived.tparameter_ep))
            eps
        in
        Type.variant (Hole.mk ()) row
  in
  let pp () = [`Text "Wrong contract parameter type"; `Line line_no] in
  let* () = assertEqual ~line_no ~pp tparameter tparameter' in
  let private_variables = privates in
  let views = onchain_views @ offchain_views in
  let derived =
    {
      tparameter
    ; tstorage
    ; tprivate
    ; init
    ; config
    ; entrypoints
    ; private_variables
    ; views
    ; initial_balance = None
    ; initial_metadata = []
    ; unknown_parts = None
    ; template_id = None
    ; flags = []
    ; line_no = cd.line_no
    }
  in
  return {name; parents; methods = methods'; derived; line_no}

let check_module_elt : Unchecked.module_elt -> Checked.module_elt t = function
  | Expr_def {name; rhs; line_no} ->
      let* s = get in
      let rhs = unfold_abbrevs_expr s.definitions rhs in
      let* rhs = check_expr rhs in
      let* name = add_var_general' ~line_no ~kind:(V_defined rhs) name rhs.et in
      return @@ Checked.Expr_def {name; rhs; line_no}
  | Fun_def {name; params; effects; body; line_no} ->
      let ({with_storage; with_operations} : effects) = effects in
      let with_storage = Option.map (fun x -> (x, None)) with_storage in
      let* body, rhs =
        check_generic_fun_def ~line_no ~with_storage ~with_operations params
          body
      in
      let* name = add_var_general' ~line_no ~kind:(V_defined rhs) name rhs.et in
      return (Fun_def {name; params; effects; body; line_no})
  | Type_def {name; rhs; line_no} ->
      let* s = get in
      let rhs = unfold_abbrevs_type s.definitions rhs in
      let* () = add_variable ~line_no name @@ Def_type rhs in
      return (Type_def {name; rhs; line_no})
  | Contract_def ({name; methods; line_no} as cd) ->
      let* s = get in
      let methods = List.map (unfold_abbrevs_method s.definitions) methods in
      let cd = {cd with methods} in
      let* cd = check_contract_def cd in
      let* () = add_definition ~line_no name @@ Def_contract cd in
      return @@ Contract_def cd

let check_module ({module_name; elts; mod_line_no} : Unchecked.module_) :
    Checked.module_ t =
  let* elts = map_list check_module_elt elts in
  let* {definitions} = get in
  let type_defs =
    definitions
    |> String.Map.filter_map @@ fun _name x ->
       match x with
       | Def_type t -> Some t
       | _ -> None
  in
  let defs =
    definitions
    |> String.Map.filter_map @@ fun _name x ->
       match x with
       | Def_variable {derived = V_defined rhs} -> Some rhs
       | _ -> None
  in
  let contract_defs =
    definitions
    |> String.Map.filter_map @@ fun _name x ->
       match x with
       | Def_contract cd -> Some cd
       | _ -> None
  in
  return
    {
      module_name
    ; elts
    ; defs (* TODO make part of definitions *)
    ; type_defs
    ; contract_defs
    ; mod_line_no
    }

let check_action config (action : Unchecked.action) : Checked.action t =
  let module Printer = (val Printer.get ~config : Printer.Printer) in
  let* () = set_config config in
  let* s = get in
  let action =
    match action with
    | Add_module _ -> action
    | _ -> unfold_abbrevs_action s.definitions action
  in
  match action with
  | Instantiate_contract {id; class_; call; balance; line_no} ->
      let* ({derived} as cd) = find_contract_def ~line_no class_ in
      let {tstorage; tprivate; tparameter; init} = derived in
      let* call, args =
        let params = Option.cata [] (fun x -> x.init_params) init in
        check_call ~line_no
          {
            qi_module = class_.qi_module
          ; qi_ident = class_.qi_ident ^ ".__init__"
          }
          (params : _ list)
          call
      in
      let* _ =
        map_list
          (fun (view : Checked.view) ->
            let source =
              match view.derived.tparameter with
              | Some t -> t
              | None -> Type.unit
            in
            add_scenario_var ~line_no
              (mk_bound @@ name_view view.view_name id)
              Type.(lambda no_effects source view.body.ct))
          derived.views
      in
      let* _ =
        add_scenario_var ~line_no
          (mk_bound (string_of_contract_id id))
          (Type.contract tparameter)
      in
      let* _ =
        add_scenario_var ~line_no
          (mk_bound (string_of_contract_id id ^ "_storage"))
          tstorage
      in
      let* _ =
        add_scenario_var ~line_no
          (mk_bound (string_of_contract_id id ^ "_private"))
          tprivate
      in
      let* balance = map_option check_expr balance in
      let* derived =
        match balance with
        | None -> return derived
        | Some balance ->
            let* () =
              assertEqual ~line_no balance.et Type.mutez ~pp:(fun () ->
                  [`Expr balance; `Text " is not a valid amount for 'balance'"])
            in
            let derived = {derived with initial_balance = Some balance} in
            return derived
      in
      let cd = {cd with derived} in
      let derived = {instantiate_derived_contract = cd; args} in
      return
        (Instantiate_contract {id; class_; call; balance; derived; line_no})
  | Compute {var; expression; context; line_no} ->
      let* context = check_context context in
      let* expression = check_expr expression in
      let* var = add_scenario_var ~line_no var expression.et in
      return (Compute {var; expression; context; line_no})
  | Message
      {
        id
      ; valid
      ; exception_
      ; params
      ; line_no
      ; context
      ; amount
      ; message
      ; show
      ; export
      } ->
      let* context = check_context context in
      let* valid = check_expr valid in
      let* exception_ = map_option check_expr exception_ in
      let* params = check_expr params in
      let* amount = check_expr amount in
      let id' = string_of_contract_id id in
      let* c =
        match id with
        | C_dynamic _ -> check_var ~line_no @@ mk_bound id'
        | _ -> check_var ~line_no @@ mk_bound @@ id'
      in
      let* entry = fresh_tvar in
      let* () =
        let pp () =
          [
            `Text id'
          ; `Text " is of type "
          ; `Type c
          ; text ", which lacks entrypoint '%s' with argument " message
          ; `Type entry
          ]
        in
        assertEqual ~line_no ~pp c (Type.contract (Type.uvariant message entry))
      in
      let pp () =
        [
          `Text "entrypoint expects parameter of type "
        ; `Type entry
        ; `Text ", but got "
        ; `Type params.et
        ]
      in
      let* () = assertEqual ~line_no params.et entry ~pp in
      return
        (Message
           {
             id
           ; valid
           ; exception_
           ; params
           ; line_no
           ; context
           ; amount
           ; message
           ; show
           ; export
           })
  | ScenarioError {message} -> return (ScenarioError {message})
  | Html {tag; inner; line_no} -> return (Html {tag; inner; line_no})
  | Verify {condition; line_no} ->
      let* condition = check_expr condition in
      let pp () = [`Text "not a boolean expression"] in
      let* () = assertEqual ~line_no condition.et Type.bool ~pp in
      return (Verify {condition; line_no})
  | Show {expression; html; stripStrings; compile; line_no} ->
      let* expression = check_expr expression in
      return (Show {expression; html; stripStrings; compile; line_no})
  | Add_flag {flag; line_no} ->
      return (Add_flag {flag; line_no}) (* Handled in Scenario.acc_config. *)
  | Set_delegate {id; line_no; baker} ->
      let* baker = check_expr baker in
      let id' = string_of_contract_id id in
      let* _ = check_var ~line_no @@ mk_bound id' in
      let pp () = [`Text "scenario set_delegate "; `Type baker.et] in
      let* () = assertEqual ~line_no baker.et (Type.option Type.key_hash) ~pp in
      return (Set_delegate {id; line_no; baker})
  | DynamicContract {id; model_id; line_no} ->
      let id' = string_of_dynamic_id id in
      let model_id' = string_of_contract_id model_id in
      let* tcontract = check_var ~line_no @@ mk_bound model_id' in
      let* tstorage =
        check_var ~line_no @@ mk_bound @@ model_id' ^ "_storage"
      in
      let* _ = add_scenario_var ~line_no (mk_bound id') tcontract in
      let* _ =
        add_scenario_var ~line_no (mk_bound @@ id' ^ "_storage") tstorage
      in
      return (DynamicContract {id; model_id; line_no})
  | Prepare_constant_value {line_no; var; hash; expression} ->
      let* expression = check_expr expression in
      let* var = add_var_general' ~line_no ~kind:V_constant var expression.et in
      return (Prepare_constant_value {line_no; var; hash; expression})
  | Mutation_test {scenarios; show_paths; line_no} ->
      return (Mutation_test {scenarios; show_paths; line_no})
  | Add_module {module_; line_no} ->
      let* module_ = scoped @@ check_module module_ in
      let* () =
        add_definition ~line_no module_.module_name (Def_module module_)
      in
      return (Add_module {module_; line_no})

let check_action' (config, (action : Unchecked.action)) :
    (Config.t * Checked.action) t =
  let* action = check_action config action in
  return (config, action)

let examine_contract_derived
    ({init; tstorage; tprivate; line_no} : _ contract_derived) =
  let line_no =
    match init with
    | None -> line_no
    | Some {line_no} -> line_no
  in
  let r =
    match init with
    | None -> Analyser.find_unassigned_no_init tstorage
    | Some {init_storage; init_body} ->
        Analyser.find_unassigned `Storage (init_storage, tstorage) init_body
  in
  let () =
    match r with
    | None -> ()
    | Some [] -> ()
    | Some path ->
        let path = String.concat "." path in
        err ~line_no "'self.data.%s' may not have been initialised." path
  in
  let r =
    match init with
    | None -> Analyser.find_unassigned_no_init tprivate
    | Some {init_private; init_body} ->
        Analyser.find_unassigned `Private (init_private, tprivate) init_body
  in
  let () =
    match r with
    | None -> ()
    | Some [] -> ()
    | Some path ->
        let path = String.concat "." path in
        err ~line_no "'self.private.%s' may not have been initialised." path
  in
  ()

let examine_contract_def (cd : Checked.contract_def) =
  examine_contract_derived cd.derived

(** {1 Non-monadic interface for the outside world} *)
let examine_module ({contract_defs} : Checked.module_) =
  let cds = contract_defs in
  String.Map.iter (fun _ cd -> examine_contract_def cd) cds

let examine_scenario {actions} =
  let f (_, a) =
    match a with
    | Add_module {module_} -> examine_module module_
    | _ -> ()
  in
  List.iter f actions

let run_with config f x =
  run (f x)
    {
      tvar_counter = 0
    ; index_counter = 0
    ; definitions = String.Map.empty
    ; privates = String.Map.empty
    ; constraints = []
    ; config
    ; warnings = []
    }

let exec_with config f definitions x =
  let f x =
    let* _ =
      map_list
        (fun (x, t) -> add_var ~line_no:None ~is_mutable:false x t)
        definitions
    in
    f x
  in
  fst (run_with config f x)

let exec_with' config f x = fst (run_with config f x)

let check_lambda config definitions =
  exec_with config
    (fun (l : _ Unchecked.lambda_f) ->
      let* e = check_lambda ~line_no:None l in
      let* subst = solve_constraints in
      let e = Substitution.on_lambda subst e in
      return e)
    definitions

let check_expr config definitions =
  exec_with config
    (fun e ->
      let* e = check_expr e in
      let* subst = solve_constraints in
      let e = Substitution.on_expr subst e in
      let e = Closer.on_expr e in
      return e)
    definitions

let check_command config definitions =
  exec_with config
    (fun c ->
      let* c = check_command context0 c in
      let* subst = solve_constraints in
      let c = Substitution.on_command subst c in
      let c = Closer.on_command c in
      return c)
    definitions

let check_contract_def config =
  exec_with config
    (fun c ->
      let* c = check_contract_def c in
      let* subst = solve_constraints in
      let c = Substitution.on_contract_def subst c in
      examine_contract_def c;
      let c = Closer.on_contract_def c in
      return c)
    []

let substitution_on_var subst {typ; index; occs; derived} =
  let typ = Substitution.on_type subst typ in
  let derived = derived |> map_derived_var (Substitution.on_expr subst) in
  {typ; index; occs; derived}

let substitution_on_definition subst = function
  | Def_module m -> Def_module (Substitution.on_module subst m)
  | Def_variable x -> Def_variable (substitution_on_var subst x)
  | Def_type _ -> assert false
  | Def_contract _ -> assert false

let closer_on_var {typ; index; occs; derived} =
  let typ = Closer.on_type typ in
  let derived = derived |> map_derived_var Closer.on_expr in
  {typ; index; occs; derived}

let closer_on_definition = function
  | Def_module m -> Def_module m
  | Def_variable x -> Def_variable (closer_on_var x)
  | Def_type _ -> assert false
  | Def_contract _ -> assert false

let check_action config definitions =
  exec_with' config @@ fun c ->
  let* s = get in
  let* () = set {s with definitions} in
  let* c = check_action config c in
  let* subst = solve_constraints in
  let c = Substitution.on_action subst c in
  let c = Closer.on_action c in
  let* {definitions} = get in
  let definitions =
    definitions |> String.Map.map (substitution_on_definition subst)
  in
  let definitions = definitions |> String.Map.map closer_on_definition in
  return (c, definitions)

let check_scenario config (s : Unchecked.scenario) =
  let check ({shortname; longname; actions} : Unchecked.scenario) :
      Checked.scenario t =
    let* actions = Action.map_list check_action' actions in
    return {shortname; longname; actions}
  in
  let (subst, x), {warnings} =
    run_with config
      (fun (s : Unchecked.scenario) ->
        let* s = check s in
        let* subst = solve_constraints in
        let s = Substitution.on_scenario subst s in
        examine_scenario s;
        let s = Closer.on_scenario s in
        return (subst, s))
      s
  in
  (subst, x, warnings)
