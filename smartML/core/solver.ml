(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Basics
open Checked
open Utils
open Ternary

let err ~line_no es = raise (SmartExcept (with_loc ~line_no es))

let pp_constraint ppf =
  let open Type in
  function
  | HasAdd (_e, e1, e2) ->
      Format.fprintf ppf "@[<v>HasAdd@;<0 2>%a@;<0 2>%a@]" pp e1.et pp e2.et
  | HasMul (_e, e1, e2) -> Format.fprintf ppf "HasMul %a, %a" pp e1.et pp e2.et
  | HasSub (_e, e1, e2) -> Format.fprintf ppf "HasSub %a, %a" pp e1.et pp e2.et
  | HasDiv (_e, e1, e2) -> Format.fprintf ppf "HasDiv %a, %a" pp e1.et pp e2.et
  | HasInvert (t1, t2) -> Format.fprintf ppf "HasInvert %a %a" pp t1 pp t2
  | HasMap (_e, e1, e2) -> Format.fprintf ppf "HasMap %a, %a" pp e1.et pp e2.et
  | IsComparable e -> Format.fprintf ppf "IsComparable %a" pp e.et
  | IsPackable t -> Format.fprintf ppf "IsPackable %a" pp t
  | HasGetItem (e1, e2, _) ->
      Format.fprintf ppf "HasGetItem %a, %a" pp e1.et pp e2.et
  | HasContains (e1, e2, _line_no) ->
      Format.fprintf ppf "HasContains %a, %a" pp e1.et pp e2.et
  | HasSize e -> Format.fprintf ppf "HasSize %a" pp e.et
  | IsStringOrBytes t -> Format.fprintf ppf "IsStringOrBytes %a" pp t
  | AssertEqual (t1, t2, _pp) ->
      Format.fprintf ppf "@[<v>AssertEqual@;<0 2>%a@;<0 2>%a@]" pp t1 pp t2
  | IsInt (t, _pp) -> Format.fprintf ppf "IsInt %a" pp t
  | SaplingVerify (state, transaction) ->
      Format.fprintf ppf "SaplingVerify %a %a" pp state.et pp transaction.et
  | HasNeg (e, t) -> Format.fprintf ppf "HasNeg %a %a" pp e.et pp t
  | HasInt e -> Format.fprintf ppf "HasInt %a" pp e.et
  | IsNotHot (n, t) -> Format.fprintf ppf "IsNotHot %s %a" n pp t
  | IsAnyMap (tk, tv, e) ->
      Format.fprintf ppf "IsAnyMap %a, %a, %a" pp tk pp tv pp e.et
  | IsConvertible (t1, t2) ->
      Format.fprintf ppf "IsConvertible %a, %a" pp t1 pp t2
  | IsInstance2 (cls, _, (t1, t2), _) ->
      Format.fprintf ppf "IsInstance2 %s, %a, %a" cls pp t1 pp t2
  | DefaultsToUnit t -> Format.fprintf ppf "DefaultsToUnit %a" pp t
  | WithStorage (t, h) ->
      let show = [%derive.show: (access * Type.t) option Hole.t] in
      Format.fprintf ppf "WithStorage %a %s" pp t (show h)

let _show_constraint = Format.asprintf "%a" pp_constraint

let equal_constraint (l1, c1) (l2, c2) =
  l1 = l2 && equal_typing_constraint c1 c2

let progress cs cs' =
  List.length cs <> List.length cs'
  || not (List.for_all2 equal_constraint cs cs')

let unresolved_error = function
  | AssertEqual _ -> assert false
  | HasSub (e, e1, e2) ->
      [`Exprs [e1; e2]; `Text " cannot be subtracted in "; `Expr e]
  | HasDiv (t, e1, e2) ->
      [
        `Exprs [e1; e2]
      ; `Text " cannot be divided in "
      ; `Type t
      ; `Br
      ; `Text "Allowed types are (int|nat, int|nat) and (tez, tez|nat)"
      ]
  | HasMap (e, e1, e2) -> [`Exprs [e1; e2]; `Text " cannot be map in "; `Expr e]
  | HasInvert (t1, _t2) ->
      [`Text "Type "; `Type t1; `Text " does not support '~'."]
  | IsComparable e -> [`Expr e; `Text " doesn't have a comparable type"]
  | IsPackable t -> [`Type t; `Text " is not packable"]
  | HasGetItem (l, pos, _t) -> [`Expr l; `Text " cannot get item"; `Expr pos]
  | HasContains (items, member, _) ->
      [`Expr items; `Text " cannot contain "; `Expr member]
  | HasSize e -> [`Expr e; `Text " has no length or size"]
  | IsStringOrBytes t -> [`Type t; `Text " is not sp.string or sp.bytes"]
  | HasAdd (e, e1, e2) ->
      [`Exprs [e1; e2]; `Text " cannot be added in "; `Expr e]
  | HasMul (e, e1, e2) ->
      [`Exprs [e1; e2]; `Text " cannot be multiplied "; `Expr e]
  | IsInt (_, pp) -> pp ()
  | SaplingVerify (state, transaction) ->
      [`Text "memo_size error"; `Exprs [state; transaction]]
  | HasNeg (e, _) -> [`Expr e; `Text " cannot be negated"]
  | HasInt e -> [`Text "Cannot apply sp.to_int to "; `Type e.et]
  | IsNotHot (n, t) ->
      [
        text "Variable '%s' of type " n
      ; `Type t
      ; `Text " cannot be used twice because it contains a ticket."
      ]
  | IsAnyMap (tk, tv, e) ->
      [
        `Expr e
      ; `Text " of type "
      ; `Type e.et
      ; `Text " is not a map from "
      ; `Type tk
      ; `Text " to "
      ; `Type tv
      ]
  | IsConvertible (t1, t2) ->
      [`Type t1; `Text " does not have the same Michelson type as "; `Type t2]
  | IsInstance2 (cls, _, (t1, t2), _) ->
      [
        `Text (Printf.sprintf "Type class %S" cls)
      ; `Text " does not have instance "
      ; `Text (Type.show t1)
      ; `Text ", "
      ; `Text (Type.show t2)
      ]
  | DefaultsToUnit _ -> (* impossible *) [`Text "Unresolved unit default."]
  | WithStorage _ -> [`Text "Unresolved with_storage effect."]

let unresolved_error ~line_no x = with_loc ~line_no (unresolved_error x)

let is_comparable =
  Type.cata (function
    | Int _
    | T0
        ( Unit
        | Never
        | Bool
        | String
        | Chain_id
        | Bytes
        | Mutez
        | Key_hash
        | Key
        | Signature
        | Timestamp
        | Address ) -> Yes
    | T0
        ( Operation
        | Bls12_381_g1
        | Bls12_381_g2
        | Bls12_381_fr
        | Chest
        | Chest_key )
    | T1 ((List | Set | Contract | Ticket), _)
    | T2 ((Map | Big_map), _, _)
    | Lambda _ | Sapling_state _ | Sapling_transaction _ | Secret_key -> No
    | Unknown _ -> Maybe
    | Record {var = Some _} as t -> Type.fold_f and_ Maybe t
    | Variant {var = Some _} as t -> Type.fold_f and_ Maybe t
    | Tuple _ as t -> Type.fold_f and_ Yes t
    | (Record {var} | Variant {var}) as t ->
        Type.fold_f and_ (if var = None then Yes else Maybe) t
    | T1 (Option, _) as t -> Type.fold_f and_ Yes t
    | T0 (Nat | Int | Sapling_transaction _ | Sapling_state _)
    | T2 ((Pair _ | Or _ | Lambda), _, _)
    | Abbrev _ -> assert false)

let is_packable =
  let of_bool x = if x then Yes else No in
  let kto_bool x k =
    match x with
    | Yes -> k true
    | No -> k false
    | Maybe -> Maybe
  in
  Type.cata
    (let open Michelson_base.Type in
    let open Michelson_base.Typing in
    function
    | T0 t -> of_bool (is_packable_f (MT0 t))
    | T1 (t, p) -> kto_bool p (fun p -> of_bool (is_packable_f (MT1 (t, p))))
    | T2 (t, p1, p2) ->
        kto_bool p1 (fun p1 ->
            kto_bool p2 (fun p2 -> of_bool (is_packable_f (MT2 (t, p1, p2)))))
    | Int _ | Lambda _ | Sapling_transaction _ -> Yes
    | Sapling_state _ | Secret_key -> No
    | Unknown _ -> Maybe
    | Record {var = None} as t -> Type.fold_f and_ Maybe t
    | (Tuple _ | Record _ | Variant _) as t -> Type.fold_f and_ Yes t
    | Abbrev _ -> assert false)

let reduce_constraint ~config rsubst (line_no, c) =
  let pp () = unresolved_error ~line_no c in
  let fail () = raise (SmartExcept (pp ())) in
  let unresolved = (line_no, c) in
  let assert_equal ?(pp = pp) t1 t2 = (line_no, AssertEqual (t1, t2, pp)) in
  match c with
  | AssertEqual (t1, t2, pp) ->
      let cs =
        try Unifier.unify ~config pp rsubst t1 t2
        with Unifier.Unification_failed e ->
          raise (SmartExcept (with_loc ~line_no (pp () @ (`Br :: e))))
      in
      List.map (fun c -> (line_no, c)) cs
  | HasSub (e, e1, _e2) -> (
      match Type.unF e1.et with
      | T0 Mutez -> [assert_equal e1.et e.et]
      | Int _ -> [assert_equal e.et Type.int]
      | T0 Timestamp -> [assert_equal e.et Type.int]
      | Unknown _ -> [unresolved]
      | _ -> fail ())
  | HasDiv (t, e1, e2) -> (
      let default_to_nat {e; et} =
        match (e, Type.unF et) with
        | ELiteral _, Int {isNat = Variable _} ->
            (Some true, [assert_equal et Type.nat])
        | _, Int {isNat = Value isNat} -> (Some isNat, [])
        | _ -> (None, [])
      in
      match (Type.unF e1.et, Type.unF e2.et) with
      | _, Unknown _ | Unknown _, _ -> [unresolved]
      | Int {isNat = Variable x1}, Int {isNat = Variable x2} when x1 = x2 ->
          [assert_equal t Type.(option (pair e1.et nat))]
      | Int {isNat = Variable _}, Int _ | Int _, Int {isNat = Variable _} -> (
          let nat1, cs1 = default_to_nat e1 in
          let nat2, cs2 = default_to_nat e2 in
          match (nat1, nat2) with
          | Some true, Some true ->
              assert_equal t Type.(option (pair Type.nat Type.nat))
              :: (cs1 @ cs2)
          | Some _, Some _ ->
              assert_equal t Type.(option (pair Type.int Type.nat))
              :: (cs1 @ cs2)
          | _ -> [unresolved])
      | Int {isNat = Value true}, Int {isNat = Value true} ->
          [assert_equal t Type.(option (pair Type.nat Type.nat))]
      | Int {isNat = Value _}, Int {isNat = Value _} ->
          [assert_equal t Type.(option (pair Type.int Type.nat))]
      | T0 Mutez, Int {isNat = Value true} ->
          [assert_equal t Type.(option (pair Type.mutez Type.mutez))]
      | T0 Mutez, Int {isNat = Variable _} ->
          [
            assert_equal t Type.(option (pair Type.mutez Type.mutez))
          ; assert_equal e2.et Type.nat
          ]
      | Int {isNat = Variable _}, _ | _, Int {isNat = Variable _} ->
          [unresolved]
      | T0 Mutez, T0 Mutez ->
          [assert_equal t Type.(option (pair Type.nat Type.mutez))]
      | _ -> fail ())
  | HasMap (e, l, f) -> (
      match Type.unF f.et with
      | Lambda ({with_storage; with_operations}, s', t') -> (
          match (Hole.get with_storage, Hole.get with_operations) with
          | Some (Some _), _ | _, Some true ->
              err ~line_no [`Text "Cannot map effectful lambda"]
          | None, _ | _, None -> [unresolved]
          | _ -> (
              match Type.unF l.et with
              | T1 (List, t) ->
                  [assert_equal t s'; assert_equal e.et (Type.list t')]
              | T1 (Option, t) ->
                  [assert_equal t s'; assert_equal e.et (Type.option t')]
              | T2 (Map, tkey, tvalue) ->
                  [
                    assert_equal (Type.key_value tkey tvalue) s'
                  ; assert_equal e.et (Type.map ~big:false ~tkey ~tvalue:t')
                  ]
              | Unknown _ -> [unresolved]
              | _ -> fail ()))
      | T2 (Lambda, _, _) -> assert false
      | _ -> fail ())
  | HasInvert (t1, t2) -> (
      match Type.unF t1 with
      | Int {isNat = Value false} -> [assert_equal t2 Type.int]
      | T0 Bytes -> [assert_equal t2 Type.bytes]
      | Unknown _ -> [unresolved]
      | _ -> fail ())
  | IsComparable e -> (
      match is_comparable e.et with
      | Yes -> []
      | No -> fail ()
      | Maybe -> [unresolved])
  | IsPackable t -> (
      match is_packable t with
      | Yes -> []
      | No -> fail ()
      | Maybe -> [unresolved])
  | HasGetItem (l, pos, t) -> (
      match Type.unF l.et with
      | T2 ((Map | Big_map), tkey, tvalue) ->
          [assert_equal tkey pos.et; assert_equal tvalue t]
      | T1 (List, _) ->
          let pp () =
            pp ()
            @ [
                `Br
              ; `Text "A list is not a map."
              ; `Br
              ; `Text
                  (Format.sprintf
                     " You can use:\n\
                     \ - sp.utils.vector(..) to create a map from a list,\n\
                     \ - sp.utils.matrix(..) to create a map of maps from a \
                      list of lists,\n\
                     \ - sp.utils.cube(..) for a list of lists of lists.")
              ]
          in
          raise (SmartExcept (pp ()))
      | Unknown _ -> [unresolved]
      | _ -> fail ())
  | HasContains (items, member, _line_no) -> (
      match Type.unF items.et with
      | T1 (Set, telement) -> [assert_equal telement member.et]
      | T2 ((Map | Big_map), tkey, _) -> [assert_equal tkey member.et]
      | Unknown _ -> [unresolved]
      | _ -> fail ())
  | HasSize e -> (
      match Type.unF e.et with
      | T0 (Nat | Int | Sapling_transaction _ | Sapling_state _) -> assert false
      | T1 (List, _) | T0 (String | Bytes) | T1 (Set, _) -> []
      | T2 (Map, _, _) -> []
      | Unknown _ -> [unresolved]
      | _ -> fail ())
  | IsStringOrBytes t -> (
      match Type.unF t with
      | T0 (String | Bytes) -> []
      | Unknown _ -> [unresolved]
      | _ -> fail ())
  | HasAdd (e, e1, e2) -> (
      match (Type.unF e1.et, Type.unF e2.et) with
      | _, Unknown _ | Unknown _, _ -> [unresolved]
      | Int {isNat = Variable x1}, Int {isNat = Variable x2} when x1 = x2 ->
          [assert_equal e.et e1.et]
      | Int {isNat = Variable _}, _ | _, Int {isNat = Variable _} ->
          [unresolved]
      | Int {isNat = Value true}, Int {isNat = Value true} ->
          [assert_equal e.et Type.nat]
      | Int {isNat = Value _}, Int {isNat = Value _} ->
          [assert_equal e.et Type.int]
      | T0 Timestamp, Int {isNat = Value false}
      | Int {isNat = Value false}, T0 Timestamp ->
          [assert_equal e.et Type.timestamp]
      | T0 Mutez, T0 Mutez -> [assert_equal e.et Type.mutez]
      | T0 Bls12_381_g1, T0 Bls12_381_g1 ->
          [assert_equal e.et Type.bls12_381_g1]
      | T0 Bls12_381_g2, T0 Bls12_381_g2 ->
          [assert_equal e.et Type.bls12_381_g2]
      | T0 Bls12_381_fr, T0 Bls12_381_fr ->
          [assert_equal e.et Type.bls12_381_fr]
      | T0 String, T0 String -> [assert_equal e.et Type.string]
      | T0 Bytes, T0 Bytes -> [assert_equal e.et Type.bytes]
      | _ -> fail ())
  | HasMul (e, e1, e2) -> (
      match (Type.unF e1.et, Type.unF e2.et) with
      | _, Unknown _ | Unknown _, _ -> [unresolved]
      | Int {isNat = Variable x1}, Int {isNat = Variable x2} when x1 = x2 ->
          [assert_equal e.et e1.et]
      | Int {isNat = Variable _}, _ | _, Int {isNat = Variable _} ->
          [unresolved]
      | Int {isNat = Value true}, Int {isNat = Value true} ->
          [assert_equal e.et Type.nat]
      | Int {isNat = Value _}, Int {isNat = Value _} ->
          [assert_equal e.et Type.int]
      | T0 Mutez, Int {isNat = Value true} | Int {isNat = Value true}, T0 Mutez
        -> [assert_equal e.et Type.mutez]
      | T0 Bls12_381_g1, T0 Bls12_381_fr ->
          [assert_equal e.et Type.bls12_381_g1]
      | T0 Bls12_381_g2, T0 Bls12_381_fr ->
          [assert_equal e.et Type.bls12_381_g2]
      | T0 Bls12_381_fr, T0 Bls12_381_fr
      | Int {isNat = Value true}, T0 Bls12_381_fr
      | Int {isNat = Value false}, T0 Bls12_381_fr
      | T0 Bls12_381_fr, Int {isNat = Value true}
      | T0 Bls12_381_fr, Int {isNat = Value false} ->
          [assert_equal e.et Type.bls12_381_fr]
      | _ -> fail ())
  | IsInt (t, pp) -> (
      match Type.unF t with
      | Int _ -> []
      | Unknown _ -> [unresolved]
      | _ -> raise (SmartExcept (pp ())))
  | SaplingVerify (state, transaction) -> (
      match (Type.unF state.et, Type.unF transaction.et) with
      | Sapling_state m1, Sapling_transaction m2 ->
          let err () = [`Text "memo_size mismatch"] in
          Unifier.equalize_holes ~err ~eq:( = )
            (fun i -> Substitution.SInt i)
            rsubst m1 m2;
          []
      | _ -> fail ())
  | HasNeg (e, t) -> (
      match (Type.unF t, Type.unF e.et) with
      | T0 (Bls12_381_g1 | Bls12_381_g2 | Bls12_381_fr), _
      | _, T0 (Bls12_381_g1 | Bls12_381_g2 | Bls12_381_fr) ->
          [assert_equal t e.et]
      | Int _, _ | _, Int _ ->
          [assert_equal e.et (Type.intOrNat ()); assert_equal t Type.int]
      | _, Unknown _ -> [unresolved]
      | _ -> fail ())
  | HasInt e -> (
      match Type.unF e.et with
      | Int _ -> [assert_equal e.et Type.nat]
      | T0 Bls12_381_fr -> []
      | T0 Bytes -> []
      | Unknown _ -> [unresolved]
      | _ -> err ~line_no [`Expr e; `Text " cannot be cast to Int"])
  | IsNotHot (_, t) -> (
      match Type.is_hot t with
      | No -> []
      | Yes -> fail ()
      | Maybe -> [unresolved])
  | IsAnyMap (tk, tv, e) -> (
      match Type.unF e.et with
      | T2 ((Map | Big_map), tkey, tvalue) ->
          [assert_equal tk tkey; assert_equal tv tvalue]
      | Unknown _ -> [unresolved]
      | _ -> fail ())
  | IsConvertible (t1, t2) ->
      if Type.has_unknowns t1 || Type.has_unknowns t2
      then [unresolved]
      else
        let t1 = Typing.mtype_of_type ~with_annots:false t1 in
        let t2 = Typing.mtype_of_type ~with_annots:false t2 in
        if Michelson.equal_mtype t1 t2
           (* NB We could call Michelson.unifiable_types and propagate the
              resulting substitution here. *)
        then []
        else fail ()
  | IsInstance2 (_cls, e, (t1, t2), instances) -> (
      if Type.has_holes t1 || Type.has_holes t2
      then [unresolved]
      else
        match List.assoc_opt (t1, t2) instances with
        | None -> fail ()
        | Some t -> [assert_equal e.et t])
  | DefaultsToUnit (F (Type.Unknown _)) -> [unresolved]
  | DefaultsToUnit _ -> []
  | WithStorage (tstorage, with_storage) -> (
      match Hole.get with_storage with
      | None -> [unresolved]
      | Some None -> []
      | Some (Some (_access, tstorage')) ->
          let pp () = [] in
          [assert_equal ~pp tstorage tstorage'])

let default_constraint ~config:_ rsubst (line_no, c) =
  let pp () = unresolved_error ~line_no c in
  let unresolved = (line_no, c) in
  let assert_equal t1 t2 = (line_no, AssertEqual (t1, t2, pp)) in
  match c with
  | IsAnyMap (tkey, tvalue, e) ->
      [assert_equal e.et (Type.map ~big:false ~tkey ~tvalue)]
  | DefaultsToUnit (F (Type.Unknown _) as t) -> [assert_equal t Type.unit]
  | WithStorage (_tstorage, Hole.Variable with_storage) ->
      Substitution.insert
        [(with_storage, SWithStorage (Hole.Value None))]
        rsubst;
      [unresolved]
  | _ -> [unresolved]

let pass ~config reduce =
  let rec pass subst r = function
    | [] ->
        let r =
          List.map (fun (ln, c) -> (ln, Substitution.on_constraint subst c)) r
        in
        List.rev r
    | c :: cs ->
        let c = Control.map_snd (Substitution.on_constraint subst) c in
        let cs' = reduce ~config subst c in
        let p = progress [c] cs' in
        if p then pass subst r (cs' @ cs) else pass subst (cs' @ r) cs
  in
  fun subst cs -> (subst, pass subst [] cs)

let run ~config constraints =
  let verbosity = 0 in
  let debug v f = if verbosity >= v then f () in
  debug 1 (fun () -> Format.(pp_set_margin std_formatter 300));
  let rec passes subst i = function
    | [] -> (subst, [])
    | cs when i > 100 ->
        ( subst
        , List.map (fun (ln, c) -> (ln, Substitution.on_constraint subst c)) cs
        )
    | cs ->
        debug 2 (fun () ->
            List.iter
              (fun (_, c) -> Format.printf "    %a@." pp_constraint c)
              cs);
        debug 1 (fun () -> Format.printf "  Pass #%d...@." i);
        let subst, cs' = pass ~config reduce_constraint subst cs in
        let p = progress cs cs' in
        debug 3 (fun () -> Format.printf "    Progress: %b@." p);
        if p
        then passes subst (i + 1) cs'
        else
          let subst, cs'' = pass ~config default_constraint subst cs in
          (* Instead of doing a full pass we could stop after
             progress has been made on one constraint here. *)
          let p = progress cs' cs'' in
          debug 3 (fun () -> Format.printf "    Progress (defaulting): %b@." p);
          if p then passes subst (i + 1) cs'' else (subst, cs'')
  in
  debug 1 (fun () -> print_endline "Constraint resolution...");
  let subst, cs = passes (Substitution.empty ()) 1 (List.rev constraints) in
  let mandatory = function
    | _, IsPackable _ -> false
    | _, IsComparable _ -> false
    | _, IsNotHot _ -> false
    | _ -> true
  in
  (match List.find_opt mandatory cs with
  | None -> ()
  | Some (line_no, c) ->
      raise
        (SmartExcept
           (unresolved_error ~line_no c @ [`Br; `Text "(unresolved constraint)"])));
  debug 1 (fun () -> print_endline "All constraints resolved.");
  debug 2 (fun () -> Substitution.dump subst);
  subst
