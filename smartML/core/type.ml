(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Utils
open Control
module Set = VarId.Set
open Sexplib.Std

let sort_row r = List.sort (fun (lbl1, _) (lbl2, _) -> compare lbl1 lbl2) r

type 't row = (string * 't) list
[@@deriving eq, ord, map, fold, show {with_path = false}, sexp]

type access =
  | Read_only
  | Read_write
[@@deriving eq, ord, show {with_path = false}, sexp]

let access_of_sexp : Sexplib.Sexp.t -> access = function
  | Atom "read-only" -> Read_only
  | Atom "read-write" -> Read_write
  | _ -> failwith "access_of_sexp"

type 't effects_f = {
    with_storage : (access * 't) option Hole.t
  ; with_operations : bool Hole.t
}
[@@deriving eq, ord, show {with_path = false}, map, fold, sexp]

type 't f =
  | T0 of Michelson_base.Type.type0
  | T1 of Michelson_base.Type.type1 * 't
  | T2 of Michelson_base.Type.type2 * 't * 't
  | Int of {isNat : bool Hole.t}
  | Lambda of 't effects_f * 't * 't
  | Record of {
        row : 't row
      ; var : VarId.t option
      ; layout : Layout.t Hole.t
    }
  | Variant of {
        row : 't row
      ; var : VarId.t option
      ; layout : Layout.t Hole.t
    }
  | Unknown of VarId.t * string
  | Tuple of 't list [@sexp.list]
  | Secret_key
  | Sapling_state of int Hole.t
  | Sapling_transaction of int Hole.t
  | Abbrev of {
        module_ : string option
      ; name : string
      ; line_no : (string * int) option
    }
[@@deriving eq, ord, show {with_path = false}, map, fold, sexp]

module F = struct
  type 'a t = 'a f [@@deriving eq, ord, show {with_path = false}, map]
end

include Fix (F)
include FixEQ (F)
include FixORD (F)
include FixSHOW (F)
include FixFUNCTOR (F)

type effects = t effects_f

type tvariable = string * t [@@deriving eq, show {with_path = false}]

let rec t_of_sexp x = F (f_of_sexp t_of_sexp x)

let rec sexp_of_t (F x) = sexp_of_f sexp_of_t x

let tree_layout l =
  let l = ref l in
  let rec layout n =
    if n = 1
    then (
      match !l with
      | [] -> assert false
      | a :: rest ->
          l := rest;
          Layout.leaf a a)
    else
      let n2 = n / 2 in
      let l1 = layout n2 in
      let l2 = layout (n - n2) in
      Binary_tree.Node (l1, l2)
  in
  let size = List.length !l in
  if size = 0 then failwith "default_layout of size 0";
  layout size

let rec comb_layout right_left = function
  | [] -> failwith "comb_layout"
  | [lbl] -> Layout.leaf lbl lbl
  | lbl :: xs -> (
      match right_left with
      | `Right ->
          Binary_tree.node (Layout.leaf lbl lbl) (comb_layout right_left xs)
      | `Left ->
          Binary_tree.node (comb_layout right_left xs) (Layout.leaf lbl lbl))

let build t = F t

let unknown_raw x = F (Unknown (VarId.mk (), x))

let mk0 t = build (T0 t)

let mk1 t t1 = build (T1 (t, t1))

let mk2 t t1 t2 = build (T2 (t, t1, t2))

let unit = build (T0 Unit)

let address = build (T0 Address)

let contract t = build (T1 (Contract, t))

let bool = build (T0 Bool)

let bytes = build (T0 Bytes)

let variant layout row =
  build (Variant {layout; row = sort_row row; var = None})

let variant_default_layout row =
  build
    (Variant
       {
         layout = Value (tree_layout (List.map fst row))
       ; row = sort_row row
       ; var = None
       })

let key_hash = build (T0 Key_hash)

let int = build (Int {isNat = Value false})

let nat = build (Int {isNat = Value true})

let intOrNat () = F (Int {isNat = Hole.mk ()})

let key = build (T0 Key)

let chain_id = build (T0 Chain_id)

let secret_key = build Secret_key

let operation = build (T0 Operation)

let sapling_state memo =
  let memo = Option.cata (Hole.mk ()) Hole.value memo in
  build (Sapling_state memo)

let sapling_transaction memo =
  let memo = Option.cata (Hole.mk ()) Hole.value memo in
  build (Sapling_transaction memo)

let never = build (T0 Never)

let map ~big ~tkey ~tvalue =
  if big
  then build (T2 (Big_map, tkey, tvalue))
  else build (T2 (Map, tkey, tvalue))

let set ~telement = build (T1 (Set, telement))

let record layout row = build (Record {layout; row = sort_row row; var = None})

let record_default_layout row =
  let layout = Hole.Value (comb_layout `Right (List.map fst row)) in
  build (Record {layout; row = sort_row row; var = None})

let record_or_unit layout = function
  | [] -> unit
  | l -> record layout l

let signature = build (T0 Signature)

let option t = build (T1 (Option, t))

let key_value tkey tvalue =
  record_default_layout [("key", tkey); ("value", tvalue)]

let head_tail thead ttail =
  record_default_layout [("head", thead); ("tail", ttail)]

let tor t u = variant_default_layout [("Left", t); ("Right", u)]

let string = build (T0 String)

let timestamp = build (T0 Timestamp)

let mutez = build (T0 Mutez)

let uvariant name t =
  let row = [(name, t)] in
  let var = Some (VarId.mk ()) in
  let layout = Hole.mk () in
  F (Variant {row; var; layout})

let urecord fields =
  let cmp (x, _) (y, _) = Stdlib.compare x y in
  let row = List.sort cmp fields in
  let var = Some (VarId.mk ()) in
  let layout = Hole.mk () in
  F (Record {row; var; layout})

let account =
  record_default_layout
    [
      ("seed", string)
    ; ("address", address)
    ; ("public_key", key)
    ; ("public_key_hash", key_hash)
    ; ("secret_key", secret_key)
    ]

let pair t1 t2 = build (Tuple [t1; t2])

let tuple ts = build (Tuple ts)

let list t = build (T1 (List, t))

let ticket t = build (T1 (Ticket, t))

let lambda effects t1 t2 = build (Lambda (effects, t1, t2))

let bls12_381_g1 = build (T0 Bls12_381_g1)

let bls12_381_g2 = build (T0 Bls12_381_g2)

let bls12_381_fr = build (T0 Bls12_381_fr)

let chest_key = build (T0 Chest_key)

let chest = build (T0 Chest)

let abbrev ~line_no module_ name = build (Abbrev {module_; name; line_no})

let has_unknowns =
  cata (function
    | Unknown _ -> true
    | x -> fold_f ( || ) false x)

let has_holes =
  let is_variable = function
    | Hole.Value _ -> false
    | Variable _ -> true
  in
  let f x =
    let r =
      match x with
      | T0 _ | T1 _ | T2 _ | Lambda _ -> false
      | Int {isNat} -> is_variable isNat
      | Record {var} | Variant {var} -> Option.is_some var
      | Tuple _ -> false
      | Unknown _ -> true
      | Secret_key -> false
      | Sapling_state memo | Sapling_transaction memo -> is_variable memo
      | Abbrev _ -> assert false
    in
    fold_f ( || ) r x
  in
  cata f

let is_hot =
  let open Ternary in
  cata (function
    | T1 (Ticket, _) -> Yes
    | Lambda _ -> No
    | T2 (Lambda, _, _) -> assert false
    | T1 (Contract, _) -> No
    | Unknown _ -> Maybe
    | t -> fold_f or_ No t)

let view_variant t =
  match unF t with
  | Variant {layout; row} -> Some (layout, row)
  | T1 (Option, t) ->
      let row = [("None", unit); ("Some", t)] in
      let layout = tree_layout (List.map fst row) in
      Some (Value layout, row)
  | _ -> None

let of_mtype =
  let f ?annot_type:_ ?annot_variable:_ : _ Michelson_base.Type.mtype_f -> _ =
    function
    | MT0 Nat -> nat
    | MT0 Int -> int
    | MT0 (Sapling_state {memo}) -> F (Sapling_state (Value memo))
    | MT0 (Sapling_transaction {memo}) -> F (Sapling_transaction (Value memo))
    | MT0 c -> F (T0 c)
    | MT1 (c, t1) -> F (T1 (c, t1))
    | MT2 (Map, tkey, tvalue) -> map ~big:false ~tkey ~tvalue
    | MT2 (Big_map, tkey, tvalue) -> map ~big:true ~tkey ~tvalue
    | MT2 (Pair _, t1, t2) -> pair t1 t2
    | MT2 (Or {annot_left = Some left; annot_right = Some right}, t1, t2) ->
        variant_default_layout [(left, t1); (right, t2)]
    | MT2 (Or _, t1, t2) -> variant_default_layout [("Left", t1); ("Right", t2)]
    | MT2 (c, t1, t2) -> F (T2 (c, t1, t2))
    | MT_var _ -> assert false
  in
  Michelson_base.Type.cata_mtype f

let type_of_literal = function
  | Literal.Unit -> unit
  | Bool _ -> bool
  | Int {is_nat} -> build (Int {isNat = is_nat})
  | String _ -> string
  | Bytes _ -> bytes
  | Chain_id _ -> chain_id
  | Timestamp _ -> timestamp
  | Mutez _ -> mutez
  | Address _ -> address
  | Key _ -> key
  | Secret_key _ -> secret_key
  | Key_hash _ -> key_hash
  | Signature _ -> signature
  | Sapling_test_state {memo} -> sapling_state (Some memo)
  | Sapling_test_transaction {memo} -> sapling_transaction (Some memo)
  | Bls12_381_g1 _ -> bls12_381_g1
  | Bls12_381_g2 _ -> bls12_381_g2
  | Bls12_381_fr _ -> bls12_381_fr
  | Chest_key _ -> chest_key
  | Chest _ -> chest

let no_effects = {with_storage = Value None; with_operations = Value false}

let unknown_effects () =
  {with_storage = Hole.mk (); with_operations = Hole.mk ()}

let rawify_lambda ~with_storage ~with_operations t1 t2 =
  let r t =
    match (with_storage, with_operations) with
    | None, false -> t
    | Some tstorage, false -> pair t tstorage
    | None, true -> pair t (list operation)
    | Some tstorage, true -> pair t (pair (list operation) tstorage)
  in
  (r t1, r t2)

let has_effects {with_storage; with_operations} =
  let msg = "rawify_lambda: unknown effect" in
  let with_storage = Option.of_some ~msg (Hole.get with_storage) in
  let with_operations = Option.of_some ~msg (Hole.get with_operations) in
  Option.is_some with_storage || with_operations

let frees_f t =
  let frees = fold_f Set.union Set.empty t in
  match t with
  | Unknown (i, _)
  | Int {isNat = Variable i}
  | Sapling_state (Variable i)
  | Sapling_transaction (Variable i) -> Set.singleton i
  | Lambda ({with_storage; with_operations}, _, _) ->
      let with_storage =
        match with_storage with
        | Variable x -> Set.singleton x
        | _ -> Set.empty
      in
      let with_operations =
        match with_operations with
        | Variable x -> Set.singleton x
        | _ -> Set.empty
      in
      Set.union frees (Set.union with_storage with_operations)
  | Tuple _ -> frees
  | Variant {var; layout} | Record {var; layout} ->
      let layout =
        match layout with
        | Variable i -> Set.singleton i
        | _ -> Set.empty
      in
      let var = Set.of_option var in
      Set.(union frees (union layout var))
  | t -> fold_f Set.union Set.empty t

let frees = cata frees_f
