(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Basics
open Unchecked

type t = expr [@@deriving show]

type nullary_expr = line_no:line_no -> t

type unary_expr = line_no:line_no -> t -> t

type binary_expr = line_no:line_no -> t -> t -> t

type ternary_expr = line_no:line_no -> t -> t -> t -> t

val none : line_no:line_no -> t

val self : nullary_expr

val amount : nullary_expr

val balance : nullary_expr

val chain_id : nullary_expr

val level : nullary_expr

val now : nullary_expr

val self_address : nullary_expr

val sender : nullary_expr

val source : nullary_expr

val total_voting_power : nullary_expr

val abs : unary_expr

val attr : name:string -> unary_expr

val sign : unary_expr

val sum : unary_expr

val implicit_account : unary_expr

val is_nat : unary_expr

val left : unary_expr

val neg : unary_expr

val not : unary_expr

val invert : unary_expr

val right : unary_expr

val some : unary_expr

val to_int : unary_expr

val voting_power : unary_expr

val variant : name:string -> unary_expr

val is_variant : name:string -> unary_expr

val is_failing : unary_expr

val catch_exception : t:Type.t -> unary_expr

val type_annotation : t:Type.t -> unary_expr

val hash_key : unary_expr

val blake2b : unary_expr

val sha256 : unary_expr

val sha512 : unary_expr

val keccak : unary_expr

val sha3 : unary_expr

val pack : unary_expr

val constant : line_no:line_no -> string -> Type.t -> t

val concat_list : unary_expr

val size : unary_expr

val list_rev : unary_expr

val first : unary_expr

val second : unary_expr

val to_address : unary_expr

val read_ticket : unary_expr

val join_tickets : unary_expr

val pairing_check : unary_expr

val add : binary_expr

val add_prim : binary_expr

val and_ : binary_expr

val and_bytes : binary_expr

val and_infix : binary_expr

val div : binary_expr

val ediv : binary_expr

val eq : binary_expr

val ge : binary_expr

val gt : binary_expr

val le : binary_expr

val lt : binary_expr

val max : binary_expr

val min : binary_expr

val mod_ : binary_expr

val mul_homo : binary_expr

val mul_prim : binary_expr

val compare : binary_expr

val neq : binary_expr

val or_ : binary_expr

val or_bytes : binary_expr

val or_infix : binary_expr

val sub : binary_expr

val xor_bytes : binary_expr

val xor_infix : binary_expr

val add_seconds : binary_expr

val map_function : binary_expr

val call : line_no:line_no -> t -> t option -> t

val apply_lambda : binary_expr

val invert_bytes : unary_expr

val lshift_infix : binary_expr

val rshift_infix : binary_expr

val lshift_bytes : binary_expr

val rshift_bytes : binary_expr

val sub_mutez : binary_expr

val cons : binary_expr

val contains : binary_expr

val sapling_verify_update : binary_expr

val ticket : binary_expr

val split_ticket : binary_expr

val get_opt : binary_expr

val update_map : ternary_expr

val range : ternary_expr

val check_signature : ternary_expr

val eif : ternary_expr

val test_ticket : ternary_expr

val get_and_update : ternary_expr

val split_tokens : ternary_expr

val open_chest : ternary_expr

val slice : line_no:line_no -> offset:t -> length:t -> buffer:t -> t

val open_variant : line_no:line_no -> string -> t -> t option -> t

val item : line_no:line_no -> t -> t -> t option -> t option -> t

val literal : line_no:line_no -> Literal.t -> t

val record : line_no:line_no -> (string * t) list -> t

val list : line_no:line_no -> elems:t list -> t

val map : line_no:line_no -> big:bool -> entries:(t * t) list -> t

val set : line_no:line_no -> entries:t list -> t

val unpack : line_no:line_no -> t -> Type.t -> t

val account_of_seed : seed:string -> line_no:line_no -> t

val make_signature :
     line_no:line_no
  -> secret_key:t
  -> message:t
  -> message_format:[ `Hex | `Raw ]
  -> t

val var : line_no:line_no -> string -> t

val self_entrypoint : line_no:line_no -> string -> t

val list_items : line_no:line_no -> t -> bool -> t

val list_keys : line_no:line_no -> t -> bool -> t

val list_values : line_no:line_no -> t -> bool -> t

val list_elements : line_no:line_no -> t -> bool -> t

val contract : line_no:line_no -> string option -> Type.t -> t -> t

val view : line_no:line_no -> string -> t -> t -> Type.t -> t

val static_view : line_no:line_no -> static_id -> string -> t -> t

val tuple : line_no:line_no -> t list -> t

val inline_michelson : line_no:line_no -> Type.t inline_michelson -> t list -> t

val lambda :
     line_no:line_no
  -> bound
  -> command
  -> with_storage:(Type.access * Type.t option) option
  -> with_operations:bool
  -> recursive:string option
  -> t

val transfer :
  line_no:line_no -> arg:expr -> amount:expr -> destination:expr -> t

val emit : line_no:line_no -> string option -> bool -> expr -> t

val set_delegate : line_no:line_no -> expr -> t

val contract_data : line_no:line_no -> contract_id -> t

val contract_private : line_no:line_no -> contract_id -> t

val contract_address : line_no:line_no -> string option -> contract_id -> t

val contract_typed : line_no:line_no -> string option -> contract_id -> t

val contract_balance : line_no:line_no -> contract_id -> t

val contract_baker : line_no:line_no -> contract_id -> t

val sapling_empty_state : int -> t

val of_value : tvalue -> t

val private_ : line_no:line_no -> string -> t

val convert : unary_expr
