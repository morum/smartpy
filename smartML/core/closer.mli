(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Basics
open Checked

val on_expr : expr -> expr

val on_type : Type.t -> Type.t

val on_command : command -> command

val on_contract_def : Checked.contract_def -> Checked.contract_def

val on_action : Checked.action -> Checked.action

val on_scenario : Checked.scenario -> Checked.scenario
