(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Utils

type l = {
    source : string
  ; target : string
}
[@@deriving eq, ord, show]

type t = l Binary_tree.t [@@deriving eq, ord, show, sexp]

val leaf : string -> string -> t

val on_row : (string * 'a) list -> l Binary_tree.t -> (l * 'a) Binary_tree.t
