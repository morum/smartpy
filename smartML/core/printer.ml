(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Utils
open Control
open Basics
open Checked
open Unchecked
open Printf

module type Printer = sig
  (** {1 Types} *)

  module Options : sig
    type t = private {
        html : bool
      ; stripStrings : bool
      ; types : bool
      ; show_lambda_defs : bool
    }

    val string : t

    val stringNoLambdas : t

    val html : t

    val htmlStripStrings : t

    val types : t
  end

  val type_to_string : ?options:Options.t -> Type.t -> string

  val value_to_json : Basics.tvalue -> Yojson.Safe.t

  val type_to_json : Type.t -> Yojson.Safe.t

  val value_to_type_value_json : Basics.tvalue -> Yojson.Safe.t

  (** {1 Values} *)

  val render_storage_tvalue : Options.t -> tvalue -> string

  val tvalue_to_string :
    ?options:Options.t -> ?deep:bool -> ?noEmptyList:bool -> tvalue -> string

  val value_to_string : value -> string

  val variant_to_string :
       ?options:Options.t
    -> ?deep:bool
    -> ?noEmptyList:bool
    -> string
    -> tvalue
    -> string

  val literal_to_sp_string :
    html:bool -> ?protect:unit -> ?strip_strings:unit -> Literal.t -> string

  val literal_to_string :
    options:Options.t -> ?deep:bool -> Basics.Literal.t -> string

  val ppAmount : bool -> Utils.Bigint.t -> string

  (** {1 Expressions, Commands, Contracts} *)

  val layout_to_string : Layout.t Hole.t -> string

  val mprim0_to_string :
    Michelson.mtype Michelson_base.Primitive.prim0 -> string

  val mprim1_to_string :
    Michelson.mtype Michelson_base.Primitive.prim1 -> string

  val mprim2_to_string :
    Michelson.mtype Michelson_base.Primitive.prim2 -> string

  val mprim3_to_string : Michelson_base.Primitive.prim3 -> string

  val expr_to_string : ?options:Options.t -> ?protect:unit -> expr -> string

  val texpr_to_string :
    ?options:Options.t -> ?protect:unit -> Checked.expr -> string

  val command_to_string :
    ?options:Options.t -> ?indent:string -> command -> string

  val tcommand_to_string :
    ?options:Options.t -> ?indent:string -> Checked.command -> string

  val binding_to_string :
    options:Options.t -> ?indent:string -> expr * expr -> string

  val tbinding_to_string :
    options:Options.t -> ?indent:string -> Checked.expr * Checked.expr -> string

  val render_record_list :
    string list * 'a list list -> ('a -> string) -> string

  (** {1 Operations} *)

  val operation_to_string : ?options:Options.t -> toperation -> string

  (** {1 Exceptions} *)

  val wrong_condition_string : Checked.expr -> string

  val error_to_string : Execution.error -> string

  val exception_to_smart_except : exn -> Basics.smart_except list

  val exception_to_string : bool -> exn -> string

  val pp_smart_except : bool -> Basics.smart_except list -> string

  val string_of_contract_id : contract_id -> string

  val qual_ident : qual_ident -> string
end

module type Language = sig
  val config : Config.t
end

module Build_printer (Language : Language) = struct
  include Language

  let protocol = config.protocol

  let rec layout_to_string = function
    | Binary_tree.Leaf Layout.{source; target} ->
        if source = target
        then sprintf "%S" source
        else sprintf "\"%s as %s\"" source target
    | Binary_tree.Node (l1, l2) ->
        sprintf "(%s, %s)" (layout_to_string l1) (layout_to_string l2)

  let layout_to_string =
    let open Hole in
    function
    | Value l -> sprintf ".layout(%s)" (layout_to_string l)
    | _ -> ""

  let unknown_memo memo =
    Option.value (Option.map string_of_int (Hole.get memo)) ~default:""

  module Options = struct
    type t = {
        html : bool
      ; stripStrings : bool
      ; types : bool
      ; show_lambda_defs : bool
    }

    let string =
      {
        html = false
      ; stripStrings = false
      ; types = false
      ; show_lambda_defs = true
      }

    let stringNoLambdas =
      {
        html = false
      ; stripStrings = false
      ; types = false
      ; show_lambda_defs = false
      }

    let html = {string with html = true; show_lambda_defs = true}

    let htmlStripStrings =
      {html with stripStrings = true; show_lambda_defs = true}

    let types = {html with types = true; show_lambda_defs = true}
  end

  let render_record_list (columns, data) f =
    let ppRow row =
      sprintf "<tr>%s</tr>"
        (String.concat ""
           (List.map (fun s -> sprintf "<td class='data'>%s</td>" (f s)) row))
    in
    sprintf "<table class='recordList'><tr>%s</tr>%s</table>"
      (String.concat "\n"
         (List.map
            (fun s ->
              sprintf "<td class='dataColumn'>%s</td>"
                (String.capitalize_ascii s))
            columns))
      (String.concat "\n" (List.map ppRow data))

  let ppAmount html amount =
    let oneMillion = Bigint.of_int 1000000 in
    let quotient, modulo = Big_int.quomod_big_int amount oneMillion in
    if html
    then
      let mutez = Big_int.string_of_big_int amount in
      let mutez =
        if String.length mutez < 7
        then String.sub "0000000" 0 (7 - String.length mutez) ^ mutez
        else mutez
      in
      sprintf
        "%s.%s<img height=20 width=20 src='static/img/tezos.svg' alt='tz'/>"
        (String.sub mutez 0 (String.length mutez - 6))
        (String.sub mutez (String.length mutez - 6) 6)
    else if Big_int.compare_big_int modulo Big_int.zero_big_int = 0
    then sprintf "sp.tez(%s)" (Big_int.string_of_big_int quotient)
    else sprintf "sp.mutez(%s)" (Big_int.string_of_big_int amount)

  let literal_to_sp_string ~html ?protect ?strip_strings =
    let to_string ?protect (l : Literal.t) =
      let open Format in
      let entrypoint_opt ppf = function
        | None | Some "default" -> fprintf ppf ""
        | Some ep -> fprintf ppf "%%%s" ep
      in
      let apply name arguments =
        sprintf "%s(%s)" name (String.concat ", " arguments)
      in
      let quote = "'" in
      let mono name s = apply ("sp." ^ name) [s] in
      let mono_string name s = mono name (sprintf "'%s'" s) in
      let mono_hex name s =
        mono_string name ("0x" ^ Hex.(show (of_string s)))
      in
      match l with
      | Unit -> "sp.unit"
      | Bool x ->
          let b = string_of_bool x in
          String.capitalize_ascii b
      | Int {i} ->
          let r = Big_int.string_of_big_int i in
          if protect = Some () && Big_int.sign_big_int i < 0
          then "(" ^ r ^ ")"
          else r
      | String s when strip_strings = Some () -> s
      | String s -> sprintf "%s%s%s" quote s quote
      | Bytes s -> mono_hex "bytes" s
      | Chain_id s -> mono_hex "chain_id" s
      | Mutez i -> ppAmount html i
      | Address {address; entrypoint} ->
          asprintf "sp.address('%s%a')" address entrypoint_opt entrypoint
      | Timestamp i -> mono "timestamp" (Big_int.string_of_big_int i)
      | Key s -> mono_string "key" s
      | Secret_key s -> mono_string "secret_key" s
      | Key_hash s -> mono_string "key_hash" s
      | Signature s -> mono_string "signature" s
      | Sapling_test_state _ -> "sapling test state"
      | Sapling_test_transaction _ -> "sapling test transaction"
      | Bls12_381_g1 s -> mono_hex "bls12_381_g1" s
      | Bls12_381_g2 s -> mono_hex "bls12_381_g2" s
      | Bls12_381_fr s -> mono_hex "bls12_381_fr" s
      | Chest_key s -> mono_hex "chest_key" s
      | Chest s -> mono_hex "chest" s
    in
    fun s -> to_string ?protect s

  let literal_to_string ~(options : Options.t) ?(deep = false) :
      Literal.t -> string = function
    | Bool x ->
        let res = string_of_bool x in
        String.capitalize_ascii res
    | Int {i} ->
        let pp = Big_int.string_of_big_int i in
        if options.html && not deep
        then sprintf "<input type='text' value='%s' readonly></input>" pp
        else pp
    | String s -> if options.stripStrings then s else sprintf "'%s'" s
    | Bytes s ->
        if options.html
        then sprintf "<span class='bytes'>0x%s</span>" Hex.(show (of_string s))
        else sprintf "sp.bytes('0x%s')" Hex.(show (of_string s))
    | Bls12_381_g1 s ->
        if options.html
        then sprintf "<span class='bytes'>0x%s</span>" Hex.(show (of_string s))
        else sprintf "sp.bls12_381_g1('0x%s')" Hex.(show (of_string s))
    | Bls12_381_g2 s ->
        if options.html
        then sprintf "<span class='bytes'>0x%s</span>" Hex.(show (of_string s))
        else sprintf "sp.bls12_381_g2('0x%s')" Hex.(show (of_string s))
    | Bls12_381_fr s ->
        if options.html
        then sprintf "<span class='bytes'>0x%s</span>" Hex.(show (of_string s))
        else sprintf "sp.bls12_381_fr('0x%s')" Hex.(show (of_string s))
    | Chest_key s ->
        if options.html
        then sprintf "<span class='bytes'>0x%s</span>" Hex.(show (of_string s))
        else sprintf "sp.chest_key('0x%s')" Hex.(show (of_string s))
    | Chest s ->
        if options.html
        then sprintf "<span class='bytes'>0x%s</span>" Hex.(show (of_string s))
        else sprintf "sp.chest('0x%s')" Hex.(show (of_string s))
    | Chain_id s ->
        if options.html
        then sprintf "<span class='bytes'>0x%s</span>" Hex.(show (of_string s))
        else sprintf "sp.chain_id('0x%s')" Hex.(show (of_string s))
    | Unit -> "()"
    | Key_hash s ->
        if options.html
        then sprintf "<span class='key'>%s</span>" s
        else sprintf "sp.key_hash('%s')" s
    | Key s ->
        if options.html
        then sprintf "<span class='key'>%s</span>" s
        else sprintf "sp.key('%s')" s
    | Secret_key s ->
        if options.html
        then sprintf "<span class='key'>%s</span>" s
        else sprintf "sp.secret_key('%s')" s
    | Signature s ->
        if options.html
        then sprintf "<span class='signature'>%s</span>" s
        else sprintf "sp.signature('%s')" s
    | Address {address} as lit ->
        if options.html
        then sprintf "<span class='address'>%s</span>" address
        else literal_to_sp_string ~html:false lit
    | Timestamp i ->
        if options.html
        then
          sprintf "<span class='timestamp'>timestamp(%s)</span>"
            (Big_int.string_of_big_int i)
        else sprintf "sp.timestamp(%s)" (Big_int.string_of_big_int i)
    | Mutez i ->
        let amount = ppAmount options.html i in
        if options.html
        then sprintf "<span class='token'>%s</span>" amount
        else amount
    | Sapling_test_state {test = false} ->
        if options.html
        then render_record_list (["SaplingState"], []) (fun s -> s)
        else
          sprintf "[%s]" (String.concat "; " (List.map (String.concat ",") []))
    | Sapling_test_state {elements} ->
        let l =
          List.map
            (fun (key, amount) -> [key; Bigint.string_of_big_int amount])
            (List.sort compare elements)
        in
        if options.html
        then render_record_list (["key"; "amount"], l) (fun s -> s)
        else
          sprintf "[%s]" (String.concat "; " (List.map (String.concat ",") l))
    | Sapling_test_transaction {source; target; amount} ->
        sprintf "sp.sapling_test_transaction(%S, %S, %s)"
          (Utils.Option.default "" source)
          (Utils.Option.default "" target)
          (Bigint.string_of_big_int amount)

  let rec type_to_string_new t =
    let open Type in
    let pp = type_to_string_new in
    let mono name arg = sprintf "%s(%s)" name arg in
    let bin name t1 t2 = sprintf "%s(%s, %s)" name t1 t2 in
    let print_row ?(sep = ", ") row =
      String.concat sep
        (List.map
           (fun (s, t) -> sprintf "%s = %s" s (type_to_string_new t))
           row)
    in
    match unF t with
    | T0 Bool -> "sp.bool"
    | T0 String -> "sp.string"
    | T0 Timestamp -> "sp.timestamp"
    | T0 Bytes -> "sp.bytes"
    | Int {isNat} -> (
        match Typing.intType isNat with
        | `Unknown -> "sp.intOrNat"
        | `Nat -> "sp.nat"
        | `Int -> "sp.int")
    | Record {row; layout; var = None} ->
        sprintf "sp.record(%s)%s" (print_row ~sep:", " row)
          (layout_to_string layout)
    | Variant {row; layout; var = None} ->
        sprintf "sp.variant(%s)%s" (print_row row) (layout_to_string layout)
    | T1 (Set, telement) -> sprintf "sp.set(%s)" (pp telement)
    | T2 (((Map | Big_map) as tm), tkey, tvalue) ->
        let name =
          match tm with
          | Big_map -> "sp.big_map"
          | Map -> "sp.map"
          | _ -> assert false
        in
        bin name (pp tkey) (pp tvalue)
    | T0 Mutez -> "sp.mutez"
    | T0 Unit -> "sp.unit"
    | T0 Address -> "sp.address"
    | T0 Key_hash -> "sp.keyHash"
    | T0 Key -> "sp.key"
    | Secret_key -> "sp.secretkey"
    | T0 Chain_id -> "sp.chainId"
    | T0 Signature -> "sp.signature"
    | T1 (Contract, t) -> mono "sp.contract" (pp t)
    | Unknown _ -> sprintf "sp.unknown"
    | Record {row; var = Some _; layout = _} ->
        sprintf "TRecord++(%s)" (print_row row)
    | Variant {row; var = Some _; layout = _} ->
        sprintf "TVariant++(%s)" (print_row row)
    | Tuple [t1; t2] ->
        let t1 = pp t1 in
        let t2 = pp t2 in
        sprintf "sp.pair(%s, %s)" t1 t2
    | Tuple ts -> sprintf "sp.tuple(%s)" (String.concat ", " (List.map pp ts))
    | T1 (List, t) -> mono "sp.list" (pp t)
    | T1 (Ticket, t) -> mono "sp.ticket" (pp t)
    | Lambda ({with_storage; with_operations}, t1, t2) ->
        let f name = function
          | Some true -> ", " ^ name ^ "=True"
          | Some false -> ""
          | None -> ", " ^ name ^ "=?"
        in
        let with_storage =
          match Hole.get with_storage with
          | None | Some None -> ""
          | Some (Some (Read_only, tstorage)) ->
              ", with_storage=\"read-only\", tstorage=" ^ pp tstorage
          | Some (Some (Read_write, tstorage)) ->
              ", with_storage=\"read-write\", tstorage=" ^ pp tstorage
        in
        let to_string = pp in
        sprintf "sp.lambda_(%s, %s%s%s)" (to_string t1) (to_string t2)
          with_storage
          (f "with_operations" (Hole.get with_operations))
    | T2 (Lambda, _, _) -> assert false
    | T0 Operation -> "sp.operation"
    | Sapling_state memo -> mono "sp.saplingState" (unknown_memo memo)
    | Sapling_transaction memo ->
        mono "sp.saplingTransaction" (unknown_memo memo)
    | T0 Never -> "sp.never"
    | T0 Bls12_381_g1 -> "sp.bls12_381_g1"
    | T0 Bls12_381_g2 -> "sp.bls12_381_g2"
    | T0 Bls12_381_fr -> "sp.bls12_381_fr"
    | T0 Chest_key -> "sp.chest_key"
    | T0 Chest -> "sp.chest"
    | T0 (Nat | Int | Sapling_state _ | Sapling_transaction _) -> assert false
    | T1 (Option, t) -> mono "sp.option" (pp t)
    | Abbrev {module_ = None; name} -> name
    | Abbrev {module_ = Some module_; name} -> module_ ^ "." ^ name
    | T2 ((Pair _ | Or _), _, _) -> assert false

  let type_to_string ~(options : Options.t) t =
    let t = type_to_string_new t in
    if options.html then sprintf "<span class='type'>%s</span>" t else t

  let rec type_to_json t =
    let l t = [`String t] in
    let ppRow row =
      List.flatten (List.map (fun (s, t) -> [`String s; type_to_json t]) row)
    in
    let open Type in
    `List
      (match unF t with
      | T0 Bool -> l "Bool"
      | T0 String -> l "String"
      | T0 Timestamp -> l "Timestamp"
      | T0 Bytes -> l "Bytes"
      | T0 Mutez -> l "Mutez"
      | T0 Unit -> l "Unit"
      | T0 Address -> l "Address"
      | T0 Key_hash -> l "Key_hash"
      | T0 Key -> l "Key"
      | Secret_key -> l "Secretkey"
      | T0 Chain_id -> l "Chain_id"
      | T0 Signature -> l "Signature"
      | Int {isNat} -> (
          match Typing.intType isNat with
          | `Unknown -> l "IntOrNat"
          | `Nat -> l "Nat"
          | `Int -> l "Int")
      | T0 Never -> l "Never"
      | T0 Bls12_381_g1 -> l "Bls12_381_g1"
      | T0 Bls12_381_g2 -> l "Bls12_381_g2"
      | T0 Bls12_381_fr -> l "Bls12_381_fr"
      | T0 Chest -> l "Chest"
      | T0 Chest_key -> l "Chest_key"
      (* TODO *)
      (* TODO: layout *)
      | Record {row; var = _; _} -> `String "Record" :: ppRow row
      (* TODO: layout *)
      | Variant {row; var = _; layout = _} -> `String "Variant" :: ppRow row
      | T1 (Set, telement) -> [`String "Set"; type_to_json telement]
      | T2 (Map, tkey, tvalue) ->
          [`String "Map"; type_to_json tkey; type_to_json tvalue]
      | T2 (Big_map, tkey, tvalue) ->
          [`String "BigMap"; type_to_json tkey; type_to_json tvalue]
      | T1 (Contract, t) -> [`String "Contract"; type_to_json t]
      | Unknown (_, id) -> [`String "Unknown"; `String id]
      | Tuple l ->
          `String "Tuple"
          :: List.flatten
               (List.mapi
                  (fun s t -> [`String (string_of_int s); type_to_json t])
                  l)
      | T1 (List, t) -> [`String "List"; type_to_json t]
      | T1 (Ticket, t) -> [`String "Ticket"; type_to_json t]
      | Lambda ({with_storage; with_operations}, t1, t2) ->
          let f = function
            | Some true -> `Bool true
            | Some false -> `Bool true
            (* | None -> `Null *)
            | None -> assert false
          in
          let with_storage =
            match Hole.get with_storage with
            | None | Some None -> `String ""
            | Some (Some (Read_only, _)) ->
                `String "read-only" (* TODO print tstorage *)
            | Some (Some (Read_write, _)) -> `String "read-write"
          in
          [
            `String "Lambda"
          ; type_to_json t1
          ; type_to_json t2
          ; `List [f (Hole.get with_operations); with_storage]
          ]
      | T0 Operation -> [`String "Operation"]
      (* TODO *)
      | Sapling_state _ -> assert false
      (* TODO *)
      | Sapling_transaction _ -> assert false
      | T1 (Option, t) ->
          [
            `String "Option"
          ; `String "None"
          ; `List [`String "Unit"]
          ; `String "Some"
          ; type_to_json t
          ]
      | T0 (Nat | Int | Sapling_state _ | Sapling_transaction _) -> assert false
      | T2 ((Pair _ | Or _), _, _) -> assert false
      | T2 (Lambda, _, _) -> assert false
      | Abbrev _ -> failwith "type_to_json: TAbbrev")

  let rec value_to_json v =
    let intV i = `Intlit (Big_int.string_of_big_int i) in
    let l v = [v] in
    let literal = function
      | Literal.Unit -> l (`String "unit")
      | Bool v -> (
          match v with
          | true -> l (`String "true")
          | false -> l (`String "false"))
      | Int {i; is_nat} -> (
          match Typing.intType is_nat with
          | `Unknown -> l (intV i)
          | `Nat -> l (intV i)
          | `Int -> l (intV i))
      | String v -> l (`String v)
      | Bytes v -> l (`String v)
      | Chain_id v -> l (`String v)
      | Timestamp v -> l (intV v)
      | Mutez v -> l (intV v)
      | Address {address; entrypoint} -> (
          match entrypoint with
          | None -> l (`String address)
          | Some ep -> [`String address; `String ep])
      | Key v -> l (`String v)
      | Secret_key v -> l (`String v)
      | Key_hash v -> l (`String v)
      | Signature v -> l (`String v)
      | Sapling_test_state _ -> assert false
      | Sapling_test_transaction _ -> assert false
      | Bls12_381_g1 v -> l (`String v)
      | Bls12_381_g2 v -> l (`String v)
      | Bls12_381_fr v -> l (`String v)
      | Chest_key v -> l (`String v)
      | Chest v -> l (`String v)
    in
    `List
      (match v.tv with
      | Literal l -> literal l
      | Contract _ -> assert false
      | Record entries ->
          List.flatten
            (List.map (fun (k, v) -> [`String k; value_to_json v]) entries)
      | Variant (k, v) -> [`String k; value_to_json v]
      | List l -> List.map (fun v -> value_to_json v) l
      | Set l -> List.map (fun v -> value_to_json v) l
      | Map entries ->
          List.flatten
            (List.map
               (fun (k, v) -> [value_to_json k; value_to_json v])
               entries)
      | Tuple l -> List.map (fun v -> value_to_json v) l
      | Closure (_, _) -> assert false
      | Operation _ -> assert false
      | Ticket (e, v, a) -> [`String e; value_to_json v; intV a])

  let value_to_type_value_json v = `List [type_to_json v.t; value_to_json v]

  let string_of_contract_id = function
    | C_static {static_id} -> sprintf "%i" static_id
    | C_dynamic {dynamic_id} -> sprintf "Dyn_%i" dynamic_id

  let unrec = function
    | {tv = Record l} -> List.map snd l
    | x -> [x]

  let is_range_keys l =
    let rec aux n = function
      | [] -> true
      | ({tv = Literal (Int {i})}, _) :: l ->
          Big_int.eq_big_int (Bigint.of_int n) i && aux (n + 1) l
      | (_, _) :: _ -> false
    in
    aux 0 l

  let is_vector v =
    match v.tv with
    | Map l -> if is_range_keys l then Some (List.map snd l) else None
    | _ -> None

  let is_matrix l =
    match is_vector l with
    | None -> None
    | Some vect -> (
        let new_line lines a =
          match (lines, is_vector a) with
          | None, _ | _, None -> None
          | Some lines, Some v -> Some (v :: lines)
        in
        match List.fold_left new_line (Some []) vect with
        | Some lines
          when 2 <= List.length lines
               && List.exists (fun line -> 2 <= List.length line) lines ->
            Some ([], List.rev lines)
        | _ -> None)

  let is_record_list = function
    | {tv = Record _} :: _ -> true
    | _ -> false

  let with_vclass ~html vclass x =
    if html then sprintf "<span class='%s'>%s</span>" vclass x else x

  let variable_to_string ~(options : Options.t) ?protect (s : string) vclass =
    let prot s = if protect = Some () then sprintf "(%s)" s else s in
    match options with
    | {html = false; types = false} -> s
    | {html; types = true} -> prot (with_vclass ~html vclass s)
    | {types = false} -> sprintf "<span class='%s'>%s</span>" vclass s

  let string_of_binOp op =
    match op with
    | EAdd -> "+"
    | EAdd_seconds -> "add_seconds"
    | EAnd -> "and"
    | EApply_lambda -> "apply_lambda"
    | EAnd_bytes -> "and_bytes"
    | EAnd_infix -> "&"
    | EOr_bytes -> "or_bytes"
    | EOr_infix -> "|"
    | ECons -> "::"
    | EContains -> "contains"
    | EDiv -> "/"
    | EEDiv -> "ediv"
    | EEq -> "=="
    | EGe -> ">="
    | EGet_opt -> "get_opt"
    | EGt -> ">"
    | ELe -> "<="
    | ELshift_infix -> "<<"
    | ELshift_bytes -> "lshift_bytes"
    | ERshift_infix -> ">>"
    | ERshift_bytes -> "rshift_bytes"
    | ELt -> "<"
    | EMax -> "max"
    | EMin -> "min"
    | EMod -> "%"
    | EMul_homo -> "*"
    | ENeq -> "!="
    | EOr -> "or"
    | ESplit_ticket -> "split_ticket_raw"
    | ESub -> "-"
    | ETicket -> "ticket"
    | EView _ -> assert false
    | EXor_bytes -> "xor_bytes"
    | EXor_infix -> "^"

  let pps, ppS =
    let comma buf = bprintf buf ", " in
    ( List.buffer_sep comma (fun buf -> bprintf buf "%s")
    , List.buffer_sep comma (fun buf -> bprintf buf "%S") )

  let prefix name = "sp." ^ name

  let mprim0_to_string p =
    let Michelson.{name} = Michelson.spec_of_instr ~protocol (MI0 p) in
    String.lowercase_ascii name

  let mprim1_to_string : _ Michelson_base.Primitive.prim1 -> _ = function
    | IsNat -> "sp.is_nat"
    | p ->
        let Michelson.{name} = Michelson.spec_of_instr ~protocol (MI1 p) in
        prefix (String.lowercase_ascii name)

  let mprim2_to_string p =
    let Michelson.{name} = Michelson.spec_of_instr ~protocol (MI2 p) in
    String.lowercase_ascii name

  let mprim3_to_string p =
    let Michelson.{name} = Michelson.spec_of_instr ~protocol (MI3 p) in
    String.lowercase_ascii name

  let rec expr_to_string ~(options : Options.t) ?protect e =
    let prot s = if protect = Some () then sprintf "(%s)" s else s in
    let htmlClass name s =
      if options.html then sprintf "<span class='%s'>%s</span>" name s else s
    in
    let putSelf s = sprintf "%s.%s" (htmlClass "self" "self") s in
    let to_string = expr_to_string ~options in
    let apply name arguments =
      sprintf "%s(%s)" name (String.concat ", " arguments)
    in
    let std name arguments = apply ("sp." ^ name) arguments in
    let std_no_prefix name arguments = apply name arguments in
    let pp_exprs arguments = List.map (fun s -> to_string s) arguments in
    let mono name s = std name (pp_exprs [s]) in
    let bin name x y = std name (pp_exprs [x; y]) in
    let tern name x y z = std name (pp_exprs [x; y; z]) in
    let mprim0_to_string p =
      match (p : _ Michelson_base.Primitive.prim0) with
      | Self None -> prefix "self"
      | Self (Some name) -> sprintf "sp.self_entrypoint('%s')" name
      | Sapling_empty_state {memo} -> sprintf "sp.sapling_empty_state(%i)" memo
      | p -> prefix (mprim0_to_string p)
    in
    let mprim1_to_string p x =
      match (p : _ Michelson_base.Primitive.prim1) with
      | Abs -> prot (apply "abs" (pp_exprs [x]))
      | p -> std_no_prefix (mprim1_to_string p) (pp_exprs [x])
    in
    let mprim2_to_string p x1 x2 =
      match (p : _ Michelson_base.Primitive.prim2) with
      | Lsl -> prot (infix ~options "<<" x1 x2)
      | Lsr -> prot (infix ~options ">>" x1 x2)
      | p -> bin (mprim2_to_string p) x1 x2
    in
    let mprim3_to_string p x1 x2 x3 =
      match (p : Michelson_base.Primitive.prim3) with
      | Check_signature ->
          sprintf "sp.check_signature(%s, %s, %s)" (to_string x1) (to_string x2)
            (to_string x3)
      | p ->
          let Michelson.{name} = Michelson.spec_of_instr ~protocol (MI3 p) in
          tern (String.lowercase_ascii name) x1 x2 x3
    in
    match e.e with
    | EVar {name = {bound = "__parameter__"}} -> sprintf "params"
    | EVar {name = {bound = "__operations__"}} -> prefix "operations"
    | EVar {name = {bound = "__storage__"}} -> putSelf "data"
    | EVar {name} -> name.bound
    | EPrivate x -> sprintf "self.%s" x
    | EMPrim0 p -> mprim0_to_string p
    | EMPrim1 (p, x) -> mprim1_to_string p x
    | EMPrim1_fail _ -> assert false
    | EMPrim2 (p, x1, x2) -> mprim2_to_string p x1 x2
    | EMPrim3 (p, x1, x2, x3) -> mprim3_to_string p x1 x2 x3
    | EAttr (name, x) -> sprintf "%s.%s" (to_string ~protect:() x) name
    | EVariant (name, x) -> (
        match (name, x) with
        | "None", {e = ELiteral Unit} -> prefix "none"
        | "Some", x -> mono "some" x
        | name, x -> sprintf "variant('%s', %s)" name (to_string x))
    | ELiteral v ->
        htmlClass "constant"
          (literal_to_sp_string ~html:false ?protect
             ?strip_strings:(if options.stripStrings then Some () else None)
             v)
    | EPrim0 prim -> (
        match prim with
        | ECst_contract {entrypoint; address} ->
            htmlClass "constant"
              (literal_to_sp_string ~html:false
                 ?strip_strings:(if options.stripStrings then Some () else None)
                 (Literal.address ?entrypoint address))
        | EContract_address (id, entrypoint) ->
            sprintf "sp.address('%s')"
              (Aux.address_of_contract_id ~html:false id entrypoint)
        | EContract_balance id ->
            sprintf "sp.contract_balance(%s)" (string_of_contract_id id)
        | EContract_baker id ->
            sprintf "sp.contract_baker(%s)" (string_of_contract_id id)
        | EContract_data id ->
            sprintf "sp.contract_data(%s)" (string_of_contract_id id)
        | EContract_private id ->
            sprintf "sp.contract_private(%s)" (string_of_contract_id id)
        | EContract_typed (id, entrypoint) ->
            sprintf "sp.contract_typed(%s)"
              (Aux.address_of_contract_id ~html:false id entrypoint)
        | EAccount_of_seed {seed} -> sprintf "sp.test_account(%S)" seed
        | EConstant (e, t) ->
            prefix
              (sprintf "constant(%S, t = %s)" e
                 (type_to_string ~options:Options.string t)))
    | EPrim1 (prim, x) -> (
        let mod_fun name x = sprintf "%s.%s()" (to_string ~protect:() x) name in
        match prim with
        | ENot -> sprintf "not %s" (to_string ~protect:() x)
        | EInvert -> sprintf "~ %s" (to_string ~protect:() x)
        | EList_rev -> mod_fun "rev" x
        | EList_items false -> mod_fun "items" x
        | EList_keys false -> mod_fun "keys" x
        | EList_values false -> mod_fun "values" x
        | EList_elements false -> mod_fun "elements" x
        | EList_items true -> mod_fun "rev_items" x
        | EList_keys true -> mod_fun "rev_keys" x
        | EList_values true -> mod_fun "rev_values" x
        | EList_elements true -> mod_fun "rev_elements" x
        | EPack -> mono "pack" x
        | ETo_nat -> mono "to_nat" x
        | ETo_int -> mono "to_int" x
        | ETo_bytes -> mono "to_bytes" x
        | ENeg -> prot (sprintf "- %s" (to_string ~protect:() x))
        | ESign -> mono "sign" x
        | ESum -> mono "sum" x
        | EUnpack t ->
            std "unpack"
              (pp_exprs [x] @ [type_to_string ~options:Options.string t])
        | EAddress -> (
            match x.e with
            | EMPrim0 (Self None) -> prefix "self_address"
            | EMPrim0 (Self (Some name)) ->
                sprintf "sp.self_entrypoint_address('%s')" name
            | _ -> mono "to_address" x)
        | EImplicit_account -> mono "implicit_account" x
        | EFst -> mono "fst" x
        | ESnd -> mono "snd" x
        | EConcat_list -> mono "concat" x
        | EInvert_bytes -> mono "invert_bytes" x
        | ESize -> mono "len" x
        | ESet_delegate -> mono "set_delegate_operation" x
        | EType_annotation t ->
            std "cast"
              (pp_exprs [x] @ [type_to_string ~options:Options.string t])
        | EIs_variant "Some" -> sprintf "%s.is_some()" (to_string ~protect:() x)
        | EIs_variant name -> sprintf "%s.is_variant('%s')" (to_string x) name
        | ERead_ticket -> mono "read_ticket_raw" x
        | EJoin_tickets -> mono "join_tickets_raw" x
        | EPairing_check -> mono "pairing_check" x
        | EVoting_power -> mono "voting_power" x
        | EConvert -> mono "convert" x
        | EStatic_view (static_id, name) ->
            sprintf "sp.contract_view(%s, %S, %s)"
              (string_of_contract_id (C_static static_id))
              name (to_string x)
        | EEmit (tag, _with_type) ->
            let tag =
              match tag with
              | None -> ""
              | Some tag -> sprintf ", tag = %S" tag
            in
            sprintf "sp.emit_operation(%s%s)" (to_string x) tag)
    | ECall (x, None) ->
        let lambda = x in
        sprintf "%s()" (to_string ~protect:() lambda)
    | ECall (x, Some y) ->
        let lambda, parameter = (x, y) in
        sprintf "%s(%s)" (to_string ~protect:() lambda) (to_string parameter)
    | EPrim2 (prim2, x, y) -> (
        match prim2 with
        | EApply_lambda ->
            let parameter, lambda = (x, y) in
            sprintf "%s.apply(%s)"
              (to_string ~protect:() lambda)
              (to_string parameter)
        | (EEDiv | EMax | EMin | EAdd_seconds | ETicket | ESplit_ticket) as op
          -> bin (string_of_binOp op) x y
        | ( EAdd
          | EAnd
          | EAnd_bytes
          | EAnd_infix
          | EOr_bytes
          | EOr_infix
          | EDiv
          | EEq
          | EGe
          | EGt
          | ELe
          | ELshift_infix
          | ELshift_bytes
          | ERshift_infix
          | ERshift_bytes
          | ELt
          | EMod
          | EMul_homo
          | ENeq
          | EOr
          | ESub
          | EXor_bytes
          | EXor_infix ) as op -> prot (infix ~options (string_of_binOp op) x y)
        | EGet_opt ->
            let k, m = (x, y) in
            sprintf "%s.get_opt(%s)" (to_string ~protect:() m) (to_string k)
        | ECons ->
            let e1, e2 = (x, y) in
            sprintf "sp.cons(%s, %s)" (to_string e1) (to_string e2)
        | EContains ->
            let member, items = (x, y) in
            prot
              (sprintf "%s.contains(%s)"
                 (to_string ~protect:() items)
                 (to_string member))
        | EView (name, return_type) ->
            let param, address = (x, y) in
            sprintf "sp.view(%S, %s, %s, %s)" name (to_string param)
              (to_string address)
              (type_to_string ~options:Options.string return_type))
    | EPrim3 (prim3, x, y, z) -> (
        match prim3 with
        | ERange -> (
            match (x, y, z) with
            | a, b, {e = ELiteral (Int {i = step})}
              when Big_int.eq_big_int step (Bigint.of_int 1) ->
                sprintf "sp.range(%s, %s)" (to_string a) (to_string b)
            | e1, e2, e3 ->
                sprintf "sp.range(%s, %s, %s)" (to_string e1) (to_string e2)
                  (to_string e3))
        | EUpdate_map ->
            sprintf "sp.update_map(%s, %s, %s)" (to_string z) (to_string x)
              (to_string y)
        | EGet_and_update ->
            sprintf "sp.get_and_update(%s, %s, %s)" (to_string z) (to_string x)
              (to_string y)
        | ESplit_tokens -> (
            match (x, y, z) with
            | {e = ELiteral (Mutez tok)}, quantity, {e = ELiteral (Int {i})}
              when Big_int.eq_big_int tok (Bigint.of_int 1000000)
                   && Big_int.eq_big_int i (Bigint.of_int 1) ->
                sprintf "sp.tez(%s)" (to_string quantity)
            | {e = ELiteral (Mutez tok)}, quantity, {e = ELiteral (Int {i})}
              when Big_int.eq_big_int tok (Bigint.of_int 1)
                   && Big_int.eq_big_int i (Bigint.of_int 1) ->
                sprintf "sp.mutez(%s)" (to_string quantity)
            | e1, e2, e3 ->
                sprintf "sp.split_tokens(%s, %s, %s)" (to_string e1)
                  (to_string e2) (to_string e3))
        | ETest_ticket ->
            sprintf "sp.test_ticket(%s, %s, %s)" (to_string x) (to_string y)
              (to_string z))
    | EOpen_variant (name, x, missing_message) -> (
        let missing_message =
          match missing_message with
          | None -> None
          | Some x -> Some (sprintf "message = %s" (to_string x))
        in
        match (name, x, missing_message) with
        | "Some", {e = EMPrim1 (IsNat, x)}, None -> mono "as_nat" x
        | "Some", {e = EMPrim1 (IsNat, x)}, Some message ->
            sprintf "sp.as_nat(%s, %s)" (to_string x) message
        | "Some", x, None -> sprintf "%s.open_some()" (to_string x)
        | "Some", x, Some message ->
            sprintf "%s.open_some(%s)" (to_string x) message
        | name, x, None -> sprintf "%s.open_variant('%s')" (to_string x) name
        | name, x, Some message ->
            sprintf "%s.open_variant('%s', %s)" (to_string x) name message)
    | ELambda
        {
          lambda_var
        ; body = {c = CResult {e = EMichelson (michelson, [{e = EVar var'}])}}
        }
      when lambda_var = var'.name ->
        sprintf "sp.lambda_michelson(%S)" michelson.name
    | ELambda {lambda_var; body = {c = CResult r}} ->
        sprintf "sp.build_lambda(lambda %s: %s)" lambda_var.bound (to_string r)
    | ELambda {lambda_var} -> sprintf "sp.build_lambda(f%s)" lambda_var.bound
    | EMap_function {f; l} ->
        sprintf "map(%s,%s)" (to_string ~protect:() l) (to_string f)
    | ECreate_contract _ -> sprintf "create contract ..."
    | EItem {items; key; default_value = None; missing_message = None} ->
        sprintf "%s[%s]" (to_string ~protect:() items) (to_string key)
    | EItem {items; key; default_value = Some d} ->
        sprintf "%s.get(%s, default_value = %s)"
          (to_string ~protect:() items)
          (to_string key) (to_string d)
    | EItem {items; key; default_value = None; missing_message = Some message}
      ->
        sprintf "%s.get(%s, message = %s)"
          (to_string ~protect:() items)
          (to_string key) (to_string message)
    | ERecord entries ->
        sprintf "sp.record(%s)"
          (String.concat ", "
             (List.map
                (fun (n, e) -> sprintf "%s = %s" n (to_string e))
                entries))
    | EList l ->
        sprintf "sp.list([%s])" (String.concat ", " (List.map to_string l))
    | EList_comprehension (e, x, xs) ->
        sprintf "[%s for %s in %s]" (to_string e) x.bound (to_string xs)
    | EMap (_, l) ->
        sprintf "{%s}"
          (String.concat ", "
             (List.map
                (fun (k, e) -> sprintf "%s : %s" (to_string k) (to_string e))
                l))
    | ESet l ->
        sprintf "sp.set([%s])" (String.concat ", " (List.map to_string l))
    | EContract {arg_type; entrypoint; address} ->
        sprintf "sp.contract(%s, %s%s)"
          (type_to_string ~options:Options.string arg_type)
          (to_string address)
          (Option.cata "" (fun ep -> sprintf ", entrypoint='%s'" ep) entrypoint)
    | ETuple es ->
        let es = List.map to_string es in
        sprintf "(%s)" (String.concat ", " es)
    | ESlice {offset; length; buffer} ->
        prot
          (sprintf "sp.slice(%s, %s, %s)" (to_string buffer) (to_string offset)
             (to_string length))
    | EMake_signature {secret_key; message; message_format} ->
        sprintf
          "sp.make_signature(secret_key = %s, message = %s, message_format = \
           %s)"
          (to_string secret_key) (to_string message)
          (match message_format with
          | `Hex -> "'Hex'"
          | `Raw -> "'Raw'")
    | EMichelson (michelson, exprs) ->
        sprintf "sp.michelson(%S)(%s)" michelson.name
          (String.concat ", " (List.map to_string exprs))
    | ETransfer {arg; amount; destination} ->
        sprintf "sp.transfer_operation(%s, %s, %s)" (to_string arg)
          (to_string amount) (to_string destination)
    | ESapling_verify_update {transaction; state} ->
        sprintf "sp.sapling_verify_update(%s, %s)" (to_string state)
          (to_string transaction)
    | EIf (cond, a, b) ->
        sprintf "sp.eif(%s, %s, %s)" (to_string cond) (to_string a)
          (to_string b)
    | EIs_failing x -> mono "is_failing" x
    | ECatch_exception (t, x) ->
        std "catch_exception"
          (pp_exprs [x] @ [type_to_string ~options:Options.string t])

  and infix ~options op x y =
    let x = expr_to_string ~options ~protect:() x in
    let y = expr_to_string ~options ~protect:() y in
    sprintf "%s %s %s" x op y

  and pp_assign ~options ~indent buf s t =
    let expr_to_string = expr_to_string ~options in
    match t.e with
    | EPrim2 (ECons, u, s') when equal_expr_modulo_line_nos s s' -> (
        match (s.e, u.e) with
        | ( EVar {name = {bound = "__operations__"}}
          , ETransfer
              {
                destination =
                  {
                    e =
                      EOpen_variant
                        ( "Some"
                        , {e = EContract {arg_type = F (T0 Unit); address}}
                        , None )
                  }
              ; amount
              } ) ->
            bprintf buf "%ssp.send(%s, %s)" indent (expr_to_string address)
              (expr_to_string amount)
        | ( EVar {name = {bound = "__operations__"}}
          , ETransfer {arg; amount; destination} ) ->
            bprintf buf "%ssp.transfer(%s, %s, %s)" indent (expr_to_string arg)
              (expr_to_string amount)
              (expr_to_string destination)
        | EVar {name = {bound = "__operations__"}}, EPrim1 (ESet_delegate, e) ->
            bprintf buf "%ssp.set_delegate(%s)" indent (expr_to_string e)
        | ( EVar {name = {bound = "__operations__"}}
          , EPrim1 (EEmit (tag, with_type), x) ) ->
            let tag =
              match tag with
              | None -> ""
              | Some tag -> sprintf ", tag = %S" tag
            in
            let with_type = if with_type then "" else ", with_type = false" in
            bprintf buf "%ssp.emit(%s%s%s)" indent (expr_to_string x) tag
              with_type
        | _ ->
            bprintf buf "%s%s.push(%s)" indent (expr_to_string s)
              (expr_to_string u))
    | EPrim2 (((EAdd | ESub | EDiv) as op), s', u)
      when equal_expr_modulo_line_nos s s' ->
        bprintf buf "%s%s %s= %s" indent (expr_to_string s) (string_of_binOp op)
          (expr_to_string u)
    | EPrim2 (EMul_homo, s', u) when equal_expr_modulo_line_nos s s' ->
        bprintf buf "%s%s %s= %s" indent (expr_to_string s) "*"
          (expr_to_string u)
    | _ -> bprintf buf "%s%s = %s" indent (expr_to_string s) (expr_to_string t)

  and pp_command ?(indent = "") ~(options : Options.t) =
    let expr_to_string = expr_to_string ~options in
    let shiftIdent = if options.html then "&nbsp;&nbsp;" else "  " in
    let newline = if options.html then "\n<br>" else "\n" in
    let rec pp ~indent buf c =
      let pp_exprs arguments = List.map expr_to_string arguments in
      let std name arguments =
        bprintf buf "%ssp.%s(%s)" indent name (String.concat ", " arguments)
      in
      let mono name s = std name (pp_exprs [s]) in
      match c.c with
      | CCall_init (name, _args, _) ->
          bprintf buf "%s.__init__(...)" (qual_ident name)
      | CNever message -> mono "never" message
      | CFailwith message -> mono "failwith" message
      | CIf (c, t, e) -> (
          let (then_format, else_format) : _ format * _ format =
            ("%ssp.if %s:%s%a", "%s%ssp.else:%s%a")
          in
          bprintf buf then_format indent (expr_to_string c) newline
            (pp ~indent:(indent ^ shiftIdent))
            t;
          match e.c with
          | CResult {e = ELiteral Literal.Unit} -> ()
          | CBlock [] -> ()
          | _ ->
              bprintf buf else_format newline indent newline
                (pp ~indent:(indent ^ shiftIdent))
                e)
      | CMatch (scrutinee, [(constructor, param, c)]) ->
          bprintf buf "%swith %s.match('%s') as %s:%s%a" indent
            (expr_to_string ~protect:() scrutinee)
            constructor param.bound newline
            (pp ~indent:(indent ^ shiftIdent))
            c
      | CMatch (scrutinee, cases) ->
          bprintf buf "%swith %s.match_cases() as arg:%s%a" indent
            (expr_to_string ~protect:() scrutinee)
            newline
            (fun buf ->
              List.iter (fun (constructor, (param : bound), c) ->
                  bprintf buf "%swith arg.match('%s') as %s:%s%a%s"
                    (indent ^ shiftIdent) constructor param.bound newline
                    (pp ~indent:(indent ^ shiftIdent ^ shiftIdent))
                    c newline))
            cases
      | CModify_product (_, Some _, Pattern_single _, _) -> assert false
      | CModify_product (s, None, Pattern_single x, c) ->
          let indent' = indent ^ shiftIdent in
          let f = bprintf buf "%swith sp.modify(%s, %S) as %S:%s%a" in
          f indent (expr_to_string s) x.bound x.bound newline
            (pp ~indent:indent') c
      | CModify_product (_, Some _, Pattern_tuple _, _) -> assert false
      | CModify_product (s, None, Pattern_tuple ls, c) ->
          let ls = List.map (fun (x : bound) -> x.bound) ls in
          let indent' = indent ^ shiftIdent in
          let f = bprintf buf "%swith sp.match_tuple(%s, %a) as %a:%s%a" in
          f indent (expr_to_string s) ppS ls pps ls newline (pp ~indent:indent')
            c
      | CModify_product (_, _, Pattern_record _, _) -> assert false
      | CModify_record (s, x, c) ->
          let indent' = indent ^ shiftIdent in
          let f = bprintf buf "%swith sp.match_record(%s, %S) as %s:%s%a" in
          f indent (expr_to_string s) x.bound x.bound newline
            (pp ~indent:indent') c
      | CMatch_list matcher ->
          bprintf buf "%swith sp.match_cons(%s) as %s:%s%a%s%selse:%s%a" indent
            (expr_to_string ~protect:() matcher.expr)
            matcher.id.bound newline
            (pp ~indent:(indent ^ shiftIdent))
            matcher.clause_cons newline indent newline
            (pp ~indent:(indent ^ shiftIdent))
            matcher.clause_nil
      | CExpr e -> bprintf buf "%s%s%s" indent (expr_to_string e) newline
      | CAssign {lhs; rhs} -> pp_binding ~options ~indent buf (lhs, rhs)
      | CBlock [] -> bprintf buf "%spass" indent
      | CBlock cs ->
          let cs, cn = List.unsnoc cs in
          let f c =
            pp_command ~options ~indent buf c;
            bprintf buf "%s" newline
          in
          List.iter f cs;
          pp_command ~options ~indent buf cn
      | CResult {e = ELiteral Literal.Unit} -> bprintf buf "%spass" indent
      | CResult r -> bprintf buf "%ssp.result(%s)" indent (expr_to_string r)
      | CFor (var, e, c) ->
          bprintf buf "%ssp.for %s in %s:%s%a" indent
            (variable_to_string ~options var.bound "variable")
            (expr_to_string e) newline
            (pp ~indent:(indent ^ shiftIdent))
            c
      | CWhile (e, c) ->
          bprintf buf "%ssp.while %s:%s%a" indent (expr_to_string e) newline
            (pp ~indent:(indent ^ shiftIdent))
            c
      | CDel_item (expr, item) ->
          bprintf buf "%s%s %s[%s]" indent
            (if options.html then "<span class='keyword'>del</span>" else "del")
            (expr_to_string expr) (expr_to_string item)
      | CUpdate_set (expr, element, add) ->
          bprintf buf "%s%s.%s(%s)" indent (expr_to_string expr)
            (if add then "add" else "remove")
            (expr_to_string element)
      | CVerify (e, None) -> bprintf buf "%sassert %s" indent (expr_to_string e)
      | CVerify (e, Some msg) ->
          bprintf buf "%sassert %s, %s" indent (expr_to_string e)
            (expr_to_string msg)
      | CComment s -> bprintf buf "%s# %s" indent s
      | CSet_type (e, t) ->
          std "set_type"
            (pp_exprs [e] @ [type_to_string ~options:Options.string t])
      | CSet_result_type (c, t) ->
          bprintf buf "%swith sp.set_result_type(%s):%s" indent
            (type_to_string ~options:Options.string t)
            newline;
          pp ~indent:(indent ^ shiftIdent) buf c
      | CTrace e -> mono "trace" e
    in
    pp ~indent

  and pp_binding ~options ?(indent = "") buf (lhs, rhs) =
    pp_assign ~options ~indent buf lhs rhs

  and command_to_string ~(options : Options.t) ?indent c =
    let buf = Buffer.create 64 in
    pp_command ?indent ~options buf c;
    Buffer.contents buf

  and qual_ident {qi_module; qi_ident} =
    Option.cata "" (fun s -> s ^ ".") qi_module ^ qi_ident

  let texpr_to_string ~(options : Options.t) ?protect x =
    expr_to_string ~options ?protect (erase_types_expr x)

  let tcommand_to_string ~(options : Options.t) ?indent x =
    command_to_string ~options ?indent (erase_types_command x)

  let binding_to_string ~options ?indent c =
    let buf = Buffer.create 64 in
    pp_binding ~options ?indent buf c;
    Buffer.contents buf

  let tbinding_to_string ~options ?indent (lhs, rhs) =
    binding_to_string ~options ?indent
      (erase_types_expr lhs, erase_types_expr rhs)

  let rec value_to_string v =
    let options = Options.string in
    let to_string = value_to_string in
    let literal_to_string = literal_to_string ~options in
    match v.v with
    | Literal x -> literal_to_string x
    | Contract {address; entrypoint = None} -> sprintf "contract(%s)" address
    | Contract {address; entrypoint = Some ep} ->
        sprintf "contract(%s, %s)" address ep
    | Record xs ->
        let f (n, x) = sprintf "%s = %s" n (to_string x) in
        sprintf "record(%s)" (String.concat ", " (List.map f xs))
    | Variant (name, v) -> sprintf "variant.%s(%s)" name (to_string v)
    | List xs -> sprintf "[%s]" (String.concat ", " (List.map to_string xs))
    | Ticket (ticketer, content, amount) ->
        Format.asprintf "ticket(%S, %s, %a)" ticketer (to_string content)
          Bigint.pp amount
    | Set xs -> sprintf "set([%s])" (String.concat ", " (List.map to_string xs))
    | Map map ->
        let f (k, v) = sprintf "%s : %s" (to_string k) (to_string v) in
        sprintf "{%s}" (String.concat ", " (List.map f map))
    | Tuple vs ->
        let vs = List.map to_string vs in
        sprintf "(%s)" (String.concat ", " vs)
    | Closure _ -> "<closure>"
    | Operation operation -> (
        let opt f = Option.cata "None" (fun x -> sprintf "Some(%s)" (f x)) in
        match operation with
        | Transfer {params; destination = {address; entrypoint; type_}; amount}
          ->
            sprintf "transfer(%s, %s, %s, %s)" (to_string params)
              (literal_to_string (Literal.address ?entrypoint address))
              (type_to_string ~options type_)
              (literal_to_string (Literal.mutez amount))
        | SetDelegate d ->
            sprintf "set_delegate(%s)"
              (opt (fun x -> literal_to_string (Literal.key_hash x)) d)
        | CreateContract {id; baker; storage} ->
            let baker =
              opt (fun x -> literal_to_string (Literal.key_hash x)) baker
            in
            let storage = opt to_string storage in
            sprintf "create_contract(%s, %s, %s)"
              (Aux.address_of_contract_id ~html:false id None)
              baker storage
        | Event (tag, _type, v) ->
            let tag =
              match tag with
              | None -> ""
              | Some tag -> sprintf "tag: %S" tag
            in
            sprintf "Event(%s)\n%s" tag (to_string v))

  let rec tvalue_to_string ~(options : Options.t) ?(deep = false)
      ?(noEmptyList = false) v =
    let to_string = tvalue_to_string ~options in
    match v.tv with
    | Literal x -> literal_to_string ~deep ~options x
    | Contract {address; entrypoint} ->
        let t =
          match Type.unF v.t with
          | T1 (Contract, t) -> t
          | _ -> assert false
        in
        Printf.sprintf "sp.contract(%s, %s).open_some()"
          (type_to_string ~options:Options.string t)
          (literal_to_string ~deep ~options
             (Literal.address ?entrypoint address))
    | Record l ->
        let layout =
          match Type.unF v.t with
          | Record {layout = Value layout} -> layout
          | _ -> assert false
        in
        let fields =
          let f Layout.{source; target} = (source, target) in
          List.map f (Binary_tree.to_list layout)
        in
        if options.html
        then
          render_record_list
            ( List.map snd fields
            , [List.map (fun (source, _) -> List.assoc source l) fields] )
            (fun s -> to_string ~noEmptyList:true s)
        else
          sprintf "sp.record(%s)"
            (String.concat ", "
               (List.map
                  (fun (n, _) ->
                    let x = List.assoc n l in
                    sprintf "%s = %s" n (to_string x))
                  fields))
    | Variant (name, v) -> variant_to_string ~options name v
    | List [] when noEmptyList -> ""
    | List l when deep && not (is_record_list l) ->
        if options.html
        then
          sprintf "[%s]"
            (String.concat ", " (List.map (to_string ~deep:true) l))
        else sprintf "[%s]" (String.concat ", " (List.map to_string l))
    | List l when options.html ->
        let l =
          match l with
          | {tv = Record r} :: _ as l ->
              ( List.map fst r
              , List.map
                  (function
                    | {tv = Record r} -> List.map snd r
                    | _ -> assert false)
                  l )
          | _ -> ([""], List.map (fun x -> [x]) l)
        in
        render_record_list l (fun s -> to_string ~noEmptyList:true ~deep:true s)
    | List l -> sprintf "[%s]" (String.concat ", " (List.map to_string l))
    | Ticket (ticketer, content, amount) ->
        if options.html
        then
          render_record_list
            ( ["Ticketer"; "Content"; "Amount"]
            , [
                [
                  sprintf "<span class='address'>%s</span>" ticketer
                ; to_string ~noEmptyList:true ~deep:true content
                ; Bigint.string_of_big_int amount
                ]
              ] )
            (fun s -> s)
        else
          Format.asprintf "sp.ticket(sp.address('%s'), %s, %a)" ticketer
            (to_string content) Bigint.pp amount
    | Set [] when noEmptyList -> ""
    | Set set ->
        if options.html
        then
          render_record_list
            ([""], List.map (fun x -> [x]) set)
            (fun s -> to_string ~noEmptyList:true ~deep:true s)
        else
          sprintf "sp.set([%s])" (String.concat ", " (List.map to_string set))
    | Map map -> (
        match Type.unF v.t with
        | T2 ((Map | Big_map), _, tvalue) ->
            if options.html
            then
              match is_matrix v with
              | Some (columns, l) ->
                  render_record_list (columns, l) (fun s ->
                      to_string ~noEmptyList:true s)
              | None -> (
                  let result =
                    match Type.unF tvalue with
                    | Record {row} ->
                        Some
                          (render_record_list
                             ( "Key" :: List.map fst row
                             , List.map (fun (x, y) -> x :: unrec y) map )
                             (fun s -> to_string ~noEmptyList:true s))
                    | _ -> None
                  in
                  match result with
                  | None ->
                      render_record_list
                        (["Key"; "Value"], List.map (fun (x, y) -> [x; y]) map)
                        (fun s -> to_string ~noEmptyList:true s)
                  | Some t -> t)
            else
              sprintf "{%s}"
                (String.concat ", "
                   (List.map
                      (fun (k, v) ->
                        sprintf "%s : %s" (to_string k) (to_string v))
                      map))
        | _ -> assert false)
    | Tuple vs ->
        if options.html
        then
          render_record_list ([], [vs]) (fun s -> to_string ~noEmptyList:true s)
        else
          let vs = List.map (to_string ~noEmptyList:true) vs in
          sprintf "(%s)" (String.concat ", " vs)
    | Closure ({lambda_var; body = {c = CResult r}}, _) ->
        sprintf "sp.build_lambda(lambda %s: %s)" lambda_var.bound
          (texpr_to_string ~options r)
    | Closure _ -> "lambda"
    | Operation operation -> operation_to_string ~options operation

  and variant_to_string ~(options : Options.t) ?(deep = false)
      ?(noEmptyList = false) name v =
    if options.html
    then
      sprintf
        "<div class='subtype'><select class='selection'><option \
         value='%s'>%s</option></select>%s</div>"
        name
        (String.capitalize_ascii name)
        (tvalue_to_string v ~deep ~noEmptyList ~options)
    else
      match (name, v) with
      | "None", {tv = Literal Unit} -> "sp.none"
      | name, {tv = Literal Unit} -> name
      | "Some", v -> sprintf "sp.some(%s)" (tvalue_to_string ~options v)
      | x, v -> sprintf "%s(%s)" x (tvalue_to_string ~options v)

  and operation_to_string ~(options : Options.t) operation =
    if options.html
    then
      match operation with
      | Transfer {params; destination = {address; entrypoint; type_}; amount} ->
          let c =
            Printf.sprintf "sp.contract(%s, %s).open_some()"
              (type_to_string ~options:Options.string type_)
              (literal_to_string ~deep:false ~options
                 (Literal.address ?entrypoint address))
          in
          sprintf "<div class='operation'>Transfer %s to %s<br>%s</div>"
            (literal_to_string ~options (Literal.mutez amount))
            c
            (tvalue_to_string ~options params)
      | SetDelegate None -> "<div class='operation'>Remove Delegate</div>"
      | SetDelegate (Some d) ->
          sprintf "<div class='operation'>Set Delegate(%s)</div>"
            (literal_to_string ~options (Literal.key_hash d))
      | CreateContract {id; baker; storage} ->
          let baker =
            match baker with
            | None -> "sp.none"
            | Some baker ->
                sprintf "sp.some(%s)"
                  (literal_to_string ~options:Options.string
                     (Literal.key_hash baker))
          in
          sprintf
            "<div class='operation'>Create Contract(address: %s, baker: \
             %s)%s</div>"
            (Aux.address_of_contract_id ~html:options.html id None)
            baker
            (Option.cata "" (tvalue_to_string ~options) storage)
      | Event (tag, _type, v) ->
          let tag =
            match tag with
            | None -> ""
            | Some tag -> sprintf "tag: %S" tag
          in
          sprintf "<div class='operation'>Event(%s)<br>%s</div>" tag
            (tvalue_to_string ~options v)
    else
      match operation with
      | Transfer {params; destination = {address; entrypoint; type_}; amount} ->
          let c =
            Printf.sprintf "sp.contract(%s, %s).open_some()"
              (type_to_string ~options:Options.string type_)
              (literal_to_string ~deep:false ~options
                 (Literal.address ?entrypoint address))
          in
          sprintf "Transfer\n     params: %s\n     amount: %s\n     to:     %s"
            (tvalue_to_string ~options params)
            (literal_to_string ~options (Literal.mutez amount))
            c
      | SetDelegate None -> "Remove Delegate"
      | SetDelegate (Some d) ->
          sprintf "Set Delegate(%s)"
            (literal_to_string ~options (Literal.key_hash d))
      | CreateContract {id; baker; storage} ->
          let baker =
            match baker with
            | None -> "sp.none"
            | Some baker ->
                sprintf "sp.some(%s)"
                  (literal_to_string ~options:Options.string
                     (Literal.key_hash baker))
          in
          sprintf "Create Contract(address: %s, baker: %s)%s"
            (Aux.address_of_contract_id ~html:options.html id None)
            baker
            (Option.cata "" (tvalue_to_string ~options) storage)
      | Event (tag, _type, v) ->
          let tag =
            match tag with
            | None -> ""
            | Some tag -> sprintf "tag: %S" tag
          in
          sprintf "Event(%s)\n%s" tag (tvalue_to_string ~options v)

  let render_storage_tvalue options data =
    Printf.sprintf "<div id='storageDivInternal'>%s</div>"
      (tvalue_to_string ~options data)

  let ppType html t =
    if html
    then
      sprintf "<span class='type'>%s</span>"
        (type_to_string ~options:Options.html t)
    else type_to_string ~options:Options.string t

  let of_file (s, l) =
    match l with
    | -1 -> ""
    | _ ->
        let s =
          let l = sprintf "line %i" l in
          match s with
          | "" -> l
          | s -> s ^ ", " ^ l
        in
        sprintf " (%s)" s

  let ppExpr html (e : Checked.expr) =
    if html
    then
      let pp =
        match e.line_no with
        | Some (_, l) ->
            sprintf
              "<button class='text-button' onClick='showLine(%i)'>%s</button>" l
        | None -> id
      in
      pp
        (sprintf "(%s : %s)"
           (texpr_to_string ~options:Options.string e)
           (ppType html e.et))
    else
      sprintf "(%s : %s)"
        (texpr_to_string ~options:Options.string e)
        (ppType html e.et)

  let ppExpr_unchecked html (e : expr) =
    if html
    then
      let pp =
        match e.line_no with
        | Some (_, l) ->
            sprintf
              "<button class='text-button' onClick='showLine(%i)'>%s</button>" l
        | None -> id
      in
      pp (expr_to_string ~options:Options.string e)
    else
      sprintf "(%s)%s"
        (expr_to_string ~options:Options.string e)
        (Option.cata "" of_file e.line_no)

  let rec simplify acc (x : smart_except list) =
    match x with
    | [] -> acc
    | (( `Literal _
       | `Value _
       | `UValue _
       | `Expr _
       | `Exprs _
       | `UExpr _
       | `Text _
       | `Type _
       | `With_loc _ ) as x)
      :: rest -> simplify (x :: acc) rest
    | `Br :: rest ->
        let acc =
          match acc with
          | `Br :: _ -> acc
          | _ -> `Br :: acc
        in
        simplify acc rest
    | `Rec l :: rest -> simplify acc (l @ rest)
    | `Line i :: rest -> (
        match i with
        | None -> simplify acc rest
        | Some i -> (
            match acc with
            | `Line (Some j) :: `Br :: _ when i = j -> simplify acc rest
            | `Br :: _ -> simplify (`Line (Some i) :: acc) rest
            | _ -> simplify (`Line (Some i) :: `Br :: acc) rest))

  let simplify x = List.rev (simplify [] x)

  let rec flatten acc = function
    | `Br :: rest -> List.rev acc :: flatten [] rest
    | `Rec l :: rest -> flatten acc (l @ rest)
    | x :: rest -> flatten (x :: acc) rest
    | [] -> if acc = [] then [] else [List.rev acc]

  let rec pp_smart_except1 html = function
    | `Literal literal -> literal_to_sp_string ~html literal
    | `UValue value -> value_to_string value
    | `Value value -> tvalue_to_string ~options:Options.string value
    | `Expr expr -> ppExpr html expr
    | `Exprs exprs -> String.concat ", " (List.map (ppExpr html) exprs)
    | `UExpr expr -> ppExpr_unchecked html expr
    | `Line line_no ->
        if html
        then
          let line_no, i =
            match line_no with
            | None -> (("", -1), -1)
            | Some (s, i) -> ((s, i), i)
          in
          sprintf
            "<button class='text-button' onClick='showLine(%i)'>%s</button>" i
            (of_file line_no)
        else Option.cata "" of_file line_no
    | `Text s -> s
    | `Type t -> ppType html t
    | `With_loc (ln, e) when html -> pp_smart_except true (e @ [`Line ln])
    | `With_loc (ln, e) ->
        let e = pp_smart_except false e in
        let f (path, l) =
          let l = if l = -1 then "" else string_of_int l in
          let c = "" in
          sprintf "%s:%s:%s" path l c
        in
        let locs = Option.cata "" f ln in
        sprintf "%s: %s" locs e
    | `Rec _ | `Br -> assert false

  and pp_smart_except html l =
    let l = flatten [] (simplify l) in
    let f xs =
      let xs = List.map (pp_smart_except1 html) xs in
      String.concat "" xs
    in
    String.concat (if html then "<br>" else "\n") (List.map f l)

  let wrong_condition_string x =
    Printf.sprintf "WrongCondition: %s"
      (texpr_to_string ~options:Options.string x)

  let error_to_string operation =
    let open Execution in
    match operation with
    | Exec_failure (_, message) -> pp_smart_except false message

  let exception_to_smart_except = function
    | Failure s -> [`Text s]
    | SmartExcept l -> l
    | ExecFailure (_value, message) -> message
    | Yojson.Safe.Util.Type_error (msg, _t) ->
        [`Text ("Yojson exception - " ^ msg)]
    | e -> [`Text (Printexc.to_string e)]

  let exception_to_string html exc =
    pp_smart_except html (exception_to_smart_except exc)

  let type_to_string ?(options = Options.string) = type_to_string ~options

  let expr_to_string ?(options = Options.string) = expr_to_string ~options

  let texpr_to_string ?(options = Options.string) = texpr_to_string ~options

  let command_to_string ?(options = Options.string) = command_to_string ~options

  let tcommand_to_string ?(options = Options.string) =
    tcommand_to_string ~options

  let tvalue_to_string ?(options = Options.string) = tvalue_to_string ~options

  let variant_to_string ?(options = Options.string) = variant_to_string ~options

  let operation_to_string ?(options = Options.string) =
    operation_to_string ~options
end

let get ~config : (module Printer) =
  (module Build_printer (struct
    let config = config
  end))
