open Basics

val mutate_loaded_scenarios :
     config:Config.t
  -> show_paths:bool
  -> (Import.loaded_scenario * int) list
  -> (string * Checked.contract_def * Import.loaded_scenario list) list
