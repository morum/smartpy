(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Basics
open Unchecked

type t = command

val ifte : line_no:line_no -> expr -> t -> t -> t

val ifteSome : line_no:line_no -> Expr.t -> t -> t -> t

val mk_match : line_no:line_no -> expr -> (string * string * t) list -> t

val for_loop : line_no:line_no -> string -> expr -> t -> t

val while_loop : line_no:line_no -> expr -> t -> t

val block : line_no:line_no -> command list -> t

val mk_match_cons : line_no:line_no -> expr -> string -> t -> t -> t

val mk_modify_product :
  line_no:line_no -> expr -> bound option -> pattern -> t -> t

val mk_modify_record : line_no:line_no -> expr -> string -> t -> t

val sp_failwith : line_no:line_no -> expr -> t

val verify : line_no:line_no -> expr -> expr option -> t

val never : line_no:line_no -> expr -> t

val del_item : line_no:line_no -> expr -> expr -> t

val update_set : line_no:line_no -> expr -> expr -> bool -> t

val result : line_no:line_no -> expr -> t

val set_result_type : line_no:line_no -> t -> Type.t -> t

val set_type : line_no:line_no -> expr -> Type.t -> t

val trace : line_no:line_no -> expr -> t

val comment : line_no:line_no -> string -> t

val expr : line_no:line_no -> expr -> command

val assign : line_no:line_no -> expr -> expr -> command
