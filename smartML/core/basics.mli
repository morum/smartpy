(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Utils

include module type of Ids

module Literal = Literal

module type Deco = sig
  type 'a deco [@@deriving eq, ord, show, map, fold, sexp]
end

module Deco_unchecked : sig
  type 'a deco = U [@@deriving eq, ord, show, map, fold, sexp]
end

module Deco_checked : sig
  type 'a deco = 'a [@@deriving eq, ord, show, map, fold, sexp]
end

type 'expr derived_var =
  | V_simple of {is_mutable : bool}
  | V_scenario
  | V_exploded
  | V_constant
  | V_match_cons
  | V_defined of 'expr
[@@deriving eq, ord, show, fold, map]

type 'v record_f = (string * 'v) list [@@deriving eq, ord, show]

type micheline =
  | Int of string
  | String of string
  | Bytes of string
  | Primitive of {
        name : string
      ; annotations : string list
      ; arguments : micheline list
    }
  | Sequence of micheline list
[@@deriving eq, show, ord]

type 't inline_michelson = {
    name : string
  ; parsed : micheline
  ; typesIn : 't list
  ; typesOut : 't list
}
[@@deriving eq, show, ord]

type line_no = (string * int) option [@@deriving eq, ord, show, sexp]

type view_kind =
  | Onchain
  | Offchain
[@@deriving eq, ord, show, map, fold]

type address =
  | Real of string
  | Local of contract_id
[@@deriving eq, ord, show]

module Meta : sig
  type 'a t =
    | List of 'a t list
    | Map of ('a * 'a t) list
    | Other of 'a
    | View of string
  [@@deriving eq, ord, show, map, fold]
end

type 'expr call_f =
  | Call_positional of 'expr list
  | Call_keyword of (string * 'expr) list
[@@deriving eq, ord, map, fold]

type 'type_ lcontract = {
    address : string
  ; entrypoint : string option
  ; type_ : 'type_
}
[@@deriving eq, ord, show, map, fold]

type 'type_ prim0 =
  | ECst_contract of 'type_ lcontract
  | EAccount_of_seed of {seed : string}
  | EContract_address of contract_id * string option
  | EContract_balance of contract_id
  | EContract_baker of contract_id
  | EContract_data of contract_id
  | EContract_private of contract_id
  | EContract_typed of contract_id * string option
  | EConstant of string * 'type_
[@@deriving eq, ord, show, map, fold]

type 'type_ prim1 =
  | ENot
  | EInvert
  | EConcat_list
  | EAddress
  | EFst
  | ESnd
  | EImplicit_account
  | EList_elements of bool
  | EList_items of bool
  | EList_keys of bool
  | EList_rev
  | EList_values of bool
  | ENeg
  | EPack
  | ESet_delegate
  | ESign
  | ESize
  | ESum
  | ETo_nat
  | ETo_int
  | ETo_bytes
  | EUnpack of 'type_
  | EType_annotation of 'type_
  | EIs_variant of string
  | ERead_ticket
  | EJoin_tickets
  | EPairing_check
  | EVoting_power
  | EConvert
  | EStatic_view of static_id * string (* view name *)
  | EEmit of string option * bool
  | EInvert_bytes
[@@deriving eq, ord, show, map, fold]

type 'type_ prim2 =
  | EAdd
  | EAdd_seconds
  | EAnd
  | EApply_lambda
  | EAnd_bytes
  | EAnd_infix
  | EOr_bytes
  | EOr_infix
  | ECons
  | EContains
  | EDiv
  | EEDiv
  | EEq
  | EGe
  | EGet_opt
  | EGt
  | ELe
  | ELshift_bytes
  | ELshift_infix
  | ELt
  | EMax
  | EMin
  | EMod
  | EMul_homo
  | ENeq
  | EOr
  | ERshift_bytes
  | ERshift_infix
  | ESplit_ticket
  | ESub
  | ETicket
  | EView of string (* view name *) * 'type_ (* return type *)
  | EXor_bytes
  | EXor_infix
[@@deriving eq, ord, show, map, fold]

type prim3 =
  | ESplit_tokens
  | ERange
  | EUpdate_map
  | EGet_and_update
  | ETest_ticket
[@@deriving eq, ord, show, map, fold]

type qual_ident = {
    qi_module : string option
  ; qi_ident : string
}
[@@deriving eq, ord, show, sexp]

module Qual_ident : Data.S with type t = qual_ident

val string_of_line_no : line_no -> string

val head_line_no : line_no -> int

type effects = {
    with_storage : Type.access option
  ; with_operations : bool
}
[@@deriving show]

type 'address account_or_address =
  | Account of Primitives.account
  | Address of 'address
[@@deriving show, map]

type 'expr action_context = {
    source : 'expr account_or_address option
  ; sender : 'expr account_or_address option
  ; chain_id : 'expr option
  ; time : 'expr option
  ; level : 'expr option
  ; voting_powers : 'expr option
}
[@@deriving show]

module Syntax : functor (E : Deco) -> sig
  open E

  type bound = {
      bound : string
    ; index : int deco
  }
  [@@deriving eq, ord, show {with_path = false}]

  type 'command fun_def_f = {
      name : bound
    ; params : string list
    ; effects : effects
    ; body : 'command
    ; line_no : line_no
  }
  [@@deriving eq, ord, show, map, fold, sexp]

  type 'type_ entrypoint_derived = {
      parameter : bound
    ; storage : bound
    ; private_ : bound
    ; operations : bound
    ; tparameter_ep : 'type_
  }
  [@@deriving show, fold]

  type 'type_ derived_view = {
      parameter : bound
    ; storage : bound
    ; tparameter : 'type_ option
  }
  [@@deriving eq, ord, show, map, fold]

  type ('command, 'type_) init_f = {
      init_params : (bound * 'type_) list
    ; init_body : 'command
    ; init_storage : bound
    ; init_private : bound
    ; init_supers : (qual_ident * ('command, 'type_) init_f option) list
    ; line_no : line_no
  }
  [@@deriving eq, ord, show, map, fold]

  type 'type_ lambda_derived = {
      tParams : 'type_
    ; tResult : 'type_
    ; with_operations : bound option
    ; with_storage : (Type.access * 'type_ * bound) option
  }
  [@@deriving show, fold]

  type 'expr lhs =
    | Var_def of bound
    | Assign of 'expr
    | Product of pattern

  and ('command, 'type_) call_init_derived = {
      storage : bound
    ; private_ : bound
    ; super : ('command, 'type_) init_f option
  }

  and ('command, 'type_) entrypoint_f = {
      channel : string
    ; tparameter_ep : [ `Absent | `Present | `Annotated of Type.t ]
    ; check_no_incoming_transfer : bool option
    ; line_no : line_no
    ; body : 'command
    ; derived : 'type_ entrypoint_derived deco
  }

  and ('command, 'type_) view_f = {
      view_kind : view_kind
    ; view_name : string
    ; has_param : bool
    ; pure : bool
    ; body : 'command
    ; doc : string
    ; derived : 'type_ derived_view deco
  }

  and ('expr, 'command, 'type_) contract_derived = {
      tparameter : 'type_
    ; tstorage : 'type_
    ; tprivate : 'type_
    ; init : ('command, 'type_) init_f option
    ; config : Config.t
    ; entrypoints : ('command, 'type_) entrypoint_f list
    ; private_variables : (string * 'expr) list
    ; views : ('command, 'type_) view_f list
    ; initial_balance : 'expr option
    ; initial_metadata : (string * 'expr Meta.t) list
    ; unknown_parts : string option
    ; template_id : static_id option
    ; flags : Config.flag list
    ; line_no : line_no
  }

  and ('command, 'type_) lambda_f = {
      lambda_var : bound
    ; body : 'command
    ; with_storage : (Type.access * 'type_ option) option
    ; with_operations : bool
    ; recursive : bound option
    ; derived : 'type_ lambda_derived deco
  }

  and record_field_binding = {
      var : bound
    ; field : string
  }

  and pattern =
    | Pattern_single of bound
    | Pattern_tuple of bound list
    | Pattern_record of record_field_binding list

  and ('expr, 'command, 'type_) expr_f =
    | EVar of {
          name : bound
        ; derived : 'expr derived_var deco
      }
    | EPrivate of string
    | ELiteral of Literal.t
    | EMPrim0 of Michelson_base.Type.mtype Michelson_base.Primitive.prim0
    | EMPrim1 of
        Michelson_base.Type.mtype Michelson_base.Primitive.prim1 * 'expr
    | EMPrim1_fail of Michelson_base.Primitive.prim1_fail * 'expr
    | EMPrim2 of
        Michelson_base.Type.mtype Michelson_base.Primitive.prim2 * 'expr * 'expr
    | EMPrim3 of Michelson_base.Primitive.prim3 * 'expr * 'expr * 'expr
    | EPrim0 of 'type_ prim0
    | EPrim1 of 'type_ prim1 * 'expr
    | EPrim2 of 'type_ prim2 * 'expr * 'expr
    | EPrim3 of prim3 * 'expr * 'expr * 'expr
    | EAttr of string * 'expr
    | EVariant of string * 'expr
    | ECall of 'expr * 'expr option
    | EOpen_variant of string * 'expr * 'expr option
    | EItem of {
          items : 'expr
        ; key : 'expr
        ; default_value : 'expr option
        ; missing_message : 'expr option
      }
    | ETuple of 'expr list
    | ERecord of (string * 'expr) list
    | EList of 'expr list
    | EList_comprehension of 'expr * bound * 'expr
    | EMap of bool * ('expr * 'expr) list
    | ESet of 'expr list
    | ESapling_verify_update of {
          state : 'expr
        ; transaction : 'expr
      }
    | EMichelson of 'type_ inline_michelson * 'expr list
    | EMap_function of {
          f : 'expr
        ; l : 'expr
      }
    | ELambda of ('command, 'type_) lambda_f
    | ECreate_contract of {
          push : bool
        ; template_ref : qual_ident
        ; baker : 'expr
        ; balance : 'expr
        ; storage : 'expr
        ; derived : ('expr, 'command, 'type_) contract_def_f deco
      }
    | EContract of {
          entrypoint : string option
        ; arg_type : 'type_
        ; address : 'expr
      }
    | ESlice of {
          offset : 'expr (* nat *)
        ; length : 'expr (* nat *)
        ; buffer : 'expr
      }
    | EMake_signature of {
          secret_key : 'expr
        ; message : 'expr
        ; message_format : [ `Raw | `Hex ]
      }
    | ETransfer of {
          arg : 'expr
        ; amount : 'expr
        ; destination : 'expr
      }
    | EIf of 'expr * 'expr * 'expr
    | EIs_failing of 'expr
    | ECatch_exception of 'type_ * 'expr

  and ('expr, 'command, 'type_) command_f =
    | CCall_init of
        qual_ident * 'expr call_f * ('command, 'type_) call_init_derived deco
    | CNever of 'expr
    | CFailwith of 'expr
    | CVerify of 'expr * 'expr option (* message *)
    | CIf of 'expr * 'command * 'command
    | CMatch of 'expr * (string * bound * 'command) list
    | CModify_product of 'expr * bound option * pattern * 'command
    | CModify_record of 'expr * bound * 'command
    | CMatch_list of {
          expr : 'expr
        ; id : bound
        ; clause_cons : 'command
        ; clause_nil : 'command
      }
    | CDel_item of 'expr * 'expr
    | CUpdate_set of 'expr * 'expr * bool
    | CBlock of 'command list
    | CExpr of 'expr
    | CAssign of {
          lhs : 'expr
        ; rhs : 'expr
        ; derived : 'expr lhs deco
      }
    | CFor of bound * 'expr * 'command
    | CWhile of 'expr * 'command
    | CResult of 'expr
    | CComment of string
    | CSet_type of 'expr * 'type_
    | CSet_result_type of 'command * 'type_
    | CTrace of 'expr

  and expr = {
      e : (expr, command, Type.t) expr_f
    ; et : Type.t deco
    ; line_no : line_no
  }

  and command = {
      c : (expr, command, Type.t) command_f
    ; ct : Type.t deco
    ; line_no : line_no
  }

  and lambda = (command, Type.t) lambda_f [@@deriving eq, ord, show, map, fold]

  and init = (command, Type.t) init_f [@@deriving eq, ord, show, map, fold]

  and entrypoint = (command, Type.t) entrypoint_f

  and view = (command, Type.t) view_f [@@deriving eq, ord, show]

  and method_kind =
    | Init of init deco
    | Entrypoint of string * entrypoint deco
    | Private of string * Type.access option * bool * expr deco
    | Onchain_view of string * view deco
    | Offchain_view of string * view deco

  and 'command method_f = {
      kind : method_kind
    ; parameters : string list
    ; body : 'command
    ; line_no : line_no
  }

  and method_ = command method_f

  and ('expr, 'command, 'type_) contract_def_f = {
      name : string
    ; parents :
        (qual_ident * ('expr, 'command, 'type_) contract_def_f deco) list
    ; methods : 'command method_f list
    ; derived : ('expr, 'command, 'type_) contract_derived deco
    ; line_no : line_no
  }

  and contract_def = (expr, command, Type.t) contract_def_f [@@deriving show]

  and ('expr, 'command, 'type_) module_elt_f =
    | Expr_def of {
          name : bound
        ; rhs : 'expr
        ; line_no : line_no
      }
    | Fun_def of 'command fun_def_f
    | Type_def of {
          name : string
        ; rhs : 'type_
        ; line_no : line_no
      }
    | Contract_def of ('expr, 'command, 'type_) contract_def_f

  and ('expr, 'command, 'type_) module_f = {
      module_name : string
    ; elts : ('expr, 'command, 'type_) module_elt_f list
    ; defs : 'expr String.Map.t deco
    ; type_defs : Type.t String.Map.t deco
    ; contract_defs : ('expr, 'command, 'type_) contract_def_f String.Map.t deco
    ; mod_line_no : line_no
  }

  and ('expr, 'command, 'type_) instantiate_derived = {
      instantiate_derived_contract : ('expr, 'command, 'type_) contract_def_f
    ; args : 'expr list
  }

  and ('expr, 'command, 'type_) action_f =
    | Add_module of {
          module_ : ('expr, 'command, 'type_) module_f
        ; line_no : line_no
      }
    | Instantiate_contract of {
          id : contract_id
        ; class_ : qual_ident
        ; call : 'expr call_f
        ; balance : 'expr option
        ; derived : ('expr, 'command, 'type_) instantiate_derived deco
        ; line_no : line_no
      }
    | Compute of {
          var : bound
        ; expression : 'expr
        ; context : 'expr action_context
        ; line_no : line_no
      }
    | Message of {
          id : contract_id
        ; valid : 'expr
        ; exception_ : 'expr option
        ; params : 'expr
        ; line_no : line_no
        ; amount : 'expr
        ; context : 'expr action_context
        ; message : string
        ; show : bool
        ; export : bool
      }
    | ScenarioError of {message : string}
    | Html of {
          tag : string
        ; inner : string
        ; line_no : line_no
      }
    | Verify of {
          condition : 'expr
        ; line_no : line_no
      }
    | Show of {
          expression : 'expr
        ; html : bool
        ; stripStrings : bool
        ; compile : bool
        ; line_no : line_no
      }
    | Set_delegate of {
          id : contract_id
        ; line_no : line_no
        ; baker : 'expr
      }
    | DynamicContract of {
          id : dynamic_id
        ; model_id : contract_id
        ; line_no : line_no
      }
    | Add_flag of {
          flag : Config.flag
        ; line_no : line_no
      }
    | Prepare_constant_value of {
          line_no : line_no
        ; var : bound
        ; hash : string option
        ; expression : 'expr
      }
    | Mutation_test of {
          scenarios : (string * int) list
        ; show_paths : bool
        ; line_no : line_no
      }

  and module_elt = (expr, command, Type.t) module_elt_f

  and module_ = (expr, command, Type.t) module_f

  and action = (expr, command, Type.t) action_f [@@deriving show]

  and ('expr, 'command, 'type_) scenario_f = {
      shortname : string
    ; longname : string
    ; actions : (Config.t * ('expr, 'command, 'type_) action_f) list
  }

  and scenario = (expr, command, Type.t) scenario_f
  [@@deriving eq, ord, show, map, fold]
end

module Unchecked : module type of Syntax (Deco_unchecked)

module Checked : module type of Syntax (Deco_checked)

val mk_bound : string -> Unchecked.bound

module BoundMap : Map.S with type key = Unchecked.bound

module TBoundSet : Set.S with type elt = Checked.bound

module TBoundMap : Map.S with type key = Checked.bound

val record_field_binding_of_sexp :
  Sexplib.Sexp.t -> Unchecked.record_field_binding

val equal_expr_modulo_line_nos : Unchecked.expr -> Unchecked.expr -> bool

val equal_command_modulo_line_nos :
  Unchecked.command -> Unchecked.command -> bool

type ('e, 'c, 't) tsyntax_alg = {
    f_texpr : line_no -> Type.t -> ('e, 'c, 't) Checked.expr_f -> 'e
  ; f_tcommand : line_no -> Type.t -> ('e, 'c, 't) Checked.command_f -> 'c
  ; f_ttype : Type.t -> 't
}

val cata_texpr : ('e, 'c, 't) tsyntax_alg -> Checked.expr -> 'e

val cata_tcommand : ('e, 'c, 't) tsyntax_alg -> Checked.command -> 'c

val monoid_talg : ('a -> 'a -> 'a) -> 'a -> ('a, 'a, 'a) tsyntax_alg

val para_texpr :
     (Checked.expr * 'e, Checked.command * 'c, Type.t * 't) tsyntax_alg
  -> Checked.expr
  -> 'e

val para_tcommand :
     (Checked.expr * 'e, Checked.command * 'c, Type.t * 't) tsyntax_alg
  -> Checked.command
  -> 'c

type ('e, 'c, 't) texpr_p =
  (Checked.expr * 'e, Checked.command * 'c, Type.t * 't) Checked.expr_f

type ('e, 'c, 't) tcommand_p =
  (Checked.expr * 'e, Checked.command * 'c, Type.t * 't) Checked.command_f

val para_talg :
     p_texpr:(line_no -> Type.t -> ('e, 'c, 't) texpr_p -> 'e)
  -> p_tcommand:(line_no -> Type.t -> ('e, 'c, 't) tcommand_p -> 'c)
  -> p_ttype:(Type.t -> 't)
  -> (Checked.expr * 'e, Checked.command * 'c, Type.t * 't) tsyntax_alg

val monoid_para_talg :
     ('a -> 'a -> 'a)
  -> 'a
  -> (Checked.expr * 'a, Checked.command * 'a, Type.t * 'a) tsyntax_alg

type ('e, 'c, 't) syntax_alg = {
    f_expr : line_no -> ('e, 'c, 't) Unchecked.expr_f -> 'e
  ; f_command : line_no -> ('e, 'c, 't) Unchecked.command_f -> 'c
  ; f_type : Type.t -> 't
}

val cata_expr : ('e, 'c, 't) syntax_alg -> Unchecked.expr -> 'e

val cata_command : ('e, 'c, 't) syntax_alg -> Unchecked.command -> 'c

val monoid_alg : ('a -> 'a -> 'a) -> 'a -> ('a, 'a, 'a) syntax_alg

type ('e, 'c, 't) expr_p =
  (Unchecked.expr * 'e, Unchecked.command * 'c, Type.t * 't) Unchecked.expr_f

type ('e, 'c, 't) command_p =
  (Unchecked.expr * 'e, Unchecked.command * 'c, Type.t * 't) Unchecked.command_f

val para_alg :
     p_expr:(line_no -> ('e, 'c, 't) expr_p -> 'e)
  -> p_command:(line_no -> ('e, 'c, 't) command_p -> 'c)
  -> p_type:(Type.t -> 't)
  -> (Unchecked.expr * 'e, Unchecked.command * 'c, Type.t * 't) syntax_alg

val para_expr :
     (Unchecked.expr * 'e, Unchecked.command * 'c, Type.t * 't) syntax_alg
  -> Unchecked.expr
  -> 'e

val para_command :
     (Unchecked.expr * 'e, Unchecked.command * 'c, Type.t * 't) syntax_alg
  -> Unchecked.command
  -> 'c

type call = Unchecked.expr call_f
[@@deriving eq, ord, show {with_path = false}, map, fold]

type tcall = Checked.expr call_f
[@@deriving eq, ord, show {with_path = false}, map, fold]

type 'v value_f =
  | Literal of Literal.t
  | Contract of {
        address : string
      ; entrypoint : string option
    }
  | Record of (string * 'v) list
  | Variant of string * 'v
  | List of 'v list
  | Set of 'v list
  | Map of ('v * 'v) list
  | Tuple of 'v list
  | Closure of Checked.lambda * 'v list
  | Operation of 'v operation_f
  | Ticket of string * 'v * Bigint.t

and 'v operation_f =
  | Transfer of {
        params : 'v
      ; destination : Type.t lcontract
      ; amount : Bigint.t
    }
  | SetDelegate of string option
  | CreateContract of {
        id : contract_id
      ; template : Checked.contract_def
      ; baker : string option
      ; balance : Bigint.t
      ; storage : 'v option
    }
  | Event of string option * Type.t * 'v
[@@deriving show, fold]

type value = {v : value value_f} [@@deriving show]

type tvalue = {
    t : Type.t
  ; tv : tvalue value_f
}
[@@deriving show]

type 'v contract_state_f = {
    balance : Bigint.t
  ; storage : 'v option
  ; private_ : 'v option
  ; baker : string option
  ; metadata : (string * value Meta.t) list
}
[@@deriving show, map]

type 'v instance_f = {
    template : Checked.contract_def
  ; state : 'v contract_state_f
}
[@@deriving show, map]

val map_operation_f : ('v -> 'a) -> 'v operation_f -> 'a operation_f

val map_value_f : ('v -> 'a) -> 'v value_f -> 'a value_f

type operation = value operation_f [@@deriving show]

type toperation = tvalue operation_f [@@deriving show]

type instance = value instance_f [@@deriving show]

type tinstance = tvalue instance_f [@@deriving show]

type contract_state = value contract_state_f [@@deriving show]

val build_value : value value_f -> value

val cata_value : ('a value_f -> 'a) -> value -> 'a

val cata_tvalue : (Type.t -> 'a value_f -> 'a) -> tvalue -> 'a

val para_value : ((value * 'a) value_f -> 'a) -> value -> 'a

val para_tvalue : (Type.t -> (tvalue * 'a) value_f -> 'a) -> tvalue -> 'a

type smart_except =
  [ `Expr of Checked.expr
  | `UExpr of Unchecked.expr
  | `Exprs of Checked.expr list
  | `Value of tvalue
  | `UValue of value
  | `Literal of Literal.t
  | `Line of line_no
  | `Text of string
  | `Type of Type.t
  | `Br
  | `Rec of smart_except list
  | `With_loc of line_no * smart_except list
  ]
[@@deriving show]

val with_loc : line_no:line_no -> smart_except list -> smart_except list

val text : ('a, unit, string, smart_except) format4 -> 'a

type typing_constraint =
  | HasAdd of Checked.expr * Checked.expr * Checked.expr
  | HasMul of Checked.expr * Checked.expr * Checked.expr
  | HasSub of Checked.expr * Checked.expr * Checked.expr
  | HasDiv of Type.t * Checked.expr * Checked.expr
  | HasInvert of Type.t * Type.t
  | HasMap of Checked.expr * Checked.expr * Checked.expr
  | IsComparable of Checked.expr
  | IsPackable of Type.t
  | HasGetItem of Checked.expr * Checked.expr * Type.t
  | HasContains of Checked.expr * Checked.expr * line_no
  | HasSize of Checked.expr
  | IsStringOrBytes of Type.t
  | AssertEqual of Type.t * Type.t * (unit -> smart_except list)
  | IsInt of Type.t * (unit -> smart_except list)
  | SaplingVerify of Checked.expr * Checked.expr
  | HasNeg of Checked.expr * Type.t
  | HasInt of Checked.expr
  | IsNotHot of string * Type.t
  | IsAnyMap of Type.t * Type.t * Checked.expr
  | IsConvertible of Type.t * Type.t
  | IsInstance2 of
      string
      * Checked.expr
      * (Type.t * Type.t)
      * ((Type.t * Type.t) * Type.t) list
  | DefaultsToUnit of Type.t
  | WithStorage of Type.t * (Type.access * Type.t) option Hole.t
[@@deriving eq, show]

exception SmartExcept of smart_except list

exception ExecFailure of value * smart_except list

(** Contract execution results, see also 'interpret_message_inner'. *)
module Execution : sig
  (** Execution errors, see also the {!Error} module. *)

  type error = Exec_failure of value * smart_except list [@@deriving show]

  val error_of_exception : exn -> error

  val to_smart_except : error -> smart_except list

  type 'html exec_message = {
      ok : bool
    ; contract : instance option
    ; operations : toperation list
    ; error : error option
    ; html : 'html
    ; storage : value
  }
  [@@deriving show]
end

(* Checks whether there are exists given sub-expressions or sub-commands
   for which one of the given predicates hold.*)

type 'a exists_in =
     exclude_create_contract:bool
  -> (Checked.expr -> bool)
  -> (Checked.command -> bool)
  -> 'a
  -> bool

val exists_expr : Checked.expr exists_in

val exists_command : Checked.command exists_in

val size_texpr : Checked.expr -> int

val size_tcommand : Checked.command -> int

val erase_types_alg : (Unchecked.expr, Unchecked.command, Type.t) tsyntax_alg

val erase_types_expr : Checked.expr -> Unchecked.expr

val erase_types_command : Checked.command -> Unchecked.command

val erase_types_contract_def : Checked.contract_def -> Unchecked.contract_def

val erase_types_value : tvalue -> value

val erase_types_instance : tinstance -> instance

val erase_types_lambda : Checked.lambda -> Unchecked.lambda

val erase_types_method : Checked.method_ -> Unchecked.method_

val check_initial_flag : line_no:line_no -> Config.flag -> unit

val address_of_contract_id : contract_id -> string

val check_no_incoming_transfer :
  config:Config.t -> _ Checked.entrypoint_f -> bool
