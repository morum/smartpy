(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Utils
include Ids
module Literal = Literal
open Sexplib.Std

module type Deco = sig
  type 'a deco [@@deriving eq, ord, show, map, fold, sexp]
end

module Deco_unchecked = struct
  type _ deco = U [@@deriving eq, ord, show, map, fold, sexp]

  let equal_deco _ = equal_deco

  let compare_deco _ = compare_deco

  let pp_deco _ = pp_deco

  let show_deco _ = show_deco

  let map_deco _ = map_deco

  let fold_deco _ = fold_deco
end

module Deco_checked = struct
  type 'a deco = 'a [@@deriving eq, ord, show, map, fold, sexp]
end

type 'expr derived_var =
  | V_simple of {is_mutable : bool}
  | V_scenario
  | V_exploded
  | V_constant
  | V_match_cons
  | V_defined of 'expr
    (* TODO Add a level of indirection, so substitution doesn't have to traverse each occurrence. *)
[@@deriving eq, ord, show {with_path = false}, map, fold, sexp]

type 'v record_f = (string * 'v) list [@@deriving show {with_path = false}]

let sort_record r = List.sort (fun (lbl1, _) (lbl2, _) -> compare lbl1 lbl2) r

let equal_record_f eq_val x y =
  let eq_entry (lbl1, val1) (lbl2, val2) = lbl1 = lbl2 && eq_val val1 val2 in
  List.equal eq_entry (sort_record x) (sort_record y)

let compare_record_f cmp_val x y =
  let cmp_entry (lbl1, val1) (lbl2, val2) =
    match compare lbl1 lbl2 with
    | 0 -> cmp_val val1 val2
    | c -> c
  in
  List.compare cmp_entry (sort_record x) (sort_record y)

type micheline =
  | Int of string
  | String of string
  | Bytes of string
  | Primitive of {
        name : string
      ; annotations : string list
      ; arguments : micheline list
    }
  | Sequence of micheline list
[@@deriving eq, ord, show {with_path = false}, sexp]

type 't inline_michelson = {
    name : string
  ; parsed : micheline
  ; typesIn : 't list
  ; typesOut : 't list
}
[@@deriving eq, ord, show {with_path = false}, map, fold, sexp]

type line_no = (string * int) option
[@@deriving eq, ord, show {with_path = false}, sexp]

type view_kind =
  | Onchain
  | Offchain
[@@deriving eq, ord, show {with_path = false}, map, fold, sexp]

type address =
  | Real of string
  | Local of contract_id
[@@deriving eq, ord, show {with_path = false}]

module Meta = struct
  type 'a t =
    | List of 'a t list
    | Map of ('a * 'a t) list
    | Other of 'a
    | View of string
  [@@deriving eq, ord, show {with_path = false}, map, fold, sexp]
end

type 'expr call_f =
  | Call_positional of 'expr list
  | Call_keyword of (string * 'expr) list
[@@deriving eq, ord, show {with_path = false}, map, fold, sexp]

type 'type_ lcontract = {
    address : string
  ; entrypoint : string option
  ; type_ : 'type_
}
[@@deriving eq, ord, show {with_path = false}, map, fold, sexp]

type 'type_ prim0 =
  | ECst_contract of 'type_ lcontract
  | EAccount_of_seed of {seed : string}
  | EContract_address of contract_id * string option
  | EContract_balance of contract_id
  | EContract_baker of contract_id
  | EContract_data of contract_id
  | EContract_private of contract_id
  | EContract_typed of contract_id * string option
  | EConstant of string * 'type_
[@@deriving eq, ord, show {with_path = false}, map, fold, sexp]

type 'type_ prim1 =
  | ENot
  | EInvert
  | EConcat_list
  | EAddress
  | EFst
  | ESnd
  | EImplicit_account
  | EList_elements of bool
  | EList_items of bool
  | EList_keys of bool
  | EList_rev
  | EList_values of bool
  | ENeg
  | EPack
  | ESet_delegate
  | ESign
  | ESize
  | ESum
  | ETo_nat
  | ETo_int
  | ETo_bytes
  | EUnpack of 'type_
  | EType_annotation of 'type_
  | EIs_variant of string
  | ERead_ticket
  | EJoin_tickets
  | EPairing_check
  | EVoting_power
  | EConvert
  | EStatic_view of static_id * string (* view name *)
  | EEmit of string option * bool
  | EInvert_bytes
[@@deriving eq, ord, show {with_path = false}, map, fold, sexp]

type 'type_ prim2 =
  | EAdd
  | EAdd_seconds
  | EAnd
  | EApply_lambda
  | EAnd_bytes
  | EAnd_infix
  | EOr_bytes
  | EOr_infix
  | ECons
  | EContains
  | EDiv
  | EEDiv
  | EEq
  | EGe
  | EGet_opt
  | EGt
  | ELe
  | ELshift_bytes
  | ELshift_infix
  | ELt
  | EMax
  | EMin
  | EMod
  | EMul_homo
  | ENeq
  | EOr
  | ERshift_bytes
  | ERshift_infix
  | ESplit_ticket
  | ESub
  | ETicket
  | EView of string (* view name *) * 'type_ (* return type *)
  | EXor_bytes
  | EXor_infix
[@@deriving eq, ord, show {with_path = false}, map, fold, sexp]

type prim3 =
  | ESplit_tokens
  | ERange
  | EUpdate_map
  | EGet_and_update
  | ETest_ticket
[@@deriving eq, ord, show {with_path = false}, map, fold, sexp]

type qual_ident = {
    qi_module : string option
  ; qi_ident : string
}
[@@deriving eq, ord, show {with_path = false}, sexp]

module Qual_ident = Data.Make (struct
  type t = qual_ident [@@deriving ord, show {with_path = false}, sexp]
end)

let string_of_line_no = function
  | None -> "(no location info)"
  | Some (_, i) -> string_of_int i

let head_line_no = function
  | Some (_, hd) -> hd
  | None -> -1

type effects = {
    with_storage : Type.access option
  ; with_operations : bool
}
[@@deriving eq, ord, show {with_path = false}, sexp]

type 'address account_or_address =
  | Account of Primitives.account
  | Address of 'address
[@@deriving eq, ord, show {with_path = false}, map, fold, sexp]

type 'expr action_context = {
    source : 'expr account_or_address option
  ; sender : 'expr account_or_address option
  ; chain_id : 'expr option
  ; time : 'expr option
  ; level : 'expr option
  ; voting_powers : 'expr option
}
[@@deriving eq, ord, show {with_path = false}, map, fold, sexp]

module Syntax (E : Deco) = struct
  open E

  type bound = {
      bound : string
    ; index : int deco
  }
  [@@deriving eq, ord, show {with_path = false}, sexp]

  type 'command fun_def_f = {
      name : bound
    ; params : string list
    ; effects : effects
    ; body : 'command
    ; line_no : line_no
  }
  [@@deriving eq, ord, show {with_path = false}, map, fold, sexp]

  type 'type_ entrypoint_derived = {
      parameter : bound
    ; storage : bound
    ; private_ : bound
    ; operations : bound
    ; tparameter_ep : 'type_
  }
  [@@deriving eq, ord, show {with_path = false}, map, fold, sexp]

  type 'type_ derived_view = {
      parameter : bound
    ; storage : bound
    ; tparameter : 'type_ option
  }
  [@@deriving eq, ord, show {with_path = false}, map, fold, sexp]

  type ('command, 'type_) init_f = {
      init_params : (bound * 'type_) list
    ; init_body : 'command
    ; init_storage : bound
    ; init_private : bound
    ; init_supers : (qual_ident * ('command, 'type_) init_f option) list
    ; line_no : line_no
  }
  [@@deriving eq, ord, show {with_path = false}, map, fold, sexp]

  type 'type_ lambda_derived = {
      tParams : 'type_
    ; tResult : 'type_
    ; with_operations : bound option
    ; with_storage : (Type.access * 'type_ * bound) option
  }
  [@@deriving eq, ord, show {with_path = false}, map, fold, sexp]

  type 'expr lhs =
    | Var_def of bound
    | Assign of 'expr
    | Product of pattern

  and ('command, 'type_) call_init_derived = {
      storage : bound
    ; private_ : bound
    ; super : ('command, 'type_) init_f option
  }

  and ('command, 'type_) entrypoint_f = {
      channel : string
    ; tparameter_ep : [ `Absent | `Present | `Annotated of Type.t ]
    ; check_no_incoming_transfer : bool option
    ; line_no : line_no
    ; body : 'command
    ; derived : 'type_ entrypoint_derived deco
  }

  and ('command, 'type_) view_f = {
      view_kind : view_kind
    ; view_name : string
    ; has_param : bool
    ; pure : bool
    ; body : 'command
    ; doc : string
    ; derived : 'type_ derived_view deco
  }

  and ('expr, 'command, 'type_) contract_derived = {
      tparameter : 'type_
    ; tstorage : 'type_
    ; tprivate : 'type_
    ; init : ('command, 'type_) init_f option
    ; config : Config.t
    ; entrypoints : ('command, 'type_) entrypoint_f list
    ; private_variables : (string * 'expr) list
    ; views : ('command, 'type_) view_f list
    ; initial_balance : 'expr option
    ; initial_metadata : (string * 'expr Meta.t) list
    ; unknown_parts : string option
    ; template_id : static_id option
    ; flags : Config.flag list
    ; line_no : line_no
  }

  and ('command, 'type_) lambda_f = {
      lambda_var : bound
    ; body : 'command
    ; with_storage : (Type.access * 'type_ option) option
    ; with_operations : bool
    ; recursive : bound option
    ; derived : 'type_ lambda_derived deco
  }

  and record_field_binding = {
      var : bound
    ; field : string
  }

  and pattern =
    | Pattern_single of bound
    | Pattern_tuple of bound list
    | Pattern_record of record_field_binding list

  and ('expr, 'command, 'type_) expr_f =
    | EVar of {
          name : bound
        ; derived : 'expr derived_var deco
      }
    | EPrivate of string
    | ELiteral of Literal.t
    | EMPrim0 of Michelson_base.Type.mtype Michelson_base.Primitive.prim0
    | EMPrim1 of
        Michelson_base.Type.mtype Michelson_base.Primitive.prim1 * 'expr
    | EMPrim1_fail of Michelson_base.Primitive.prim1_fail * 'expr
    | EMPrim2 of
        Michelson_base.Type.mtype Michelson_base.Primitive.prim2 * 'expr * 'expr
    | EMPrim3 of Michelson_base.Primitive.prim3 * 'expr * 'expr * 'expr
    | EPrim0 of 'type_ prim0
    | EPrim1 of 'type_ prim1 * 'expr
    | EPrim2 of 'type_ prim2 * 'expr * 'expr
    | EPrim3 of prim3 * 'expr * 'expr * 'expr
    | EAttr of string * 'expr
    | EVariant of string * 'expr
    | ECall of 'expr * 'expr option
    | EOpen_variant of string * 'expr * 'expr option
    | EItem of {
          items : 'expr
        ; key : 'expr
        ; default_value : 'expr option
        ; missing_message : 'expr option
      }
    | ETuple of 'expr list
    | ERecord of (string * 'expr) list
    | EList of 'expr list
    | EList_comprehension of 'expr * bound * 'expr
    | EMap of bool * ('expr * 'expr) list
    | ESet of 'expr list
    | ESapling_verify_update of {
          state : 'expr
        ; transaction : 'expr
      }
    | EMichelson of 'type_ inline_michelson * 'expr list
    | EMap_function of {
          f : 'expr
        ; l : 'expr
      }
    | ELambda of ('command, 'type_) lambda_f
    | ECreate_contract of {
          push : bool
        ; template_ref : qual_ident
        ; baker : 'expr
        ; balance : 'expr
        ; storage : 'expr
        ; derived : ('expr, 'command, 'type_) contract_def_f deco
      }
    | EContract of {
          entrypoint : string option
        ; arg_type : 'type_
        ; address : 'expr
      }
    | ESlice of {
          offset : 'expr (* nat *)
        ; length : 'expr (* nat *)
        ; buffer : 'expr
      }
    | EMake_signature of {
          secret_key : 'expr
        ; message : 'expr
        ; message_format : [ `Raw | `Hex ]
      }
    | ETransfer of {
          arg : 'expr
        ; amount : 'expr
        ; destination : 'expr
      }
    | EIf of 'expr * 'expr * 'expr
    | EIs_failing of 'expr
    | ECatch_exception of 'type_ * 'expr

  and ('expr, 'command, 'type_) command_f =
    | CCall_init of
        qual_ident * 'expr call_f * ('command, 'type_) call_init_derived deco
    | CNever of 'expr
    | CFailwith of 'expr
    | CVerify of 'expr * 'expr option (* message *)
    | CIf of 'expr * 'command * 'command
    | CMatch of 'expr * (string * bound * 'command) list
    | CModify_product of 'expr * bound option * pattern * 'command
    | CModify_record of 'expr * bound * 'command
    | CMatch_list of {
          expr : 'expr
        ; id : bound
        ; clause_cons : 'command
        ; clause_nil : 'command
      }
    | CDel_item of 'expr * 'expr
    | CUpdate_set of 'expr * 'expr * bool
    | CBlock of 'command list
    | CExpr of 'expr
    | CAssign of {
          lhs : 'expr
        ; rhs : 'expr
        ; derived : 'expr lhs deco
      }
    | CFor of bound * 'expr * 'command
    | CWhile of 'expr * 'command
    | CResult of 'expr
    | CComment of string
    | CSet_type of 'expr * 'type_
    | CSet_result_type of 'command * 'type_
    | CTrace of 'expr

  and expr = {
      e : (expr, command, Type.t) expr_f
    ; et : Type.t deco
    ; line_no : line_no
  }

  and command = {
      c : (expr, command, Type.t) command_f
    ; ct : Type.t deco
    ; line_no : line_no
  }

  and lambda = (command, Type.t) lambda_f

  and init = (command, Type.t) init_f

  and entrypoint = (command, Type.t) entrypoint_f

  and view = (command, Type.t) view_f

  (* TODO Factor out the contents of the "deco" arguments as much as
     possible. *)
  and method_kind =
    | Init of init deco
    | Entrypoint of string * entrypoint deco
    | Private of string * Type.access option * bool * expr deco
    | Onchain_view of string * view deco
    | Offchain_view of string * view deco

  and 'command method_f = {
      kind : method_kind
    ; parameters : string list
    ; body : 'command
    ; line_no : line_no
  }

  and method_ = command method_f

  and ('expr, 'command, 'type_) contract_def_f = {
      name : string
    ; parents :
        (qual_ident * ('expr, 'command, 'type_) contract_def_f deco) list
    ; methods : 'command method_f list
    ; derived : ('expr, 'command, 'type_) contract_derived deco
    ; line_no : line_no
  }

  and contract_def = (expr, command, Type.t) contract_def_f

  and ('expr, 'command, 'type_) module_elt_f =
    | Expr_def of {
          name : bound
        ; rhs : 'expr
        ; line_no : line_no
      }
    | Fun_def of 'command fun_def_f
    | Type_def of {
          name : string
        ; rhs : 'type_
        ; line_no : line_no
      }
    | Contract_def of ('expr, 'command, 'type_) contract_def_f

  and ('expr, 'command, 'type_) module_f = {
      module_name : string
    ; elts : ('expr, 'command, 'type_) module_elt_f list
    ; defs : 'expr String.Map.t deco
    ; type_defs : Type.t String.Map.t deco
    ; contract_defs : ('expr, 'command, 'type_) contract_def_f String.Map.t deco
    ; mod_line_no : line_no
  }

  and ('expr, 'command, 'type_) instantiate_derived = {
      instantiate_derived_contract : ('expr, 'command, 'type_) contract_def_f
    ; args : 'expr list
  }

  and ('expr, 'command, 'type_) action_f =
    | Add_module of {
          module_ : ('expr, 'command, 'type_) module_f
        ; line_no : line_no
      }
    | Instantiate_contract of {
          id : contract_id
        ; class_ : qual_ident
        ; call : 'expr call_f
        ; balance : 'expr option
        ; derived : ('expr, 'command, 'type_) instantiate_derived deco
        ; line_no : line_no
      }
    | Compute of {
          var : bound
        ; expression : 'expr
        ; context : 'expr action_context
        ; line_no : line_no
      }
    | Message of {
          id : contract_id
        ; valid : 'expr
        ; exception_ : 'expr option
        ; params : 'expr
        ; line_no : line_no
        ; amount : 'expr
        ; context : 'expr action_context
        ; message : string
        ; show : bool
        ; export : bool
      }
    | ScenarioError of {message : string}
    | Html of {
          tag : string
        ; inner : string
        ; line_no : line_no
      }
    | Verify of {
          condition : 'expr
        ; line_no : line_no
      }
    | Show of {
          expression : 'expr
        ; html : bool
        ; stripStrings : bool
        ; compile : bool
        ; line_no : line_no
      }
    | Set_delegate of {
          id : contract_id
        ; line_no : line_no
        ; baker : 'expr
      }
    | DynamicContract of {
          id : dynamic_id
        ; model_id : contract_id
        ; line_no : line_no
      }
    | Add_flag of {
          flag : Config.flag
        ; line_no : line_no
      }
    | Prepare_constant_value of {
          line_no : line_no
        ; var : bound
        ; hash : string option
        ; expression : 'expr
      }
    | Mutation_test of {
          scenarios : (string * int) list
        ; show_paths : bool
        ; line_no : line_no
      }

  and module_ = (expr, command, Type.t) module_f

  and module_elt = (expr, command, Type.t) module_elt_f

  and action = (expr, command, Type.t) action_f

  and ('expr, 'command, 'type_) scenario_f = {
      shortname : string
    ; longname : string
    ; actions : (Config.t * ('expr, 'command, 'type_) action_f) list
  }

  and scenario = (expr, command, Type.t) scenario_f
  [@@deriving eq, ord, show {with_path = false}, map, fold, sexp]
end

module Unchecked = Syntax (Deco_unchecked)
module Checked = Syntax (Deco_checked)

let mk_bound bound = Unchecked.{bound; index = U}

module BoundMap = Map.Make (struct
  type t = Unchecked.bound

  let compare = Unchecked.compare_bound

  let pp = Unchecked.pp_bound
end)

module TBoundSet = Set.Make (struct
  type t = Checked.bound

  let compare x y = Checked.compare_bound x y

  let pp = Checked.pp_bound
end)

module TBoundMap = Map.Make (struct
  type t = Checked.bound

  let compare = Checked.compare_bound

  let pp = Checked.pp_bound
end)

let record_field_binding_of_sexp :
    Sexplib.Sexp.t -> Unchecked.record_field_binding = function
  | List [Atom "binding"; Atom name; Atom field] -> {var = mk_bound name; field}
  | x -> failwith ("record_field_binding_of_sexp: " ^ Sexplib.Sexp.to_string x)

let rec equal_expr_modulo_line_nos e1 e2 =
  let open Unchecked in
  equal_expr_f equal_expr_modulo_line_nos equal_command_modulo_line_nos
    Type.equal e1.e e2.e

and equal_command_modulo_line_nos c1 c2 =
  let open Unchecked in
  equal_command_f equal_expr_modulo_line_nos equal_command_modulo_line_nos
    Type.equal c1.c c2.c

type ('e, 'c, 't) tsyntax_alg = {
    f_texpr : line_no -> Type.t -> ('e, 'c, 't) Checked.expr_f -> 'e
  ; f_tcommand : line_no -> Type.t -> ('e, 'c, 't) Checked.command_f -> 'c
  ; f_ttype : Type.t -> 't
}

let cata_t {f_texpr; f_tcommand; f_ttype} =
  let open Checked in
  let rec ce {e; et; line_no} = f_texpr line_no et (map_expr_f ce cc f_ttype e)
  and cc {c; ct; line_no} =
    f_tcommand line_no ct (map_command_f ce cc f_ttype c)
  in
  (ce, cc)

let cata_texpr alg = fst (cata_t alg)

let cata_tcommand alg = snd (cata_t alg)

type ('e, 'c, 't) texpr_p =
  (Checked.expr * 'e, Checked.command * 'c, Type.t * 't) Checked.expr_f

type ('e, 'c, 't) tcommand_p =
  (Checked.expr * 'e, Checked.command * 'c, Type.t * 't) Checked.command_f

let para_talg ~p_texpr ~p_tcommand ~p_ttype =
  let open Checked in
  let f_texpr line_no et e =
    ({e = map_expr_f fst fst fst e; et; line_no}, p_texpr line_no et e)
  in
  let f_tcommand line_no ct c =
    ({c = map_command_f fst fst fst c; ct; line_no}, p_tcommand line_no ct c)
  in
  let f_ttype t = (t, p_ttype t) in
  {f_texpr; f_tcommand; f_ttype}

let para_texpr alg e = snd (cata_texpr alg e)

let para_tcommand alg c = snd (cata_tcommand alg c)

(* A paramorphic algebra on a monoid. *)
let monoid_para_talg append empty =
  let open Checked in
  let append x y = append x (snd y) in
  let p_texpr _ _ = fold_expr_f append append append empty in
  let p_tcommand _ _ = fold_command_f append append append empty in
  let p_ttype _ = empty in
  para_talg ~p_texpr ~p_tcommand ~p_ttype

type ('e, 'c, 't) syntax_alg = {
    f_expr : line_no -> ('e, 'c, 't) Unchecked.expr_f -> 'e
  ; f_command : line_no -> ('e, 'c, 't) Unchecked.command_f -> 'c
  ; f_type : Type.t -> 't
}

let cata {f_expr; f_command; f_type} =
  let open Unchecked in
  let rec ce {e; line_no} = f_expr line_no (map_expr_f ce cc f_type e)
  and cc {c; line_no} = f_command line_no (map_command_f ce cc f_type c) in
  (ce, cc)

let cata_expr alg = fst (cata alg)

let cata_command alg = snd (cata alg)

(* An algebra on a monoid. *)
let monoid_alg append empty =
  let open Unchecked in
  {
    f_expr = (fun _ -> fold_expr_f append append append empty)
  ; f_command = (fun _ -> fold_command_f append append append empty)
  ; f_type = (fun _ -> empty)
  }

type ('e, 'c, 't) expr_p =
  (Unchecked.expr * 'e, Unchecked.command * 'c, Type.t * 't) Unchecked.expr_f

type ('e, 'c, 't) command_p =
  (Unchecked.expr * 'e, Unchecked.command * 'c, Type.t * 't) Unchecked.command_f

let para_alg ~p_expr ~p_command ~p_type =
  let open Unchecked in
  let f_expr line_no e =
    ({e = map_expr_f fst fst fst e; et = U; line_no}, p_expr line_no e)
  in
  let f_command line_no c =
    ({c = map_command_f fst fst fst c; ct = U; line_no}, p_command line_no c)
  in
  let f_type t = (t, p_type t) in
  {f_expr; f_command; f_type}

let para_expr alg e = snd (cata_expr alg e)

let para_command alg c = snd (cata_command alg c)

(* An algebra on a monoid. *)
let monoid_talg append empty =
  {
    f_texpr = (fun _ _ -> Checked.fold_expr_f append append append empty)
  ; f_tcommand = (fun _ _ -> Checked.fold_command_f append append append empty)
  ; f_ttype = (fun _ -> empty)
  }

let size_talg = monoid_talg ( + ) 1

let size_tcommand = cata_tcommand size_talg

let size_texpr = cata_texpr size_talg

type call = Unchecked.expr call_f
[@@deriving eq, ord, show {with_path = false}, map, fold]

type tcall = Checked.expr call_f
[@@deriving eq, ord, show {with_path = false}, map, fold]

type 'v value_f =
  | Literal of Literal.t
  | Contract of {
        address : string
      ; entrypoint : string option
    }
  | Record of (string * 'v) list
  | Variant of string * 'v
  | List of 'v list
  | Set of 'v list
  | Map of ('v * 'v) list
  | Tuple of 'v list
  | Closure of Checked.lambda * 'v list
  | Operation of 'v operation_f
  | Ticket of string * 'v * Bigint.t

and 'v operation_f =
  | Transfer of {
        params : 'v
      ; destination : Type.t lcontract
      ; amount : Bigint.t
    }
  | SetDelegate of string option
  | CreateContract of {
        id : contract_id
      ; template : Checked.contract_def
      ; baker : string option
      ; balance : Bigint.t
      ; storage : 'v option
    }
  | Event of string option * Type.t * 'v
[@@deriving show {with_path = false}, map, fold]

type value = {v : value value_f} [@@deriving show {with_path = false}]

type tvalue = {
    t : Type.t
  ; tv : tvalue value_f
}
[@@deriving show {with_path = false}]

type 'v contract_state_f = {
    balance : Bigint.t
  ; storage : 'v option
  ; private_ : 'v option
  ; baker : string option
  ; metadata : (string * value Meta.t) list
}
[@@deriving show {with_path = false}, map]

type 'v instance_f = {
    template : Checked.contract_def
  ; state : 'v contract_state_f
}
[@@deriving show {with_path = false}, map]

type operation = value operation_f [@@deriving show {with_path = false}]

type toperation = tvalue operation_f [@@deriving show {with_path = false}]

type instance = value instance_f [@@deriving show {with_path = false}]

type tinstance = tvalue instance_f [@@deriving show {with_path = false}]

type contract_state = value contract_state_f
[@@deriving show {with_path = false}]

let build_value v = {v}

let rec cata_value f {v} = f (map_value_f (cata_value f) v)

let rec cata_tvalue f {t; tv} = f t (map_value_f (cata_tvalue f) tv)

let para_value f =
  let f x = ({v = map_value_f fst x}, f x) in
  fun x -> snd (cata_value f x)

let para_tvalue f =
  let f t x = ({t; tv = map_value_f fst x}, f t x) in
  fun x -> snd (cata_tvalue f x)

type smart_except =
  [ `Expr of Checked.expr
  | `UExpr of Unchecked.expr
  | `Exprs of Checked.expr list
  | `Value of tvalue
  | `UValue of value
  | `Literal of Literal.t
  | `Line of line_no
  | `Text of string
  | `Type of Type.t
  | `Br
  | `Rec of smart_except list
  | `With_loc of line_no * smart_except list
  ]
[@@deriving show {with_path = false}]

let with_loc ~line_no es = [`With_loc (line_no, es)]

let text fmt = Printf.ksprintf (fun s -> `Text s) fmt

type lazy_smart_except_list = unit -> smart_except list
[@@deriving show {with_path = false}]

let equal_lazy_smart_except_list _ _ = true

type typing_constraint =
  | HasAdd of Checked.expr * Checked.expr * Checked.expr
  | HasMul of Checked.expr * Checked.expr * Checked.expr
  | HasSub of Checked.expr * Checked.expr * Checked.expr
  | HasDiv of Type.t * Checked.expr * Checked.expr
  | HasInvert of Type.t * Type.t
  | HasMap of Checked.expr * Checked.expr * Checked.expr
  | IsComparable of Checked.expr
  | IsPackable of Type.t
  | HasGetItem of Checked.expr * Checked.expr * Type.t
  | HasContains of Checked.expr * Checked.expr * line_no
  | HasSize of Checked.expr
  | IsStringOrBytes of Type.t
  | AssertEqual of Type.t * Type.t * lazy_smart_except_list
  | IsInt of Type.t * lazy_smart_except_list
  | SaplingVerify of Checked.expr * Checked.expr
  | HasNeg of Checked.expr * Type.t
  | HasInt of Checked.expr
  | IsNotHot of string * Type.t
  | IsAnyMap of Type.t * Type.t * Checked.expr
  | IsConvertible of Type.t * Type.t
  | IsInstance2 of
      string
      * Checked.expr
      * (Type.t * Type.t)
      * ((Type.t * Type.t) * Type.t) list
  | DefaultsToUnit of Type.t
  | WithStorage of Type.t * (Type.access * Type.t) option Hole.t
[@@deriving eq, show {with_path = false}]

exception SmartExcept of smart_except list

exception ExecFailure of value * smart_except list

module Execution = struct
  type error = Exec_failure of value * smart_except list
  [@@deriving show {with_path = false}]

  let error_of_exception exn =
    let string s = {v = Literal (Literal.string s)} in
    match exn with
    | Failure f -> Exec_failure (string f, [`Text "Failure:"; `Text f])
    | ExecFailure (value, message) -> Exec_failure (value, message)
    | SmartExcept l -> Exec_failure (string "Error", l)
    | _exn -> assert false

  let to_smart_except = function
    | Exec_failure (_, message) -> message

  type 'html exec_message = {
      ok : bool
    ; contract : instance option
    ; operations : toperation list
    ; error : error option
    ; html : 'html
    ; storage : value
  }
  [@@deriving show {with_path = false}]
end

type 'a exists_in =
     exclude_create_contract:bool
  -> (Checked.expr -> bool)
  -> (Checked.command -> bool)
  -> 'a
  -> bool

let exists_talg ~exclude_create_contract:_ f_expr f_command =
  let open Checked in
  let sub b1 (_, b2) = b1 || b2 in
  let p_texpr line_no et e =
    let holds_below =
      match e with
      (* FIXME Migrate this to the new ECreate_contract.
            | ECreate_contract_old {baker; balance; storage}
            when exclude_create_contract -> snd baker || snd balance || snd storage
      *)
      | e -> fold_expr_f sub sub sub false e
    in
    f_expr {e = map_expr_f fst fst fst e; et; line_no} || holds_below
  in
  let p_tcommand line_no ct c =
    f_command {c = map_command_f fst fst fst c; ct; line_no}
    || fold_command_f sub sub sub false c
  in
  let p_ttype _ = false in
  para_talg ~p_texpr ~p_tcommand ~p_ttype

let exists_expr ~exclude_create_contract f_expr f_command =
  para_texpr (exists_talg ~exclude_create_contract f_expr f_command)

let exists_command ~exclude_create_contract f_expr f_command =
  para_tcommand (exists_talg ~exclude_create_contract f_expr f_command)

let erase_types_bound ({bound} : Checked.bound) : Unchecked.bound =
  {bound; index = U}

let erase_types_record_field_binding Checked.{var; field} =
  Unchecked.{var = erase_types_bound var; field}

let erase_types_pattern : Checked.pattern -> Unchecked.pattern = function
  | Pattern_single x -> Pattern_single (erase_types_bound x)
  | Pattern_tuple xs -> Pattern_tuple (List.map erase_types_bound xs)
  | Pattern_record xs ->
      Pattern_record (List.map erase_types_record_field_binding xs)

let erase_types_alg =
  {
    f_texpr =
      (fun line_no _ e ->
        let e =
          match e with
          | ECreate_contract {push; template_ref; baker; balance; storage} ->
              Unchecked.ECreate_contract
                {push; template_ref; baker; balance; storage; derived = U}
          | ELambda {lambda_var; body; with_storage; with_operations; recursive}
            ->
              ELambda
                {
                  lambda_var = erase_types_bound lambda_var
                ; body
                ; with_storage
                ; with_operations
                ; recursive = Option.map erase_types_bound recursive
                ; derived = U
                }
          | EVar {name; derived = _} ->
              EVar {name = erase_types_bound name; derived = U}
          | ELiteral x -> ELiteral x
          | EMPrim0 p -> EMPrim0 p
          | EMPrim1 (p, x0) -> EMPrim1 (p, x0)
          | EMPrim1_fail (p, x0) -> EMPrim1_fail (p, x0)
          | EMPrim2 (p, x0, x1) -> EMPrim2 (p, x0, x1)
          | EMPrim3 (p, x0, x1, x2) -> EMPrim3 (p, x0, x1, x2)
          | EPrim0 p -> EPrim0 p
          | EPrim1 (p, x0) -> EPrim1 (p, x0)
          | EPrim2 (p, x0, x1) -> EPrim2 (p, x0, x1)
          | EPrim3 (p, x0, x1, x2) -> EPrim3 (p, x0, x1, x2)
          | EAttr (x, e) -> EAttr (x, e)
          | EVariant (x, e) -> EVariant (x, e)
          | ECall (e1, e2) -> ECall (e1, e2)
          | EOpen_variant (x1, x2, x3) -> EOpen_variant (x1, x2, x3)
          | ETuple x -> ETuple x
          | ERecord x -> ERecord x
          | EList x -> EList x
          | EList_comprehension (e, x, xs) ->
              EList_comprehension (e, erase_types_bound x, xs)
          | EMap (x1, x2) -> EMap (x1, x2)
          | ESet x -> ESet x
          | EItem {items; key; default_value; missing_message} ->
              EItem {items; key; default_value; missing_message}
          | ESapling_verify_update {state; transaction} ->
              ESapling_verify_update {state; transaction}
          | EMichelson (x1, x2) -> EMichelson (x1, x2)
          | EMap_function {f; l} -> EMap_function {f; l}
          | EContract {entrypoint; arg_type; address} ->
              EContract {entrypoint; arg_type; address}
          | ESlice {offset; length; buffer} -> ESlice {offset; length; buffer}
          | EMake_signature {secret_key; message; message_format} ->
              EMake_signature {secret_key; message; message_format}
          | ETransfer {arg; amount; destination} ->
              ETransfer {arg; amount; destination}
          | EPrivate x -> EPrivate x
          | EIf (x1, x2, x3) -> EIf (x1, x2, x3)
          | EIs_failing x -> EIs_failing x
          | ECatch_exception (t, x) -> ECatch_exception (t, x)
        in
        Unchecked.{e; et = U; line_no})
  ; f_tcommand =
      (fun line_no _ c ->
        let c =
          match c with
          | CBlock xs -> Unchecked.CBlock xs
          | CExpr e -> Unchecked.CExpr e
          | CAssign {lhs; rhs} -> Unchecked.CAssign {lhs; rhs; derived = U}
          | CMatch (x, cases) ->
              let f (x, param, body) = (x, erase_types_bound param, body) in
              CMatch (x, List.map f cases)
          | CMatch_list {expr; id; clause_cons; clause_nil} ->
              CMatch_list
                {expr; id = erase_types_bound id; clause_cons; clause_nil}
          | CModify_product (e, r, p, body) ->
              CModify_product
                (e, Option.map erase_types_bound r, erase_types_pattern p, body)
          | CModify_record (e, r, body) ->
              CModify_record (e, erase_types_bound r, body)
          | CFor (x, xs, body) -> CFor (erase_types_bound x, xs, body)
          | CCall_init (a, e, _) -> CCall_init (a, e, U)
          | CNever x -> CNever x
          | CFailwith x -> CFailwith x
          | CVerify (x1, x2) -> CVerify (x1, x2)
          | CIf (x1, x2, x3) -> CIf (x1, x2, x3)
          | CDel_item (x1, x2) -> CDel_item (x1, x2)
          | CUpdate_set (x1, x2, x3) -> CUpdate_set (x1, x2, x3)
          | CWhile (x1, x2) -> CWhile (x1, x2)
          | CResult x -> CResult x
          | CComment x -> CComment x
          | CSet_type (x1, x2) -> CSet_type (x1, x2)
          | CSet_result_type (x1, x2) -> CSet_result_type (x1, x2)
          | CTrace x -> CTrace x
        in

        {c; ct = U; line_no})
  ; f_ttype = (fun t -> t)
  }

let erase_types_command = cata_tcommand erase_types_alg

let erase_types_expr = cata_texpr erase_types_alg

let erase_types_value = cata_tvalue (fun _t v -> {v})

let erase_types_instance = map_instance_f erase_types_value

let erase_types_lambda
    ({lambda_var; body; with_storage; with_operations; recursive} :
      Checked.lambda) : Unchecked.lambda =
  {
    lambda_var = erase_types_bound lambda_var
  ; body = erase_types_command body
  ; with_storage
  ; with_operations
  ; recursive = Option.map erase_types_bound recursive
  ; derived = U
  }

let erase_types_method_kind : Checked.method_kind -> Unchecked.method_kind =
  function
  | Init _ -> Init U
  | Entrypoint (name, _) -> Entrypoint (name, U)
  | Private (name, access, x, _) -> Private (name, access, x, U)
  | Onchain_view (name, _) -> Onchain_view (name, U)
  | Offchain_view (name, _) -> Offchain_view (name, U)

let erase_types_method ({kind; parameters; body; line_no} : Checked.method_) :
    Unchecked.method_ =
  {
    kind = erase_types_method_kind kind
  ; parameters
  ; body = erase_types_command body
  ; line_no
  }

let erase_types_contract_def
    ({name; parents; methods; line_no} : Checked.contract_def) :
    Unchecked.contract_def =
  let methods = List.map erase_types_method methods in
  let parents = List.map (fun (x, _) -> (x, Deco_unchecked.U)) parents in
  {name; parents; methods; derived = U; line_no}

let check_initial_flag ~line_no flag =
  if Config.is_initial_flag flag
  then
    raise
      (SmartExcept
         (with_loc ~line_no
            [
              text
                "Flag %S can only be set in the command line or at the \
                 beginning of a scenario."
                (Config.show_flag flag)
            ]))

let static_addresses =
  [|
     "KT1TezoooozzSmartPyzzSTATiCzzzwwBFA1"
   ; "KT1Tezooo1zzSmartPyzzSTATiCzzzyfC8eF"
   ; "KT1Tezooo2zzSmartPyzzSTATiCzzzwqqQ4H"
   ; "KT1Tezooo3zzSmartPyzzSTATiCzzzseJjWC"
   ; "KT1Tezooo4zzSmartPyzzSTATiCzzzyPVdv3"
   ; "KT1Tezooo5zzSmartPyzzSTATiCzzzz48Z4p"
   ; "KT1Tezooo6zzSmartPyzzSTATiCzzztY1196"
   ; "KT1Tezooo7zzSmartPyzzSTATiCzzzvTbG1z"
   ; "KT1Tezooo8zzSmartPyzzSTATiCzzzzp29d1"
   ; "KT1Tezooo9zzSmartPyzzSTATiCzzztdBMLX"
   ; "KT1Tezoo1ozzSmartPyzzSTATiCzzzw8CmuY"
   ; "KT1Tezoo11zzSmartPyzzSTATiCzzzzYMWmQ"
   ; "KT1Tezoo12zzSmartPyzzSTATiCzzzw2ZWPM"
   ; "KT1Tezoo13zzSmartPyzzSTATiCzzzyrBos2"
   ; "KT1Tezoo14zzSmartPyzzSTATiCzzzrnXGeA"
   ; "KT1Tezoo15zzSmartPyzzSTATiCzzzzjjBRr"
   ; "KT1Tezoo16zzSmartPyzzSTATiCzzzyHEQ7h"
   ; "KT1Tezoo17zzSmartPyzzSTATiCzzzrQkWKN"
   ; "KT1Tezoo18zzSmartPyzzSTATiCzzzt4Mx4x"
   ; "KT1Tezoo19zzSmartPyzzSTATiCzzzzvx27N"
   ; "KT1Tezoo2ozzSmartPyzzSTATiCzzzt3UDWP"
   ; "KT1Tezoo21zzSmartPyzzSTATiCzzzzPeb4T"
   ; "KT1Tezoo22zzSmartPyzzSTATiCzzzzKyojP"
   ; "KT1Tezoo23zzSmartPyzzSTATiCzzzzLNLUR"
   ; "KT1Tezoo24zzSmartPyzzSTATiCzzzuimc59"
   ; "KT1Tezoo25zzSmartPyzzSTATiCzzzrmbVsM"
   ; "KT1Tezoo26zzSmartPyzzSTATiCzzzwDQDCm"
   ; "KT1Tezoo27zzSmartPyzzSTATiCzzztVDNZ1"
   ; "KT1Tezoo28zzSmartPyzzSTATiCzzzurgreu"
   ; "KT1Tezoo29zzSmartPyzzSTATiCzzzoiUVra"
   ; "KT1Tezoo3ozzSmartPyzzSTATiCzzzzpy1LW"
   ; "KT1Tezoo31zzSmartPyzzSTATiCzzzxZF7L4"
   ; "KT1Tezoo32zzSmartPyzzSTATiCzzzzJfoLr"
   ; "KT1Tezoo33zzSmartPyzzSTATiCzzzt9n3Ca"
   ; "KT1Tezoo34zzSmartPyzzSTATiCzzzxZRxk5"
   ; "KT1Tezoo35zzSmartPyzzSTATiCzzzxEBbbo"
   ; "KT1Tezoo36zzSmartPyzzSTATiCzzzzHx9Mg"
   ; "KT1Tezoo37zzSmartPyzzSTATiCzzzyWyuj1"
   ; "KT1Tezoo38zzSmartPyzzSTATiCzzzwrpEt4"
   ; "KT1Tezoo39zzSmartPyzzSTATiCzzzsxXjKw"
   ; "KT1Tezoo4ozzSmartPyzzSTATiCzzzwJqoPS"
   ; "KT1Tezoo41zzSmartPyzzSTATiCzzzsmVb4M"
   ; "KT1Tezoo42zzSmartPyzzSTATiCzzzwtnVrK"
   ; "KT1Tezoo43zzSmartPyzzSTATiCzzzqz2ouC"
   ; "KT1Tezoo44zzSmartPyzzSTATiCzzztSh3PS"
   ; "KT1Tezoo45zzSmartPyzzSTATiCzzzzteuw9"
   ; "KT1Tezoo46zzSmartPyzzSTATiCzzzyu6S2X"
   ; "KT1Tezoo47zzSmartPyzzSTATiCzzzxjfKR8"
   ; "KT1Tezoo48zzSmartPyzzSTATiCzzzxeybmd"
   ; "KT1Tezoo49zzSmartPyzzSTATiCzzzsYs8mm"
   ; "KT1Tezoo5ozzSmartPyzzSTATiCzzzwq7cM4"
   ; "KT1Tezoo51zzSmartPyzzSTATiCzzzsie8hD"
   ; "KT1Tezoo52zzSmartPyzzSTATiCzzzy6jfJn"
   ; "KT1Tezoo53zzSmartPyzzSTATiCzzzpzJxjJ"
   ; "KT1Tezoo54zzSmartPyzzSTATiCzzzsC58yB"
   ; "KT1Tezoo55zzSmartPyzzSTATiCzzzzB9g1d"
   ; "KT1Tezoo56zzSmartPyzzSTATiCzzzvJRMF4"
   ; "KT1Tezoo57zzSmartPyzzSTATiCzzzyRNXjo"
   ; "KT1Tezoo58zzSmartPyzzSTATiCzzzuoQJPC"
   ; "KT1Tezoo59zzSmartPyzzSTATiCzzzxrzq7o"
   ; "KT1Tezoo6ozzSmartPyzzSTATiCzzzxn4BAQ"
   ; "KT1Tezoo61zzSmartPyzzSTATiCzzzsCVUrJ"
   ; "KT1Tezoo62zzSmartPyzzSTATiCzzzzNq6gW"
   ; "KT1Tezoo63zzSmartPyzzSTATiCzzzvo99GZ"
   ; "KT1Tezoo64zzSmartPyzzSTATiCzzzxyJU8a"
   ; "KT1Tezoo65zzSmartPyzzSTATiCzzzzadcFa"
   ; "KT1Tezoo66zzSmartPyzzSTATiCzzzxPC5nQ"
   ; "KT1Tezoo67zzSmartPyzzSTATiCzzzzyq8B1"
   ; "KT1Tezoo68zzSmartPyzzSTATiCzzzxpBPfQ"
   ; "KT1Tezoo69zzSmartPyzzSTATiCzzzsPcuJf"
   ; "KT1Tezoo7ozzSmartPyzzSTATiCzzzxgiesf"
   ; "KT1Tezoo71zzSmartPyzzSTATiCzzzyGZS4Q"
   ; "KT1Tezoo72zzSmartPyzzSTATiCzzzzc5bP5"
   ; "KT1Tezoo73zzSmartPyzzSTATiCzzzzjqD8v"
   ; "KT1Tezoo74zzSmartPyzzSTATiCzzzqjbsH6"
   ; "KT1Tezoo75zzSmartPyzzSTATiCzzzweg7Kr"
   ; "KT1Tezoo76zzSmartPyzzSTATiCzzzzJQexk"
   ; "KT1Tezoo77zzSmartPyzzSTATiCzzzxzCsXF"
   ; "KT1Tezoo78zzSmartPyzzSTATiCzzzx3Yt9E"
   ; "KT1Tezoo79zzSmartPyzzSTATiCzzzy1eJ9s"
   ; "KT1Tezoo8ozzSmartPyzzSTATiCzzzuGszvB"
   ; "KT1Tezoo81zzSmartPyzzSTATiCzzzyn1ZHZ"
   ; "KT1Tezoo82zzSmartPyzzSTATiCzzzyr5ES9"
   ; "KT1Tezoo83zzSmartPyzzSTATiCzzzxUVHDg"
   ; "KT1Tezoo84zzSmartPyzzSTATiCzzzw2LQfK"
   ; "KT1Tezoo85zzSmartPyzzSTATiCzzzrZRBHF"
   ; "KT1Tezoo86zzSmartPyzzSTATiCzzzwwr66A"
   ; "KT1Tezoo87zzSmartPyzzSTATiCzzzx2asuv"
   ; "KT1Tezoo88zzSmartPyzzSTATiCzzzvVdzo9"
   ; "KT1Tezoo89zzSmartPyzzSTATiCzzzxMckJy"
   ; "KT1Tezoo9ozzSmartPyzzSTATiCzzzxX37Py"
   ; "KT1Tezoo91zzSmartPyzzSTATiCzzzyegtLQ"
   ; "KT1Tezoo92zzSmartPyzzSTATiCzzzvtdVek"
   ; "KT1Tezoo93zzSmartPyzzSTATiCzzzznaYBh"
   ; "KT1Tezoo94zzSmartPyzzSTATiCzzzwqPLoM"
   ; "KT1Tezoo95zzSmartPyzzSTATiCzzzrn7SPm"
   ; "KT1Tezoo96zzSmartPyzzSTATiCzzzw2aWmL"
   ; "KT1Tezoo97zzSmartPyzzSTATiCzzzyYc6Fd"
   ; "KT1Tezoo98zzSmartPyzzSTATiCzzzvpPECg"
   ; "KT1Tezoo99zzSmartPyzzSTATiCzzzzYoJJi"
   ; "KT1Tezo1oozzSmartPyzzSTATiCzzzw6i9jd"
  |]

let dynamic_addresses =
  [|
     "KT1TezoooozzSmartPyzzDYNAMiCzzpLu4LU"
   ; "KT1Tezooo1zzSmartPyzzDYNAMiCzztcr8AZ"
   ; "KT1Tezooo2zzSmartPyzzDYNAMiCzzxyHfG9"
   ; "KT1Tezooo3zzSmartPyzzDYNAMiCzzvqsJQk"
   ; "KT1Tezooo4zzSmartPyzzDYNAMiCzzywTMhC"
   ; "KT1Tezooo5zzSmartPyzzDYNAMiCzzvwBH3X"
   ; "KT1Tezooo6zzSmartPyzzDYNAMiCzzvyu5w3"
   ; "KT1Tezooo7zzSmartPyzzDYNAMiCzztDqbVQ"
   ; "KT1Tezooo8zzSmartPyzzDYNAMiCzzq2URWu"
   ; "KT1Tezooo9zzSmartPyzzDYNAMiCzzwMosaF"
   ; "KT1Tezoo1ozzSmartPyzzDYNAMiCzzzknqsi"
   ; "KT1Tezoo11zzSmartPyzzDYNAMiCzzufK1yA"
   ; "KT1Tezoo12zzSmartPyzzDYNAMiCzzp8MwtN"
   ; "KT1Tezoo13zzSmartPyzzDYNAMiCzzuFSRii"
   ; "KT1Tezoo14zzSmartPyzzDYNAMiCzzz7RGrK"
   ; "KT1Tezoo15zzSmartPyzzDYNAMiCzzvGSR2o"
   ; "KT1Tezoo16zzSmartPyzzDYNAMiCzzwFpt7J"
   ; "KT1Tezoo17zzSmartPyzzDYNAMiCzzykrvy7"
   ; "KT1Tezoo18zzSmartPyzzDYNAMiCzztCAt8v"
   ; "KT1Tezoo19zzSmartPyzzDYNAMiCzzs7uP4i"
   ; "KT1Tezoo2ozzSmartPyzzDYNAMiCzzwSnrN6"
   ; "KT1Tezoo21zzSmartPyzzDYNAMiCzzs5DfTE"
   ; "KT1Tezoo22zzSmartPyzzDYNAMiCzzrduLfP"
   ; "KT1Tezoo23zzSmartPyzzDYNAMiCzzwZfMMD"
   ; "KT1Tezoo24zzSmartPyzzDYNAMiCzzwjiGTo"
   ; "KT1Tezoo25zzSmartPyzzDYNAMiCzzyT75uA"
   ; "KT1Tezoo26zzSmartPyzzDYNAMiCzzwgevwP"
   ; "KT1Tezoo27zzSmartPyzzDYNAMiCzzuNESHj"
   ; "KT1Tezoo28zzSmartPyzzDYNAMiCzzstgZLG"
   ; "KT1Tezoo29zzSmartPyzzDYNAMiCzzua3qt8"
   ; "KT1Tezoo3ozzSmartPyzzDYNAMiCzzzydbTn"
   ; "KT1Tezoo31zzSmartPyzzDYNAMiCzzzV6Fv1"
   ; "KT1Tezoo32zzSmartPyzzDYNAMiCzztwKPMA"
   ; "KT1Tezoo33zzSmartPyzzDYNAMiCzztAevd2"
   ; "KT1Tezoo34zzSmartPyzzDYNAMiCzzyHyztq"
   ; "KT1Tezoo35zzSmartPyzzDYNAMiCzzzseQRS"
   ; "KT1Tezoo36zzSmartPyzzDYNAMiCzzxG7VFz"
   ; "KT1Tezoo37zzSmartPyzzDYNAMiCzzvanMaF"
   ; "KT1Tezoo38zzSmartPyzzDYNAMiCzzxow7N5"
   ; "KT1Tezoo39zzSmartPyzzDYNAMiCzzseasUf"
   ; "KT1Tezoo4ozzSmartPyzzDYNAMiCzzwCUzLT"
   ; "KT1Tezoo41zzSmartPyzzDYNAMiCzzuKdM6z"
   ; "KT1Tezoo42zzSmartPyzzDYNAMiCzzvoxbXQ"
   ; "KT1Tezoo43zzSmartPyzzDYNAMiCzzxHNrWB"
   ; "KT1Tezoo44zzSmartPyzzDYNAMiCzzqzWSLJ"
   ; "KT1Tezoo45zzSmartPyzzDYNAMiCzzwoma3L"
   ; "KT1Tezoo46zzSmartPyzzDYNAMiCzzwagsDr"
   ; "KT1Tezoo47zzSmartPyzzDYNAMiCzzx9gczA"
   ; "KT1Tezoo48zzSmartPyzzDYNAMiCzzujzDEL"
   ; "KT1Tezoo49zzSmartPyzzDYNAMiCzzyzNqLS"
   ; "KT1Tezoo5ozzSmartPyzzDYNAMiCzzyhNzVP"
   ; "KT1Tezoo51zzSmartPyzzDYNAMiCzztanVCy"
   ; "KT1Tezoo52zzSmartPyzzDYNAMiCzzukPXQf"
   ; "KT1Tezoo53zzSmartPyzzDYNAMiCzzwertzS"
   ; "KT1Tezoo54zzSmartPyzzDYNAMiCzzxrXndJ"
   ; "KT1Tezoo55zzSmartPyzzDYNAMiCzzwZDrJ6"
   ; "KT1Tezoo56zzSmartPyzzDYNAMiCzzz7MLT7"
   ; "KT1Tezoo57zzSmartPyzzDYNAMiCzzx1SNRa"
   ; "KT1Tezoo58zzSmartPyzzDYNAMiCzzspyLrZ"
   ; "KT1Tezoo59zzSmartPyzzDYNAMiCzzpFMM5S"
   ; "KT1Tezoo6ozzSmartPyzzDYNAMiCzzzc7Vf5"
   ; "KT1Tezoo61zzSmartPyzzDYNAMiCzzxQGTz1"
   ; "KT1Tezoo62zzSmartPyzzDYNAMiCzzzCFRxE"
   ; "KT1Tezoo63zzSmartPyzzDYNAMiCzzxSbaFw"
   ; "KT1Tezoo64zzSmartPyzzDYNAMiCzzrZxQjf"
   ; "KT1Tezoo65zzSmartPyzzDYNAMiCzzuE8zrh"
   ; "KT1Tezoo66zzSmartPyzzDYNAMiCzzxL3RNg"
   ; "KT1Tezoo67zzSmartPyzzDYNAMiCzzxDHbQ2"
   ; "KT1Tezoo68zzSmartPyzzDYNAMiCzzv9uuPs"
   ; "KT1Tezoo69zzSmartPyzzDYNAMiCzzziAEo3"
   ; "KT1Tezoo7ozzSmartPyzzDYNAMiCzzssee8N"
   ; "KT1Tezoo71zzSmartPyzzDYNAMiCzzy8o38T"
   ; "KT1Tezoo72zzSmartPyzzDYNAMiCzzvJQgMd"
   ; "KT1Tezoo73zzSmartPyzzDYNAMiCzzzQKejF"
   ; "KT1Tezoo74zzSmartPyzzDYNAMiCzzxGc1Le"
   ; "KT1Tezoo75zzSmartPyzzDYNAMiCzzwS4VD5"
   ; "KT1Tezoo76zzSmartPyzzDYNAMiCzzsu5trk"
   ; "KT1Tezoo77zzSmartPyzzDYNAMiCzzx7fRph"
   ; "KT1Tezoo78zzSmartPyzzDYNAMiCzzubSbwk"
   ; "KT1Tezoo79zzSmartPyzzDYNAMiCzzwzStmF"
   ; "KT1Tezoo8ozzSmartPyzzDYNAMiCzzq35Rq4"
   ; "KT1Tezoo81zzSmartPyzzDYNAMiCzzzQyrWb"
   ; "KT1Tezoo82zzSmartPyzzDYNAMiCzzuk3Zgh"
   ; "KT1Tezoo83zzSmartPyzzDYNAMiCzzzamuiH"
   ; "KT1Tezoo84zzSmartPyzzDYNAMiCzztuhJts"
   ; "KT1Tezoo85zzSmartPyzzDYNAMiCzzsEUusH"
   ; "KT1Tezoo86zzSmartPyzzDYNAMiCzzywXBn3"
   ; "KT1Tezoo87zzSmartPyzzDYNAMiCzzto1Zr6"
   ; "KT1Tezoo88zzSmartPyzzDYNAMiCzzzYK2k5"
   ; "KT1Tezoo89zzSmartPyzzDYNAMiCzzvEXcr3"
   ; "KT1Tezoo9ozzSmartPyzzDYNAMiCzzzhhryG"
   ; "KT1Tezoo91zzSmartPyzzDYNAMiCzzzsUuqY"
   ; "KT1Tezoo92zzSmartPyzzDYNAMiCzzvE65Pi"
   ; "KT1Tezoo93zzSmartPyzzDYNAMiCzzyo94pw"
   ; "KT1Tezoo94zzSmartPyzzDYNAMiCzzz3TFhN"
   ; "KT1Tezoo95zzSmartPyzzDYNAMiCzzuzjFR1"
   ; "KT1Tezoo96zzSmartPyzzDYNAMiCzzsS2Wj8"
   ; "KT1Tezoo97zzSmartPyzzDYNAMiCzzywXC4c"
   ; "KT1Tezoo98zzSmartPyzzDYNAMiCzzuttMz3"
   ; "KT1Tezoo99zzSmartPyzzDYNAMiCzzr8xqaW"
   ; "KT1Tezo1oozzSmartPyzzDYNAMiCzzxmuJG3"
  |]

let address_of_contract_id contract_id =
  match contract_id with
  | C_static {static_id} -> static_addresses.(static_id)
  | C_dynamic {dynamic_id} -> dynamic_addresses.(dynamic_id)

let check_no_incoming_transfer ~(config : Config.t) ep =
  match
    ( config.default_check_no_incoming_transfer
    , ep.Checked.check_no_incoming_transfer )
  with
  | b, None | _, Some b -> b
