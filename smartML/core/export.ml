(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Utils

let export_layout layout =
  match Hole.get layout with
  | None -> "Variable"
  | Some layout ->
      let leaf Layout.{source; target} =
        if source = target
        then Printf.sprintf "(\"%s\")" source
        else Printf.sprintf "(\"%s as %s\")" source target
      in
      let node l1 l2 = Printf.sprintf "(%s %s)" l1 l2 in
      Printf.sprintf "(Value %s)" (Binary_tree.cata leaf node layout)

let export_type =
  let line_no = "None" in
  let rec export_type t =
    match Type.unF t with
    | T0 t ->
        let t, memo = Michelson_base.Type.string_of_type0 t in
        assert (memo = None);
        t
    | Int {isNat} -> (
        match Hole.get isNat with
        | None -> "intOrNat"
        | Some true -> "nat"
        | Some false -> "int")
    | Record {row; layout; var} ->
        assert (var = None);
        Printf.sprintf "(record (%s) %s %s)"
          (String.concat " "
             (List.map
                (fun (s, t) -> Printf.sprintf "(%s %s)" s (export_type t))
                row))
          (export_layout layout) line_no
    | T1 (Option, t) -> Printf.sprintf "(option %s)" (export_type t)
    | Variant {layout; row} ->
        Printf.sprintf "(variant (%s) %s %s)"
          (String.concat " "
             (List.map
                (fun (s, t) -> Printf.sprintf "(%s %s)" s (export_type t))
                row))
          (export_layout layout) line_no
    | T1 (Set, telement) -> Printf.sprintf "(set %s)" (export_type telement)
    | T2 ((Map | Big_map), tkey, tvalue) ->
        Printf.sprintf "(map %s %s)" (export_type tkey) (export_type tvalue)
        (* TODO big ? *)
    | T1 (Contract, t) -> Printf.sprintf "(contract %s)" (export_type t)
    | Secret_key -> "secret_key"
    | Unknown (_, id) -> Printf.sprintf "(unknown %s)" id
    | Tuple ts ->
        let ts = List.map (fun t -> export_type t) ts in
        Printf.sprintf "(tuple %s)" (String.concat " " ts)
    | T1 (List, t) -> Printf.sprintf "(list %s)" (export_type t)
    | Lambda ({with_storage; with_operations}, t1, t2) ->
        let f x =
          match Hole.get x with
          | None -> "None"
          | Some b -> if b then "True" else "False"
        in
        let with_storage, tstorage =
          match Hole.get with_storage with
          | None | Some None -> ("None", "None")
          | Some (Some (Read_only, tstorage)) ->
              ("read-only", export_type tstorage)
          | Some (Some (Read_write, tstorage)) ->
              ("read-write", export_type tstorage)
        in
        Printf.sprintf "(lambda %s %s %s %s %s)" with_storage
          (f with_operations) tstorage (export_type t1) (export_type t2)
    | T2 (Lambda, _, _) -> assert false
    | Sapling_state memo ->
        let memo =
          match Hole.get memo with
          | None -> "unknown"
          | Some i -> string_of_int i
        in
        Printf.sprintf "(sapling_state %s)" memo
    | Sapling_transaction memo ->
        let memo =
          match Hole.get memo with
          | None -> "unknown"
          | Some i -> string_of_int i
        in
        Printf.sprintf "(sapling_transaction %s)" memo
    | T1 (Ticket, t) -> Printf.sprintf "(ticket %s)" (export_type t)
    | Abbrev {module_ = None; name} -> Printf.sprintf "(abbrev  %s)" name
    | Abbrev {module_ = Some module_; name} ->
        Printf.sprintf "(abbrev %s %s)" module_ name
    | T2 ((Pair _ | Or _), _, _) -> assert false
  in
  export_type

let _export_type t =
  Printf.printf "export_type IN %s\n" (Type.show t);
  let r = export_type t in
  Printf.printf "export_type OUT %s\n" r;
  r
