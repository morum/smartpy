(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Michelson

type group

type pipeline = group list

val run : pipeline -> instr -> instr

val run_on_tcontract :
     protocol:Config.protocol
  -> pipeline
  -> Michelson.tcontract
  -> Michelson.tcontract

val collapse_drops : pipeline

val simplify : protocol:Config.protocol -> pipeline

val remove_comments : pipeline
