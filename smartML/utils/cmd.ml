(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

module type S = sig
  val run : ?env:(string * string) list -> string -> string list -> int
end
