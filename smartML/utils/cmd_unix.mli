(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

include Cmd.S

val sys_command : ('a, unit, string, unit) format4 -> 'a
