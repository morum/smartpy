(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

type ('a, 'b) t = 'a * 'b [@@deriving eq, ord, show]
