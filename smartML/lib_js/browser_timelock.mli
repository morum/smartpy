(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

(** Bindings for Timelock (generated with [gen_js_api]). *)

val open_chest : string -> string -> int -> string
  [@@js.global "smartpyContext.Timelock.openChest"]
