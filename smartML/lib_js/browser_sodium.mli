(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

(** Encapsulation of the byte-arrays used by the library. *)
module Crypto_bytes : sig
  type t = Uint8_array.t

  val to_hex : t -> string [@@js.global "sodium.to_hex"]

  val of_hex : string -> t [@@js.global "sodium.from_hex"]

  val append : t -> t -> t [@@js.global "eztz.utility.mergebuf"]

  module B58_prefix : sig
    type t = Uint8_array.t

    val edsig : t [@@js.global "eztz.prefix.edsig"]

    val edpk : t [@@js.global "eztz.prefix.edpk"]

    val tz1 : t [@@js.global "eztz.prefix.tz1"]

    val edsk32 : t
      [@@js.global "eztz.prefix.edsk2"]
    (** Ed25519 Secret-keys in the tezos-style (a.k.a. “seed”). *)

    val edsk64 : t
      [@@js.global "eztz.prefix.edsk"]
    (** Ed25519 Secret-keys in the NaCl-style (public and secret keys
        concatenated). *)
  end
end

val crypto_hash_blake_2b : size:int -> Crypto_bytes.t -> Crypto_bytes.t
  [@@js.global "sodium.crypto_generichash"]
(** Implementation of BLAKE2B, the [~size] argument seems was only
    tested with [32] and [64]. *)

module Ed25519 : sig
  val verify_signature :
       Crypto_bytes.t
    -> message:Crypto_bytes.t
    -> public_key:Crypto_bytes.t
    -> bool
    [@@js.global "sodium.crypto_sign_verify_detached"]

  val sign :
    message:Crypto_bytes.t -> secret_key:Crypto_bytes.t -> Crypto_bytes.t
    [@@js.global "sodium.crypto_sign_detached"]

  type keypair = {
      publicKey : Crypto_bytes.t
    ; privateKey : Crypto_bytes.t
    ; keyType : string
  }

  val keypair_of_seed : string -> keypair
    [@@js.global "sodium.crypto_sign_seed_keypair"]
  (** Make a deterministic key-pair from a 32-byte C-string.
      {[
        sodium.crypto_sign_seed_keypair('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
        {publicKey: Uint8Array(32), privateKey: Uint8Array(64), keyType: "ed25519"}
     ]} *)
end
