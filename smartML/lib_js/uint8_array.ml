type t = char list

let char_to_js x = Ojs.int_to_js (int_of_char x)

let char_of_js x = char_of_int (Ojs.int_of_js x)

let t_to_js x =
  let x = Ojs.(list_to_js char_to_js x) in
  let c = Ojs.variable "Uint8Array" in
  Ojs.(new_obj c [|x|])

let t_of_js x = Ojs.(list_of_js char_of_js x)
