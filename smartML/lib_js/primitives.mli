(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

include Core.Primitives.Primitives

val test : (module Core.Primitives.Primitives) -> unit
