(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Utils
open Control
open Expr
open Michelson_base.Protocol

type checked_precontract = private {checked_precontract : texpr precontract}
[@@deriving eq]

val print_checked_precontract : Format.formatter -> checked_precontract -> unit

val show_checked_precontract : checked_precontract -> string

val typecheck_precontract :
  protocol:protocol -> expr precontract -> checked_precontract Result.t

val typecheck :
     protocol:protocol
  -> tparameter:Type.ty * string option
  -> (string * Type.ty) list
  -> Expr.expr
  -> Expr.texpr Result.t
