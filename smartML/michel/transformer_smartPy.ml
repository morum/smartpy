(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

(** A half-baked collection of transformations intended to transform
    expressions into a shape akin to SmartPy. *)

open Utils
open Control
open Type
open Binary_tree
open Expr
module TO = Binary_tree.Traversable (Option)
open Transformer

let is_var = function
  | {expr = Var _} -> true
  | _ -> false

let is_simple =
  let f = function
    | Var _ -> true
    | Lit _ -> true
    | Prim0 _ -> true
    | Prim1 ((Car | Cdr), x) -> x
    | Prim2 (Pair _, x, y) -> x && y
    | Record r -> for_all (fun (_, e) -> e) r
    | Vector xs -> List.for_all id xs
    | Variant (_, _, e) -> e
    | _ -> false
  in
  cata_expr f

let _distribute_lets =
  let f = function
    | Let_in
        ( P_var (Some x)
        , {expr = Match_variant (({expr = Var _} as scrutinee), clauses)}
        , {
            expr =
              Prim2
                ( Pair _
                , ({expr = Prim0 (Nil _toperation)} as l)
                , {expr = Var x'} )
          } )
      when x = x' ->
        let f {cons; var; rhs} = {cons; var; rhs = pair None None l rhs} in
        let clauses = Binary_tree.map f clauses in
        match_variant scrutinee clauses
    | Let_in (xs, {expr = Match_variant (scrutinee, clauses)}, e)
      when is_var scrutinee && is_simple e ->
        let f {cons; var; rhs} = {cons; var; rhs = let_in xs rhs e} in
        let clauses = Binary_tree.map f clauses in
        match_variant scrutinee clauses
    | Prim2 (Pair (a1, a2), l, {expr = Match_variant (scrutinee, clauses)})
      when is_var scrutinee && is_simple l ->
        let f {cons; var; rhs} = {cons; var; rhs = pair a1 a2 l rhs} in
        let clauses = Binary_tree.map f clauses in
        match_variant scrutinee clauses
    | expr -> {expr}
  in
  (* Does not preserve 'bindings_unique'. *)
  mk "distribute_lets" (cata_expr f) []

let _unfold_getn_and_updaten st =
  let f = function
    | Prim1 (Getn 0, x) -> x
    | Prim1 (Getn 1, x) -> prim1 Car x
    | Prim1 (Getn k, x) when k >= 2 -> prim1 (Getn (k - 2)) (prim1 Cdr x)
    | Prim2 (Updaten 0, r, x) -> let_in_var None x r
    | Prim2 (Updaten 1, r, x) ->
        let x1 = fresh st "x" in
        let x2 = fresh st "x" in
        let_in_vector [Some x1; Some x2]
          {expr = Unpair (2, x)}
          (let_in_var None (var x1) (pair None None r (var x2)))
    | Prim2 (Updaten k, r, x) when k >= 2 ->
        let x1 = fresh st "x" in
        let x2 = fresh st "x" in
        let_in_vector [Some x1; Some x2]
          {expr = Unpair (2, x)}
          (pair None None (var x1) (prim2 (Updaten (k - 2)) r (var x2)))
    | expr -> {expr}
  in
  mk "unfold_getn_and_updaten" (cata_expr f) [preserves bindings_unique]

let _binarize_matches st =
  let f = function
    | Match_variant (s, Node ((Node _ as l), r)) ->
        let ls = fresh st "l" in
        let rhs = {expr = Match_variant ({expr = Var ls}, l)} in
        let l = Leaf {cons = None; var = Some ls; rhs} in
        match_variant s (Node (l, r))
    | Match_variant (s, Node (l, (Node _ as r))) ->
        let rs = fresh st "r" in
        let rhs = {expr = Match_variant ({expr = Var rs}, r)} in
        let r = Leaf {cons = None; var = Some rs; rhs} in
        match_variant s (Node (l, r))
    | expr -> {expr}
  in
  mk "binarize_matches" (cata_expr f) [preserves bindings_unique]

let _single_lets st =
  let f = function
    | Match_record ((Node _ as rp), e1, e2) ->
        let x = fresh st "x" in
        let rec mk_vars v = function
          | Leaf n -> [(Option.default (fresh st "x") n, v)]
          | Node (x, y) -> mk_vars (prim1 Car v) x @ mk_vars (prim1 Cdr v) y
        in
        let bindings = (x, e1) :: mk_vars (var x) rp in
        List.fold_right (fun (x, e) r -> let_in_var (Some x) e r) bindings e2
    | expr -> {expr}
  in
  mk "single_lets" (cata_expr f) [preserves bindings_unique]

let _binarize_records =
  let f = function
    | Record xs ->
        let pair a b = Expr.tuple [a; b] in
        Binary_tree.cata snd pair xs
    | expr -> {expr}
  in
  mk "binarize_records" (cata_expr f) [preserves bindings_unique]

let _binarize_projs =
  let f e _t =
    match e with
    | Proj_field (fld, (e, Stack_ok (Record r))) ->
        let rec proj r p other =
          match r with
          | Leaf (Some n, _) when fld = n -> p
          | Leaf _ -> other ()
          | Node (x, y) ->
              proj x
                {expr = Prim1 (Car, p)}
                (fun () -> proj y {expr = Prim1 (Cdr, p)} other)
        in
        proj r e (fun () -> assert false)
    | e -> {expr = map_expr_f fst e}
  in
  mk_t "binarize_projs" (cata_texpr f) [preserves bindings_unique]

exception No_label

let tree_of_record tree e =
  let leaf = function
    | None -> raise No_label
    | Some fld -> proj_field fld e
  in
  try Binary_tree.cata leaf (pair None None) tree with No_label -> e

let _eta_expand_parameter_and_storage st {tstorage; parameter_and_storage} =
  let pre, post =
    match tstorage with
    | Record r ->
        let r = Binary_tree.map fst r in
        let post body =
          let ops' = fresh st "ops" in
          let s' = fresh st "s" in
          match_record
            Binary_tree.(Node (Leaf (Some ops'), Leaf (Some s')))
            body
            (tuple [var ops'; record_of_tree r (var s')])
        in
        (tree_of_record r, post)
    | _ -> (id, id)
  in
  let f body =
    match parameter_and_storage with
    | None -> body
    | Some ps ->
        let gp = "__parameter" in
        let s = "__storage" in
        let body =
          substitute [(ps, pair None None (var gp) (pre (var s)))] body
        in
        let body = post body in
        let body = let_in_var (Some s) (prim1 Cdr (var ps)) body in
        let body = let_in_var (Some gp) (prim1 Car (var ps)) body in
        body
  in
  mk "eta_expand_parameter_and_storage" f [preserves bindings_unique]

let _erase_comments =
  let f = function
    | Comment (_, e) -> e
    | expr -> {expr}
  in
  mk "tuplefy" (cata_expr f) [preserves bindings_unique]

let _tuplefy =
  let has_missing_label r =
    List.exists (fun (lbl, _) -> Option.is_none lbl) (Binary_tree.to_list r)
  in
  let f = function
    | Record r when has_missing_label r ->
        Binary_tree.cata (fun (_, t) -> t) (Expr.pair None None) r
    | expr -> {expr}
  in
  mk "tuplefy" (cata_expr f) [preserves bindings_unique]

let _recordify st =
  let f lhs rhs e =
    let fresh = Binary_tree.map (fun x -> (x, fresh st "x")) lhs in
    let lhs = Binary_tree.map (fun (_, x) -> Some x) fresh in
    let subst =
      let g = function
        | Some x, x' -> Some (x, Expr.var x')
        | _ -> None
      in
      List.map_some g (Binary_tree.to_list fresh)
    in
    let e = substitute subst e in
    match rhs with
    | {expr = Prim1_fail _} -> rhs (* TODO look at stack type, not syntax *)
    | _ -> match_record lhs rhs e
  in
  let f = function
    | Match_record (lhs, {expr = If (s, l, r)}, e) ->
        if_ s (f lhs l e) (f lhs r e)
    | Match_record (lhs, {expr = Match_variant (s, clauses)}, e) ->
        let clauses =
          Binary_tree.map (fun c -> {c with rhs = f lhs c.rhs e}) clauses
        in
        match_variant s clauses
    | Match_record (Node (Leaf x, Leaf y), {expr = Prim2 (Pair (_, _), t, u)}, e)
      -> let_in_var x t (let_in_var y u e)
    (* ------------------------------------------------------------------------ *)
    (* Distribute record_of_tree: *)
    | Let_in
        ( P_var (Some x)
        , e1
        , {
            expr =
              Record
                Binary_tree.(
                  Node
                    ( Leaf
                        ( _
                        , ({expr = Prim0 (Nil _)} as e2)
                          (* TODO generalize to any expression not mentioning x *)
                        )
                    , Leaf (_, {expr = Record_of_tree (row, {expr = Var x'})})
                    ))
          } )
      when x = x' ->
        let_in_var (Some x) (record_of_tree row e1) (tuple [e2; var x'])
    | Record_of_tree (row, {expr = Match_variant (scrutinee, clauses)}) ->
        let f c = {c with rhs = record_of_tree row c.rhs} in
        match_variant scrutinee (Binary_tree.map f clauses)
    | Record_of_tree (_row, ({expr = Prim1_fail _} as e)) -> e
    (* Fallback: convert to record if possible: *)
    | Record_of_tree (row, tree) as expr ->
        let open Option in
        let rec parse_tree row tree =
          match (row, tree.expr) with
          | Leaf l, _ -> Some (Binary_tree.Leaf (l, tree))
          | Node (r1, r2), Prim2 (Pair _, x1, x2) ->
              let* x1 = parse_tree r1 x1 in
              let* x2 = parse_tree r2 x2 in
              Some (Binary_tree.Node (x1, x2))
          | _ -> None
        in
        Option.cata {expr} record (parse_tree row tree)
    | expr -> {expr}
  in
  mk "recordify" (cata_expr f) [preserves bindings_unique]

let _unfold_unpair =
  let f = function
    | Unpair (2, x) -> vector [prim1 Car x; prim1 Cdr x]
    | Prim1 (Cast _, x) -> x
    | expr -> {expr}
  in
  mk "unfold_unpair" (cata_expr f) [preserves bindings_unique]

let _reify_rev_ops =
  let f = function
    | Iter_over
        ( [l; {expr = Prim0 (Nil (T0 Operation))}]
        , [x; xs]
        , {
            expr =
              Vector [{expr = Prim2 (Cons, {expr = Var x'}, {expr = Var xs'})}]
          } )
      when (x, xs) = (Some x', Some xs') ->
        vector [prim2 Exec l (var "!rev_ops")]
    | Prim2 (Exec, x, {expr = Var "!rev_ops"}) ->
        (* FIXME Don't simply delete this. *) x
    | expr -> {expr}
  in
  mk "reify_rev_ops" (cata_expr f) [preserves bindings_unique]

let dummy = ()
