(* Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC *)

open Expr
open Michelson_base.Protocol

type state = {var_counter : int ref}

type transformer =
  | Atom_typed of (texpr -> expr)
  | Atom_untyped of (expr -> expr)
  | Fixpoint of transformer
  | Sequence of transformer list
  | Label of string * transformer
  | Precondition of (expr -> string option) * transformer
  | Postcondition of (expr -> expr -> string option) * transformer

val mk :
     string
  -> (Expr.expr -> Expr.expr)
  -> (transformer -> transformer) list
  -> transformer

val mk_t :
     string
  -> (Expr.texpr -> Expr.expr)
  -> (transformer -> transformer) list
  -> transformer

val preserves : string * (Expr.expr -> bool) -> transformer -> transformer

val bindings_unique : string * (Expr.expr -> bool)

val fresh : state -> string -> string

val freshen : state -> string -> string list -> string list

val simplify :
  protocol:protocol -> state -> expr precontract -> expr precontract

val michelsonify :
  protocol:protocol -> state -> expr precontract -> expr precontract
