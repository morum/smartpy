import smartpy as sp


@sp.module
def utils():
    def seconds_of_timestamp(_timestamp):
        return abs(_timestamp - sp.timestamp(0))

    def mutez_to_nat(x):
        sp.cast(x, sp.mutez)
        return sp.fst(sp.ediv(x, sp.mutez(1)).unwrap_some())

    def nat_to_mutez(x):
        sp.cast(x, sp.nat)
        return sp.mul(x, sp.mutez(1))

    def nat_to_tez(x):
        sp.cast(x, sp.nat)
        return sp.mul(x, sp.tez(1))
