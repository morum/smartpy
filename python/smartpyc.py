# Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC

# This module is used by smartpyc.ml. No other code should rely on it.

import sys, traceback, json, browser, smartpyio
from pathlib import Path

debug_mode = False


def fail_with_compile_error(e):
    for i in traceback.format_exception_only(type(e), e):
        sys.stderr.write(i)
    sys.exit(1)


def fail_with_filtered_traceback(fns):
    if debug_mode:
        traceback.print_exc(file=sys.stderr)
    else:
        etype, value, tb = sys.exc_info()
        xs = [i for i in traceback.extract_tb(tb) if i.filename in fns]
        if xs:
            sys.stderr.write("Traceback (most recent call last):\n")
            for i in traceback.format_list(xs):
                sys.stderr.write(i)
            for i in traceback.format_exception_only(etype, value):
                sys.stderr.write(i)
        else:
            traceback.print_exc(file=sys.stderr)
    sys.exit(1)


def run_script(in_py, code, context, script_args):
    smartpyio.script_filename = in_py
    sys.argv = [in_py] + list(script_args)
    try:
        code = compile(code, in_py, "exec")
    except Exception as e:
        fail_with_compile_error(e)
    try:
        exec(code, context)
    except:
        fail_with_filtered_traceback([in_py])


def init_contract(fn_py, arg, context):
    try:
        contract = compile(arg, "init", "eval")
    except Exception as e:
        fail_with_compile_error(e)
    try:
        contract = eval(contract, context)
    except:
        sys.stderr.write("Error while evaluating '%s':\n" % arg)
        fail_with_filtered_traceback([fn_py, "init"])
    return contract


def run_tests(in_py):
    scenarios = []
    for test in browser.window.pythonTests:
        try:
            test.eval()
        except Exception as exn:
            data = {}
            data["action"] = "error"
            data["message"] = str(exn)
            if browser.scenario is not None:
                browser.scenario += [data]
            else:
                browser.scenario = [data]
                sys.stderr.write("Exception while testing\n")
            fail_with_filtered_traceback([in_py])
        if isinstance(browser.scenario, list):
            scenario = browser.scenario
        else:
            scenario = browser.scenario.messages
        scenarios.append(
            {"shortname": test.shortname, "longname": test.name, "scenario": scenario}
        )
    return json.dumps(scenarios, indent=1)


def write_tests(out_scenario_sc, fn_py, *script_args):
    code = Path(fn_py).read_text()
    run_script(fn_py, code, {"__name__": "__main__"}, script_args)
    scenarios = run_tests(fn_py)
    open(out_scenario_sc, "w").write(scenarios)


if sys.argv[1] == "write_tests":
    write_tests(*sys.argv[2:])
else:
    assert False
