import smartpy as sp


@sp.module
def main():
    class C(sp.Contract):
        def __init__(self, x):
            self.data.x = x

        @sp.entrypoint
        def ep(self, x):
            self.data.x = sp.Some(x)

    class C2(sp.Contract):
        def __init__(self, sv):
            self.private.sv = sv

        @sp.entrypoint
        def ep2(self):
            assert self.private.sv(0) == 43


@sp.add_test(name="Test")
def test():
    s = sp.test_scenario(main)

    c = main.C(sp.none)
    s += c
    s.verify(c.data.x == sp.none)
    v1 = s.compute(421)
    v2 = s.compute(43)
    s += main.C(sp.some(v1))
    c.ep(v1)

    sv = sp.build_lambda(lambda _: v2)
    c2 = main.C2(sv)
    s += c2
    c2.ep2()
