import smartpy as sp


@sp.module
def main():
    class C(sp.Contract):
        @sp.entrypoint
        def test_none(self, p):
            assert p.is_none()

        @sp.entrypoint
        def test_some(self, p):
            assert p.is_some()


@sp.add_test(name="Test")
def test():
    s = sp.test_scenario(main)
    s += main.C()
