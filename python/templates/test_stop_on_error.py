import smartpy as sp


@sp.module
def main():
    class MyContract(sp.Contract):
        def __init__(self, x):
            self.data.x = x

        @sp.entrypoint
        def entrypoint_1(self):
            pass


@sp.add_test(name="Test")
def test():
    scenario = sp.test_scenario(main)
    c1 = main.MyContract(x=12)
    scenario += c1
    scenario.verify(c1.data.x == 12)
    scenario.verify(c1.data.x == 13)
    scenario.verify(c1.data.x == 14)
    scenario.add_flag("stop-on-error")
    scenario.verify(c1.data.x == 12)
    scenario.verify(c1.data.x == 13)
    scenario.verify(c1.data.x == 14)


@sp.add_test(name="Test2")
def test2():
    scenario = sp.test_scenario(main)
    c1 = main.MyContract(x=12)
    scenario += c1
    scenario.add_flag("stop-on-error")
    scenario.verify(c1.data.x == 12)
    scenario.verify(c1.data.x == 13)
    scenario.verify(c1.data.x == 14)
