import smartpy as sp


@sp.module
def main():
    class MyContract(sp.Contract):
        def __init__(self):
            self.data.x = []


@sp.add_test(name="Test")
def test():
    scenario = sp.test_scenario(main)
    scenario += main.MyContract()
