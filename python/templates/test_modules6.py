import smartpy as sp
from utils import utils


@sp.module
def M2():
    m1 = {"a": 42}
    m2 = sp.cast({"a": 42}, sp.map[sp.string, sp.int])
    bm1 = sp.big_map({"a": 42})
    bm2 = sp.cast(sp.big_map({"a": 42}), sp.big_map[sp.string, sp.int])
    bm3 = sp.cast(sp.big_map(), sp.big_map[sp.string, sp.int])
    bm4 = sp.cast(sp.big_map({}), sp.big_map[sp.string, sp.int])

    class C(sp.Contract):
        def __init__(self):
            self.data.a = 0
            self.data.x = 0
            self.private.c = 42

        @sp.private(with_storage="read-write")
        def f(self, x):
            assert utils.nat_to_tez(0) == sp.tez(0)
            self.data.a = 1000
            return x + 2

        @sp.onchain_view
        def view_on(self, x):
            _ = self.private.c
            _ = self.f(42)
            return x + 1

        @sp.offchain_view
        def view_off(self, x):
            _ = self.f(42)
            self.data.x = 42
            return x + 1

        @sp.entrypoint
        def ep(self):
            assert self.f(2) == 4


@sp.add_test(name="Test")
def test():
    s = sp.test_scenario([utils, M2])
    c = M2.C()
    s += c
    c.ep()
    s.verify(c.view_off(1) == 2)
