import smartpy as sp


@sp.module
def main():
    class Created(sp.Contract):
        def __init__(self):
            self.data.a = sp.int(0)
            self.data.b = sp.nat(0)

        @sp.entrypoint
        def myEntryPoint(self, params):
            self.data.a += params.x
            self.data.b += params.y

    class Created2(sp.Contract):
        def __init__(self):
            self.data.a = sp.int(1)
            self.data.b = sp.nat(2)

        @sp.entrypoint
        def myEntryPoint(self, params):
            self.data.a += params.x
            self.data.b += params.y

    def opopop(x):
        return sp.create_contract_operation(
            Created, None, sp.mutez(0), sp.record(a=x, b=15)
        )

    class Creator(sp.Contract):
        def __init__(self, baker):
            self.private.baker = baker
            self.data.x = None
            self.data.l = opopop

        @sp.entrypoint
        def create1(self):
            self.data.x = sp.Some(
                sp.create_contract(Created, None, sp.mutez(0), sp.record(a=12, b=15))
            )

        @sp.entrypoint
        def create2(self):
            _ = sp.create_contract(Created, None, sp.tez(2), sp.record(a=12, b=15))
            _ = sp.create_contract(Created, None, sp.tez(2), sp.record(a=12, b=16))

        @sp.entrypoint
        def create3(self):
            self.data.x = sp.Some(
                sp.create_contract(
                    Created, self.private.baker, sp.tez(0), sp.record(a=12, b=15)
                )
            )

        @sp.entrypoint
        def create4(self, l):
            for x in l:
                _ = sp.create_contract(Created, None, sp.mutez(0), sp.record(a=x, b=15))

        @sp.entrypoint
        def create5(self):
            _ = sp.create_contract(Created2, None, sp.mutez(0), sp.record(a=1, b=2))

        @sp.entrypoint
        def create_op(self):
            record(operation, address).match = self.data.l(42)
            sp.operations.push(operation)
            self.data.x = sp.Some(address)


@sp.add_test(name="Create")
def test():
    scenario = sp.test_scenario(main)
    scenario.h1("Create Contract")
    baker = sp.test_account("My baker")
    c1 = main.Creator(sp.some(baker.public_key_hash))
    c1.set_initial_balance(sp.tez(10))
    scenario += c1
    c1.create1()
    c1.create2()
    # c1.create3()
    c1.create4([1, 2])


#    scenario.register(c1.created)
#    dyn0 = scenario.dynamic_contract(0, c1.created)
#
#    dyn0.call("myEntryPoint", sp.record(x = 1, y = 16))
#    scenario.verify(dyn0.data.a == 13)
#    scenario.verify(dyn0.balance == sp.tez(0))
#
#    dyn0.call("myEntryPoint", sp.record(x = 1, y = 15)).run(amount = sp.tez(2))
#    scenario.verify(dyn0.data.a == 14)
#    scenario.verify(dyn0.balance == sp.tez(2))
#    scenario.show(dyn0.baker)
#    scenario.show(dyn0.address)

# TODO make arguments optional
