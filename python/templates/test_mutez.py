import smartpy as sp
from utils import utils


@sp.module
def main():
    class TestAddressComparison(sp.Contract):
        @sp.entrypoint
        def test(self, x):
            assert utils.mutez_to_nat(sp.amount) == x
            assert utils.nat_to_mutez(x) == sp.amount

        @sp.entrypoint
        def test_diff(self, x, y):
            sp.cast(x, sp.mutez)
            sp.cast(y, sp.mutez)
            r = x - y

    class MyContract(sp.Contract):
        def __init__(self):
            self.data.result = None

        @sp.entrypoint
        def store(self, params):
            self.data.result = sp.Some(params)

        @sp.entrypoint
        def decrease(self, params):
            self.data.result = sp.Some(self.data.result.unwrap_some() - params)

        @sp.entrypoint
        def sub(self, params):
            self.data.result = sp.sub_mutez(params.x, params.y)


if "templates" not in __name__:

    @sp.add_test(name="Test")
    def test():
        scenario = sp.test_scenario([utils, main])
        c1 = main.TestAddressComparison()

        scenario += c1

        c1.test(10).run(amount=sp.mutez(10))
        c1.test(10000000).run(amount=sp.tez(10))
        c1.test_diff(x=sp.mutez(100), y=sp.mutez(50))
        c1.test_diff(x=sp.mutez(70), y=sp.mutez(100)).run(valid=False)

    @sp.add_test(name="Test2")
    def test():
        scenario = sp.test_scenario([utils, main])
        c1 = main.MyContract()
        scenario += c1
        c1.store(sp.mutez(42))
        c1.decrease(sp.mutez(22))
        c1.decrease(sp.mutez(22)).run(valid=False)
        c1.sub(sp.record(x=sp.mutez(1), y=sp.mutez(2)))
        scenario.verify(c1.data.result == sp.none)
        c1.sub(sp.record(x=sp.mutez(100), y=sp.mutez(1)))
        scenario.verify(c1.data.result == sp.some(sp.mutez(99)))
