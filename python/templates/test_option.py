import smartpy as sp


@sp.module
def main():
    class C(sp.Contract):
        def __init__(self):
            self.data.x = sp.Some(42)

        @sp.entrypoint
        def set(self, x):
            self.data.x = x

        @sp.entrypoint
        def map(self, f):
            self.data.x = sp.map(f, self.data.x)


@sp.add_test(name="Test")
def test():
    s = sp.test_scenario(main)
    c = main.C()
    s += c
    c.map(lambda x: x + 1)
    s.verify(c.data.x == sp.some(43))
    c.map(lambda x: x + 2)
    s.verify(c.data.x == sp.some(45))

    c.set(sp.none)
    c.map(lambda x: x - 1)
    s.verify(c.data.x == sp.none)
