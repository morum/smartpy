import smartpy as sp


@sp.module
def main():
    class TestUnification(sp.Contract):
        def __init__(self):
            self.data.x = None

        @sp.entrypoint
        def push(self, params):
            self.data.x = sp.Some(params)


if "templates" not in __name__:

    @sp.add_test(name="TestUnification")
    def test():
        scenario = sp.test_scenario(main)
        c1 = main.TestUnification()
        scenario += c1
        c1.push(4)
