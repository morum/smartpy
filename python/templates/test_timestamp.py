import smartpy as sp


@sp.module
def main():
    class C(sp.Contract):
        def __init__(self):
            self.data.out = False
            self.data.next = sp.timestamp(0)

        @sp.entrypoint
        def ep(self):
            self.data.out = sp.now > sp.add_seconds(sp.now, 1)
            assert sp.add_seconds(sp.now, 12) - sp.now == 12
            assert sp.now - sp.add_seconds(sp.now, 12) == -12
            assert sp.now - sp.add_seconds(sp.now, 12) == -12
            assert sp.add(sp.now, sp.int(500)) == sp.timestamp(1500)
            assert sp.add(sp.int(500), sp.now) == sp.timestamp(1500)
            self.data.next = sp.add_seconds(sp.now, 24 * 3600)


@sp.add_test(name="Timestamp")
def test():
    scenario = sp.test_scenario(main)
    scenario.h1("Timestamps")
    c = main.C()
    scenario += c
    c.ep().run(now=sp.timestamp(1000))
