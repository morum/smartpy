import smartpy as sp


@sp.module
def main():
    class A(sp.Contract):
        @sp.entrypoint
        def f(self, x):
            trace(x * 2)
            trace("double")


@sp.add_test(name="Trace")
def test():
    scenario = sp.test_scenario(main)
    c1 = main.A()
    scenario += c1
    c1.f(12)
