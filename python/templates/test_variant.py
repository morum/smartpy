# Options and Variants - Example for illustrative purposes only.

import smartpy as sp


@sp.module
def main():
    class TestOptionsAndVariants(sp.Contract):
        def __init__(self):
            self.data.x = sp.variant.A(-1)
            self.data.y = sp.Some(-42)
            self.data.z = sp.Left(-10)
            self.data.r = 0
            self.data.s = sp.record(x=0, y=1)

        @sp.entrypoint
        def options(self):
            if self.data.y.is_some():
                self.data.r = 44 + self.data.y.unwrap_some(error="Not a some!")
                self.data.y = None
            else:
                self.data.r = 3
                self.data.y = sp.Some(12)

        @sp.entrypoint
        def ep1(self):
            with sp.match(self.data.x):
                with sp.case.A as arg:
                    self.data.x = sp.variant.B(-2)
                    self.data.r = arg
            self.data.x = sp.variant.C(3)

        @sp.entrypoint
        def ep3(self):
            with sp.match(self.data.z):
                with sp.case.Left as arg:
                    self.data.r = arg

        @sp.entrypoint
        def ep4(self):
            with sp.match(self.data.x):
                with sp.case.A as a1:
                    with sp.match(self.data.z):
                        with sp.case.Right as a2:
                            self.data.r = a1 + a2

        @sp.entrypoint
        def ep5(self):
            if self.data.x.is_variant.Toto():
                self.data.r = 42

        @sp.entrypoint
        def ep6(self):
            self.data.s = self.data.x.unwrap.Toto(error="no toto")

        @sp.entrypoint
        def ep7(self, params):
            self.data.x = params

        @sp.entrypoint
        def ep8(self, params):
            with sp.match(params.x):
                with sp.case.A as dummy:
                    self.data.x = params.x
                with sp.case.B as arg:
                    self.data.y = sp.Some(12 + arg + params.y)

        @sp.entrypoint
        def ep9(self, params):
            sp.cast(params.other, int)
            with sp.match(params.z):
                with sp.case.A as dummy:
                    self.data.x = params.z


@sp.add_test(name="variant")
def test():
    scenario = sp.test_scenario(main)
    scenario.h1("Variants")
    c = main.TestOptionsAndVariants()
    scenario += c
    c.options()
    c.ep1()
    c.ep3()
    c.ep4()
    c.ep5()
    c.ep6().run(valid=False)
