import smartpy as sp


@sp.module
def main():
    class Sub(sp.Contract):
        def __init__(self):
            self.data.a = sp.nat(0)

        @sp.entrypoint
        def sub_incr(self):
            self.data.a = self.data.a + 1

        @sp.entrypoint
        def sub_double(self):
            self.data.a = 2 * self.data.a

    class Main(sp.Contract):
        def __init__(self, sub):
            self.private.sub = sub
            self.data.a = sp.nat(0)

        @sp.private(with_storage="read-write")
        def incr(self, x):
            self.data.a = self.data.a + 1
            return x + 1

        @sp.private(with_storage="read-write")
        def double(self, x):
            self.data.a = 2 * self.data.a
            return 2 * x

        @sp.private(with_operations=True)
        def sub_incr(self, params):
            sub_incr = sp.contract(
                sp.unit, params.sub, entrypoint="sub_incr"
            ).unwrap_some()
            sp.transfer((), sp.tez(0), sub_incr)
            return params.x + 1

        @sp.private(with_operations=True)
        def sub_double(self, params):
            sub_double = sp.contract(
                sp.unit, params.sub, entrypoint="sub_double"
            ).unwrap_some()
            sp.transfer((), sp.tez(0), sub_double)
            return 2 * params.x

        @sp.private(with_storage="read-write", with_operations=True)
        def sub_incr_both(self, params):
            sub_incr = sp.contract(
                sp.unit, params.sub, entrypoint="sub_incr"
            ).unwrap_some()
            sp.transfer((), sp.tez(0), sub_incr)
            self.data.a = self.data.a + 1
            return params.x + 1

        @sp.entrypoint
        def test_with_storage(self):
            self.data.a = 0
            assert self.incr(100) == 101
            assert self.data.a == 1

            self.data.a = 10
            assert self.double(100) == 200
            assert self.data.a == 20

            self.data.a = 10
            assert self.double(self.incr(100)) == 202
            assert self.data.a == 22

            self.data.a = 10
            assert self.double(100) + self.incr(1000) == 1201
            assert self.data.a == 22

            self.data.a = 10
            assert self.incr(1000) + self.double(100) == 1201
            assert self.data.a == 21

        @sp.entrypoint
        def test_sub_incr(self):
            self.data.a = 0
            assert self.sub_incr(sp.record(x=100, sub=self.private.sub)) == 101

        @sp.entrypoint
        def test_sub_incr_both(self):
            self.data.a = 0
            assert self.sub_incr_both(sp.record(x=100, sub=self.private.sub)) == 101
            assert self.data.a == 1

        @sp.entrypoint
        def test_sub_incr_store(self):
            self.data.a = self.sub_incr_both(sp.record(x=100, sub=self.private.sub))


@sp.add_test(name="Effects")
def test():
    s = sp.test_scenario(main)
    sub = main.Sub()
    s += sub
    main_ = main.Main(sub.address)
    s += main_
    main_.test_with_storage()

    s.verify(sub.data.a == 0)

    main_.test_sub_incr()
    s.verify(sub.data.a == 1)

    main_.test_sub_incr_both()
    s.verify(sub.data.a == 2)

    main_.test_sub_incr_store()
    s.verify(sub.data.a == 3)
    s.verify(main_.data.a == 101)
