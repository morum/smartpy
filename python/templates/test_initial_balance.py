import smartpy as sp


@sp.module
def main():
    class InitialBalance(sp.Contract):
        def __init__(self):
            self.data.x = 2

        @sp.private(with_storage="read-write")
        def sub(self, params):
            self.data.x = 3


@sp.add_test(name="Sub")
def test():
    scenario = sp.test_scenario(main)

    initialBalance = sp.mutez(123)

    c1 = main.InitialBalance()
    c1.set_initial_balance(initialBalance)
    scenario += c1
    scenario.verify(c1.balance == initialBalance)
