#!/usr/bin/env bash

set -e

VERSION="0.16.0"
export FORCE_COLOR=1

INSTALLED_SWITCH=$(dirname -- "${BASH_SOURCE[0]}")/_opam

export OPAM_SWITCH_PREFIX=${OPAM_SWITCH_PREFIX:-$INSTALLED_SWITCH}

export smartml_app_name=SmartPy.sh

install_path=$(dirname "$0")
export smartpy_install_path="$install_path"

usage () {
    echo "Usage:"
    echo "   $0 test        <script> <output> <options>* (execute all test targets)"
    echo "   $0 doc         <script> <output>            (document script)"
    echo
    echo "   Parameters:"
    echo "         <script>              : a script containing SmartPy, SmartTS or SmartML code"
    echo "         <output>              : a directory for the results"
    echo
    echo "   Options:"
    echo "         --protocol <protocol> : optional, select target protocol - default is kathmandu"
    echo "         --<flag> <arguments>  : optional, set some flag with arguments"
    echo "         --<flag>              : optional, activate some boolean flag"
    echo "         --no-<flag>           : optional, deactivate some boolean flag"
    echo
    echo "   Protocols: kathmandu | lima | mumbai"
}

native=no
args="$@"
set --
for arg in $args
do
    if [[ "$arg" == --native ]]; then
        native=yes
    elif [[ "$arg" == --no-native ]]; then
        native=no
    else
        set -- "$@" "$arg"
    fi
done


if [[ "$native" == yes ]]; then
    smartpyc="$install_path/smartpyc"
else
    smartpyc="node --single-threaded --stack-size=4096 -- $install_path/smartpyc.js"
fi

command="$1"
shift

case "$command" in
    "" | "help" | "--help" | "-h")
        usage
        ;;
    --version)
        echo "SmartPy Version: $VERSION"
        ;;
    "test")
        [ "$#" -lt 2 ] && { usage; exit 1; }
        script="$1"
        output="$2"
        shift 2
        $smartpyc "$script" --output "$output" "$@"
        ;;
    "doc")
        [ "$#" -lt 2 ] && { usage; exit 1; }
        script="$1"
        output="$2"
        shift 2
        mkdir -p $output
        PYTHONPATH=$install_path pdoc --html "$script" --force --config sort_identifiers=False -o "$output"
        ;;
    * )
        echo "SmartPy.sh. Unknown argument: $*"
        usage
        exit 1
        ;;
esac
