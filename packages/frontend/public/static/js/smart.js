// Copyright 2022-2023 Morum LLC, 2019-2022 Smart Chain Arena LLC

function copyMichelson(x) {
  var range = document.createRange();
  var children = x.parentNode.parentNode.childNodes;
  children.forEach(function (child) {
    if (child.nodeType != Node.TEXT_NODE) {
      if (child.className.includes('michelson')) {
        range.selectNode(child);
        console.log(child);
        window.getSelection().removeAllRanges();
        window.getSelection().addRange(range);
        document.execCommand('copy');
      }
    }
  });
}

function openTabLazy(evt, id, global_id) {
  var i, tabcontent, tablinks;

  tabcontent = evt.currentTarget.parentNode.parentNode.childNodes;
  for (i = 0; i < tabcontent.length; i++) {
    if (tabcontent[i].className == 'tabcontent')
      tabcontent[i].style.display = 'none';
  }
  tablinks = evt.currentTarget.parentNode.childNodes;
  for (i = 0; i < tablinks.length; i++) {
    if (tablinks[i].nodeType != Node.TEXT_NODE) {
      tablinks[i].className = tablinks[i].className.replace(
        ' active',
        ''
      );
    }
  }
  tablinks = evt.currentTarget.parentNode.parentNode.childNodes;
  k = 0;
  for (i = 0; i < tablinks.length; i++) {
    if (tablinks[i].nodeType != Node.TEXT_NODE) {
      if (id + 1 == k) {
        tablinks[i].style.display = 'block';
        lazy_tab = window.smartmlCtx.call_exn_handler(
          'lazy_tab',
          id,
          global_id
        );
        if (lazy_tab) tablinks[i].innerHTML = lazy_tab;
      }
      k += 1;
    }
  }

  evt.currentTarget.className += ' active';
}

function openTab(evt, name) {
  var i, tabcontent, tablinks;

  tabcontent = evt.currentTarget.parentNode.parentNode.childNodes;
  for (i = 0; i < tabcontent.length; i++) {
    if (tabcontent[i].className == 'tabcontent')
      tabcontent[i].style.display = 'none';
  }
  tablinks = evt.currentTarget.parentNode.childNodes;
  for (i = 0; i < tablinks.length; i++) {
    if (tablinks[i].nodeType != Node.TEXT_NODE) {
      tablinks[i].className = tablinks[i].className.replace(
        ' active',
        ''
      );
    }
  }
  tablinks = evt.currentTarget.parentNode.parentNode.childNodes;
  k = 0;
  for (i = 0; i < tablinks.length; i++) {
    if (tablinks[i].nodeType != Node.TEXT_NODE) {
      if (name + 1 == k) tablinks[i].style.display = 'block';
      k += 1;
    }
  }

  evt.currentTarget.className += ' active';
}

class SmartmlCtx {
  constructor() {}
  call(f, ...rest) {
    try {
      return exports[f](...rest);
    } catch (error) {
      var result = 0;
      try {
        result = this.call('stringOfException', true, error);
      } catch (_error) {
        throw error;
      }
      if (
        typeof window.toException === 'function' &&
        !window.location.pathname.includes('ts-ide')
      )
        throw toException(result);
      else throw result;
    }
  }
  call_exn(f, ...rest) {
    return exports[f](...rest);
  }
  call_exn_handler(f, ...rest) {
    try {
      return exports[f](...rest);
    } catch (error) {
      try {
        var result = '';
        try {
          result = this.call('stringOfException', false, error);
        } catch (_error) {
          showErrorPre(error);
        }
        console.log(result);
        showErrorPre(result);
      } catch (_error) {
        showErrorPre(error);
      }
    }
  }
}

smartmlCtx = new SmartmlCtx();
in_browser = true;
